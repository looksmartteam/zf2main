<?php
namespace Shop;
return array(
  'router' => array(
    'routes' => array(
      'shop_index' => array(
        'type' => 'Zend\Mvc\Router\Http\Literal',
        'options' => array(
          'route' => '/shop',
          'defaults' => array(
            '__NAMESPACE__' => 'Shop\Controller',
            'controller' => 'Index',
            'action' => 'index',
          ),
        ),
        'may_terminate' => true,
        'child_routes' => array(
          'shop_index_process' => array(
            'type' => 'Segment',
            'options' => array(
              'route' => '/[:action]',
              'constraints' => array(
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
              ),
              'defaults' => array(),
            ),
          ),
        ),
      ),
      'shop_cart' => array(
        'type' => 'Zend\Mvc\Router\Http\Literal',
        'options' => array(
          'route' => '/shop/cart',
          'defaults' => array(
            '__NAMESPACE__' => 'Shop\Controller',
            'controller' => 'Cart',
            'action' => 'index',
          ),
        ),
        'may_terminate' => true,
        'child_routes' => array(
          'shop_cart_process' => array(
            'type' => 'Segment',
            'options' => array(
              'route' => '/[:action]',
              'constraints' => array(
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
              ),
              'defaults' => array(),
            ),
          ),
        ),
      ),
      'shop_payu' => array(
        'type' => 'Zend\Mvc\Router\Http\Literal',
        'options' => array(
          'route' => '/shop/payu',
          'defaults' => array(
            '__NAMESPACE__' => 'Shop\Controller',
            'controller' => 'Payu',
            'action' => 'index',
          ),
        ),
        'may_terminate' => true,
        'child_routes' => array(
          'shop_payu_process' => array(
            'type' => 'Segment',
            'options' => array(
              'route' => '/[:action]',
              'constraints' => array(
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
              ),
              'defaults' => array(),
            ),
          ),
        ),
      ),
      'shop_test' => array(
        'type' => 'Zend\Mvc\Router\Http\Literal',
        'options' => array(
          'route' => '/shop/test',
          'defaults' => array(
            '__NAMESPACE__' => 'Shop\Controller',
            'controller' => 'Payu',
            'action' => 'index',
          ),
        ),
        'may_terminate' => true,
        'child_routes' => array(
          'shop_test_process' => array(
            'type' => 'Segment',
            'options' => array(
              'route' => '/[:action]',
              'constraints' => array(
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
              ),
              'defaults' => array(),
            ),
          ),
        ),
      ),
    ),
  ),
  'service_manager' => array(
    'factories' => array(),
  ),
  'controllers' => array(
    'invokables' => array(
      'Shop\Controller\Index' => 'Shop\Controller\IndexController',
      'Shop\Controller\Cart' => 'Shop\Controller\CartController',
      'Shop\Controller\Payu' => 'Shop\Controller\PayuController',
    ),
  ),
  'controller_plugins' => array(
    'invokables' => array()
  ),
  'view_manager' => array(
    'template_map' => array(
      'partials/table_items_control_panel' => __DIR__ . '/../view/partials/table_items_control_panel.phtml',
      'partials/table_items_row_buttons' => __DIR__ . '/../view/partials/table_items_row_buttons.phtml',
      'partials/table_cart_items_row_buttons' => __DIR__ . '/../view/partials/table_cart_items_row_buttons.phtml',
    ),
    'template_path_stack' => array(
      'shop' => __DIR__ . '/../view',
    ),
  ),
  'view_helpers' => array(
    'invokables' => array(
      'Cart' => 'Shop\View\Helper\Cart',
    )
  ),
);

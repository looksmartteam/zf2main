<?php
namespace Shop\View\Helper;

use Shop\Model\UserSessionContainer as UserSessionContainer;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Helper\AbstractHelper;

class Cart extends AbstractHelper implements ServiceLocatorAwareInterface
{
  private $_oCartItemTable;
  private $_oCartTable;
  private $_oServiceLocator;

  protected function getItemTable()
  {
    if (!$this->_oItemTable && $this->_oServiceLocator->getServiceLocator()->has('ItemTableService')) {
      $this->_oItemTable = $this->_oServiceLocator->getServiceLocator()->get('ItemTableService');
    }
    if (!isset($this->_oItemTable)) {
      throw \Exception();
    }
    return $this->_oItemTable;
  }

  public function __invoke()
  {
    $this->_oServiceLocator = $this->getServiceLocator();
    $oUserSessionContainer = new UserSessionContainer();
    if ($oUserSessionContainer->getIsInit()) {
      $nUserId = $oUserSessionContainer->getUserId();
      if ($nUserId) {
        $oCart = $this->getCartTable()->getRow(array('user_id' => $nUserId));
        if ($oCart) {
          $sPaymentDescription = '';
          $oAllCartItems = $this->getCartItemTable()->getAll(array('cart_id' => $oCart->id));
          if ($oAllCartItems) {
            foreach ($oAllCartItems as $oItem) {
              $sPaymentDescription .= $this->getItemTable()->getRow(array('id' => $oItem->item_id))->name;
              if (count($oAllCartItems) > 1)
                $sPaymentDescription .= '; ';
            }
            $sPaymentDescription = trim($sPaymentDescription);
          }
          return array('cart' => $this->getCartItemTable()->getCartSum(array('cart_id' => $oCart->id)), 'description' => $sPaymentDescription);
        }
      }
    }
    return null;
  }

  public function getCartItemTable()
  {
    if (!$this->_oCartItemTable && $this->_oServiceLocator->getServiceLocator()->has('CartItemTableService')) {
      $this->_oCartItemTable = $this->_oServiceLocator->getServiceLocator()->get('CartItemTableService');
    }
    return $this->_oCartItemTable;
  }

  public function getCartTable()
  {
    if (!$this->_oCartTable && $this->_oServiceLocator->getServiceLocator()->has('CartTableService')) {
      $this->_oCartTable = $this->_oServiceLocator->getServiceLocator()->get('CartTableService');
    }
    return $this->_oCartTable;
  }

  public function getServiceLocator()
  {
    return $this->_oServiceLocator;
  }

  public function setServiceLocator(ServiceLocatorInterface $oServiceLocator)
  {
    $this->_oServiceLocator = $oServiceLocator;
    return $this;
  }
}

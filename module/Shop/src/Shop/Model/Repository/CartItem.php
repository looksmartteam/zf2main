<?php
namespace Shop\Model\Repository;

use Application\RepositoryAbstract;
use Shop\Model\UserSessionContainer as UserSessionContainer;
use Shop\Model\Entity\CartItem as Entity;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;

class CartItem extends RepositoryAbstract
{
  protected $table = 'cart_item';

  public function __construct(Adapter $oAdapter)
  {
    parent::__construct($oAdapter);
  }

  protected function createNewEntity()
  {
    return new Entity();
  }

  public function addRow($oEntity)
  {
    $aData = array();
    $oRow = $this->getRow(array('cart_id' => $oEntity->cart_id, 'item_id' => $oEntity->item_id));
    if ($oRow) {
      $aData['updated_date'] = time();
      $aData['count'] = $oRow->count + 1;
      return $this->update($aData, array('cart_id' => $oEntity->cart_id, 'item_id' => $oEntity->item_id));;
    } else {
      $aData['cart_id'] = $oEntity->cart_id;
      $aData['item_id'] = $oEntity->item_id;
      $aData['count'] = 1;
      $aData['created_date'] = time();
      $aData['updated_date'] = $aData['created_date'];
      return parent::addRow($aData);
    }
  }

  public function editRow($oEntity)
  {
    $aData = array();
    $aData['id'] = $oEntity->id;
    $aData['count'] = $oEntity->count;
    $aData['updated_date'] = time();
    return parent::editRow($aData);
  }

  public function getAll(array $aData = null)
  {
    $oUserSessionContainer = new UserSessionContainer();
    if ($oUserSessionContainer->getIsInit()) {
      $aData['user_id'] = $oUserSessionContainer->getUserId();
      $oThat = $this;
      $oResultSet = $this->select(function (Select $oSelect) use ($aData, $oThat) {
        $oSelect->columns(array('*', 'sum_item_price_net' => new Expression('count * (price_net) / 100'), 'sum_item_price_gross' => new Expression('count * (price_gross) / 100')));
        $oSelect->join('cart', 'cart.id = cart_item.cart_id', array('cart_created_date' => 'created_date', 'cart_updated_date' => 'updated_date'));
        $oSelect->join('item', 'item.id = cart_item.item_id', array('name', 'price_net' => new Expression('price_net / 100'), 'price_gross' => new Expression('price_gross / 100')), $oSelect::JOIN_INNER);
        if ($aData) {
          foreach ($aData as $sKey => $mValue) {
            if ($sKey === 'filter' && isset($mValue)) {
              foreach ($mValue as $sColumnName => $sValue) {
                $sDataType = $oThat->getColumnDataType($sColumnName);
                if ($sColumnName === 'id') {
                  $sColumnName = $oThat->table . '.' . $sColumnName;
                }
                if (in_array($sDataType, array('int', 'tinyint'))) {
                  $oSelect->where(array($sColumnName . ' =?' => (int)$sValue));
                } else {
                  $oSelect->where->like($sColumnName, '%' . $sValue . '%');
                }
              }
            } else if ($sKey === 'sort' && is_string($mValue['sort_column']) && is_string($mValue['sort_method'])) {
              if ($mValue['sort_column'] === 'id') {
                $mValue['sort_column'] = $oThat->table . '.' . $mValue['sort_column'];
              }
              $oSelect->order(array($mValue['sort_column'] => $mValue['sort_method']));
            } else if ($sKey === 'limit' && isset($mValue) && is_numeric($mValue)) {
              $oSelect->limit($mValue);
            } else if ($sKey === 'offset' && isset($mValue) && is_numeric($mValue)) {
              $oSelect->offset($mValue);
            } else if (!in_array($sKey, array('filter', 'sort', 'limit', 'offset'))) {
              if ($sKey === 'id') {
                $sKey = $oThat->table . '.id';
              }
              $oSelect->where(array($sKey => $mValue));
            }
          }
        }
      });
      $aResult = $this->getEntities($oResultSet);
    }
    return $aResult;
  }

  public function getCartSum(array $aData = null)
  {
    $oUserSessionContainer = new UserSessionContainer();
    if ($oUserSessionContainer->getIsInit()) {
      $aData['user_id'] = $oUserSessionContainer->getUserId();
      $oThat = $this;
      $oResultSet = $this->select(function (Select $oSelect) use ($aData, $oThat) {
        $oSelect->columns(array('cart_items_count' => new Expression('SUM(count)'), 'cart_sum_price_net' => new Expression('SUM(count * (price_net)) / 100'), 'cart_sum_price_gross' => new Expression('SUM(count * (price_gross) / 100)')));
        $oSelect->join('cart', 'cart.id = cart_item.cart_id', array('user_id'));
        $oSelect->join('item', 'item.id = cart_item.item_id', array(), $oSelect::JOIN_INNER);
        if ($aData) {
          foreach ($aData as $sKey => $mValue) {
            $oSelect->where(array($sKey => $mValue));
          }
        }
        $oSelect->limit(1);
      });
      $aResult = $this->getEntities($oResultSet);
      if (count($aResult) === 1)
        return $aResult[0];
    }
    return null;
  }
}

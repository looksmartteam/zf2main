<?php
namespace Shop\Model\Repository;

use Application\RepositoryAbstract;
use Shop\Model\Entity\OrderPayment as Entity;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;

class OrderPayment extends RepositoryAbstract
{
  protected $table = 'order_payment';

  public function __construct(Adapter $oAdapter)
  {
    parent::__construct($oAdapter);
  }

  protected function createNewEntity()
  {
    return new Entity();
  }

  public function addRow($oEntity)
  {
    $aData = array();
    $aData['session_id'] = $oEntity->session_id;
    $aData['order_id'] = $oEntity->order_id;
    $aData['user_id'] = $oEntity->user_id;
    $aData['amount'] = $oEntity->amount;
    $aData['description'] = $oEntity->description;
    $aData['first_name'] = $oEntity->first_name;
    $aData['last_name'] = $oEntity->last_name;
    $aData['email_address'] = $oEntity->email_address;
    $aData['start'] = 1;
    $aData['date_start'] = time();
    return parent::addRow($aData);
  }

  public function getAll(array $aData = null)
  {
    $oThat = $this;
    $oResultSet = $this->select(function (Select $oSelect) use ($aData, $oThat) {
      if ($aData) {
        foreach ($aData as $sKey => $mValue) {
          if ($sKey === 'filter' && isset($mValue)) {
            foreach ($mValue as $sColumnName => $sValue) {
              $sDataType = $oThat->getColumnDataType($sColumnName);
              if ($sColumnName === 'id') {
                $sColumnName = $oThat->table . '.' . $sColumnName;
              }
              if (in_array($sDataType, array('int', 'tinyint')) && $sColumnName !== 'created_date' && $sColumnName !== 'updated_date') {
                $oSelect->where(array($sColumnName . '=?' => (int)$sValue));
              } else if ($sColumnName === 'created_date' || $sColumnName === 'updated_date') {
                $oSelect->where(array('date_start ' . '>=?' => $sValue));
              } else {
                $oSelect->where->like($sColumnName, '%' . $sValue . '%');
              }
            }
          } else if ($sKey === 'sort' && is_string($mValue['sort_column']) && is_string($mValue['sort_method'])) {
            if ($mValue['sort_column'] === 'id') {
              $mValue['sort_column'] = $oThat->table . '.' . $mValue['sort_column'];
            }
            $oSelect->order(array($mValue['sort_column'] => $mValue['sort_method']));
          } else if ($sKey === 'limit' && isset($mValue) && is_numeric($mValue)) {
            $oSelect->limit($mValue);
          } else if ($sKey === 'offset' && isset($mValue) && is_numeric($mValue)) {
            $oSelect->offset($mValue);
          } else if (!in_array($sKey, array('filter', 'sort', 'limit', 'offset'))) {
            if ($sKey === 'id') {
              $sKey = $oThat->table . '.id';
            }
            $oSelect->where(array($sKey => $mValue));
          }
        }
      }
      //var_dump($oSelect->getSqlString($oThat->adapter->getPlatform()));
    });
    $aResult = $this->getEntities($oResultSet);
    return $aResult;
  }
}

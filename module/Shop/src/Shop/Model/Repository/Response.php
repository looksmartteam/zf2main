<?php
namespace Shop\Model\Repository;

use Application\RepositoryAbstract;
use Shop\Model\Entity\Response as Entity;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;

class Response extends RepositoryAbstract
{
  protected $table = 'response';

  public function __construct(Adapter $oAdapter)
  {
    parent::__construct($oAdapter);
  }

  protected function createNewEntity()
  {
    return new Entity();
  }

  public function addRow($oEntity)
  {
    $aData = array();
    $aData['order_payment_id'] = $oEntity->order_payment_id;
    $aData['payment_id'] = $oEntity->payment_id;
    $aData['pos_id'] = $oEntity->pos_id;
    $aData['session_id'] = $oEntity->session_id;
    $aData['order_id'] = $oEntity->order_id;
    $aData['amount'] = $oEntity->amount;
    $aData['status'] = $oEntity->status;
    $aData['pay_type'] = $oEntity->pay_type;
    $aData['pay_gw_name'] = $oEntity->pay_gw_name;
    $aData['desc'] = $oEntity->desc;
    $aData['desc2'] = $oEntity->desc2;
    $aData['create'] = $oEntity->create;
    $aData['init'] = $oEntity->init;
    $aData['sent'] = $oEntity->sent;
    $aData['recv'] = $oEntity->recv;
    $aData['cancel'] = $oEntity->cancel;
    $aData['auth_fraud'] = $oEntity->auth_fraud;
    $aData['ts'] = $oEntity->ts;
    $aData['sig'] = $oEntity->sig;
    $aData['date'] = $oEntity->date;
    return parent::addRow($aData);
  }

  public function getAll(array $aData = null)
  {
    $oThat = $this;
    $oResultSet = $this->select(function (Select $oSelect) use ($aData, $oThat) {
      if ($aData) {
        foreach ($aData as $sKey => $mValue) {
          if ($sKey === 'filter' && isset($mValue)) {
            foreach ($mValue as $sColumnName => $sValue) {
              $sDataType = $oThat->getColumnDataType($sColumnName);
              if ($sColumnName === 'id') {
                $sColumnName = $oThat->table . '.' . $sColumnName;
              }
              if (in_array($sDataType, array('int', 'tinyint')) && $sColumnName !== 'created_date' && $sColumnName !== 'updated_date') {
                $oSelect->where(array($sColumnName . '=?' => (int)$sValue));
              } else if ($sColumnName === 'created_date') {
                $oSelect->where(array('date_start' . '>=?' => strtotime($sValue)));
                $oSelect->where(array('date_start' . '<=?' => strtotime($sValue) + 86400));
              } else {
                $oSelect->where->like($sColumnName, '%' . $sValue . '%');
              }
            }
          } else if ($sKey === 'sort' && is_string($mValue['sort_column']) && is_string($mValue['sort_method'])) {
            if ($mValue['sort_column'] === 'id') {
              $mValue['sort_column'] = $oThat->table . '.' . $mValue['sort_column'];
            }
            $oSelect->order(array($mValue['sort_column'] => $mValue['sort_method']));
          } else if ($sKey === 'limit' && isset($mValue) && is_numeric($mValue)) {
            $oSelect->limit($mValue);
          } else if ($sKey === 'offset' && isset($mValue) && is_numeric($mValue)) {
            $oSelect->offset($mValue);
          } else if (!in_array($sKey, array('filter', 'sort', 'limit', 'offset'))) {
            if ($sKey === 'id') {
              $sKey = $oThat->table . '.id';
            }
            $oSelect->where(array($sKey => $mValue));
          }
        }
      }
    });
    $aResult = $this->getEntities($oResultSet);
    return $aResult;
  }
}

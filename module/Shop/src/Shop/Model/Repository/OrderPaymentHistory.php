<?php
namespace Shop\Model\Repository;

use Application\RepositoryAbstract;
use Shop\Model\Entity\OrderPaymentHistory as Entity;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;

class OrderPaymentHistory extends RepositoryAbstract
{
  protected $table = 'order_payment_history';

  public function __construct(Adapter $oAdapter)
  {
    parent::__construct($oAdapter);
  }

  protected function createNewEntity()
  {
    return new Entity();
  }

  public function addRow($oEntity)
  {
    $aData = array();
    $aData['item_id'] = $oEntity->item_id;
    $aData['order_payment_id'] = $oEntity->order_payment_id;
    $aData['date'] = time();
    return parent::addRow($aData);
  }

  public function getAll(array $aData = null)
  {
    $oThat = $this;
    $oResultSet = $this->select(function (Select $oSelect) use ($aData, $oThat) {
      if ($aData) {
        foreach ($aData as $sKey => $mValue) {
          if ($sKey === 'filter' && isset($mValue)) {
            foreach ($mValue as $sColumnName => $sValue) {
              $sDataType = $oThat->getColumnDataType($sColumnName);
              if ($sColumnName === 'id') {
                $sColumnName = $oThat->table . '.' . $sColumnName;
              }
              if (in_array($sDataType, array('int', 'tinyint'))) {
                $oSelect->where(array($sColumnName . '=?' => (int)$sValue));
              } else {
                $oSelect->where->like($sColumnName, '%' . $sValue . '%');
              }
            }
          } else if ($sKey === 'sort' && is_string($mValue['sort_column']) && is_string($mValue['sort_method'])) {
            if ($mValue['sort_column'] === 'id') {
              $mValue['sort_column'] = $oThat->table . '.' . $mValue['sort_column'];
            }
            $oSelect->order(array($mValue['sort_column'] => $mValue['sort_method']));
          } else if ($sKey === 'limit' && isset($mValue) && is_numeric($mValue)) {
            $oSelect->limit($mValue);
          } else if ($sKey === 'offset' && isset($mValue) && is_numeric($mValue)) {
            $oSelect->offset($mValue);
          } else if (!in_array($sKey, array('filter', 'sort', 'limit', 'offset'))) {
            if ($sKey === 'id') {
              $sKey = $oThat->table . '.id';
            }
            $oSelect->where(array($sKey => $mValue));
          }
        }
      }
    });
    $aResult = $this->getEntities($oResultSet);
    return $aResult;
  }
}

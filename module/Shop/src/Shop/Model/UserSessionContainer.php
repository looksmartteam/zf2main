<?php
namespace Shop\Model;
class UserSessionContainer extends \Zend\Session\Container
{
  protected $_sApplicationKey = 'd0631e15a8461abe2598e82ccdfd7139';
  protected $_sContainerName;

  public function __construct()
  {
    $this->_sContainerName = 'user_session';
    parent::__construct($this->_sContainerName);
  }

  protected function getApplicationKey()
  {
    return $this->_sApplicationKey;
  }

  public function clear()
  {
    $nIsInit = $this->getIsInit();
    $this->getManager()->getStorage()->clear($this->_sContainerName);
    if ($nIsInit == 1) {
      $this->setIsInit($nIsInit);
    }
  }

  public function clearAll()
  {
    $this->getStorage()->clear();
  }

  public function getBbarcode()
  {
    return $this->bbarcode;
  }

  public function getBorrowerHash()
  {
    return $this->borrower_hash;
  }

  public function getBorrowerNote()
  {
    return $this->borrower_note;
  }

  public function getBtype()
  {
    return $this->btype;
  }

  public function getCity()
  {
    return $this->city;
  }

  public function getEmailAddress()
  {
    return $this->email_address;
  }

  public function getEmployeeId()
  {
    return $this->employee_id;
  }

  public function getFirstName()
  {
    return $this->first_name;
  }

  public function getIsInit()
  {
    return $this->is_init;
  }

  public function getLastName()
  {
    return $this->last_name;
  }

  public function getOrganizationBtype()
  {
    return $this->organization_btype;
  }

  public function getOrganizationKey()
  {
    return $this->organization_key;
  }

  public function getPesel()
  {
    return $this->pesel;
  }

  public function getPhoneNumber()
  {
    return $this->phone_number;
  }

  public function getPostCode()
  {
    return $this->post_code;
  }

  public function getPreIsInit()
  {
    return $this->pre_is_init;
  }

  public function getStreet()
  {
    return $this->street;
  }

  public function getStudentId()
  {
    return $this->student_id;
  }

  public function getUserId()
  {
    return $this->user_id;
  }

  public function getUserParamId()
  {
    return $this->user_param_id;
  }

  public function getWho()
  {
    return $this->who;
  }

  public function setBbarcode($sBbarcode)
  {
    $this->bbarcode = $sBbarcode;
  }

  public function setBorrowerHash($nBorrowerHash)
  {
    if (is_numeric($nBorrowerHash)) {
      $this->borrower_hash = (int)$nBorrowerHash;
    } else if ($nBorrowerHash === null) {
      $this->borrower_hash = $nBorrowerHash;
    }
  }

  public function setBorrowerNote($sBorrowerNote)
  {
    $this->borrower_note = $sBorrowerNote;
  }

  public function setBtype($sBtype)
  {
    $this->btype = $sBtype;
  }

  public function setCity($sCity)
  {
    $this->city = $sCity;
  }

  public function setCurrentBorrowerData($aData)
  {
    $this->setWho('borrower');
    $this->setBorrowerHash($aData['borrower#']);
    $this->setBbarcode($aData['bbarcode']);
    $this->setPesel($aData['second_id']);
    $this->setFirstName($aData['first_name']);
    $this->setLastName($aData['last_name']);
    $this->setIsInit(1);
  }

  public function setCurrentEmployeeData($aData)
  {
    $this->setWho('employee');
    $this->setBorrowerHash($aData['borrower_hash']);
    $this->setBbarcode($aData['bbarcode']);
    $this->setEmployeeId($aData['UID']);
    $this->setPesel($aData['PESEL']);
    $this->setFirstName($aData['FirstName']);
    $this->setLastName($aData['LastName']);
    //$this->setStreet($aData['AddressLine1']);
    //$this->setCity($aData['AddressCity']);
    //$this->setPostCode($aData['AddressCode']);
    $this->setIsInit(1);
  }

  public function setCurrentStudentData($aData)
  {
    $this->setWho('student');
    $this->setBorrowerHash($aData['borrower_hash']);
    $this->setBbarcode($aData['bbarcode']);
    $this->setStudentId($aData['Indeks']);
    $this->setPesel($aData['PESEL']);
    $this->setFirstName($aData['FirstName']);
    $this->setLastName($aData['LastName']);
    //$this->setStreet($aData['AddressLine1']);
    //$this->setCity($aData['AddressCity']);
    //$this->setPostCode($aData['AddressCode']);
    $this->setIsInit(1);
  }

  public function setEmailAddress($sEmailAddress)
  {
    $this->email_address = $sEmailAddress;
  }

  public function setEmployeeId($nEmployeeId)
  {
    $this->employee_id = $nEmployeeId;
  }

  public function setFirstName($sFirstName)
  {
    $this->first_name = $sFirstName;
  }

  public function setIsInit($nValue)
  {
    $this->is_init = (int)$nValue;
  }

  public function setLastName($sLastName)
  {
    $this->last_name = $sLastName;
  }

  public function setNewBorrowerData($aData)
  {
    $this->setWho('borrower');
    $this->setNewUserData($aData);
  }

  public function setNewEmployeeData($aData)
  {
    $this->setWho('employee');
    $this->setEmployeeId($aData['employee_id']);
    $this->setNewUserData($aData);
  }

  public function setNewStudentData($aData)
  {
    $this->setWho('student');
    $this->setStudentId($aData['student_id']);
    $this->setNewUserData($aData);
  }

  public function setNewUserData($aData)
  {
    $this->setBorrowerHash(null);
    $this->setBbarcode($aData['email_address']);
    $this->setBorrowerNote($aData['borrower_note']);
    $this->setPesel($aData['pesel']);
    $this->setFirstName($aData['first_name']);
    $this->setLastName($aData['last_name']);
    $this->setEmailAddress($aData['email_address']);
    $this->setPhoneNumber($aData['phone_number']);
    $this->setStreet($aData['street']);
    $this->setCity($aData['city']);
    $this->setPostCode($aData['post_code']);
    $this->setOrganizationKey($aData['organization_key']);
    $this->setOrganizationBtype($aData['organization_btype']);
    $this->setPreIsInit();
    $this->setIsInit(1);
  }

  public function setOrganizationBtype($aOrganizationBtype)
  {
    $this->organization_btype = $aOrganizationBtype;
  }

  public function setOrganizationKey($aOrganizationKey)
  {
    $this->organization_key = $aOrganizationKey;
  }

  public function setPesel($sPesel)
  {
    $this->pesel = $sPesel;
  }

  public function setPhoneNumber($sPhoneNumber)
  {
    $this->phone_number = $sPhoneNumber;
  }

  public function setPostCode($sPostCode)
  {
    $this->post_code = $sPostCode;
  }

  public function setPreIsInit()
  {
    $nTime = time();
    if ($this->getWho() === 'student' && $this->getStudentId()) {
      $this->pre_is_init = md5($this->getPesel() . $this->getStudentId() . $this->getApplicationKey() . $nTime);
    }
    if ($this->getWho() === 'employee' && $this->getEmployeeId()) {
      $this->pre_is_init = md5($this->getPesel() . $this->getEmployeeId() . $this->getApplicationKey() . $nTime);
    }
  }

  public function setStreet($sStreet)
  {
    $this->street = $sStreet;
  }

  public function setStudentId($nStudentId)
  {
    $this->student_id = $nStudentId;
  }

  public function setUserId($nUserId)
  {
    $this->user_id = (int)$nUserId;
  }

  public function setUserParamId($nUserParamId)
  {
    $this->user_param_id = (int)$nUserParamId;
  }

  public function setWho($sValue)
  {
    if (in_array($sValue, array('borrower', 'student', 'employee'))) {
      $this->who = $sValue;
    }
  }
}
<?php
namespace Shop\Model\Entity;

use Application\EntityAbstract;

class Cart extends EntityAbstract
{
  public $created_date;
  public $id;
  public $updated_date;
  public $user_id;
}

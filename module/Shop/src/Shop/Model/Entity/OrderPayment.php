<?php
namespace Shop\Model\Entity;

use Application\EntityAbstract;

class OrderPayment extends EntityAbstract
{
  public $amount;
  public $date_end;
  public $date_start;
  public $description;
  public $email_address;
  public $end;
  public $first_name;
  public $last_name;
  public $order_id;
  public $session_id;
  public $start;
  public $user_id;
}

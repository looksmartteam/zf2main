<?php
namespace Shop\Model\Entity;

use Application\EntityAbstract;

class CartItem extends EntityAbstract
{
  public $cart_id;
  public $cart_items_count;
  public $cart_sum_price_gross;
  public $cart_sum_price_net;
  public $count;
  public $created_date;
  public $item_id;
  public $name;
  public $price_gross; //wartość netto pozycji
  public $price_net; //wartość brutto pozycji
  public $sum_item_price_gross; //łączna liczba pozycji w koszyku
  public $sum_item_price_net; //łączna wartość netto wszystkich pozycji w koszyku
  public $updated_date; //łączna wartość brutto wszystkich pozycji w koszyku

  public function getCartSumPriceGross($nCartSumPriceGross)
  {
    $this->cart_sum_price_gross = round((float)$nCartSumPriceGross, 2);
    return $this;
  }

  public function getCartSumPriceNet($nCartSumPriceNet)
  {
    $this->cart_sum_price_net = round((float)$nCartSumPriceNet, 2);
    $this->cart_sum_price_gross = $this->cart_sum_price_net;
    return $this;
  }

  public function getPriceGross($nPriceGross)
  {
    $this->price_gross = round((float)$nPriceGross, 2);
    return $this;
  }

  public function getPriceNet($nPriceNet)
  {
    $this->price_net = round((float)$nPriceNet, 2);
    return $this;
  }

  public function setCartItemsCount($nCount)
  {
    $this->cart_items_count = (int)$nCount;
    return $this;
  }

  public function setCartSumPriceGross($nCartSumPriceGross)
  {
    $this->cart_sum_price_gross = round((float)$nCartSumPriceGross, 2);
    return $this;
  }

  public function setCartSumPriceNet($nCartSumPriceNet)
  {
    $this->cart_sum_price_net = round((float)$nCartSumPriceNet, 2);
    return $this;
  }

  public function setPriceGross($nPriceGross)
  {
    $this->price_gross = round((float)$nPriceGross, 2);
    return $this;
  }

  public function setPriceNet($nPriceNet)
  {
    $this->price_net = round((float)$nPriceNet, 2);
    return $this;
  }
}

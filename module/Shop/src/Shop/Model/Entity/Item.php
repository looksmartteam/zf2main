<?php
namespace Shop\Model\Entity;

use Application\EntityAbstract;

class Item extends EntityAbstract
{
  public $name;
  public $price_gross;
  public $price_net;

  public function getPriceGross($nPriceGross)
  {
    $this->price_gross = round((float)$nPriceGross, 2);
    return $this;
  }

  public function getPriceNet($nPriceNet)
  {
    $this->price_net = round((float)$nPriceNet, 2);
    return $this;
  }

  public function setPriceGross($nPriceGross)
  {
    $this->price_gross = round((float)$nPriceGross, 2);
    return $this;
  }

  public function setPriceNet($nPriceNet)
  {
    $this->price_net = round((float)$nPriceNet, 2);
    return $this;
  }
}

<?php
namespace Shop\Model\Entity;

use Application\EntityAbstract;

class OrderPaymentHistory extends EntityAbstract
{
  public $date;
  public $item_id;
  public $order_payment_id;
}

<?php
namespace Shop\Model\Entity;

use Application\EntityAbstract;

class Response extends EntityAbstract
{
  public $amount;
  public $auth_fraud;
  public $cancel;
  public $create;
  public $date;
  public $desc;
  public $desc2;
  public $init;
  public $order_id;
  public $order_payment_id;
  public $pay_gw_name;
  public $pay_type;
  public $payment_id;
  public $pos_id;
  public $recv;
  public $sent;
  public $session_id;
  public $sig;
  public $status;
  public $ts;
}

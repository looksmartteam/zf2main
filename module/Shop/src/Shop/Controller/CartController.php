<?php
namespace Shop\Controller;

use Shop\Model\UserSessionContainer as UserSessionContainer;
use Shop\Model\Entity\Cart as CartEntity;
use Shop\Model\Entity\CartItem as CartItemEntity;
use Shop\Model\Entity\OrderPayment as OrderPaymentEntity;
use Shop\Model\Entity\OrderPaymentHistory as OrderPaymentHistoryEntity;
use User\Model\Entity\User as UserEntity;
use User\Model\Entity\UserParam as UserParamEntity;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class CartController extends AbstractActionController
{
  protected $_oCartItemTable;
  protected $_oCartTable;
  protected $_oItemTable;
  protected $_oOrderPaymentHistoryTable;
  protected $_oOrderPaymentTable;
  protected $_oResponseTable;
  protected $_oUserParamTable;
  protected $_oUserTable;

  protected function getCartItemTable()
  {
    if (!$this->_oCartItemTable && $this->getServiceLocator()->has('CartItemTableService')) {
      $this->_oCartItemTable = $this->getServiceLocator()->get('CartItemTableService');
    }
    if (!isset($this->_oCartItemTable)) {
      throw \Exception();
    }
    return $this->_oCartItemTable;
  }

  protected function getCartTable()
  {
    if (!$this->_oCartTable && $this->getServiceLocator()->has('CartTableService')) {
      $this->_oCartTable = $this->getServiceLocator()->get('CartTableService');
    }
    if (!isset($this->_oCartTable)) {
      throw \Exception();
    }
    return $this->_oCartTable;
  }

  protected function getItemTable()
  {
    if (!$this->_oItemTable && $this->getServiceLocator()->has('ItemTableService')) {
      $this->_oItemTable = $this->getServiceLocator()->get('ItemTableService');
    }
    if (!isset($this->_oItemTable)) {
      throw \Exception();
    }
    return $this->_oItemTable;
  }

  protected function getOrderPaymentHistoryTable()
  {
    if (!$this->_oOrderPaymentHistoryTable && $this->getServiceLocator()->has('OrderPaymentHistoryTableService')) {
      $this->_oOrderPaymentHistoryTable = $this->getServiceLocator()->get('OrderPaymentHistoryTableService');
    }
    if (!isset($this->_oOrderPaymentHistoryTable)) {
      throw \Exception();
    }
    return $this->_oOrderPaymentHistoryTable;
  }

  protected function getOrderPaymentTable()
  {
    if (!$this->_oOrderPaymentTable && $this->getServiceLocator()->has('OrderPaymentTableService')) {
      $this->_oOrderPaymentTable = $this->getServiceLocator()->get('OrderPaymentTableService');
    }
    if (!isset($this->_oOrderPaymentTable)) {
      throw \Exception();
    }
    return $this->_oOrderPaymentTable;
  }

  protected function getResponseTable()
  {
    if (!$this->_oResponseTable && $this->getServiceLocator()->has('ResponseTableService')) {
      $this->_oResponseTable = $this->getServiceLocator()->get('ResponseTableService');
    }
    if (!isset($this->_oResponseTable)) {
      throw \Exception();
    }
    return $this->_oResponseTable;
  }

  protected function getUserParamTable()
  {
    if (!$this->_oUserParamTable && $this->getServiceLocator()->has('UserParamTableService')) {
      $this->_oUserParamTable = $this->getServiceLocator()->get('UserParamTableService');
    }
    if (!isset($this->_oUserParamTable)) {
      throw \Exception();
    }
    return $this->_oUserParamTable;
  }

  protected function getUserTable()
  {
    if (!$this->_oUserTable && $this->getServiceLocator()->has('UserTableService')) {
      $this->_oUserTable = $this->getServiceLocator()->get('UserTableService');
    }
    if (!isset($this->_oUserTable)) {
      throw \Exception();
    }
    return $this->_oUserTable;
  }

  public function addtocartajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $oUserSessionContainer = new UserSessionContainer();
      if ($oUserSessionContainer->getIsInit()) {
        $nItemId = $this->params()->fromPost('id');
        if (is_numeric($nItemId)) {
          $nUser = $oUserSessionContainer->getUserId();
          $oCartEntity = new CartEntity();
          $oCartItemEntity = new CartItemEntity();
          $oData = new \ArrayObject(array('item_id' => $nItemId, 'user_id' => $nUser));
          $oData->offsetSet('cart_id', $this->getCartTable()->findRow(array('user_id' => $nUser)));
          $this->getCartTable()->beginTransaction();
          if ($oData->offsetGet('cart_id')) {
            $oData->offsetSet('cart_item_id', $this->getCartItemTable()->addRow($oCartItemEntity->setOptions($oData)));
          } else {
            $oData->offsetSet('cart_id', $this->getCartTable()->addRow($oCartEntity->setOptions($oData)));
            $oData->offsetSet('cart_item_id', $this->getCartItemTable()->addRow($oCartItemEntity->setOptions($oData)));
          }
          if ($oData->offsetGet('cart_id') && $oData->offsetGet('cart_item_id')) {
            $this->getCartTable()->commit();
            $bSuccess = true;
          } else {
            $this->getCartTable()->rollBack();
          }
        }
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function autocompletecartitemajaxAction()
  {
    $oResponse = $this->getResponse();
    $aFilter = null;
    $aItems = array();
    $sColumnName = $this->params()->fromQuery('column_name') !== '' ? $this->params()->fromQuery('column_name') : null;
    if (is_array($this->params()->fromQuery('filter')) && count($this->params()->fromQuery('filter'))) {
      $aFilter = $this->params()->fromQuery('filter');
      foreach ($aFilter as $sKey => $sValue) {
        if (strlen(trim($sValue)))
          $aFilter[$sKey] = trim($sValue);
        else
          unset($aFilter[$sKey]);
      }
    }
    if (isset($aFilter)) {
      $aRowset = $this->getCartItemTable()->getAll(array('filter' => $aFilter));
      $aLabels = array();
      foreach ($aRowset as $nKey => $oCartItem) {
        if (!in_array($oCartItem->$sColumnName, $aLabels)) {
          $aItems[$nKey]['label'] = $oCartItem->$sColumnName;
          array_push($aLabels, $oCartItem->$sColumnName);
        }
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode(array('rowset' => $aItems)));
    return $oResponse;
  }

  public function deletecartitemajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $nId = $this->params()->fromPost('id') !== '' ? (int)$this->params()->fromPost('id') : null;
      if ($nId) {
        $this->getCartItemTable()->deleteRow($nId);
        $bSuccess = true;
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function editcartitemajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $oEditCartItemForm = $this->getServiceLocator()->get('EditCartItemFormService');
      $oEditCartItemForm->setData($this->getRequest()->getPost());
      if ($oEditCartItemForm->isValid()) {
        $oCartItemEntity = new CartItemEntity();
        $oData = new \ArrayObject($oEditCartItemForm->getData());
        $this->getCartItemTable()->editRow($oCartItemEntity->setOptions($oData));
        $bSuccess = true;
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function itemsAction()
  {
    $bAutoTrigger = null;
    $oUserSessionContainer = new UserSessionContainer();
    $nUserId = $oUserSessionContainer->getUserId();
    $nUserParamId = $this->getUserParamTable()->findRow(array('user_id' => $nUserId));
    if (isset($nUserId) && isset($nUserParamId)) {
      $oCart = $this->getCartTable()->getRow(array('user_id' => $nUserId));
      if (isset($oCart) && $oUserSessionContainer->getIsInit()) {
        $oCartSum = $this->getCartItemTable()->getCartSum();
        $oEditCartItemForm = $this->getServiceLocator()->get('EditCartItemFormService');
        $oNewPaymentForm = $this->getServiceLocator()->get('NewPaymentFormService');
        $oNewPaymentForm->setData(array('amount' => '12 PLN', 'first_name' => $oUserSessionContainer->getFirstName(), 'last_name' => $oUserSessionContainer->getLastName(), 'email_address' => $oUserSessionContainer->getEmailAddress(), 'phone_number' => $oUserSessionContainer->getPhoneNumber()));
        if ($this->getRequest()->isPost()) {
          $oNewPaymentForm->setData($this->getRequest()->getPost());
          if ($oNewPaymentForm->isValid()) {
            try {
              $this->getOrderPaymentTable()->beginTransaction();
              $oOrderPaymentEntity = new OrderPaymentEntity();
              $oOrderPaymentHistoryEntity = new OrderPaymentHistoryEntity();
              $oUserEntity = new UserEntity();
              $oUserParamEntity = new UserParamEntity();
              $aValidatedData = $oNewPaymentForm->getData();
              $sEmailAddress = $aValidatedData['email_address'];
              $sFirstName = $aValidatedData['first_name'];
              $sLastName = $aValidatedData['last_name'];
              $sPhoneNumber = $aValidatedData['phone_number'];
              $sPaymentDescription = '';
              $oAllCartItems = $this->getCartItemTable()->getAll(array('cart_id' => $oCart->id));
              if ($oAllCartItems) {
                foreach ($oAllCartItems as $oItem) {
                  $sPaymentDescription .= $this->getItemTable()->getRow(array('id' => $oItem->item_id))->name;
                  if (count($oAllCartItems) > 1)
                    $sPaymentDescription .= '; ';
                }
                $sPaymentDescription = trim($sPaymentDescription);
              }
              $oUserData = new \ArrayObject(array('id' => $nUserId, 'user_param_id' => $nUserParamId, 'email_address' => $sEmailAddress, 'phone_number' => $sPhoneNumber));
              $this->getUserTable()->changeEmailAddress($oUserEntity->setOptions($oUserData));
              $this->getUserParamTable()->changePhoneNumber($oUserParamEntity->setOptions($oUserData));
              $oPayU = new \Shop\PayU();
              $oPayU->setSessionId();
              $oPayU->setOrderId();
              $oPayU->setAmount($oCartSum->cart_sum_price_gross);
              $oPayU->setUserId($nUserId);
              $oPayU->setFirstName($sFirstName);
              $oPayU->setLastName($sLastName);
              $oPayU->setEmailAddress($sEmailAddress);
              $oPayU->setStreet('');
              $oPayU->setPostCode('');
              $oPayU->setCity('');
              $oPayU->setPhone('');
              $oPayU->setClientIp();
              $oPayU->setDesc($sPaymentDescription);
              $oPayU->setSig();
              $oPayUForm = $this->getServiceLocator()->get('PayUFormService');
              $oPaymentForm = $oPayUForm->setPayment($oPayU);
              $oOrderPaymentData = new \ArrayObject($oPayU->getPaymentParam());
              $oOrderPaymentData->offsetSet('order_payment_id', $this->getOrderPaymentTable()->addRow($oOrderPaymentEntity->setOptions($oOrderPaymentData)));
              $oAllCartItems = $this->getCartItemTable()->getAll(array('cart_id' => $oCart->id));
              if ($oAllCartItems) {
                foreach ($oAllCartItems as $oItem) {
                  $oOrderPaymentData->offsetSet('item_id', $oItem->item_id);
                  $this->getOrderPaymentHistoryTable()->addRow($oOrderPaymentHistoryEntity->setOptions($oOrderPaymentData));
                }
              }
              $bAutoTrigger = true;
              $this->getOrderPaymentTable()->commit();
            } catch (\Exception $e) {
              $this->getOrderPaymentTable()->rollBack();
            }
          }
        } else if ($oUserSessionContainer->getIsInit()) {
          $oPaymentForm = $oNewPaymentForm;
        }
      }
      return new ViewModel(array('cart_sum' => $oCartSum, 'edit_cart_item_form' => $oEditCartItemForm, 'payment_form' => $oPaymentForm, 'auto_trigger' => $bAutoTrigger));
    }
  }

  public function loadcartitemsajaxAction()
  {
    if ($this->getRequest()->isPost()) {
      $oResponse = $this->getResponse();
      $aSort = null;
      $aFilter = null;
      if (is_array($this->params()->fromPost('filter')) && count($this->params()->fromPost('filter'))) {
        $aFilter = $this->params()->fromPost('filter');
        foreach ($aFilter as $sKey => $sValue) {
          if (strlen(trim($sValue)))
            $aFilter[$sKey] = trim($sValue);
          else
            unset($aFilter[$sKey]);
        }
      }
      $sSortColumn = $this->params()->fromPost('sort_column') !== '' ? $this->params()->fromPost('sort_column') : 'id';
      $sSortMethod = $this->params()->fromPost('sort_method') !== '' ? $this->params()->fromPost('sort_method') : 'asc';
      $nLimit = $this->params()->fromPost('limit');
      $nOffset = $this->params()->fromPost('offset');
      if (isset($sSortColumn) && isset($sSortMethod))
        $aSort = array('sort_column' => $sSortColumn, 'sort_method' => $sSortMethod);
      if (isset($nLimit) && is_numeric($nLimit)) {
        $nLimit = (int)$nLimit;
      } else {
        $nLimit = 20;
      }
      if (isset($nOffset) && is_numeric($nOffset)) {
        $nOffset = (int)$nOffset;
      } else {
        $nOffset = 0;
      }
      $aCartItem = $this->getCartItemTable()->getAll(array('filter' => $aFilter, 'sort' => $aSort, 'limit' => $nLimit, 'offset' => $nOffset));
      $nCount = count($this->getCartItemTable()->getAll(array('filter' => $aFilter, 'sort' => $aSort)));
      $oResponse->setContent(\Zend\Json\Json::encode(array('rowset' => $aCartItem, 'num_rows' => $nCount)));
      return $oResponse;
    }
  }

  public function refreshcartajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $oResponse = $this->getResponse();
      $oUserSessionContainer = new UserSessionContainer();
      if ($oUserSessionContainer->getUserId()) {
        $nUser = $oUserSessionContainer->getUserId();
        $oCart = $this->getCartTable()->getRow(array('user_id' => $nUser));
        if ($oCart) {
          $bSuccess = $this->getCartItemTable()->getCartSum(array('cart_id' => $oCart->id));
        }
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }
}
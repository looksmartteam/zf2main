<?php
namespace Shop\Controller;

use Shop\Model\UserSessionContainer as UserSessionContainer;
use Shop\Model\Entity\OrderPayment as OrderPaymentEntity;
use Shop\Model\Entity\Response as ResponseEntity;
use User\Model\Entity\User as UserEntity;
use User\Model\Entity\UserParam as UserParamEntity;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class PayuController extends AbstractActionController
{
  protected $_oCartItemTable;
  protected $_oCartTable;
  protected $_oItemTable;
  protected $_oOrderPaymentHistoryTable;
  protected $_oOrderPaymentTable;
  protected $_oResponseTable;
  protected $_oUserParamTable;
  protected $_oUserTable;

  protected function getBorrowerTable()
  {
    if (!$this->_oBorrowerTable && $this->getServiceLocator()->has('BorrowerTableService')) {
      $this->_oBorrowerTable = $this->getServiceLocator()->get('BorrowerTableService');
    }
    if (!isset($this->_oBorrowerTable)) {
      throw \Exception();
    }
    return $this->_oBorrowerTable;
  }

  protected function getCartItemTable()
  {
    if (!$this->_oCartItemTable && $this->getServiceLocator()->has('CartItemTableService')) {
      $this->_oCartItemTable = $this->getServiceLocator()->get('CartItemTableService');
    }
    if (!isset($this->_oCartItemTable)) {
      throw \Exception();
    }
    return $this->_oCartItemTable;
  }

  protected function getCartTable()
  {
    if (!$this->_oCartTable && $this->getServiceLocator()->has('CartTableService')) {
      $this->_oCartTable = $this->getServiceLocator()->get('CartTableService');
    }
    if (!isset($this->_oCartTable)) {
      throw \Exception();
    }
    return $this->_oCartTable;
  }

  protected function getItemTable()
  {
    if (!$this->_oItemTable && $this->getServiceLocator()->has('ItemTableService')) {
      $this->_oItemTable = $this->getServiceLocator()->get('ItemTableService');
    }
    if (!isset($this->_oItemTable)) {
      throw \Exception();
    }
    return $this->_oItemTable;
  }

  protected function getOrderPaymentHistoryTable()
  {
    if (!$this->_oOrderPaymentHistoryTable && $this->getServiceLocator()->has('OrderPaymentHistoryTableService')) {
      $this->_oOrderPaymentHistoryTable = $this->getServiceLocator()->get('OrderPaymentHistoryTableService');
    }
    if (!isset($this->_oOrderPaymentHistoryTable)) {
      throw \Exception();
    }
    return $this->_oOrderPaymentHistoryTable;
  }

  protected function getOrderPaymentTable()
  {
    if (!$this->_oOrderPaymentTable && $this->getServiceLocator()->has('OrderPaymentTableService')) {
      $this->_oOrderPaymentTable = $this->getServiceLocator()->get('OrderPaymentTableService');
    }
    if (!isset($this->_oOrderPaymentTable)) {
      throw \Exception();
    }
    return $this->_oOrderPaymentTable;
  }

  protected function getResponseTable()
  {
    if (!$this->_oResponseTable && $this->getServiceLocator()->has('ResponseTableService')) {
      $this->_oResponseTable = $this->getServiceLocator()->get('ResponseTableService');
    }
    if (!isset($this->_oResponseTable)) {
      throw \Exception();
    }
    return $this->_oResponseTable;
  }

  protected function getUserCategoryTable()
  {
    if (!$this->_oUserCategoryTable && $this->getServiceLocator()->has('UserCategoryTableService')) {
      $this->_oUserCategoryTable = $this->getServiceLocator()->get('UserCategoryTableService');
    }
    if (!isset($this->_oUserCategoryTable)) {
      throw \Exception();
    }
    return $this->_oUserCategoryTable;
  }

  protected function getUserParamTable()
  {
    if (!$this->_oUserParamTable && $this->getServiceLocator()->has('UserParamTableService')) {
      $this->_oUserParamTable = $this->getServiceLocator()->get('UserParamTableService');
    }
    if (!isset($this->_oUserParamTable)) {
      throw \Exception();
    }
    return $this->_oUserParamTable;
  }

  protected function getUserTable()
  {
    if (!$this->_oUserTable && $this->getServiceLocator()->has('UserTableService')) {
      $this->_oUserTable = $this->getServiceLocator()->get('UserTableService');
    }
    if (!isset($this->_oUserTable)) {
      throw \Exception();
    }
    return $this->_oUserTable;
  }

  public function errorAction()
  {
  }

  public function reportAction()
  {
    if ($this->getRequest()->isPost()) {
      $oPayU = new \Shop\PayU();
      if ($oPayU->getResponse((array)$this->getRequest()->getPost())) {
        $oPaymentStatusData = new \ArrayObject($oPayU->getPaymentStatus());
        $oOrderPayment = $this->getOrderPaymentTable()->getRow(array('session_id' => $oPaymentStatusData->offsetGet('session_id'), 'order_id' => $oPaymentStatusData->offsetGet('order_id'), 'amount' => $oPaymentStatusData->offsetGet('amount')));
        if ($oOrderPayment) {
          try {
            $this->getOrderPaymentTable()->beginTransaction();
            $oResponseEntity = new ResponseEntity();
            $nUserId = $oOrderPayment->user_id;
            if ($nUserId) {
              $oUserEntity = new UserEntity();
              $oUserParamEntity = new UserParamEntity();
              $oUserStd = new \stdClass();
              $oMail = new \Application\Mail($this->getServiceLocator());
              $oUserParam = $this->getUserParamTable()->getRow(array('user_id' => $nUserId));
              foreach ($oUserParam as $sKey => $mValue) {
                if (in_array($sKey, array('user_id'))) {
                  continue;
                }
                if (in_array($sKey, array('id'))) {
                  $sKey = 'user_param_id';
                }
                $oUserStd->$sKey = $mValue;
              }
              foreach ($oOrderPayment as $sKey => $mValue) {
                if (in_array($sKey, array('user_id', 'first_name', 'last_name', 'e-mail address'))) {
                  continue;
                }
                if (in_array($sKey, array('id'))) {
                  $sKey = 'order_payment_id';
                }
                if (in_array($sKey, array('amount'))) {
                  $sKey = 'user_amount';
                  $mValue = ((int)$mValue / 100);
                }
                $oUserStd->$sKey = $mValue;
              }
              $oUserStd->issue_user_date = date('d.m.Y, H:i:s');
              if ($oPaymentStatusData->offsetGet('status') == '1') { //nowa
                $oMail->fillMessage('sendPayUReport1', $oUserStd);
              } else if ($oPaymentStatusData->offsetGet('status') == '2') { //anulowana
                $oMail->fillMessage('sendPayUReport2', $oUserStd);
              } else if ($oPaymentStatusData->offsetGet('status') == '3') { //odrzucona
                $oMail->fillMessage('sendPayUReport3', $oUserStd);
              } else if ($oPaymentStatusData->offsetGet('status') == '4') { //rozpoczęta
                $oMail->fillMessage('sendPayUReport4', $oUserStd);
              } else if ($oPaymentStatusData->offsetGet('status') == '5') { //oczekuje na odbiór
                $oMail->fillMessage('sendPayUReport5', $oUserStd);
              } else if ($oPaymentStatusData->offsetGet('status') == '7') { //odrzucona
                $oMail->fillMessage('sendPayUReport7', $oUserStd);
              } else if ($oPaymentStatusData->offsetGet('status') == '99') { //zakończona
                if (!$this->getOrderPaymentTable()->getRow(array('id' => $oOrderPayment->id))->end) {
                  $oOrderPaymentEntity = new OrderPaymentEntity();
                  $nBorrowerHash = (int)$oUserParam->borrower_hash;
                  if ($nBorrowerHash) { //użytkownik posiada konto biblioteczne (student, employee, borrower)
                    if (in_array($oUserParam->user_category_name, array('student'))) {
                      $this->getBorrowerTable()->activateStudentAccount($nBorrowerHash);
                    } else if (in_array($oUserParam->user_category_name, array('employee'))) {
                      $this->getBorrowerTable()->activateEmployeeAccount($nBorrowerHash);
                    } else if (in_array($oUserParam->user_category_name, array('borrower'))) {
                      $this->getBorrowerTable()->activateBorrowerAccount($nBorrowerHash);
                      //dodać btype ZA dla osób z zewnątrz tylko dla tych czytelników, których btype nie należy do HAN (zostawiamy CZxxx)
                    }
                  } else { //użytkownik nie posiada konta bibliotecznego (student, employee, borrower)
                    $nNewBorrowerHash = $this->getBorrowerTable()->addNewBorrower($oUserParam);
                    if ($nNewBorrowerHash) {
                      $oUserParamData = new \ArrayObject(array('id' => $oUserStd->user_param_id, 'borrower_hash' => $nNewBorrowerHash));
                      $this->getUserParamTable()->editRow($oUserParamEntity->setOptions($oUserParamData, array('id', 'borrower_hash')));
                    }
                  }
                  $oOrderPaymentData = new \ArrayObject(array('id' => $oOrderPayment->id, 'date_end' => time(), 'end' => 1));
                  $this->getOrderPaymentTable()->editRow($oOrderPaymentEntity->setOptions($oOrderPaymentData, array('id', 'date_end', 'end')));
                  $oUserData = new \ArrayObject(array('id' => $nUserId, 'active' => 1));
                  $this->getUserTable()->changeActive($oUserEntity->setOptions($oUserData));
                  $oMail->fillMessage('sendPayUReport99', $oUserStd);
                  if ($nBorrowerHash) {
                    $oMail->fillMessage('successPaymentActivateBorrower', $oUserStd);
                  } else if (in_array($oUserParam->user_category_name, array('student', 'employee'))) {
                    $oMail->fillMessage('successPaymentNewBorrowerUAM', $oUserStd);
                  } else if (in_array($oUserParam->user_category_name, array('borrower'))) {
                    $oMail->fillMessage('successPaymentNewBorrower', $oUserStd);
                  }
                }
              }
              $oPaymentStatusData->offsetSet('order_payment_id', $oOrderPayment->id);
              $oPaymentStatusData->offsetSet('date', time());
              $this->getResponseTable()->addRow($oResponseEntity->setOptions($oPaymentStatusData));
              $this->getOrderPaymentTable()->commit();
            }
          } catch (\Exception $e) {
            $this->getOrderPaymentTable()->rollBack();
            exit;
          }
        }
      }
    }
    echo 'OK';
    exit;
  }

  public function setPaymentStatus($aParts)
  {
    $nPosId = 168613;
    if (!is_array($aParts)) {
      throw new \Exception('Błęda tablica statusu transakcji');
    }
    if ($aParts[2][0] != $nPosId) {
      throw new \Exception('Niewłaściwa wartość pos_id');
    }
    $sTmpSig = md5($aParts[2][0] . $aParts[3][0] . $aParts[4][0] . $aParts[6][0] . $aParts[5][0] . $aParts[9][0] . $aParts[17][0] . $this->_sKey2);
    if ($aParts[18][0] != $sTmpSig) {
      throw new \Exception('Niewłaściwa wartość sig');
    }
    $aTmpPaymentStatus = array(
      'payment_id' => $aParts[1][0],
      'pos_id' => $aParts[2][0],
      'session_id' => $aParts[3][0],
      'order_id' => $aParts[4][0],
      'amount' => $aParts[5][0],
      'status' => $aParts[6][0],
      'pay_type' => $aParts[7][0],
      'pay_gw_name' => $aParts[8][0],
      'desc' => $aParts[9][0],
      'desc2' => $aParts[10][0],
      'create' => $aParts[11][0],
      'init' => $aParts[12][0],
      'sent' => $aParts[13][0],
      'recv' => $aParts[14][0],
      'cancel' => $aParts[15][0],
      'auth_fraud' => $aParts[16][0],
      'ts' => $aParts[17][0],
      'sig' => $aParts[18][0]
    );
    return $aTmpPaymentStatus;
  }

  public function successAction()
  {
    $oMail = new \Application\Mail($this->getServiceLocator());
    $oUserSessionContainer = new UserSessionContainer;
    $nUserId = $oUserSessionContainer->getUserId();
    if ($nUserId) {
      $nCartId = $this->getCartTable()->findRow(array('user_id' => $nUserId));
      if ($nCartId) {
        $this->getCartItemTable()->deleteRow(array('cart_id' => $nCartId));
      }
      $oUserSessionContainer->clear();
    }
    $sOrderId = $this->params()->fromQuery('id');
    $sSessionId = $this->params()->fromQuery('session');
    if (isset($sOrderId) && isset($sSessionId)) {
      $oOrderPayment = $this->getOrderPaymentTable()->getRow(array('session_id' => $sSessionId, 'order_id' => $sOrderId));
      $nUserId = $oOrderPayment->user_id;
      if ($oOrderPayment && $nUserId) {
        $oUserStd = new \stdClass();
        $oUserParam = $this->getUserParamTable()->getRow(array('user_id' => $nUserId));
        foreach ($oUserParam as $sKey => $mValue) {
          if (in_array($sKey, array('user_id'))) {
            continue;
          }
          if (in_array($sKey, array('id'))) {
            $sKey = 'user_param_id';
          }
          $oUserStd->$sKey = $mValue;
        }
        foreach ($oOrderPayment as $sKey => $mValue) {
          if (in_array($sKey, array('user_id', 'first_name', 'last_name', 'e-mail address'))) {
            continue;
          }
          if (in_array($sKey, array('id'))) {
            $sKey = 'order_payment_id';
          }
          if (in_array($sKey, array('amount'))) {
            $sKey = 'user_amount';
            $mValue = ((int)$mValue / 100);
          }
          $oUserStd->$sKey = $mValue;
        }
        $oUserStd->issue_user_date = date('d.m.Y, H:i:s');
        if ((int)$oUserParam->borrower_hash) {
          $sMessage = $oMail->getMessage('successPaymentActivateBorrower', $oUserStd);
        } else if (in_array($oUserParam->user_category_name, array('student', 'employee'))) {
          $sMessage = $oMail->getMessage('successPaymentNewBorrowerUAM', $oUserStd);
        } else if (in_array($oUserParam->user_category_name, array('borrower'))) {
          $sMessage = $oMail->getMessage('successPaymentNewBorrower', $oUserStd);
        }
      }
    }
    return new ViewModel(array('message' => $sMessage));
  }

  public function testpayureportAction()
  {
    $oTestPayUReportForm = $this->getServiceLocator()->get('TestPayUReportFormService');
    if ($this->getRequest()->isPost()) {
      $oPayU = new \Shop\PayU();
      if (function_exists('curl_exec') && $oPayU->getResponse((array)$this->getRequest()->getPost())) {
        $sServer = 'www.platnosci.pl';
        $sServerScript = '/paygw/UTF/Payment/get';
        $nPosId = '168613';
        $sKey1 = 'f5506754d92c6729072cd8114f02b7a4';
        $sKey2 = 'af5d4e6c8cb4cdf8e401d99e696f5df2';
        $oOrderPayment = $this->getOrderPaymentTable()->getAll(array('filter' => array('start' => 1, 'end' => 0, 'created_date' => '1444694400'), 'sort' => array('sort_column' => 'date_start', 'sort_method' => 'ASC')));
        foreach ($oOrderPayment as $nKey => $oValue) {
          $sSessionId = $oValue->session_id;
          $nTs = time();
          $sSigPayU = md5($nPosId . $sSessionId . $nTs . $sKey1);
          $sSigZapisy = md5($nPosId . $sSessionId . $nTs . $sKey2);
          $sTmpParamPayU = 'pos_id=' . $nPosId . '&session_id=' . $sSessionId . '&ts=' . $nTs . '&sig=' . $sSigPayU;
          $sTmpParamZapisy = 'pos_id=' . $nPosId . '&session_id=' . $sSessionId . '&ts=' . $nTs . '&sig=' . $sSigZapisy;
          $rCh = curl_init();
          curl_setopt($rCh, CURLOPT_URL, 'https://' . $sServer . $sServerScript);
          curl_setopt($rCh, CURLOPT_SSL_VERIFYPEER, FALSE);
          curl_setopt($rCh, CURLOPT_HEADER, 0);
          curl_setopt($rCh, CURLOPT_TIMEOUT, 20);
          curl_setopt($rCh, CURLOPT_POST, 1);
          curl_setopt($rCh, CURLOPT_POSTFIELDS, $sTmpParamPayU);
          curl_setopt($rCh, CURLOPT_RETURNTRANSFER, 1);
          $sResponse = curl_exec($rCh);
          curl_close($rCh);
          xml_parse_into_struct(xml_parser_create(), $sResponse, $aVals, $aIndex);
          xml_parser_free(xml_parser_create());
          foreach ($aVals as $nKey => $aValue) {
            if ($aValue['tag'] === 'STATUS' && $aValue['value'] == '99') {
              \Zend\Debug\Debug::dump(date('d-m-Y, H:i:s', $oValue->date_start) . ' // ' . $oValue->user_id . ' // ' . $oValue->borrower_hash . ' // ' . $oValue->email_address . ' // ' . $aValue['value']);
            }
          }
//          $rCh = curl_init();
//          curl_setopt($rCh, CURLOPT_URL, 'http://zapisy.ebiblioteka.edu.pl/shop/payu/report');
//          curl_setopt($rCh, CURLOPT_SSL_VERIFYPEER, FALSE);
//          curl_setopt($rCh, CURLOPT_HEADER, 0);
//          curl_setopt($rCh, CURLOPT_TIMEOUT, 20);
//          curl_setopt($rCh, CURLOPT_POST, 1);
//          curl_setopt($rCh, CURLOPT_POSTFIELDS, $sTmpParamZapisy);
//          curl_setopt($rCh, CURLOPT_RETURNTRANSFER, 1);
//          $sResponse = curl_exec($rCh);
//          \Zend\Debug\Debug::dump($sResponse);
//          curl_close($rCh);
//          exit;
        }
        $oPaymentStatusData = new \ArrayObject($oPayU->getPaymentStatus());
        //\Zend\Debug\Debug::dump($oOrderPayment);
        //\Zend\Debug\Debug::dump($oPaymentStatusData);
        //\Zend\Debug\Debug::dump($sResponse);
      }
      exit;
    }
    return new ViewModel(array('form' => $oTestPayUReportForm));
  }
}
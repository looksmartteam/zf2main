<?php
namespace Shop\Controller;

use Shop\Model\Entity\Item as ItemEntity;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
  protected $_oItemTable;

  protected function getItemTable()
  {
    if (!$this->_oItemTable) {
      $this->_oItemTable = $this->getServiceLocator()->get('ItemTableService');
    }
    return $this->_oItemTable;
  }

  public function additemajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $oAddNewItemForm = $this->getServiceLocator()->get('AddNewItemFormService');
      $oAddNewItemForm->setData($this->getRequest()->getPost());
      if ($oAddNewItemForm->isValid()) {
        $oItemEntity = new ItemEntity();
        $oData = new \ArrayObject($oAddNewItemForm->getData());
        $this->getItemTable()->beginTransaction();
        $oData->offsetSet('item_id', $this->getItemTable()->addRow($oItemEntity->setOptions($oData)));
        if ($oData->offsetGet('item_id')) {
          $this->getItemTable()->commit();
          $bSuccess = true;
        } else {
          $this->getItemTable()->rollBack();
        }
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function autocompleteitemajaxAction()
  {
    $oResponse = $this->getResponse();
    $aFilter = null;
    $aItems = array();
    $sColumnName = $this->params()->fromQuery('column_name') !== '' ? $this->params()->fromQuery('column_name') : null;
    if (is_array($this->params()->fromQuery('filter')) && count($this->params()->fromQuery('filter'))) {
      $aFilter = $this->params()->fromQuery('filter');
      foreach ($aFilter as $sKey => $sValue) {
        if (strlen(trim($sValue)))
          $aFilter[$sKey] = trim($sValue);
        else
          unset($aFilter[$sKey]);
      }
    }
    if (isset($aFilter)) {
      $aRowset = $this->getItemTable()->getAll(array('filter' => $aFilter));
      $aLabels = array();
      foreach ($aRowset as $nKey => $oItem) {
        if (!in_array($oItem->$sColumnName, $aLabels)) {
          $aItems[$nKey]['label'] = $oItem->$sColumnName;
          array_push($aLabels, $oItem->$sColumnName);
        }
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode(array('rowset' => $aItems)));
    return $oResponse;
  }

  public function deleteitemajaxAction()
  {
    if ($this->getRequest()->isPost()) {
      $oResponse = $this->getResponse();
      $nId = $this->params()->fromPost('id') !== '' ? (int)$this->params()->fromPost('id') : null;
      if ($nId) {
        $this->getItemTable()->deleteRow($nId);
        $oResponse->setContent(\Zend\Json\Json::encode(true));
      }
    }
    return $oResponse;
  }

  public function edititemajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $oEditItemForm = $this->getServiceLocator()->get('EditItemFormService');
      $oEditItemForm->setData($this->getRequest()->getPost());
      if ($oEditItemForm->isValid()) {
        $oItemEntity = new ItemEntity();
        $oData = new \ArrayObject($oEditItemForm->getData());
        $this->getItemTable()->editRow($oItemEntity->setOptions($oData));
        $bSuccess = true;
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function indexAction()
  {
  }

  public function itemsAction()
  {
    $oEditItemForm = $this->getServiceLocator()->get('EditItemFormService');
    $oAddNewItemForm = $this->getServiceLocator()->get('AddNewItemFormService');
    return new ViewModel(array('edit_item_form' => $oEditItemForm, 'add_new_item_form' => $oAddNewItemForm));
  }

  public function loaditemsajaxAction()
  {
    if ($this->getRequest()->isPost()) {
      $oResponse = $this->getResponse();
      $aSort = null;
      $aFilter = null;
      if (is_array($this->params()->fromPost('filter')) && count($this->params()->fromPost('filter'))) {
        $aFilter = $this->params()->fromPost('filter');
        foreach ($aFilter as $sKey => $sValue) {
          if (strlen(trim($sValue)))
            $aFilter[$sKey] = trim($sValue);
          else
            unset($aFilter[$sKey]);
        }
      }
      $sSortColumn = $this->params()->fromPost('sort_column') !== '' ? $this->params()->fromPost('sort_column') : 'id';
      $sSortMethod = $this->params()->fromPost('sort_method') !== '' ? $this->params()->fromPost('sort_method') : 'asc';
      $nLimit = $this->params()->fromPost('limit');
      $nOffset = $this->params()->fromPost('offset');
      if (isset($sSortColumn) && isset($sSortMethod))
        $aSort = array('sort_column' => $sSortColumn, 'sort_method' => $sSortMethod);
      if (isset($nLimit) && is_numeric($nLimit)) {
        $nLimit = (int)$nLimit;
      } else {
        $nLimit = 20;
      }
      if (isset($nOffset) && is_numeric($nOffset)) {
        $nOffset = (int)$nOffset;
      } else {
        $nOffset = 0;
      }
      $aItems = $this->getItemTable()->getAll(array('filter' => $aFilter, 'sort' => $aSort, 'limit' => $nLimit, 'offset' => $nOffset));
      $nCount = count($this->getItemTable()->getAll(array('filter' => $aFilter, 'sort' => $aSort)));
      $oResponse->setContent(\Zend\Json\Json::encode(array('rowset' => $aItems, 'num_rows' => $nCount)));
    }
    return $oResponse;
  }
}

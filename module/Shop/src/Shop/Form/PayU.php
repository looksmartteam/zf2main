<?php
namespace Shop\Form;

use Application\FormAbstract;

class PayU extends FormAbstract
{
  protected $_oPayU;

  public function __construct($oServiceLocator, $sFormName)
  {
    parent::__construct($oServiceLocator, $sFormName);
  }

  public function getPayment()
  {
    if (null === $this->_oPayU) {
      throw new \Exception('Nie przekazano obiektu płatności');
    }
    return $this->_oPayU;
  }

  public function postInit()
  {
    $this->add(array(
      'type' => 'Hidden',
      'name' => 'pos_id',
      'attributes' => array(
        'value' => $this->getPayment()->getPosId(),
      )
    ));
    $this->add(array(
      'type' => 'Hidden',
      'name' => 'pos_auth_key',
      'attributes' => array(
        'value' => $this->getPayment()->getPosAuthKey(),
      )
    ));
    $this->add(array(
      'type' => 'Hidden',
      'name' => 'order_id',
      'attributes' => array(
        'value' => $this->getPayment()->getOrderId(),
      )
    ));
    $this->add(array(
      'type' => 'Hidden',
      'name' => 'session_id',
      'attributes' => array(
        'value' => $this->getPayment()->getSessionId(),
      )
    ));
    $this->add(array(
      'type' => 'Hidden',
      'name' => 'amount',
      'attributes' => array(
        'value' => $this->getPayment()->getAmount(),
      )
    ));
    $this->add(array(
      'type' => 'Hidden',
      'name' => 'desc',
      'attributes' => array(
        'value' => $this->getPayment()->getDesc(),
      )
    ));
    $this->add(array(
      'type' => 'Hidden',
      'name' => 'client_ip',
      'attributes' => array(
        'value' => $this->getPayment()->getClientIp(),
      )
    ));
    $this->add(array(
      'type' => 'Hidden',
      'name' => 'first_name',
      'attributes' => array(
        'value' => $this->getPayment()->getFirstName(),
      )
    ));
    $this->add(array(
      'type' => 'Hidden',
      'name' => 'last_name',
      'attributes' => array(
        'value' => $this->getPayment()->getLastName(),
      )
    ));
    $this->add(array(
      'type' => 'Hidden',
      'name' => 'street',
      'attributes' => array(
        'value' => $this->getPayment()->getStreet(),
      )
    ));
    $this->add(array(
      'type' => 'Hidden',
      'name' => 'city',
      'attributes' => array(
        'value' => $this->getPayment()->getCity(),
      )
    ));
    $this->add(array(
      'type' => 'Hidden',
      'name' => 'post_code',
      'attributes' => array(
        'value' => $this->getPayment()->getPostCode(),
      )
    ));
    $this->add(array(
      'type' => 'Hidden',
      'name' => 'phone',
      'attributes' => array(
        'value' => $this->getPayment()->getPhone(),
      )
    ));
    $this->add(array(
      'type' => 'Hidden',
      'name' => 'email',
      'attributes' => array(
        'value' => $this->getPayment()->getEmailAddress(),
      )
    ));
    $this->add(array(
      'type' => 'Hidden',
      'name' => 'sig',
      'attributes' => array(
        'value' => $this->getPayment()->getSig(),
      )
    ));
    $this->add(array(
      'type' => 'Hidden',
      'name' => 'ts',
      'attributes' => array(
        'value' => $this->getPayment()->getTs(),
      )
    ));
    $this->add(array(
      'type' => 'Button',
      'name' => 'hidden_submit_button',
    ));
  }

  /**
   * Set a single option for an element
   *
   * @param  string $key
   * @param  mixed $value
   * @return self
   */
  public function setOption($key, $value)
  {
    // TODO: Implement setOption() method.
  }

  public function setPayment($oPayU)
  {
    if ($oPayU instanceof \Shop\PayU) {
      $this->_oPayU = $oPayU;
      $this->postInit();
      $this->setAttribute('action', $this->getPayment()->getUrlNewPayment());
      return $this;
    } else {
      throw new \Exception('Błąd podczas inicjalizacji formularza');
    }
  }
}

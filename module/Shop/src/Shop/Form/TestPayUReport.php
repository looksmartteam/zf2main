<?php
namespace Shop\Form;

use Application\FormAbstract;

class TestPayUReport extends FormAbstract
{
  protected $_nPosId;
  protected $_nTs;
  protected $_sKey2;
  protected $_sSessionId;
  protected $_sSig;

  public function __construct($oServiceLocator, $sFormName)
  {
    $this->_nPosId = '168613';
    $this->_sSessionId = '9e8fd2b9704d5a67ccb58fe5bbb3f63d'; //wymagany parametr do zmiany z poziomy kodu źródłowego aplikacji
    $this->_sKey2 = 'af5d4e6c8cb4cdf8e401d99e696f5df2';
    $this->_nTs = time();
    $this->_sSig = md5($this->_nPosId . $this->_sSessionId . $this->_nTs . $this->_sKey2);
    parent::__construct($oServiceLocator, $sFormName);
  }

  public function init()
  {
    $this->add(array(
      'type' => 'Text',
      'name' => 'pos_id',
      'options' => array(
        'label' => 'pos_id',
      ),
      'attributes' => array(
        'value' => $this->_nPosId,
      ),
    ));
    $this->add(array(
      'type' => 'Text',
      'name' => 'session_id',
      'options' => array(
        'label' => 'session_id',
      ),
      'attributes' => array(
        'value' => $this->_sSessionId,
      ),
    ));
    $this->add(array(
      'type' => 'Text',
      'name' => 'ts',
      'options' => array(
        'label' => 'ts',
      ),
      'attributes' => array(
        'value' => $this->_nTs,
      ),
    ));
    $this->add(array(
      'type' => 'Text',
      'name' => 'sig',
      'options' => array(
        'label' => 'sig',
      ),
      'attributes' => array(
        'value' => $this->_sSig,
      ),
    ));
    $this->add(array(
      'type' => 'Button',
      'name' => 'submit_button',
      'options' => array(
        'label' => 'Test',
      ),
    ));
  }

  /**
   * Set a single option for an element
   *
   * @param  string $key
   * @param  mixed $value
   * @return self
   */
  public function setOption($key, $value)
  {
    // TODO: Implement setOption() method.
  }
}

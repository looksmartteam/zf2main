<?php
namespace Shop\Form;

use Application\FormAbstract;

class NewPayment extends FormAbstract
{
  public function __construct($oServiceLocator, $sFormName)
  {
    parent::__construct($oServiceLocator, $sFormName);
  }

  public function init()
  {
    $this->add(array(
      'type' => 'Text',
      'name' => 'first_name',
      'options' => array(
        'label' => 'Imię',
      ),
    ));
    $this->add(array(
      'type' => 'Text',
      'name' => 'last_name',
      'options' => array(
        'label' => 'Nazwisko',
      ),
    ));
    $this->add(array(
      'type' => 'Text',
      'name' => 'email_address',
      'options' => array(
        'label' => 'Adres e-mail',
      ),
    ));
    $this->add(array(
      'type' => 'Text',
      'name' => 'phone_number',
      'options' => array(
        'label' => 'Numer telefonu',
      ),
    ));
    $this->add(array(
      'type' => 'Text',
      'name' => 'amount',
      'options' => array(
        'label' => 'Kwota',
      ),
      'attributes' => array(
        'readonly' => true,
      ),
    ));
    $this->add(array(
      'type' => 'Button',
      'name' => 'submit_button',
      'options' => array(
        'label' => 'Przejdź do e-płatności',
      ),
    ));
  }

  /**
   * Set a single option for an element
   *
   * @param  string $key
   * @param  mixed $value
   * @return self
   */
  public function setOption($key, $value)
  {
    // TODO: Implement setOption() method.
  }
}

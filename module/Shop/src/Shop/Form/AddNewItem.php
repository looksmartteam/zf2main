<?php
namespace Shop\Form;

use Application\FormAbstract;

class AddNewItem extends FormAbstract
{
  public function __construct($oServiceLocator, $sFormName)
  {
    parent::__construct($oServiceLocator, $sFormName);
  }

  public function init()
  {
    $this->add(array(
      'type' => 'Text',
      'name' => 'name',
      'options' => array(
        'label' => 'Nazwa produktu',
      ),
    ));
    $this->add(array(
      'type' => 'Text',
      'name' => 'price_net',
      'options' => array(
        'label' => 'Cena netto',
      ),
    ));
    $this->add(array(
      'type' => 'Text',
      'name' => 'price_gross',
      'options' => array(
        'label' => 'Cena brutto',
      ),
    ));
    $this->add(array(
      'type' => 'Button',
      'name' => 'submit_button',
      'options' => array(
        'label' => 'Zapisz',
      ),
    ));
  }

  /**
   * Set a single option for an element
   *
   * @param  string $key
   * @param  mixed $value
   * @return self
   */
  public function setOption($key, $value)
  {
    // TODO: Implement setOption() method.
  }
}

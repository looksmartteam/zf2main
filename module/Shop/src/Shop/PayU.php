<?php
namespace Shop;
class PayU
{
  /**
   * Kody błędów
   *
   * @var array (wartość, opis)
   */
  protected $_aArrorCodes;
  /**
   * Typy płatności
   *
   * @var array
   */
  protected $_aPayTypes;
  /**
   * Tablica przechowywująca status transakcji
   *
   * @var array
   */
  protected $_aPaymentStatus;
  /**
   * Statusy transakcji
   *
   * @var array (wartość opis)
   */
  protected $_aStatuses;
  /**
   * Kwota w groszach
   *
   * @var numeric
   */
  protected $_nAmount;
  /**
   * Identyfikator POS"a
   *
   * @var int
   */
  protected $_nPosId;
  /**
   * Parametr ts (aktualny czas w sekundach)
   *
   * @var string
   */
  protected $_nTs;
  /**
   *
   * @var int
   */
  protected $_nUserId;
  /**
   *
   * @var string
   */
  protected $_sCity;
  /**
   * Adres clientIp klienta w formacie D{1,3}.D{1,3}.D{1,3}.D{1,3}
   *
   * @var string
   */
  protected $_sClientIp;
  /**
   * Krótki opis - pokazywany klientowi, trafia na wyciągi i inne miejsca
   *
   * @var string {1,50}
   */
  protected $_sDesc;
  /**
   * Krótki opis - pokazywany klientowi, trafia na wyciągi i inne miejsca
   *
   * @var string {1,1024}
   */
  protected $_sDesc2;
  /**
   * Adres e-mail
   *
   * @var string
   */
  protected $_sEmail;
  /**
   * Kodowanie. W zależności od tego jakiej strony kodowej używa aplikacja Sklepu należy wybrać odpowiednie
   * kodowanie przy odwołaniu do procedur Platnosci.pl
   *
   * @var string
   */
  protected $_sEncoding;
  //===============================================================================
  /**
   * Imię
   *
   * @var string {1,100}
   */
  protected $_sFirstName;
  //===============================================================================
  /**
   * Format danych. Dla procedur: Payment/get, Payment/confirm, Payment/cancel, możemy jeszcze
   * podać format w jakim mają być przesłane dane
   *
   * "xml" lub "txt"
   * @var string
   */
  protected $_sFormatDanych = self::FORMAT_DANYCH_XML;
  /**
   * Wartość nadana przez Platnosci.pl. Klucz używany podczas sprawdzania podpisy przysyłanego przez Sklep
   *
   * @var string {32}
   */
  protected $_sKey1;
  //===============================================================================
  /**
   * Wartość nadana przez Platnosci.pl. Klucz używany do generowania podpisu wysyłanego do Sklepu
   *
   * @var string {32}
   */
  protected $_sKey2;
  /**
   * Nazwisko
   *
   * @var string {1,100}
   */
  protected $_sLastName;
  //===============================================================================
  /**
   * Numer zamówienia
   *
   * @var string
   */
  protected $_sOrderId;
  /**
   * Typ płatności. Parametr opcjonalny, zalecane jest żeby nie podawać tego parametru
   *
   * @var string
   */
  protected $_sPayType;
  /**
   *
   * @var string
   */
  protected $_sPesel;
  //===============================================================================
  /**
   *
   * @var string
   */
  protected $_sPhone;
  /**
   * Wartość nadana przez Platnosci.pl
   *
   * @var string {7,7}
   */
  protected $_sPosAuthKey;
  /**
   *
   * @var string
   */
  protected $_sPostCode;
  //===============================================================================
  /**
   * Identyfikator płatności - unikalny ciąg znaków dla klienta generowany z klasy AppPlatnosci_GeneratePass
   *
   * @var string {0, 1024}
   */
  protected $_sSessionId;
  /**
   * Parametr sig - podpis MD5
   *
   * Każde przesłanie polecania oraz każda odpowiedź generowana przez Platnosci.pl zawiera podpis MD5,
   * dzięki temu można zweryfikować poprawność danych.
   *
   * @var string {32}
   */
  protected $_sSig;
  /**
   *
   * @var string
   */
  protected $_sStreet;
  //===============================================================================
  /**
   * Url nowej płatności
   *
   * @var string
   */
  protected $_sUrlNewPayment;
  /**
   * Url wymiany informacji o transakcjach
   *
   * @var string
   */
  protected $_sUrlPaymentGet;

  public function __construct()
  {
    $this->_nPosId = 168613; //165465;
    $this->_sKey1 = 'f5506754d92c6729072cd8114f02b7a4'; //'b694fee0ab83c7e27e0c4f1130ac3960';
    $this->_sKey2 = 'af5d4e6c8cb4cdf8e401d99e696f5df2'; //'df81d10dc632092dc590c985b9310b27';
    $this->_sPosAuthKey = 'A2uTn3n'; //'KvALFrm';
    $this->_sEncoding = self::ENCODING_UTF;
    $this->_sUrlNewPayment = self::BASE_URL . '/' . self::ENCODING_UTF . '/' . self::NEW_PAYMENT;
    $this->_sUrlPaymentGet = self::BASE_URL . '/' . self::ENCODING_UTF . '/' . self::PAYMENT_GET;
    $this->fillErrorCodes();
    $this->fillStatuses();
    $this->fillPayTypes();
  }
  //===============================================================================

  protected function fillErrorCodes()
  {
    $this->_aArrorCodes = array(
      100 => 'brak lub błędna wartość parametru pos id',
      101 => 'brak parametru session id',
      102 => 'brak parametru ts',
      103 => 'brak lub błędna wartość parametru sig',
      104 => 'brak parametru desc',
      105 => 'brak parametru client ip',
      106 => 'brak parametru first name',
      107 => 'brak parametru last name',
      108 => 'brak parametru street',
      109 => 'brak parametru city',
      110 => 'brak parametru post code',
      111 => 'brak parametru amount',
      112 => 'błędny numer konta bankowego',
      113 => 'brak parametru email',
      114 => 'brak numeru telefonu',
      200 => 'inny chwilowy błąd',
      201 => 'inny chwilowy błąd bazy danych',
      202 => 'Pos o podanym identyfikatorze jest zablokowany',
      203 => 'niedozwolona wartość pay type dla danego pos id',
      204 => 'podana metoda płatności(wartość pay type)jest chwilowo zablokowana dla danego pos id, np. przerwa konserwacyjna bramki płatniczej',
      205 => 'kwota transakcji mniejsza od wartości minimalnej',
      206 => 'kwota transakcji większa od wartości maksymalnej',
      207 => 'przekroczona wartość wszystkich transakcji dla jednego klienta w ostatnim prze-dziale czasowym',
      208 => 'Pos działa w wariancie ExpressPayment lecz nie nastąpiła aktywacja tego wariantu współpracy (czekamy na zgodę działu obsługi klienta)',
      209 => 'błędny numer pos id lub pos auth key',
      500 => 'transakcja nie istnieje',
      501 => 'brak autoryzacji dla danej transakcji',
      502 => 'transakcja rozpoczęta wcześniej',
      503 => 'autoryzacja do transakcji była już przeprowadzana',
      504 => 'transakcja anulowana wcześniej',
      505 => 'transakcja przekazana do odbioru wcześniej',
      506 => 'transakcja już odebrana',
      507 => 'błąd podczas zwrotu środków do klienta',
      599 => 'błędny stan transakcji, np. nie można uznać transakcji kilka razy lub inny, prosimy o kontakt',
      999 => 'inny błąd krytyczny - prosimy o kontakt'
    );
  }

  protected function fillPayTypes()
  {
    $this->_aPayTypes = array(
      'm' => 'mTransfer - mBank',
      'n' => 'MultiTransfer - MultiBank',
      'w' => 'BZWBK - Przelew24',
      'o' => 'Pekao24Przelew - Bank Pekao',
      'i' => 'Płacę z Inteligo',
      'd' => 'Płać z Nordea',
      'p' => 'Płać z iPKO',
      'h' => 'Płać z BPH',
      'g' => 'Płać z ING',
      'l' => 'LUKAS e-przelew',
      'u' => 'Eurobank',
      'me' => 'Meritum Bank',
      'ab' => 'Płacę z Alior Bankiem',
      'wp' => 'Przelew z Polbank',
      'wm' => 'Przelew z Millennium',
      'wk' => 'Przelew z Kredyt Bank',
      'wg' => 'Przelew z BGŻ',
      'wd' => 'Przelew z Deutsche Bank',
      'wr' => 'Przelew z Raiffeisen Bank',
      'wc' => 'Przelew z Citibank',
      'c' => 'karta kredytowa',
      'b' => 'Przelew bankowy',
      't' => 'płatność testowa'
    );
  }
  //===============================================================================

  protected function fillStatuses()
  {
    $this->_aStatuses = array(
      1 => 'nowa',
      2 => 'anulowana',
      3 => 'odrzucona',
      4 => 'rozpoczęta',
      5 => 'oczekuje na odbiór',
      7 => 'płatność odrzucona',
      99 => 'płatność odebrana - zakończona',
      888 => 'błędny status - prosimy o kontakt'
    );
  }

  /**
   *
   * @return string {32}
   */
  protected function getKey1()
  {
    return $this->_sKey1;
  }
  //===============================================================================

  /**
   *
   * @return string {32}
   */
  protected function getKey2()
  {
    return $this->_sKey2;
  }
  //===============================================================================

  /**
   * @param $aData
   * @return bool
   * @throws \Exception
   */
  public function checkSigFromPlatnosci($aData)
  {
    if (!is_array($aData)) {
      throw new \Exception('Błędna tablica POST');
    }
    if (!isset($aData['session_id']) || !isset($aData['sig']) || !isset($aData['ts'])) {
      throw new \Exception('Błędne parametry tablicy POST');
    }
    $tmpSig = md5($this->_nPosId . $aData['session_id'] . $aData['ts'] . $this->_sKey2);
    if ($tmpSig != $aData['sig']) {
      throw new \Exception('Parametr sig jest niepoprawny');
    }
    return true;
  }

  /**
   * @return numeric
   * @throws \Exception
   */
  public function getAmount()
  {
    if (null === $this->_nAmount) {
      throw new \Exception('Nie podano amount');
    }
    return $this->_nAmount;
  }
  //===============================================================================

  /**
   * @return string
   * @throws \Exception
   */
  public function getCity()
  {
    if (null === $this->_sCity) {
      throw new \Exception('Nie podano city');
    }
    return $this->_sCity;
  }

  /**
   * @return string
   * @throws \Exception
   */
  public function getClientIP()
  {
    if (null === $this->_sClientIp) {
      throw new \Exception('Nie podano client_ip');
    }
    return $this->_sClientIp;
  }
  //===============================================================================

  /**
   * @return string
   * @throws \Exception
   */
  public function getDesc()
  {
    if (null === $this->_sDesc) {
      throw new \Exception('Nie podano desc');
    }
    return $this->_sDesc;
  }

  /**
   * @return string
   * @throws \Exception
   */
  public function getDesc2()
  {
    if (null === $this->_sDesc2) {
      throw new \Exception('Nie podano desc');
    }
    return $this->_sDesc2;
  }
  //===============================================================================

  /**
   * @return string
   * @throws \Exception
   */
  public function getEmailAddress()
  {
    if ($this->_sEmail == '') {
    }
    if (null === $this->_sEmail) {
      throw new \Exception('Nie podano email');
    }
    return $this->_sEmail;
  }

  /**
   * @param $nErrorCode
   * @return mixed
   */
  public function getErrorMsg($nErrorCode)
  {
    return $this->_aArrorCodes[$nErrorCode];
  }

  //===============================================================================

  /**
   * @return string
   * @throws \Exception
   */
  public function getFirstName()
  {
    if (null === $this->_sFirstName) {
      throw new \Exception('Nie podano first_name');
    }
    return $this->_sFirstName;
  }

  /**
   * @return string
   * @throws \Exception
   */
  public function getLastName()
  {
    if (null === $this->_sLastName) {
      throw new \Exception('Nie podano last_name');
    }
    return $this->_sLastName;
  }

  public function getOrderId()
  {
    return $this->_sOrderId;
  }

  /**
   *
   */
  public function getPayType()
  {
  }

  /**
   * @return array
   */
  public function getPaymentParam()
  {
    return array(
      'session_id' => $this->getSessionId(),
      'order_id' => $this->getOrderId(),
      'amount' => $this->getAmount(),
      'description' => $this->getDesc(),
      'user_id' => $this->getUserId(),
      'first_name' => $this->getFirstName(),
      'last_name' => $this->getLastName(),
      'email_address' => $this->getEmailAddress(),
    );
  }

  /**
   * @return array
   * @throws \Exception
   */
  public function getPaymentStatus()
  {
    if (!is_array($this->_aPaymentStatus)) {
      throw new \Exception('Błędy status transakcji');
    }
    return $this->_aPaymentStatus;
  }

  /**
   * @return array
   */
  public function getPaymentStatusInfo()
  {
    switch ($this->_aPaymentStatus['status']) {
      case 1:
        return array('code' => $this->_aPaymentStatus['status'], 'message' => 'nowa');
        break;
      case 2:
        return array('code' => $this->_aPaymentStatus['status'], 'message' => 'anulowana');
        break;
      case 3:
        return array('code' => $this->_aPaymentStatus['status'], 'message' => 'odrzucona');
        break;
      case 4:
        return array('code' => $this->_aPaymentStatus['status'], 'message' => 'rozpoczęta');
        break;
      case 5:
        return array('code' => $this->_aPaymentStatus['status'], 'message' => 'oczekuje na odbiór');
        break;
      case 7:
        return array('code' => $this->_aPaymentStatus['status'], 'message' => 'płatność odrzucona');
        break;
      case 99:
        return array('code' => $this->_aPaymentStatus['status'], 'message' => 'płatność odebrana - zakończona');
        break;
      case 888:
        return array('code' => $this->_aPaymentStatus['status'], 'message' => 'błędny status');
        break;
      default:
        return array('code' => false, 'message' => 'brak statusu');
        break;
    }
  }

  /**
   * @return string
   * @throws \Exception
   */
  public function getPesel()
  {
    if (null === $this->_sPesel) {
      throw new \Exception('Nie podano pesel');
    }
    return $this->_sPesel;
  }

  /**
   * @return string
   * @throws \Exception
   */
  public function getPhone()
  {
    if (null === $this->_sPhone) {
      throw new \Exception('Nie podano phone');
    }
    return $this->_sPhone;
  }

  /**
   * @return string
   * @throws \Exception
   */
  public function getPosAuthKey()
  {
    if (null === $this->_sPosAuthKey) {
      throw new \Exception('Nie podano pos_auth_key');
    }
    return $this->_sPosAuthKey;
  }

  /**
   * @return int
   */
  public function getPosId()
  {
    return $this->_nPosId;
  }

  /**
   * @return string
   * @throws \Exception
   */
  public function getPostCode()
  {
    if (null === $this->_sPostCode) {
      throw new \Exception('Nie podano postCode');
    }
    return $this->_sPostCode;
  }

  /**
   * @param $aData
   * @return bool
   * @throws \Exception
   */
  public function getResponse($aData)
  {
    if (!isset($aData['pos_id']) || !isset($aData['session_id']) || !isset($aData['ts']) || !isset($aData['sig'])) {
      throw new \Exception('Brak wszystkich parametróww tablicy POST');
    }
    if (!$this->checkSigFromPlatnosci($aData)) {
      throw new \Exception('Parametr sig został niepoprawnie podpisany');
    }
    $sParameters = $this->getResponseParam($aData); //budowanie url żądania odpowiedzi
    $bFSocket = false;
    $bCurl = false;
    $sServer = 'www.platnosci.pl';
    $sServerScript = '/paygw/UTF/Payment/get';
    if (function_exists('curl_exec')) {
      $bCurl = true;
    } else if ((PHP_VERSION >= 4.3) && ($rFp = @fsockopen('ssl://' . $sServer, 443, $nErrno, $sErrstr, 30))) {
      $bFSocket = true;
    }
    if ($bFSocket == true) {
      $sHeader = "POST " . $sServerScript . " HTTP/1.0" . "\r\n" .
        "Host: " . $sServer . "\r\n" .
        "Content-Type: application/x-www-form-urlencoded" . "\r\n" .
        "Content-Length: " . strlen($sParameters) . "\r\n" .
        "Connection: close" . "\r\n\r\n";
      @fputs($rFp, $sHeader . $sParameters);
      $sResponse = '';
      while (!@feof($rFp)) {
        $sRes = @fgets($rFp, 1024);
        $sResponse .= $sRes;
      }
      @fclose($rFp);
    } else if ($bCurl == true) {
      $rCh = curl_init();
      curl_setopt($rCh, CURLOPT_URL, 'https://' . $sServer . $sServerScript);
      curl_setopt($rCh, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($rCh, CURLOPT_HEADER, 0);
      curl_setopt($rCh, CURLOPT_TIMEOUT, 20);
      curl_setopt($rCh, CURLOPT_POST, 1);
      curl_setopt($rCh, CURLOPT_POSTFIELDS, $sParameters);
      curl_setopt($rCh, CURLOPT_RETURNTRANSFER, 1);
      $sResponse = curl_exec($rCh);
      curl_close($rCh);
    } else {
      throw new \Exception('Nie uzyskano połączenia z płatnościami');
    }
    if (preg_match_all("#<trans>.*<id>([0-9]*)</id>.*<pos_id>([0-9]*)</pos_id>.*<session_id>(.*)</session_id>.*<order_id>(.*)</order_id>.*<amount>([0-9]*)</amount>.*<status>([0-9]*)</status>.*<pay_type>(.*)</pay_type>.*<pay_gw_name>(.*)</pay_gw_name>.*<desc>(.*)</desc>.*<desc2>(.*)</desc2>.*<create>(.*)</create>.*<init>(.*)</init>.*<sent>(.*)</sent>.*<recv>(.*)</recv>.*<cancel>(.*)</cancel>.*<auth_fraud>(.*)</auth_fraud>.*<ts>([0-9]*)</ts>.*<sig>([a-z0-9]*)</sig>.*</trans>#siU", $sResponse, $aParts)) {
      $this->setPaymentStatus($aParts);
    }
    return true;
  }

  /**
   * @param $aData
   * @return string
   * @throws \Exception
   */
  public function getResponseParam($aData)
  {
    $sSig = $this->getSigResponse($aData);
    if (!isset($this->_sSessionId)) {
      throw new \Exception('Niewłaściwa wartość session_id');
    }
    if (!isset($sSig) || !is_string($sSig) || (strlen($sSig) != 32)) {
      throw new \Exception('Niewłaściwa wartość sig');
    }
    $sTmpParam = 'pos_id=' . $this->_nPosId . '&session_id=' . $this->_sSessionId . '&ts=' . $this->_nTs . '&sig=' . $sSig;
    return $sTmpParam;
  }

  /**
   * @return string
   * @throws \Exception
   */
  public function getSessionId()
  {
    if (null === $this->_sSessionId) {
      throw new \Exception('Nie podano session_id');
    }
    return $this->_sSessionId;
  }

  /**
   * @return string
   * @throws \Exception
   */
  public function getSig()
  {
    if (!is_string($this->_sSig) && (strlen($this->_sSig) != 32)) {
      throw new \Exception('Błędnie wygenerowany parametr sig');
    }
    return $this->_sSig;
  }

  /**
   * Parametr sig - na potrzeby odczytania stanu transakcji
   *
   * @param array $aData
   * @return string {32}
   * @throws \Exception
   */
  public function getSigResponse($aData)
  {
    if (!is_array($aData)) {
      throw new \Exception('Błędna tablica POST');
    }
    if (!isset($aData['session_id']) || !isset($aData['ts'])) {
      throw new \Exception('Błędne parametry tablicy POST');
    }
    $this->_nTs = time();
    $this->_sSessionId = $aData['session_id'];
    return md5($this->_nPosId . $this->_sSessionId . $this->_nTs . $this->_sKey1);
  }

  /**
   * @param $nStatusId
   * @return mixed
   */
  public function getStatus($nStatusId)
  {
    return $this->_aStatuses[$nStatusId];
  }

  /**
   * @return string
   * @throws \Exception
   */
  public function getStreet()
  {
    if (null === $this->_sStreet) {
      throw new \Exception('Nie podano street');
    }
    return $this->_sStreet;
  }

  /**
   * @return string
   * @throws \Exception
   */
  public function getTs()
  {
    if (null === $this->_nTs) {
      throw new \Exception('Nie podano ts');
    }
    return $this->_nTs;
  }

  /**
   * @return string
   */
  public function getUrlNewPayment()
  {
    return $this->_sUrlNewPayment;
  }

  /**
   * @return string
   */
  public function getUrlPaymentGet()
  {
    return $this->_sUrlPaymentGet;
  }

  /**
   * @return string
   * @throws \Exception
   */
  public function getUserId()
  {
    if (null === $this->_nUserId) {
      throw new \Exception('Nie podano user_id');
    }
    return $this->_nUserId;
  }

  /**
   * @param $nAmount
   * @return $this
   * @throws \Exception
   */
  public function setAmount($nAmount)
  {
    if (!is_numeric($nAmount)) {
      throw new \Exception('Podana wartość amount nie jest liczbą');
    }
    $this->_nAmount = $nAmount * 100;
    return $this;
  }

  /**
   * @param $sCity
   * @return $this
   * @throws \Exception
   */
  public function setCity($sCity)
  {
    if (!is_string($sCity)) {
      throw new \Exception('Wartość city jest niepoprawna (nie jest ciągiem znaków)');
    }
    $this->_sCity = (string)$sCity;
    return $this;
  }

  /**
   * @return $this
   */
  public function setClientIp()
  {
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
      $sClientIp = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
      $sClientIp = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
      $sClientIp = $_SERVER['REMOTE_ADDR'];
    }
    $this->_sClientIp = (string)$sClientIp;
    return $this;
  }

  /**
   * @param $sDesc
   * @return $this
   * @throws \Exception
   */
  public function setDesc($sDesc)
  {
    $length = strlen($sDesc);
    if ($length < 1 || $length > 255) {
      throw new \Exception('Wartość desc jest niepoprawna');
    }
    $this->_sDesc = (string)$sDesc;
    return $this;
  }

  /**
   * @param $sDesc
   * @return $this
   * @throws \Exception
   */
  public function setDesc2($sDesc)
  {
    $length = strlen($sDesc);
    if ($length < 1 || $length > 1024) {
      throw new \Exception('Wartość desc jest niepoprawna');
    }
    $this->_sDesc2 = (string)$sDesc;
    return $this;
  }

  /**
   * @param $sEmailAddress
   * @return $this
   */
  public function setEmailAddress($sEmailAddress)
  {
    $this->_sEmail = (string)$sEmailAddress;
    return $this;
  }

  /**
   * @param $sEncoding
   * @return $this
   * @throws \Exception
   */
  public function setEncoding($sEncoding)
  {
    switch ($sEncoding) {
      case self::ENCODING_ISO:
      case self::ENCODING_UTF:
      case self::ENCODING_WIN:
        $this->_sEncoding = $sEncoding;
        break;
      default:
        throw new \Exception('Niewłaściwe kodowanie');
    }
    return $this;
  }

  /**
   * @param $sFirstName
   * @return $this
   * @throws \Exception
   */
  public function setFirstName($sFirstName)
  {
    if (!is_string($sFirstName)) {
      throw new \Exception('Wartość first_name jest niepoprawna (nie jest ciągiem znaków)');
    }
    $this->_sFirstName = (string)$sFirstName;
    return $this;
  }

  /**
   * @param $sLastName
   * @return $this
   * @throws \Exception
   */
  public function setLastName($sLastName)
  {
    if (!is_string($sLastName)) {
      throw new \Exception('Wartość last_name jest niepoprawna (nie jest ciągiem znaków)');
    }
    $this->_sLastName = (string)$sLastName;
    return $this;
  }

  /**
   * @return array
   */
  public function setNewOrder()
  {
    $aTmpNewOrder = array(
      'session_id' => $this->getSessionId(),
      'order_id' => $this->getOrderId(),
      'borrower_id' => $this->getBorrowerId(),
      'pesel' => $this->getPesel(),
      'amount' => $this->getAmount(),
      'first_name' => $this->getFirstName(),
      'last_name' => $this->getLastName(),
      'email_address' => $this->getEmailAddress(),
      'date_is_starting' => time()
    );
    return $aTmpNewOrder;
  }

  /**
   * @return array
   */
  public function setNewUserOrder()
  {
    $tmpNewOrder = array(
      'session_id' => $this->getSessionId(),
      'order_id' => $this->getOrderId(),
      'pesel' => $this->getPesel(),
      'amount' => $this->getAmount() / 100,
      'first_name' => $this->getFirstName(),
      'last_name' => $this->getLastName(),
      'email_address' => $this->getEmailAddress(),
    );
    return $tmpNewOrder;
  }

  public function setOrderId()
  {
    $oMakeHash = new \Application\MakeHash();
    $sHash = $oMakeHash->generate();
    $this->_sOrderId = $sHash;
    if (strlen($this->_sOrderId) != 32) {
      throw new \Exception('Niewłaściwa wartość session_id');
    }
    return $this;
  }

  /**
   *
   */
  public function setPayType()
  {
  }

  /**
   * @param $aParts
   * @throws \Exception
   */
  public function setPaymentStatus($aParts)
  {
    if (!is_array($aParts)) {
      throw new \Exception('Błęda tablica statusu transakcji');
    }
    if ($aParts[2][0] != $this->_nPosId) {
      throw new \Exception('Niewłaściwa wartość pos_id');
    }
    $sTmpSig = md5($aParts[2][0] . $aParts[3][0] . $aParts[4][0] . $aParts[6][0] . $aParts[5][0] . $aParts[9][0] . $aParts[17][0] . $this->_sKey2);
    if ($aParts[18][0] != $sTmpSig) {
      throw new \Exception('Niewłaściwa wartość sig');
    }
    $aTmpPaymentStatus = array(
      'payment_id' => $aParts[1][0],
      'pos_id' => $aParts[2][0],
      'session_id' => $aParts[3][0],
      'order_id' => $aParts[4][0],
      'amount' => $aParts[5][0],
      'status' => $aParts[6][0],
      'pay_type' => $aParts[7][0],
      'pay_gw_name' => $aParts[8][0],
      'desc' => $aParts[9][0],
      'desc2' => $aParts[10][0],
      'create' => $aParts[11][0],
      'init' => $aParts[12][0],
      'sent' => $aParts[13][0],
      'recv' => $aParts[14][0],
      'cancel' => $aParts[15][0],
      'auth_fraud' => $aParts[16][0],
      'ts' => $aParts[17][0],
      'sig' => $aParts[18][0]
    );
    $this->_aPaymentStatus = $aTmpPaymentStatus;
  }

  /**
   * @param $sPesel
   * @return $this
   * @throws \Exception
   */
  public function setPesel($sPesel)
  {
    if (!is_string($sPesel)) {
      throw new \Exception('Wartość pesel jest niepoprawna (nie jest ciągiem znaków)');
    }
    $this->_sPesel = (string)$sPesel;
    return $this;
  }

  /**
   * @param $sPhone
   * @return $this
   * @throws \Exception
   */
  public function setPhone($sPhone)
  {
    if (!is_string($sPhone)) {
      throw new \Exception('Wartość phone jest niepoprawna (nie jest ciągiem znaków)');
    }
    $this->_sPhone = (string)$sPhone;
    return $this;
  }

  /**
   * @param $sPostCode
   * @return $this
   * @throws \Exception
   */
  public function setPostCode($sPostCode)
  {
    if (!is_string($sPostCode)) {
      throw new \Exception('Wartość post_code jest niepoprawna (nie jest ciągiem znaków)');
    }
    $this->_sPostCode = (string)$sPostCode;
    return $this;
  }

  /**
   * @return $this
   * @throws \Exception
   */
  public function setSessionId()
  {
    $oMakeHash = new \Application\MakeHash();
    $sHash = $oMakeHash->generate();
    $this->_sSessionId = $sHash;
    if (strlen($this->_sSessionId) != 32) {
      throw new \Exception('Niewłaściwa wartość session_id');
    }
    return $this;
  }

  /**
   *
   */
  public function setSig()
  {
    $this->setTs();
    $sTmpSig = '';
    $aTmpArray = array(
      'pos_id' => $this->_nPosId,
      'pay_type' => '',
      'session_id' => $this->getSessionId(),
      'pos_auth_key' => $this->_sPosAuthKey,
      'amount' => $this->getAmount(),
      'desc' => $this->getDesc(),
      'desc2' => '',
      'trsDesc' => '',
      'order_id' => $this->getOrderId(),
      'first_name' => $this->getFirstName(),
      'last_name' => $this->getLastName(),
      'payback_login' => '',
      'street' => $this->getStreet(),
      'street_hn' => '',
      'street_an' => '',
      'city' => $this->getCity(),
      'post_code' => $this->getPostCode(),
      'country' => '',
      'email' => $this->getEmailAddress(),
      'phone' => $this->getPhone(),
      'language' => '',
      'client_ip' => $this->getClientIP(),
      'ts' => $this->getTs(),
      'key1' => $this->_sKey1
    );
    foreach ($aTmpArray as $sValue)
      $sTmpSig .= $sValue;
    $this->_sSig = md5($sTmpSig);
  }

  /**
   * @param $sStreet
   * @return $this
   * @throws \Exception
   */
  public function setStreet($sStreet)
  {
    if (!is_string($sStreet)) {
      throw new \Exception('Wartość street jest niepoprawna (nie jest ciągiem znaków)');
    }
    $this->_sStreet = (string)$sStreet;
    return $this;
  }

  /**
   * @return $this
   */
  public function setTs()
  {
    $this->_nTs = (string)time();
    return $this;
  }

  /**
   * @param $nUserId
   * @return $this
   * @throws \Exception
   */
  public function setUserId($nUserId)
  {
    if (!is_numeric($nUserId)) {
      throw new \Exception('Wartość użytkownika jest niepoprawna');
    }
    $this->_nUserId = $nUserId;
    return $this;
  }
  const BASE_URL = 'https://www.platnosci.pl/paygw';
  const NEW_PAYMENT = 'NewPayment';
  const PAYMENT_GET = 'Payment/get';
  /**
   * ISO-8859-2
   * @var string
   */
  const ENCODING_ISO = 'ISO';
  /**
   * UTF-8
   * @var string
   */
  const ENCODING_UTF = 'UTF';
  /**
   * Windows-1250
   * @var string
   */
  const ENCODING_WIN = 'WIN';
  /**
   * Format danych
   * @var string
   */
  const FORMAT_DANYCH_XML = 'xml';
  const FORMAT_DANYCH_TXT = 'txt';
  /**
   * Status 1 - nowa transakcja
   */
  const STATUS_NOWA = 1;
  /**
   * Status 2 - „anulowana” pojawi się automatycznie po określonej liczbie dni (punkt 2.4)
   * od utworzenia lub rozpoczęcia transakcji (Statusy 1 lub 4) jeśli do tego czasu
   * nie zostanie ona rozliczona (nie wpłyną środki do systemu Płatności.pl).
   * Status ten pojawi się również po wywołaniu akcji „anuluj” w panelu administracyjnym
   * bądź po wywołaniu metody Payment/cancel dla transakcji będącej w statusie 1 lub 4.
   * @var int
   */
  const STATUS_ANULOWANA = 2;
  /**
   * Status 3 - „odrzucona” pojawi się wówczas, gdy użytkownik dla transakcji będącej
   * w statusie 5 - „oczekuje na odbiór” wywoła akcję „anuluj” w panelu lub metodę Payment/cancel,
   * a wybrany typ płatności nie pozwala na automatyczne zwrócenie środków do klienta
   * (czyli w większości dostępnych typów płatności). Status 3 - „odrzucona” pojawi się również
   * gdy w przypadku „anulowanej” (status 2) transakcji nastąpi jej rozliczenie (wpływ środków
   * do systemu Płatności). W przypadku odebrania transakcji (akcja „odbierz” w panelu lub
   * wywołanie metody Payment/confirm ), która posiada status 3 - „odrzucona” oraz gdy
   * użytkownik nie ma włączonych autoodbiorów wpłat, status transakcji zmieni się na 5 -
   * „oczekuje na odbiór”, wówczas należy jeszcze raz wykonać akcję „odbierz” w panelu lub
   * wywołać metodę Payment/confirm, by zakończyć transakcję czyli przejść do statusu
   * 99 - „zakończona”. W przypadku gdy zaś chcemy zwrócić do klienta środki z transakcji
   * będącej w statusie 3, należy wykonać dla tej transakcji akcję „anuluj” w panelu bądź
   * wywołać metodę Payment/cancel.
   */
  const STATUS_ODRZUCONA = 3;
  /**
   * Status 4 - „rozpoczęta” jest stanem pośrednim i nie musi wystąpić transakcja może
   * przejść do stanu „oczekuje na odbiór” lub „zakończona” (w przypadku włączonej opcji
   * autoodbiór wpłat) bezpośrednio ze stanu „nowa”.
   */
  const STATUS_ROZPOCZETA = 4;
  /**
   * Status 5 - „oczekuje na odbiór” pojawi się tylko wtedy gdy mamy wyłączoną opcje
   * „Automatyczne odbieranie”, w takim przypadku Sklep ma 5 dni
   * (dokładnie 5 * 24 godziny od czasu rozpoczęcia transakcji) na odebranie płatności.
   * Gdy płatność taka nie zostanie odebrana w odpowiednim terminie zostanie ona automatycznie anulowana.
   * Odebranie płatności należy wykonać wywołując metodę Payment/confirm lub poprzez panel
   * administracyjny serwisu.
   */
  const STATUS_OCZEKUJE_NA_ODBIOR = 5;
  /**
   * Status 7 - „zwrot środków do klienta” pojawi się wówczas gdy transakcja ma status 3 „odrzucona”
   * a użytkownik wykona akcję „anuluj” w panelu lub wywoła metodę Payment/cancel .
   */
  const STATUS_PLATNOSC_ODRZUCONA = 7;
}
<?php
namespace Shop\Service\Form;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class AddNewItemFormService implements FactoryInterface
{
  public function createService(ServiceLocatorInterface $oServiceLocator)
  {
    $oForm = new \Shop\Form\AddNewItem();
    return $oForm;
  }
}

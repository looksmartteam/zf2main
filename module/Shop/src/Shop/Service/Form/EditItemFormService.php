<?php
namespace Shop\Service\Form;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class EditItemFormService implements FactoryInterface
{
  public function createService(ServiceLocatorInterface $oServiceLocator)
  {
    $oForm = new \Shop\Form\EditItem();
    return $oForm;
  }
}

<?php
namespace Shop\Service\Repository;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class CartItemTableService implements FactoryInterface
{
  public function createService(ServiceLocatorInterface $oServiceLocator)
  {
    $oDbAdapter = $oServiceLocator->get('DefaultDbAdapter');
    $oTable = new \Shop\Model\Repository\CartItem($oDbAdapter);
    return $oTable;
  }
}

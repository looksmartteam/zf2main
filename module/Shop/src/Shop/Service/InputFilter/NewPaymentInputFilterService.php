<?php
namespace Shop\Service\InputFilter;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class NewPaymentInputFilterService implements FactoryInterface
{
  public function createService(ServiceLocatorInterface $oServiceLocator)
  {
    $oInputFilter = new \Shop\Model\InputFilter\NewPayment();
    return $oInputFilter;
  }
}

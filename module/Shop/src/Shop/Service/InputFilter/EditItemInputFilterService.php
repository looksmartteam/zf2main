<?php
namespace Shop\Service\InputFilter;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class EditItemInputFilterService implements FactoryInterface
{
  public function createService(ServiceLocatorInterface $oServiceLocator)
  {
    $oInputFilter = new \Shop\Model\InputFilter\EditItem();
    return $oInputFilter;
  }
}

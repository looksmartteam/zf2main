<?php
namespace Shop\Service\InputFilter;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class EditCartItemInputFilterService implements FactoryInterface
{
  public function createService(ServiceLocatorInterface $oServiceLocator)
  {
    $oInputFilter = new \Shop\Model\InputFilter\EditCartItem();
    return $oInputFilter;
  }
}

<?php
namespace API;
class Module
{
  public function getAutoloaderConfig()
  {
    return array(
      'Zend\Loader\ClassMapAutoloader' => array(
        __DIR__ . '/autoload_classmap.php',
      ),
      'Zend\Loader\StandardAutoloader' => array(
        'namespaces' => array(
          __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
        ),
      )
    );
  }

  public function getConfig()
  {
    return include __DIR__ . '/config/module.config.php';
  }

  public function getServiceConfig()
  {
    $aFactories = array();
    $aSearchableDirectory = array('Form', 'Entity');
    foreach ($aSearchableDirectory as $sDirectory) {
      $sDir = __DIR__ . '/src/' . __NAMESPACE__ . '/' . $sDirectory;
      if (file_exists($sDir)) {
        $oFiles = new \DirectoryIterator($sDir);
        foreach ($oFiles as $oFileInfo) {
          $bIsFile = strpos($oFileInfo->__toString(), '~');
          if (!$oFileInfo->isDot() && !$oFileInfo->isDir() && !$bIsFile) {
            $sClassName = str_replace('.php', '', $oFileInfo->__toString());
            if ($sDirectory == 'Entity') {
              $aFactories[$sClassName . 'DoctrineEntityService'] = function ($oServiceLocator) use ($sClassName, $sDirectory) {
                $sClassName = __NAMESPACE__ . '\\' . $sDirectory . '\\' . $sClassName;
                $oNewClassInstance = new $sClassName();
                return $oNewClassInstance;
              };
            } else if ($sDirectory == 'Form') {
              $aSnippedName = preg_split('/(?=[A-Z])/', $sClassName);
              if (is_array($aSnippedName) && count($aSnippedName)) {
                $sKey = '';
                $nIdx = count($aSnippedName);
                foreach ($aSnippedName as $sSnippedName) {
                  $nIdx--;
                  if (strlen($sSnippedName)) {
                    $sKey .= lcfirst($sSnippedName);
                    if ($nIdx) {
                      $sKey .= '_';
                    }
                  }
                }
                $sFormName = $sKey;
                $sEdidFormName = 'edit_' . $sKey;
              }
              $aFactories[$sClassName . 'FormService'] = function ($oServiceLocator) use ($sClassName, $sDirectory, $sFormName) {
                $sClassName = __NAMESPACE__ . '\\' . $sDirectory . '\\' . $sClassName;
                $oNewClassInstance = new $sClassName($oServiceLocator, $sFormName);
                return $oNewClassInstance;
              };
              $aFactories['Edit' . $sClassName . 'FormService'] = function ($oServiceLocator) use ($sClassName, $sDirectory, $sEdidFormName) {
                $sClassName = __NAMESPACE__ . '\\' . $sDirectory . '\\' . $sClassName;
                $oNewClassInstance = new $sClassName($oServiceLocator, $sEdidFormName);
                return $oNewClassInstance;
              };
            }
          }
        }
      }
    }
    $aSearchableDirectory = array('Entity', 'InputFilter', 'Repository');
    foreach ($aSearchableDirectory as $sDirectory) {
      $sDir = __DIR__ . '/src/' . __NAMESPACE__ . '/Model/' . $sDirectory;
      if (file_exists($sDir)) {
        $oFiles = new \DirectoryIterator($sDir);
        foreach ($oFiles as $oFileInfo) {
          $bIsFile = strpos($oFileInfo->__toString(), '~');
          if (!$oFileInfo->isDot() && !$oFileInfo->isDir() && !$bIsFile) {
            $sClassName = str_replace('.php', '', $oFileInfo->__toString());
            if ($sDirectory == 'Entity') {
              $aFactories[$sClassName . 'NativEntityService'] = function ($oServiceLocator) use ($sClassName, $sDirectory) {
                $sClassName = __NAMESPACE__ . '\Model\\' . $sDirectory . '\\' . $sClassName;
                $oNewClassInstance = new $sClassName($oServiceLocator);
                return $oNewClassInstance;
              };
            } else if ($sDirectory == 'Repository') {
              $aFactories[$sClassName . 'TableService'] = function ($oServiceLocator) use ($sClassName, $sDirectory) {
                $sClassName = __NAMESPACE__ . '\Model\\' . $sDirectory . '\\' . $sClassName;
                $oDbAdapter = $oServiceLocator->get('DefaultDbAdapter');
                $oNewClassInstance = new $sClassName($oDbAdapter);
                return $oNewClassInstance;
              };
            }
          }
        }
      }
    }
    return array(
      'factories' => $aFactories,
    );
  }
}

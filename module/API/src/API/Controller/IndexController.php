<?php
namespace API\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Frlnc\Slack\Http\SlackResponseFactory;
use Frlnc\Slack\Http\CurlInteractor;
use Frlnc\Slack\Core\Commander;
use Firebase\FirebaseLib;
const DEFAULT_URL = 'https://beelab.firebaseio.com/';
const DEFAULT_TOKEN = 'wiLL0ANl1zLthh7YMFGx74SKUvvfRttnUDm4Y03V';
const DEFAULT_PATH = '/test';
class IndexController extends AbstractRestfulController
{
  public function create($aData) // Action used for POST requests
  {
    $oFirebase = new FirebaseLib(DEFAULT_URL, DEFAULT_TOKEN);
    $aPush = array('id' => time(), 'data' => array('a' => 'b'));
    $oFirebase->push(DEFAULT_PATH, $aPush);
    //$oEntityManager = $this->getServiceLocator()->get('\Doctrine\ORM\EntityManager');
    //$oSlackEntity = new \Application\Entity\Slack();
    //$oSlackEntity->setText($aData['text']);
    //$oSlackEntity->setCreatedDate(time());
    //$oEntityManager->persist($oSlackEntity);
    //$oEntityManager->flush();
    //$oSlack = $oEntityManager->getRepository('\Application\Entity\Slack')->findAll();
    return new JsonModel(array("text" => "123"));
  }

  public function delete($nId) // Action used for DELETE requests
  {
    return new JsonModel(array('data' => 'album id 3 deleted'));
  }

  public function get($nId) // Action used for GET requests with resource Id
  {
    return new JsonModel(array("data" => array('id' => 2, 'name' => 'Coda', 'band' => 'Led Zeppelin')));
  }

  public function getList()
  {
    return new JsonModel(array("data" => array('id' => 233, 'name' => 'Coda', 'band' => 'Led Zeppelin')));
  }

  public function update($nId, $aData) // Action used for PUT requests
  {
    return new JsonModel(array('data' => array('id' => 3, 'name' => 'Updated Album', 'band' => 'Updated Band')));
  }
}
<?php
namespace API;
return array(
  'router' => array(
    'routes' => array(
      'api_index' => array(
        'type' => 'Segment',
        'options' => array(
          'route' => '/api[/:id]',
          'constraints' => array(
            'id' => '[0-9]+',
          ),
          'defaults' => array(
            'controller' => 'API\Controller\Index',
          ),
        ),
      ),
    ),
  ),
  'service_manager' => array(
    'factories' => array(),
  ),
  'controllers' => array(
    'invokables' => array(
      'API\Controller\Index' => 'API\Controller\IndexController',
    ),
  ),
  'controller_plugins' => array(
    'invokables' => array()
  ),
  'view_manager' => array(
    'template_map' => array(),
    'template_path_stack' => array(
      'api' => __DIR__ . '/../view',
    ),
    'strategies' => array(
      'ViewJsonStrategy',
    ),
  ),
  'view_helpers' => array(
    'invokables' => array(),
  ),
  'module_layouts' => array(),
);

<?php
namespace Application\Model\Entity;

use Application\EntityAbstract;

class NavigationPrivilegeUserRole extends EntityAbstract
{
  public $navigation_privilege_id;
  public $user_role_id;
  public $user_role_name;
}

<?php
namespace Application\Model\Entity;

use Application\EntityAbstract;

class NavigationMenuType extends EntityAbstract
{
  public $value;
  public $active;
}

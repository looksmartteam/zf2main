<?php
namespace Application\Model\Entity;

use Application\EntityAbstract;

class SiteSeoMetaKeywords extends EntityAbstract
{
  public $value;
  public $primary;
  public $active;
}

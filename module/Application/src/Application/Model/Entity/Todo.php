<?php
namespace Application\Model\Entity;

use Application\EntityAbstract;

class Todo extends EntityAbstract
{
  public $description;
  public $todo_priority_id;
  public $todo_priority_name;
  public $color;
  public $active;
  public $created_date;
  public $updated_date;
}

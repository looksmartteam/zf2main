<?php
namespace Application\Model\Entity;

use Application\EntityAbstract;

class NavigationPrivilageUserRole extends EntityAbstract
{
  public $navigation_privilage_id;
  public $user_role_id;
}

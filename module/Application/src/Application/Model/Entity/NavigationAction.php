<?php
namespace Application\Model\Entity;

use Application\EntityAbstract;

class NavigationAction extends EntityAbstract
{
  public $value;
  public $ajax;
  public $created_date;
  public $updated_date;
}

<?php
namespace Application\Model\Entity;

use Application\EntityAbstract;

class NavigationOption extends EntityAbstract
{
  public $navigation_module_id;
  public $navigation_controller_id;
  public $navigation_action_id;
  public $navigation_resource_id;
  public $navigation_privilege_id;
}

<?php
namespace Application\Model\Entity;

use Application\EntityAbstract;

class NavigationMenuConfiguration extends EntityAbstract
{
  public $configuration_name;
  public $primary;
  public $active;
}

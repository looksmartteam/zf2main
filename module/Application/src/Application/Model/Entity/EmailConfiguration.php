<?php
namespace Application\Model\Entity;

use Application\EntityAbstract;

class EmailConfiguration extends EntityAbstract
{
  public $host;
  public $username;
  public $password;
  public $name;
  public $from;
  public $primary;
}

<?php
namespace Application\Model\Entity;

use Application\EntityAbstract;

class NavigationController extends EntityAbstract
{
  public $value;
  public $created_date;
  public $updated_date;
}

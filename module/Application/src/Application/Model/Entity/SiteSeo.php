<?php
namespace Application\Model\Entity;

use Application\EntityAbstract;

class SiteSeo extends EntityAbstract
{
  public $site_seo_robots_id;
  public $site_seo_robots_name;
  public $head_title;
  public $meta_description;
}

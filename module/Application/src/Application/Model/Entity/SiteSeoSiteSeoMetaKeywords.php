<?php
namespace Application\Model\Entity;

use Application\EntityAbstract;

class SiteSeoSiteSeoMetaKeywords extends EntityAbstract
{
  public $site_seo_id;
  public $site_seo_meta_keywords_id;
  public $site_seo_meta_keywords_name;
  public $active;
  public $primary;
}

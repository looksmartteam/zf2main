<?php
namespace Application\Model\Entity;

use Application\EntityAbstract;

class Geolocation extends EntityAbstract
{
  public $ip_start;
  public $ip_end;
  public $city;
  public $county;
  public $voivodeship;
}

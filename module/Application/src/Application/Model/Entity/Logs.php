<?php
namespace Application\Model\Entity;

use Application\EntityAbstract;

class Logs extends EntityAbstract
{
  public $module;
  public $controller;
  public $action;
  public $user_id;
  public $ip_address;
  public $geolocation_id;
  public $date;
}

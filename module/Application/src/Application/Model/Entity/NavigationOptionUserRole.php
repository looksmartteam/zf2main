<?php
namespace Application\Model\Entity;

use Application\EntityAbstract;

class NavigationOptionUserRole extends EntityAbstract
{
  public $navigation_option_id;
  public $user_role_id;
}

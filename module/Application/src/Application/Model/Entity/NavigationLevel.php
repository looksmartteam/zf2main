<?php
namespace Application\Model\Entity;

use Application\EntityAbstract;

class NavigationLevel extends EntityAbstract
{
  public $level;
  public $name;
  public $visible;
}

<?php
namespace Application\Model\Entity;

use Application\EntityAbstract;

class NavigationModule extends EntityAbstract
{
  public $value;
  public $created_date;
  public $updated_date;
}

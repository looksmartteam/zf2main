<?php
namespace Application\Model\Entity;

use Application\EntityAbstract;

class EmailContentConfiguration extends EntityAbstract
{
  public $email_content_configuration_type_id;
  public $email_content_configuration_type_name;
  public $trigger_action;
  public $subject;
  public $email_content_configuration_greeting_type_id;
  public $message;
  public $email_content_configuration_conclusion_type_id;
  public $active;
}

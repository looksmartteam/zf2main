<?php
namespace Application\Model\Entity;

use Application\EntityAbstract;

class NavigationMenu extends EntityAbstract
{
  public $navigation_menu_configuration_id;
  public $navigation_menu_type_id;
  public $navigation_option_id;
  public $navigation_level_id;
  public $navigation_parent_menu_id;
  public $navigation_parent_submenu_id;
  public $navigation_layout_id;
  public $navigation_layout_name;
  public $site_seo_id;
  public $label_name;
  public $description;
  public $image_id;
  public $order;
  public $active;
  public $navigation_resource_id;
  public $navigation_resource_name;
  public $navigation_privilege_id;
  public $navigation_privilege_name;
  public $unique_key;
  public $navigation_module_id;
  public $navigation_module_name;
  public $navigation_controller_id;
  public $navigation_controller_name;
  public $navigation_action_id;
  public $navigation_action_name;
  public $ajax;
  public $user_role_id = array();
  public $site_seo_robots_id;
  public $head_title;
  public $meta_description;
  public $site_seo_meta_keywords_id = array();

  public function setNavigationParentMenuId($nNavigationParentMenuId)
  {
    if (is_numeric($nNavigationParentMenuId) && $nNavigationParentMenuId > 0) {
      $this->navigation_parent_menu_id = (int)$nNavigationParentMenuId;
    } else {
      $this->navigation_parent_menu_id = null;
    }
    return $this;
  }

  public function setNavigationParentSubmenuId($nNavigationParentSubmenuId)
  {
    if (is_numeric($nNavigationParentSubmenuId) && $nNavigationParentSubmenuId > 0) {
      $this->navigation_parent_menu_id = (int)$nNavigationParentSubmenuId;
    }
    return $this;
  }
}
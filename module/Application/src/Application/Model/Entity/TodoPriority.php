<?php
namespace Application\Model\Entity;

use Application\EntityAbstract;

class TodoPriority extends EntityAbstract
{
  public $name;
  public $color;
}

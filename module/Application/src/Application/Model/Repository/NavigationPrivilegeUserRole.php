<?php
namespace Application\Model\Repository;

use Application\Model\Entity\NavigationPrivilegeUserRole as Entity;
use Application\RepositoryAbstract;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;

class NavigationPrivilegeUserRole extends RepositoryAbstract
{
  protected $table = 'navigation_privilege_user_role';

  public function __construct(Adapter $oAdapter)
  {
    parent::__construct($oAdapter);
  }

  protected function createNewEntity()
  {
    return new Entity();
  }

  public function addRow($oEntity)
  {
    $aData = array();
    $aData['navigation_privilege_id'] = $oEntity->navigation_privilege_id;
    $aData['user_role_id'] = $oEntity->user_role_id;
    return parent::addRow($aData);
  }

  public function getAll(array $aData = null)
  {
    $oResultSet = $this->select(function (Select $oSelect) use ($aData) {
      $oSelect->join('user_role', 'user_role.id = navigation_privilege_user_role.user_role_id', array('user_role_name' => 'role_name'));
      if ($aData) {
        foreach ($aData as $sKey => $mValue) {
          $oSelect->where(array($sKey => $mValue));
        }
      }
    });
    return $this->getEntities($oResultSet);
  }
}

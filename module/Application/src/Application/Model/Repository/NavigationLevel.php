<?php
namespace Application\Model\Repository;

use Application\Model\Entity\NavigationLevel as Entity;
use Application\RepositoryAbstract;
use Zend\Db\Adapter\Adapter;

class NavigationLevel extends RepositoryAbstract
{
  protected $table = 'navigation_level';

  public function __construct(Adapter $oAdapter)
  {
    parent::__construct($oAdapter);
  }

  protected function createNewEntity()
  {
    return new Entity();
  }
}

<?php
namespace Application\Model\Repository;

use Application\Model\Entity\NavigationPrivilageUserRole as Entity;
use Application\RepositoryAbstract;
use Zend\Db\Adapter\Adapter;

class NavigationPrivilageUserRole extends RepositoryAbstract
{
  protected $table = 'navigation_privilage_user_role';

  public function __construct(Adapter $oAdapter)
  {
    parent::__construct($oAdapter);
  }

  protected function createNewEntity()
  {
    return new Entity();
  }
}

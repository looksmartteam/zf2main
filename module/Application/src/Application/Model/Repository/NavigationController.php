<?php
namespace Application\Model\Repository;

use Application\Model\Entity\NavigationController as Entity;
use Application\RepositoryAbstract;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;

class NavigationController extends RepositoryAbstract
{
  protected $table = 'navigation_controller';

  public function __construct(Adapter $oAdapter)
  {
    parent::__construct($oAdapter);
  }

  protected function createNewEntity()
  {
    return new Entity();
  }

  public function addRow($oEntity)
  {
    $nId = $this->findRow(array('value' => $oEntity->value));
    if ($nId) {
      return $nId;
    } else {
      $aData = array();
      $aData['value'] = $oEntity->value;
      $aData['created_date'] = time();
      return parent::addRow($aData);
    }
  }

  public function getAll(array $aData = null)
  {
    $oThat = $this;
    $oResultSet = $this->select(function (Select $oSelect) use ($aData, $oThat) {
      if ($aData) {
        foreach ($aData as $sKey => $mValue) {
          if ($sKey === 'filter' && isset($mValue)) {
            foreach ($mValue as $sColumnName => $sValue) {
              $sDataType = $oThat->getColumnDataType($sColumnName);
              if ($sColumnName === 'id') {
                $sColumnName = $oThat->table . '.' . $sColumnName;
              }
              if (in_array($sDataType, array('int', 'tinyint')) && $sColumnName !== 'created_date' && $sColumnName !== 'updated_date') {
                $oSelect->where(array($sColumnName . '=?' => (int)$sValue));
              } else if ($sColumnName === 'created_date' || $sColumnName === 'updated_date') {
                $oSelect->where(array($sColumnName . '>=?' => strtotime($sValue)));
                $oSelect->where(array($sColumnName . '<=?' => strtotime($sValue) + 86400));
              } else {
                $oSelect->where->like($sColumnName, '%' . $sValue . '%');
              }
            }
          } else if ($sKey === 'sort' && is_string($mValue['sort_column']) && is_string($mValue['sort_method'])) {
            if ($mValue['sort_column'] === 'id') {
              $mValue['sort_column'] = $oThat->table . '.' . $mValue['sort_column'];
            }
            $oSelect->order(array($mValue['sort_column'] => $mValue['sort_method']));
          } else if ($sKey === 'limit' && isset($mValue) && is_numeric($mValue)) {
            $oSelect->limit($mValue);
          } else if ($sKey === 'offset' && isset($mValue) && is_numeric($mValue)) {
            $oSelect->offset($mValue);
          } else if (!in_array($sKey, array('filter', 'sort', 'limit', 'offset'))) {
            if ($sKey === 'id') {
              $sKey = $oThat->table . '.id';
            }
            $oSelect->where(array($sKey => $mValue));
          }
        }
      }
    });
    $aResult = $this->getEntities($oResultSet);
    return $aResult;
  }
}

<?php
namespace Application\Model\Repository;

use Application\Model\Entity\Geolocation as Entity;
use Application\RepositoryAbstract;
use Zend\Db\Adapter\Adapter;

class Logs extends RepositoryAbstract
{
  protected $table = 'logs';

  public function __construct(Adapter $oAdapter)
  {
    parent::__construct($oAdapter);
  }

  protected function createNewEntity()
  {
    return new Entity();
  }

  public function addRow($oEntity)
  {
    $aData = array();
    $aData['module'] = $oEntity->module;
    $aData['controller'] = $oEntity->controller;
    $aData['action'] = $oEntity->action;
    $aData['user_id'] = $oEntity->user_id;
    $aData['ip_address'] = $oEntity->ip_address;
    $aData['geolocation_id'] = $oEntity->geolocation_id;
    $aData['date'] = time();
    return parent::addRow($aData);
  }
}

<?php
namespace Application\Model\Repository;

use Application\Model\Entity\EmailContentConfigurationType as Entity;
use Application\RepositoryAbstract;
use Zend\Db\Adapter\Adapter;

class EmailContentConfigurationType extends RepositoryAbstract
{
  protected $table = 'email_content_configuration_type';

  public function __construct(Adapter $oAdapter)
  {
    parent::__construct($oAdapter);
  }

  protected function createNewEntity()
  {
    return new Entity();
  }

  public function addRow($oEntity)
  {
    $aData = array();
    $aData['name'] = $oEntity->name;
    return parent::addRow($aData);
  }
}
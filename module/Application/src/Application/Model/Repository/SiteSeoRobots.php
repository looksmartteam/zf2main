<?php
namespace Application\Model\Repository;

use Application\Model\Entity\SiteSeoRobots as Entity;
use Application\RepositoryAbstract;
use Zend\Db\Adapter\Adapter;

class SiteSeoRobots extends RepositoryAbstract
{
  protected $table = 'site_seo_robots';

  public function __construct(Adapter $oAdapter)
  {
    parent::__construct($oAdapter);
  }

  protected function createNewEntity()
  {
    return new Entity();
  }
}

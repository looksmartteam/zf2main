<?php
namespace Application\Model\Repository;

use Application\Model\Entity\NavigationPrivilege as Entity;
use Application\RepositoryAbstract;
use Zend\Db\Adapter\Adapter;

class NavigationPrivilege extends RepositoryAbstract
{
  protected $table = 'navigation_privilege';

  public function __construct(Adapter $oAdapter)
  {
    parent::__construct($oAdapter);
  }

  protected function createNewEntity()
  {
    return new Entity();
  }

  public function addRow($oEntity)
  {
    $aData = array();
    $aData['value'] = $oEntity->value;
    $aData['unique_key'] = $oEntity->unique_key;
    return parent::addRow($aData);
  }
}

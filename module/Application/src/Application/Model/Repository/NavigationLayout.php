<?php
namespace Application\Model\Repository;

use Application\Model\Entity\NavigationLayout as Entity;
use Application\RepositoryAbstract;
use Zend\Db\Adapter\Adapter;

class NavigationLayout extends RepositoryAbstract
{
  protected $table = 'navigation_layout';

  public function __construct(Adapter $oAdapter)
  {
    parent::__construct($oAdapter);
  }

  protected function createNewEntity()
  {
    return new Entity();
  }

  public function addRow($oEntity)
  {
    $aData = array();
    $aData['name'] = $oEntity->name;
    return parent::addRow($aData);
  }
}
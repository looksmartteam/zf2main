<?php
namespace Application\Model\Repository;

use Application\Model\Entity\NavigationResource as Entity;
use Application\RepositoryAbstract;
use Zend\Db\Adapter\Adapter;

class NavigationResource extends RepositoryAbstract
{
  protected $table = 'navigation_resource';

  public function __construct(Adapter $oAdapter)
  {
    parent::__construct($oAdapter);
  }

  protected function createNewEntity()
  {
    return new Entity();
  }

  public function addRow($oEntity)
  {
    $aData = array();
    $aData['value'] = $oEntity->value;
    return parent::addRow($aData);
  }
}

<?php
namespace Application\Model\Repository;

use Application\Model\Entity\NavigationPrivilage as Entity;
use Application\RepositoryAbstract;
use Zend\Db\Adapter\Adapter;

class NavigationPrivilage extends RepositoryAbstract
{
  protected $table = 'navigation_privilage';

  public function __construct(Adapter $oAdapter)
  {
    parent::__construct($oAdapter);
  }

  protected function createNewEntity()
  {
    return new Entity();
  }
}

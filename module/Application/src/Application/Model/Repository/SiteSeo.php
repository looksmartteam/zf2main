<?php
namespace Application\Model\Repository;

use Application\Model\Entity\SiteSeo as Entity;
use Application\RepositoryAbstract;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;

class SiteSeo extends RepositoryAbstract
{
  protected $table = 'site_seo';

  public function __construct(Adapter $oAdapter)
  {
    parent::__construct($oAdapter);
  }

  protected function createNewEntity()
  {
    return new Entity();
  }

  public function addRow($oEntity)
  {
    $aData = array();
    $aData['site_seo_robots_id'] = $oEntity->site_seo_robots_id;
    $aData['head_title'] = $oEntity->head_title;
    $aData['meta_description'] = $oEntity->meta_description;
    return parent::addRow($aData);
  }

  public function getAll(array $aData = null)
  {
    $oThat = $this;
    $oResultSet = $this->select(function (Select $oSelect) use ($aData, $oThat) {
      $oSelect->join('site_seo_robots', 'site_seo_robots.id = site_seo.site_seo_robots_id', array('site_seo_robots_name' => 'name'));
      if ($aData) {
        foreach ($aData as $sKey => $mValue) {
          if ($sKey === 'id') {
            $sKey = $oThat->table . '.' . $sKey;
          }
          $oSelect->where(array($sKey => $mValue));
        }
      }
    });
    return $this->getEntities($oResultSet);
  }

  public function getRow(array $aData = null)
  {
    $oThat = $this;
    $oResultSet = $this->select(function (Select $oSelect) use ($aData, $oThat) {
      $oSelect->join('site_seo_robots', 'site_seo_robots.id = site_seo.site_seo_robots_id', array('site_seo_robots_name' => 'name'));
      if ($aData) {
        foreach ($aData as $sKey => $mValue) {
          if ($sKey === 'id') {
            $sKey = $oThat->table . '.' . $sKey;
          }
          $oSelect->where(array($sKey => $mValue));
        }
      }
      $oSelect->limit(1);
    });
    $aResult = $this->getEntities($oResultSet);
    if (count($aResult) === 1)
      return $aResult[0];
    return null;
  }
}

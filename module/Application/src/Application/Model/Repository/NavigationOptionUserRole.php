<?php
namespace Application\Model\Repository;

use Application\Model\Entity\NavigationOptionUserRole as Entity;
use Application\RepositoryAbstract;
use Zend\Db\Adapter\Adapter;

class NavigationOptionUserRole extends RepositoryAbstract
{
  protected $table = 'navigation_option_user_role';

  public function __construct(Adapter $oAdapter)
  {
    parent::__construct($oAdapter);
  }

  protected function createNewEntity()
  {
    return new Entity();
  }
}

<?php
namespace Application\Model\Repository;

use Application\Model\Entity\NavigationMenu as Entity;
use Application\RepositoryAbstract;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;

class NavigationMenu extends RepositoryAbstract
{
  protected $table = 'navigation_menu';

  public function __construct(Adapter $oAdapter)
  {
    parent::__construct($oAdapter);
  }

  protected function createNewEntity()
  {
    return new Entity();
  }

  public function addRow($oEntity)
  {
    $aData = array();
    $aData['navigation_menu_configuration_id'] = $oEntity->navigation_menu_configuration_id;
    $aData['navigation_menu_type_id'] = $oEntity->navigation_menu_type_id;
    $aData['navigation_option_id'] = $oEntity->navigation_option_id;
    $aData['navigation_level_id'] = $oEntity->navigation_level_id;
    $aData['navigation_parent_menu_id'] = $oEntity->navigation_parent_menu_id;
    $aData['navigation_layout_id'] = $oEntity->navigation_layout_id;
    //$aData['site_seo_id'] = $oEntity->site_seo_id;
    $aData['label_name'] = $oEntity->label_name;
    $aData['description'] = $oEntity->description;
    $aData['image_id'] = $oEntity->image_id;
    $aData['order'] = $oEntity->order;
    //$aData['active'] = $oEntity->active;
    return parent::addRow($aData);
  }

  public function getAll(array $aData = null)
  {
    $oThat = $this;
    $oResultSet = $this->select(function (Select $oSelect) use ($aData, $oThat) {
      $oSelect->join('navigation_option', 'navigation_option.id = navigation_menu.navigation_option_id', array('navigation_module_id', 'navigation_controller_id', 'navigation_action_id', 'navigation_resource_id'));
      $oSelect->join('navigation_resource', 'navigation_resource.id = navigation_option.navigation_resource_id', array('navigation_resource_name' => 'value'));
      //$oSelect->join('navigation_privilege', 'navigation_privilege.id = navigation_option.navigation_privilege_id', array('navigation_privilege_name' => 'value', 'unique_key'));
      $oSelect->join('navigation_module', 'navigation_module.id = navigation_option.navigation_module_id', array('navigation_module_name' => 'value'));
      $oSelect->join('navigation_controller', 'navigation_controller.id = navigation_option.navigation_controller_id', array('navigation_controller_name' => 'value'));
      $oSelect->join('navigation_action', 'navigation_action.id = navigation_option.navigation_action_id', array('navigation_action_name' => 'value', 'ajax'));
      $oSelect->join('navigation_layout', 'navigation_layout.id = navigation_menu.navigation_layout_id', array('navigation_layout_name' => 'name'), $oSelect::JOIN_LEFT);
      //$oSelect->join('site_seo', 'site_seo.id = navigation_menu.site_seo_id', array('site_seo_robots_id', 'head_title', 'meta_description'), $oSelect::JOIN_LEFT);
      if ($aData) {
        foreach ($aData as $sKey => $mValue) {
          if ($sKey === 'filter' && isset($mValue)) {
            foreach ($mValue as $sColumnName => $sValue) {
              $sDataType = $oThat->getColumnDataType($sColumnName);
              if ($sColumnName === 'id') {
                $sColumnName = $oThat->table . '.' . $sColumnName;
              }
              if (in_array($sDataType, array('int', 'tinyint'))) {
                $oSelect->where(array($sColumnName . '=?' => (int)$sValue));
              } else {
                $oSelect->where->like($sColumnName, '%' . $sValue . '%');
              }
            }
          } else if ($sKey === 'sort' && is_string($mValue['sort_column']) && is_string($mValue['sort_method'])) {
            if ($mValue['sort_column'] === 'id') {
              $mValue['sort_column'] = $oThat->table . '.' . $mValue['sort_column'];
            }
            $oSelect->order(array($mValue['sort_column'] => $mValue['sort_method']));
          } else if ($sKey === 'limit' && isset($mValue) && is_numeric($mValue)) {
            $oSelect->limit($mValue);
          } else if ($sKey === 'offset' && isset($mValue) && is_numeric($mValue)) {
            $oSelect->offset($mValue);
          } else if (!in_array($sKey, array('filter', 'sort', 'limit', 'offset'))) {
            if ($sKey === 'id') {
              $sKey = $oThat->table . '.id';
            }
            $oSelect->where(array($sKey => $mValue));
          }
        }
      }
    });
    $aResult = $this->getEntities($oResultSet);
    return $aResult;
  }
}
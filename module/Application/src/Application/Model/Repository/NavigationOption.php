<?php
namespace Application\Model\Repository;

use Application\Model\Entity\NavigationOption as Entity;
use Application\RepositoryAbstract;
use Zend\Db\Adapter\Adapter;

class NavigationOption extends RepositoryAbstract
{
  protected $table = 'navigation_option';

  public function __construct(Adapter $oAdapter)
  {
    parent::__construct($oAdapter);
  }

  protected function createNewEntity()
  {
    return new Entity();
  }

  public function addRow($oEntity)
  {
    $aData = array();
    $aData['navigation_module_id'] = $oEntity->navigation_module_id;
    $aData['navigation_controller_id'] = $oEntity->navigation_controller_id;
    $aData['navigation_action_id'] = $oEntity->navigation_action_id;
    $aData['navigation_privilege_id'] = $oEntity->navigation_privilege_id;
    $aData['navigation_resource_id'] = $oEntity->navigation_resource_id;
    return parent::addRow($aData);
  }
}

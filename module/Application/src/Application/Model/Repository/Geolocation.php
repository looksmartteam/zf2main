<?php
namespace Application\Model\Repository;

use Application\Model\Entity\Geolocation as Entity;
use Application\RepositoryAbstract;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;

class Geolocation extends RepositoryAbstract
{
  protected $table = 'geolocation';

  public function __construct(Adapter $oAdapter)
  {
    parent::__construct($oAdapter);
  }

  protected function createNewEntity()
  {
    return new Entity();
  }

  public function find(array $aData = null)
  {
    if (ip2long($aData['ip_address'])) {
      $oThat = $this;
      $oResultSet = $this->select(function (Select $oSelect) use ($aData, $oThat) {
        $nIpAddress = ip2long($aData['ip_address']);
        $oSelect->where(array('ip_start <=?' => $nIpAddress));
        $oSelect->where(array('ip_end >=?' => $nIpAddress));
        $oSelect->limit(1);
      });
      $aResult = $this->getEntities($oResultSet);
      if (count($aResult) === 1)
        return $aResult[0]->id;
      else
        return null;
    }
  }
}

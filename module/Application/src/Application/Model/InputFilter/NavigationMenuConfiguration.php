<?php
namespace Application\Model\InputFilter;

use Application\InputFilterAbstract;
use Zend\InputFilter\InputFilter;

class NavigationMenuConfiguration extends InputFilterAbstract
{
  public function __construct($oServiceLocator)
  {
    parent::__construct($oServiceLocator);
  }

  public function getInputFilter()
  {
    if (!$this->_oInputFilter) {
      $oInputFilter = new InputFilter();
      $oInputFilter->add(array(
        'name' => 'configuration_name',
        'required' => true,
        'filters' => array(
          array('name' => 'StripTags'),
          array('name' => 'StringTrim'),
        ),
        'validators' => array(
          array(
            'name' => 'StringLength',
            'options' => array(
              'encoding' => 'UTF-8',
              'min' => 1,
              'max' => 100,
            ),
          ),
        ),
      ));
      $oInputFilter->add(array(
        'name' => 'active',
        'required' => true,
      ));
      $oInputFilter->add(array(
        'name' => 'primary',
        'required' => false,
      ));
      $this->_oInputFilter = $oInputFilter;
    }
    return $this->_oInputFilter;
  }
}
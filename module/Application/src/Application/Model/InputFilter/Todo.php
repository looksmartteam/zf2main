<?php
namespace Application\Model\InputFilter;

use Application\InputFilterAbstract;
use Zend\InputFilter\InputFilter;

class Todo extends InputFilterAbstract
{
  public function __construct($oServiceLocator)
  {
    parent::__construct($oServiceLocator);
  }

  public function getInputFilter()
  {
    if (!$this->_oInputFilter) {
      $oInputFilter = new InputFilter();
      $oInputFilter->add(array(
        'name' => 'description',
        'required' => true,
      ));
      $oInputFilter->add(array(
        'name' => 'priority',
        'required' => false,
      ));
      $oInputFilter->add(array(
        'name' => 'active',
        'required' => false,
      ));
      $this->_oInputFilter = $oInputFilter;
    }
    return $this->_oInputFilter;
  }
}
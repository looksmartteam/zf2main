<?php
namespace Application\Model\InputFilter;

use Application\InputFilterAbstract;
use Zend\InputFilter\InputFilter;

class NavigationMenu extends InputFilterAbstract
{
  public function __construct($oServiceLocator)
  {
    parent::__construct($oServiceLocator);
  }

  public function getInputFilter()
  {
    if (!$this->_oInputFilter) {
      $oInputFilter = new InputFilter();
      $oInputFilter->add(array(
        'name' => 'navigation_menu_configuration_id',
        'required' => false,
      ));
      $oInputFilter->add(array(
        'name' => 'navigation_menu_type_id',
        'required' => false,
      ));
      $oInputFilter->add(array(
        'name' => 'navigation_layout_id',
        'required' => false,
      ));
      $oInputFilter->add(array(
        'name' => 'active',
        'required' => false,
      ));
      $oInputFilter->add(array(
        'name' => 'navigation_level_id',
        'required' => false,
      ));
      $oInputFilter->add(array(
        'name' => 'navigation_parent_menu_id',
        'required' => false,
      ));
      $oInputFilter->add(array(
        'name' => 'navigation_parent_submenu_id',
        'required' => false,
      ));
      $oInputFilter->add(array(
        'name' => 'label_name',
        'required' => true,
        'filters' => array(
          array('name' => 'StripTags'),
          array('name' => 'StringTrim'),
        ),
        'validators' => array(
          array(
            'name' => 'StringLength',
            'options' => array(
              'encoding' => 'UTF-8',
              'min' => 1,
              'max' => 100,
            ),
          ),
        ),
      ));
      $oInputFilter->add(array(
        'name' => 'description',
        'required' => false,
        'filters' => array(
          array('name' => 'StripTags'),
          array('name' => 'StringTrim'),
        ),
      ));
      $oInputFilter->add(array(
        'name' => 'navigation_module_id',
        'required' => true,
      ));
      $oInputFilter->add(array(
        'name' => 'navigation_controller_id',
        'required' => true,
      ));
      $oInputFilter->add(array(
        'name' => 'navigation_action_id',
        'required' => true,
      ));
      $oInputFilter->add(array(
        'name' => 'user_role_id',
        'required' => true,
        'validators' => array(
          array(
            'name' => 'NotEmpty',
            'break_chain_on_failure' => true,
          ),
        ),
      ));
      $oInputFilter->add(array(
        'name' => 'site_seo_robots_id',
        'required' => true,
      ));
      $oInputFilter->add(array(
        'name' => 'head_title',
        'required' => false,
        'filters' => array(
          array('name' => 'StripTags'),
          array('name' => 'StringTrim'),
        ),
      ));
      $oInputFilter->add(array(
        'name' => 'site_seo_meta_keywords_id',
        'required' => false,
      ));
      $oInputFilter->add(array(
        'name' => 'meta_description',
        'required' => false,
        'filters' => array(
          array('name' => 'StripTags'),
          array('name' => 'StringTrim'),
        ),
      ));
      $this->_oInputFilter = $oInputFilter;
    }
    return $this->_oInputFilter;
  }
}
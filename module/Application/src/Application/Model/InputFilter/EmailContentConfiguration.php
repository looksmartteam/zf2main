<?php
namespace Application\Model\InputFilter;

use Application\InputFilterAbstract;
use Zend\InputFilter\InputFilter;

class EmailContentConfiguration extends InputFilterAbstract
{
  public function __construct($oServiceLocator)
  {
    parent::__construct($oServiceLocator);
  }

  public function getInputFilter()
  {
    if (!$this->_oInputFilter) {
      $oInputFilter = new InputFilter();
      $oInputFilter->add(array(
        'name' => 'email_content_configuration_type_id',
        'required' => true,
      ));
      $oInputFilter->add(array(
        'name' => 'trigger_action',
        'required' => true,
        'filters' => array(
          array('name' => 'StripTags'),
          array('name' => 'StringTrim'),
        ),
        'validators' => array(
          array(
            'name' => 'StringLength',
            'options' => array(
              'encoding' => 'UTF-8',
              'min' => 1,
              'max' => 100,
            ),
          ),
        ),
      ));
      $oInputFilter->add(array(
        'name' => 'subject',
        'required' => true,
        'filters' => array(
          array('name' => 'StripTags'),
          array('name' => 'StringTrim'),
        ),
        'validators' => array(
          array(
            'name' => 'StringLength',
            'options' => array(
              'encoding' => 'UTF-8',
              'min' => 1,
              'max' => 255,
            ),
          ),
        ),
      ));
      $oInputFilter->add(array(
        'name' => 'email_content_configuration_greeting_type_id',
        'required' => false,
      ));
      $oInputFilter->add(array(
        'name' => 'message',
        'required' => true,
      ));
      $oInputFilter->add(array(
        'name' => 'email_content_configuration_conclusion_type_id',
        'required' => false,
      ));
      $oInputFilter->add(array(
        'name' => 'active',
        'required' => false,
      ));
      $this->_oInputFilter = $oInputFilter;
    }
    return $this->_oInputFilter;
  }
}
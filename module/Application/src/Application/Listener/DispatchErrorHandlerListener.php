<?php
namespace Application\Listener;

use Zend\Mvc\MvcEvent;

class DispatchErrorHandlerListener
{
  public function __invoke(MvcEvent $e)
  {
    if (file_exists('./logs/')) {
      $oWriterStream = new \Zend\Log\Writer\Stream('./logs/' . date('Y-m-d') . '-log.txt');
      $oLogger = new \Zend\Log\Logger;
      $oMessage = new \Zend\Mail\Message();
      $oTransport = new \Zend\Mail\Transport\Sendmail();
      $oMessage->setFrom('biuro@looksmart.com.pl', 'Łukasz Staszak');
      $oMessage->addTo('lstaszak@gmail.com', 'Hello');
      $oMail = new \Zend\Log\Writer\Mail($oMessage, $oTransport);
      $oMail->setSubjectPrependText('PHP Error');
      $oLogger->addWriter($oWriterStream);
      $oLogger->addWriter($oMail);
      $oLogger->crit($e->getParam('exception'));
    }
  }
}
<?php
namespace Application;

use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class Mail extends Smtp implements ServiceLocatorAwareInterface, EventManagerAwareInterface
{
  protected $_aOptions;
  protected $_oBody;
  protected $_oEmailContentConfigurationTable;
  protected $_oTransport;
  protected $_sEmailAddress;
  protected $_sFrom;
  protected $_sMessage;
  protected $_sSubject;
  protected $events;

  public function __construct($oServiceLocator)
  {
    $this->setServiceLocator($oServiceLocator);
    $oEmailConfiguration = $this->getEmailConfigurationTable()->getRow(array('primary' => 1));
    if (!$oEmailConfiguration) {
      throw \Exception();
    }
    $this->_oTransport = new Smtp();
    $this->_aOptions = new SmtpOptions(array(
      'host' => $oEmailConfiguration->host,
      'connection_class' => 'login',
      'connection_config' => array(
        'username' => $oEmailConfiguration->username,
        'password' => $oEmailConfiguration->password,
      ),
      'name' => $oEmailConfiguration->name,
    ));
    $this->_sFrom = $oEmailConfiguration->from;
    $this->_oTransport->setOptions($this->_aOptions);
    $this->_sEmailAddress = null;
    $this->_sSubject = null;
    $this->_sMessage = null;
    $this->_oBody = null;
  }

  protected function clearAll()
  {
    $this->_sEmailAddress = null;
    $this->_sSubject = null;
    $this->_sMessage = null;
    $this->_oBody = null;
  }

  protected function getEmailConfigurationTable()
  {
    if (!$this->_oEmailConfigurationTable && $this->getServiceLocator()->has('EmailConfigurationTableService')) {
      $this->_oEmailConfigurationTable = $this->getServiceLocator()->get('EmailConfigurationTableService');
    }
    if (!isset($this->_oEmailConfigurationTable)) {
      throw \Exception();
    }
    return $this->_oEmailConfigurationTable;
  }

  protected function getEmailContentConfigurationTable()
  {
    if (!$this->_oEmailContentConfigurationTable && $this->getServiceLocator()->has('EmailContentConfigurationTableService')) {
      $this->_oEmailContentConfigurationTable = $this->getServiceLocator()->get('EmailContentConfigurationTableService');
    }
    if (!isset($this->_oEmailContentConfigurationTable)) {
      throw \Exception();
    }
    return $this->_oEmailContentConfigurationTable;
  }

  protected function sendMessage()
  {
    $oMessage = new Message();
    $oMessage
      ->setEncoding('UTF-8')
      ->addFrom($this->_sFrom)
      ->addTo($this->_sEmailAddress)
      ->setSubject($this->_sSubject)
      ->setBody($this->_oBody);
    $oHeaders = $oMessage->getHeaders();
    $oHeaders->removeHeader('Content-Type');
    $oHeaders->addHeaderLine('Content-Type', 'text/html; charset=UTF-8');
    $this->_oTransport->send($oMessage);
    $this->clearAll();
  }

  public function fillMessage($sFunctionName, $mParam)
  {
    $oEmailContentConfiguration = $this->getEmailContentConfigurationTable()->getRow(array('trigger_action' => $sFunctionName));
    if ($oEmailContentConfiguration) {
      $this->_sSubject = $oEmailContentConfiguration->subject;
      $this->_sMessage = $oEmailContentConfiguration->message;
      $nGreetingTypeId = $oEmailContentConfiguration->email_content_configuration_greeting_type_id;
      $nConclusionTypeId = $oEmailContentConfiguration->email_content_configuration_conclusion_type_id;
      if (is_numeric($nGreetingTypeId) && $nGreetingTypeId > 0) {
        $oEmailContentConfiguration = $this->getEmailContentConfigurationTable()->getRow(array('email_content_configuration_type_id' => 1, 'id' => $nGreetingTypeId));
        $sGreetingMessage = $oEmailContentConfiguration->message;
        $this->_sMessage = $sGreetingMessage . $this->_sMessage;
      }
      if (is_numeric($nConclusionTypeId) && $nConclusionTypeId > 0) {
        $oEmailContentConfiguration = $this->getEmailContentConfigurationTable()->getRow(array('email_content_configuration_type_id' => 3, 'id' => $nConclusionTypeId));
        $sConclusionMessage = $oEmailContentConfiguration->message;
        $this->_sMessage = $this->_sMessage . $sConclusionMessage;
      }
      foreach ($mParam as $sKey => $sValue) {
        if ($sKey === 'email_address') {
          $this->_sEmailAddress = $sValue;
        }
        $this->_sSubject = str_replace("{{" . $sKey . "}}", $sValue, $this->_sSubject);
        $this->_sMessage = str_replace("{{" . $sKey . "}}", $sValue, $this->_sMessage);
      }
      if (isset($this->_sEmailAddress) && isset($this->_sSubject) && isset($this->_sMessage)) {
        $oHtml = new Mime\Part($this->_sMessage);
        $oHtml->type = 'text/html';
        $this->_oBody = new Mime\Message();
        $this->_oBody->setParts(array($oHtml));
        if (isset($this->_oBody)) {
          $this->sendMessage();
          return true;
        }
      }
    }
    return null;
  }

  public function getEventManager()
  {
    if (null === $this->events) {
      $this->setEventManager(new EventManager());
    }
    return $this->events;
  }

  public function getMessage($sFunctionName, $mParam)
  {
    $oEmailContentConfiguration = $this->getEmailContentConfigurationTable()->getRow(array('trigger_action' => $sFunctionName));
    if ($oEmailContentConfiguration) {
      $sMessage = $oEmailContentConfiguration->message;
      $nGreetingTypeId = $oEmailContentConfiguration->email_content_configuration_greeting_type_id;
      $nConclusionTypeId = $oEmailContentConfiguration->email_content_configuration_conclusion_type_id;
      if (is_numeric($nGreetingTypeId) && $nGreetingTypeId > 0) {
        $oEmailContentConfiguration = $this->getEmailContentConfigurationTable()->getRow(array('email_content_configuration_type_id' => 1, 'id' => $nGreetingTypeId));
        $sGreetingMessage = $oEmailContentConfiguration->message;
        $sMessage = $sGreetingMessage . $sMessage;
      }
      if (is_numeric($nConclusionTypeId) && $nConclusionTypeId > 0) {
        $oEmailContentConfiguration = $this->getEmailContentConfigurationTable()->getRow(array('email_content_configuration_type_id' => 3, 'id' => $nConclusionTypeId));
        $sConclusionMessage = $oEmailContentConfiguration->message;
        $sMessage = $sMessage . $sConclusionMessage;
      }
      foreach ($mParam as $sKey => $sValue) {
        $sMessage = str_replace("{{" . $sKey . "}}", $sValue, $sMessage);
      }
      if (isset($sMessage)) {
        return $sMessage;
      }
    }
    return null;
  }

  public function getServiceLocator()
  {
    return $this->_oServiceLocator;
  }

  public function setEventManager(EventManagerInterface $events)
  {
    $events->setIdentifiers(array(
      __CLASS__,
      get_called_class(),
    ));
    $this->events = $events;
    return $this;
  }

  public function setServiceLocator(ServiceLocatorInterface $oServiceLocator)
  {
    $this->_oServiceLocator = $oServiceLocator;
    return $this;
  }
}
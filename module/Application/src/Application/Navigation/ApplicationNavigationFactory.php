<?php
namespace Application\Navigation;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ApplicationNavigationFactory implements FactoryInterface
{
  public function createService(ServiceLocatorInterface $oServiceLocator)
  {
    $oNavigation = new ApplicationNavigation();
    return $oNavigation->createService($oServiceLocator);
  }
}
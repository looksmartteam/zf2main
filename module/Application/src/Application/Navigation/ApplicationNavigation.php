<?php
namespace Application\Navigation;

use Zend\Navigation\Service\DefaultNavigationFactory;
use Zend\ServiceManager\ServiceLocatorInterface;

class ApplicationNavigation extends DefaultNavigationFactory
{
  protected $_oServiceLocator;
  protected $_oTranslator;

  protected function getPages(ServiceLocatorInterface $oServiceLocator)
  {
    $this->_oServiceLocator = $oServiceLocator;
    $this->_oTranslator = $this->_oServiceLocator->get('translator');
    $oMvcEvent = $this->_oServiceLocator->get('Application')->getMvcEvent();
    $oRouteMatch = $oMvcEvent->getRouteMatch();
    $oRouter = $oMvcEvent->getRouter();
    $aNavigation = array();
    if ($this->_oServiceLocator->has('NavigationMenuTableService')) {
      $oNavigationMenuTable = $this->_oServiceLocator->get('NavigationMenuTableService');
      $oNavigationMenuConfigurationTable = $this->_oServiceLocator->get('NavigationMenuConfigurationTableService');
      $nPrimaryMenuConfigurationId = $oNavigationMenuConfigurationTable->findRow(array('primary' => 1));
      $aSort = array('sort_column' => 'order', 'sort_method' => 'asc');
      $aMenu = $oNavigationMenuTable->getAll(array('navigation_menu_configuration_id' => $nPrimaryMenuConfigurationId, 'navigation_level_id' => 1, 'sort' => $aSort));
      if (count($aMenu)) {
        foreach ($aMenu as $oMenu) {
          if ($oMenu->navigation_menu_type_id === 2 && $oMenu->navigation_module_name === 'site' && $oMenu->navigation_controller_name === 'index' && $oMenu->navigation_action_name === 'index') {
            $sRouteSpacer = '_menu';
            $aNavigation[$oMenu->id] = array(
              'id' => $oMenu->id,
              'label' => $oMenu->label_name,
              'title' => $oMenu->label_name,
              'route' => $sRoute = $oMenu->navigation_module_name . '_' . $oMenu->navigation_controller_name . $sRouteSpacer . '/' . $oMenu->navigation_module_name . '_' . $oMenu->navigation_controller_name . $sRouteSpacer . '_process',
              'action' => $oMenu->navigation_action_name,
              'resource' => $oMenu->navigation_resource_name,
              'privilege' => $oMenu->navigation_privilege_name,
              'params' => array(
                'menu' => $this->getSiteHref($oMenu->label_name),
                'format' => 'html',
              )
            );
          } else {
            $sRouteSpacer = '';
            $aNavigation[$oMenu->id] = array(
              'id' => $oMenu->id,
              'label' => $oMenu->label_name,
              'title' => $oMenu->label_name,
              'route' => $sRoute = $oMenu->navigation_module_name . '_' . $oMenu->navigation_controller_name . $sRouteSpacer . '/' . $oMenu->navigation_module_name . '_' . $oMenu->navigation_controller_name . $sRouteSpacer . '_process',
              'action' => $oMenu->navigation_action_name,
              'resource' => $oMenu->navigation_resource_name,
              'privilege' => $oMenu->navigation_privilege_name,
            );
          }
          $aSubmenu = $oNavigationMenuTable->getAll(array('navigation_parent_menu_id' => $oMenu->id, 'sort' => $aSort));
          if (count($aSubmenu)) {
            $aNavigation[$oMenu->id]['label'] .= '<b class="caret margin_left-5"></b>';
            $aNavigation[$oMenu->id]['wrap_class'] = 'dropdown-toggle'; //menu li
            $aNavigation[$oMenu->id]['attribs'] = array('data-toggle' => 'dropdown'); //menu a
            foreach ($aSubmenu as $oSubmenu) {
              if ($oSubmenu->navigation_menu_type_id === 2 && $oSubmenu->navigation_module_name === 'site' && $oSubmenu->navigation_controller_name === 'index' && $oSubmenu->navigation_action_name === 'index') {
                $sRouteSpacer = '_submenu';
                $aNavigation[$oMenu->id]['pages'][$oSubmenu->id] = array(
                  'id' => $oSubmenu->id,
                  'label' => $oSubmenu->label_name,
                  'title' => $oSubmenu->label_name,
                  'route' => $sRoute = $oSubmenu->navigation_module_name . '_' . $oSubmenu->navigation_controller_name . $sRouteSpacer . '/' . $oSubmenu->navigation_module_name . '_' . $oSubmenu->navigation_controller_name . $sRouteSpacer . '_process',
                  'action' => $oSubmenu->navigation_action_name,
                  'resource' => $oSubmenu->navigation_resource_name,
                  'privilege' => $oSubmenu->navigation_privilege_name,
                  'pages_container_class' => 'dropdown-menu',
                  'params' => array(
                    'menu' => $this->getSiteHref($oMenu->label_name),
                    'submenu' => $this->getSiteHref($oSubmenu->label_name),
                    'format' => 'html',
                  )
                );
              } else {
                $sRouteSpacer = '';
                $aNavigation[$oMenu->id]['pages'][$oSubmenu->id] = array(
                  'id' => $oSubmenu->id,
                  'label' => $oSubmenu->label_name,
                  'title' => $oSubmenu->label_name,
                  'route' => $sRoute = $oSubmenu->navigation_module_name . '_' . $oSubmenu->navigation_controller_name . $sRouteSpacer . '/' . $oSubmenu->navigation_module_name . '_' . $oSubmenu->navigation_controller_name . $sRouteSpacer . '_process',
                  'action' => $oSubmenu->navigation_action_name,
                  'resource' => $oSubmenu->navigation_resource_name,
                  'privilege' => $oSubmenu->navigation_privilege_name,
                  'pages_container_class' => 'dropdown-menu',
                );
              }
              $aSubsubmenu = $oNavigationMenuTable->getAll(array('navigation_parent_menu_id' => $oSubmenu->id, 'sort' => $aSort));
              if (count($aSubsubmenu)) {
                $aNavigation[$oMenu->id]['pages'][$oSubmenu->id]['wrap_class'] = 'dropdown-submenu'; //submenu li
                foreach ($aSubsubmenu as $oSubsubmenu) {
                  if ($oSubsubmenu->navigation_menu_type_id === 2 && $oSubsubmenu->navigation_module_name === 'site' && $oSubsubmenu->navigation_controller_name === 'index' && $oSubsubmenu->navigation_action_name === 'index') {
                    $sRouteSpacer = '_subsubmenu';
                    $aNavigation[$oMenu->id]['pages'][$oSubmenu->id]['pages'][$oSubsubmenu->id] = array(
                      'id' => $oSubsubmenu->id,
                      'label' => $oSubsubmenu->label_name,
                      'title' => $oSubsubmenu->label_name,
                      'route' => $sRoute = $oSubsubmenu->navigation_module_name . '_' . $oSubsubmenu->navigation_controller_name . $sRouteSpacer . '/' . $oSubsubmenu->navigation_module_name . '_' . $oSubsubmenu->navigation_controller_name . $sRouteSpacer . '_process',
                      'action' => $oSubsubmenu->navigation_action_name,
                      'resource' => $oSubsubmenu->navigation_resource_name,
                      'privilege' => $oSubsubmenu->navigation_privilege_name,
                      'pages_container_class' => 'dropdown-menu',
                      'params' => array(
                        'menu' => $this->getSiteHref($oMenu->label_name),
                        'submenu' => $this->getSiteHref($oSubmenu->label_name),
                        'subsubmenu' => $this->getSiteHref($oSubsubmenu->label_name),
                        'format' => 'html',
                      )
                    );
                  } else {
                    $sRouteSpacer = '';
                    $aNavigation[$oMenu->id]['pages'][$oSubmenu->id]['pages'][$oSubsubmenu->id] = array(
                      'id' => $oSubsubmenu->id,
                      'label' => $oSubsubmenu->label_name,
                      'title' => $oSubsubmenu->label_name,
                      'route' => $sRoute = $oSubsubmenu->navigation_module_name . '_' . $oSubsubmenu->navigation_controller_name . $sRouteSpacer . '/' . $oSubsubmenu->navigation_module_name . '_' . $oSubsubmenu->navigation_controller_name . $sRouteSpacer . '_process',
                      'action' => $oSubsubmenu->navigation_action_name,
                      'resource' => $oSubsubmenu->navigation_resource_name,
                      'privilege' => $oSubsubmenu->navigation_privilege_name,
                      'pages_container_class' => 'dropdown-menu',
                    );
                  }
                }
              }
            }
          }
        }
      }
    }
    if (null === $this->pages) {
      $oPages = $this->getPagesFromConfig($aNavigation);
      $this->pages = $this->injectComponents(
        $oPages,
        $oRouteMatch,
        $oRouter
      );
    }
    return $this->pages;
  }

  public function getSiteHref($sLabel)
  {
    $sNewLabel = '';
    $sTempLabel = trim(strtolower($sLabel));
    $sTempLabel = explode(' ', $sTempLabel);
    foreach ($sTempLabel as $nKey => $sValue) {
      if ($nKey)
        $sNewLabel .= '-' . $sValue;
      else
        $sNewLabel .= $sValue;
    }
    return $this->removeAccents($sNewLabel);
  }

  public function removeAccents($sStr)
  {
    $aPolishChars = array('ą', 'ć', 'ę', 'ł', 'ń', 'ó', 'ś', 'ż', 'ź');
    $aReplace = array('a', 'c', 'e', 'l', 'n', 'o', 's', 'z', 'z');
    return str_replace($aPolishChars, $aReplace, $sStr);
  }
}
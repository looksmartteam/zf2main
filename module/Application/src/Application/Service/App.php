<?php
namespace Application\Service;

use Doctrine\ORM\EntityManager;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
use Zend\ServiceManager\ServiceLocatorInterface;
use Firebase\FirebaseLib;
const DEFAULT_URL = 'https://beelab.firebaseio.com/';
const DEFAULT_TOKEN = 'wiLL0ANl1zLthh7YMFGx74SKUvvfRttnUDm4Y03V';
const DEFAULT_PATH = '/test';
class App
{
  protected $_oEntityManager;
  protected $_oServiceLocator;

  public function __construct($oServiceLocator)
  {
    $this->setServiceLocator($oServiceLocator);
  }

  protected function getServiceLocator()
  {
    if (null === $this->_oServiceLocator) {
      throw new \Exception();
    }
    return $this->_oServiceLocator;
  }

  protected function setEntityManager(EntityManager $oEntityManager)
  {
    $this->_oEntityManager = $oEntityManager;
    return $this;
  }

  protected function setServiceLocator(ServiceLocatorInterface $oServiceLocator)
  {
    $this->_oServiceLocator = $oServiceLocator;
    return $this;
  }

  public function dump($aArr)
  {
    $oFirebase = new FirebaseLib(DEFAULT_URL, DEFAULT_TOKEN);
    $aPush = array('id' => time(), 'data' => $aArr);
    $oFirebase->push(DEFAULT_PATH, $aPush);
  }

  public function getEntity($sName)
  {
    $oEntity = null;
    if (null !== $sName) {
      $sName = $sName . 'DoctrineEntityService';
      if ($this->getServiceLocator()->has($sName)) {
        $oEntity = $this->getServiceLocator()->get($sName);
      }
    }
    return $oEntity;
  }

  public function getEntityManager()
  {
    if (null === $this->_oEntityManager) {
      $this->setEntityManager($this->getServiceLocator()->get('\Doctrine\ORM\EntityManager'));
    }
    return $this->_oEntityManager;
  }

  public function getNativEntity($sName)
  {
    $oEntity = null;
    if (null !== $sName) {
      $sName = $sName . 'EntityService';
      if ($this->getServiceLocator()->has($sName)) {
        $oEntity = $this->getServiceLocator()->get($sName);
      }
    }
    return $oEntity;
  }

  public function getRepository($sName)
  {
    $oRepository = null;
    if (null === $this->_oEntityManager) {
      $this->setEntityManager($this->getServiceLocator()->get('\Doctrine\ORM\EntityManager'));
    }
    if (null !== $sName) {
      $sName = 'Application\Entity\\' . $sName;
      $oRepository = $this->getEntityManager()->getRepository($sName);
    }
    return $oRepository;
  }

  public function toArray($oObject)
  {
    $oSerializer = SerializerBuilder::create()->build();
    $aArray = $oSerializer->toArray($oObject, SerializationContext::create()->enableMaxDepthChecks());
    return $aArray;
  }

  public function toJson($oObject)
  {
    $oSerializer = SerializerBuilder::create()->build();
    $sJson = $oSerializer->serialize($oObject, 'json');
    return $sJson;
  }

  public function toXml($oObject)
  {
    $oSerializer = SerializerBuilder::create()->build();
    $sXml = $oSerializer->serialize($oObject, 'xml');
    return $sXml;
  }
}
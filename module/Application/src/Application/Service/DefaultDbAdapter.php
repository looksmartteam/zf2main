<?php
namespace Application\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class DefaultDbAdapter implements FactoryInterface
{
  public function createService(ServiceLocatorInterface $oServiceLocator)
  {
    $aConfig = $oServiceLocator->get('config');
    if (isset($aConfig['main_db'])) {
      if ($oServiceLocator->has($aConfig['main_db']) && $aConfig['main_db'] === 'db1') {
        $oDbAdapter = $oServiceLocator->get($aConfig['main_db']);
      } else {
        $oDbAdapter = $oServiceLocator->get('Zend\Db\Adapter\Adapter');
      }
    } else {
      $oDbAdapter = $oServiceLocator->get('Zend\Db\Adapter\Adapter');
    }
    return $oDbAdapter;
  }
}

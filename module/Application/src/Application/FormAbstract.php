<?php
namespace Application;

use Zend\Form\Form;
use Zend\ServiceManager\ServiceLocatorInterface;

abstract class FormAbstract extends Form
{
  protected $_aTabsLabel;
  protected $_bInitInputFilter;
  protected $_oServiceLocator;
  protected $_sAuthColumn;
  protected $_sLoginLabel;

  public function __construct($oServiceLocator = null, $sFormName = null)
  {
    parent::__construct($sFormName);
    if (null !== $oServiceLocator) {
      $this->setServiceLocator($oServiceLocator);
      $this->setDefaultConfig();
      $this->init();
      $this->initInputFilter();
    }
  }

  protected function setDefaultConfig()
  {
    $aConfig = $this->getServiceLocator()->get('config');
    $this->_sAuthColumn = $aConfig['authorization']['auth_column'];
    switch ($this->_sAuthColumn) {
      case 'email_address':
        $this->_sLoginLabel = 'Adres e-mail';
        break;
      case 'nick':
        $this->_sLoginLabel = 'Nazwa użytkownika';
        break;
      default:
        $this->_sAuthColumn = 'email_address';
        $this->_sLoginLabel = 'Adres e-mail';
    }
    $this->add(array(
      'type' => 'Csrf',
      'name' => 'csrf',
      'options' => array(
        'csrf_options' => array(
          'timeout' => 3600
        ))
    ));
    $this->add(array(
      'type' => 'Hidden',
      'name' => 'form_name',
      'attributes' => array(
        'value' => $this->getName(),
      ),
    ));
    $this->add(array(
      'type' => 'Hidden',
      'name' => 'id',
    ));
  }

  public function getFormTabs()
  {
    $aTabs = array();
    if (is_array($this->_aTabsLabel) && count($this->_aTabsLabel)) {
      foreach ($this->_aTabsLabel as $sTabId => $sTabLabel) {
        $aTabs[$sTabId] = array();
      }
      foreach ($this->getElements() as $oElement) {
        if (!in_array($oElement->getName(), array('csrf', 'form_name', 'id', 'submit_button'))) {
          $sTabId = $oElement->getOption('tab_id');
          if (isset($sTabId)) {
            if (!isset($aTabs[$sTabId]))
              $aTabs[$sTabId] = array();
            if (!isset($aTabs[$sTabId]['label']))
              $aTabs[$sTabId]['label'] = $this->_aTabsLabel[$sTabId];
            $aTabs[$sTabId][] = $oElement->getName();
          } else {
            if (!isset($aTabs['other']['label']) && isset($this->_aTabsLabel[$sTabId]))
              $aTabs['other']['label'] = $this->_aTabsLabel[$sTabId];
            else
              $aTabs['other']['label'] = 'Inne';
            $aTabs['other'][] = $oElement->getName();
          }
        }
      }
      if (isset($aTabs['other']) && !count($aTabs['other'])) {
        unset($aTabs['other']);
      }
    }
    return $aTabs;
  }

  public function getInitInputFilter()
  {
    return $this->_bInitInputFilter;
  }

  public function getServiceLocator()
  {
    return $this->_oServiceLocator;
  }

  public function initInputFilter()
  {
    $sFormName = $this->getName();
    $aClassName = explode('_', $sFormName);
    if (is_array($aClassName) && count($aClassName)) {
      $sClassName = '';
      foreach ($aClassName as $nKey => $sValue) {
        $sClassName .= ucfirst($sValue);
      }
    } else {
      $sClassName = ucfirst($sFormName);
    }
    $sInputFilterServiceName = $sClassName . 'InputFilterService';
    if ($this->getServiceLocator()->has($sInputFilterServiceName)) {
      $this->setInputFilter($this->getServiceLocator()->get($sInputFilterServiceName)->getInputFilter());
      $this->_bInitInputFilter = true;
    }
  }

  public function resetForm()
  {
    $oElements = $this->getElements();
    foreach ($oElements as $oElement) {
      if ($oElement instanceof \Zend\Form\Element\Text) {
        $oElement->setValue('');
      } else if ($oElement instanceof \Zend\Form\Element\Textarea) {
        $oElement->setValue('');
      } else if ($oElement instanceof \Zend\Form\Element\Select) {
        $oElement->setEmptyOption('');
        $oElement->setValue('');
      }
    }
  }

  public function setServiceLocator(ServiceLocatorInterface $oServiceLocator)
  {
    $this->_oServiceLocator = $oServiceLocator;
    return $this;
  }
}

<?php
namespace Application;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

abstract class InputFilterAbstract implements InputFilterAwareInterface
{
  protected $_oDbAdapter;
  protected $_oInputFilter;
  protected $_oServiceLocator;
  protected $_sAuthColumn;

  public function __construct($oServiceLocator)
  {
    $this->setServiceLocator($oServiceLocator);
    $this->setDefaultConfig();
    $this->init();
  }

  public function __get($sName)
  {
    if (!property_exists($this, $sName)) {
      return null;
    }
    return $this->$sName;
  }

  public function __set($sName, $sValue)
  {
    if (!property_exists($this, $sName)) {
      return null;
    }
    return $this->$sName = $sValue;
  }

  public function exchangeArray($aData)
  {
    foreach ($aData as $sKey => $mValue) {
      $this->$sKey = $mValue;
    }
  }

  public function getInputFilter()
  {
    throw new \Exception("Not used");
  }

  public function getServiceLocator()
  {
    return $this->_oServiceLocator;
  }

  public function init()
  {
    $this->_oDbAdapter = $this->getServiceLocator()->get('DefaultDbAdapter');
  }

  public function setDefaultConfig()
  {
    $aConfig = $this->getServiceLocator()->get('config');
    $this->_sAuthColumn = $aConfig['authorization']['auth_column'];
  }

  public function setInputFilter(InputFilterInterface $oInputFilter)
  {
    throw new \Exception("Not used");
  }

  public function setServiceLocator(ServiceLocatorInterface $oServiceLocator)
  {
    $this->_oServiceLocator = $oServiceLocator;
    return $this;
  }
}

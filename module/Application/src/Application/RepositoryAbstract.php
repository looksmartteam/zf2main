<?php
//var_dump($oSelect->getSqlString($oThat->adapter->getPlatform()));
namespace Application;

use Zend\Db\Metadata\Metadata as Metadata;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\AbstractTableGateway;

abstract class RepositoryAbstract extends AbstractTableGateway
{
  protected $_oColumns;
  protected $_oServiceLocator;
  protected $_sAuthColumn;
  protected $metadata;

  public function __construct($oAdapter)
  {
    $this->adapter = $oAdapter;
    $this->metadata = new Metadata($this->adapter);
    $this->_oColumns = $this->metadata->getColumns($this->table);
  }

  public function addRow($aData)
  {
    $nAffectedRows = $this->insert($aData);
    if (isset($nAffectedRows)) {
      if ($nAffectedRows === 1) {
        return (int)$this->getLastInsertValue();
      } else if ($nAffectedRows) {
        return $nAffectedRows;
      }
    }
    return null;
  }

  public function beginTransaction()
  {
    $this->getAdapter()->getDriver()->getConnection()->beginTransaction();
  }

  public function commit()
  {
    $this->getAdapter()->getDriver()->getConnection()->commit();
  }

  public function deleteRow($mData)
  {
    if (is_numeric($mData)) {
      $aWhere = array('id' => $mData);
    } else if (is_array($mData)) {
      $aWhere = $mData;
    }
    if (is_array($aWhere)) {
      $nAffectedRows = $this->delete($aWhere);
      if (isset($nAffectedRows)) {
        return $nAffectedRows;
      }
    }
    return null;
  }

  public function editRow($aData)
  {
    if (is_numeric($aData['id'])) {
      return $this->update($aData, array('id' => $aData['id']));
    } else {
      return $this->update($aData);
    }
  }

  public function findRow(array $aData = null)
  {
    $oThat = $this;
    $oResultSet = $this->select(function (Select $oSelect) use ($aData, $oThat) {
      if ($aData) {
        foreach ($aData as $sKey => $mValue) {
          $oSelect->where(array($sKey => $mValue));
        }
      }
      $oSelect->limit(1);
    });
    $aResult = $this->getEntities($oResultSet);
    if (count($aResult) === 1)
      return $aResult[0]->id;
    return null;
  }

  public function getAll(array $aData = null)
  {
    $oResultSet = $this->select(function (Select $oSelect) use ($aData) {
      if ($aData) {
        foreach ($aData as $sKey => $mValue) {
          $oSelect->where(array($sKey => $mValue));
        }
      }
    });
    return $this->getEntities($oResultSet);
  }

  public function getColumnDataType($sColumnName)
  {
    foreach ($this->_oColumns as $oColumn) {
      if ($oColumn->getName() === $sColumnName) {
        return $oColumn->getDataType();
      }
    }
  }

  public function getEntities(ResultSet $oResultSet)
  {
    $aEntities = array();
    if ($oResultSet->count()) {
      foreach ($oResultSet as $oRow) {
        foreach ($oRow as $sKey => $sValue) {
          $oRow->$sKey = $this->getTranslatedValue($sKey, $sValue);
        }
        $oEntity = $this->createNewEntity();
        $oEntity->getOptions($oRow);
        $aEntities[] = $oEntity;
      }
    }
    return $aEntities;
  }

  public function getRow(array $aData = null)
  {
    $oThat = $this;
    $oResultSet = $this->select(function (Select $oSelect) use ($aData, $oThat) {
      if ($aData) {
        foreach ($aData as $sKey => $mValue) {
          $oSelect->where(array($sKey => $mValue));
        }
      }
      $oSelect->limit(1);
    });
    $aResult = $this->getEntities($oResultSet);
    if (count($aResult) === 1)
      return $aResult[0];
    return null;
  }

  public function getTranslatedValue($sColumnName, $sValue)
  {
    $sDataType = $this->getColumnDataType($sColumnName);
    if (in_array($sDataType, array('int', 'tinyint'))) {
      return (int)$sValue;
    }
    if (in_array($sDataType, array('float'))) {
      return round((float)$sValue, 2);
    }
    return $sValue;
  }

  public function recordExists($sColumn, $sValue, $nId)
  {
    $oThat = $this;
    $oResultSet = $this->select(function (Select $oSelect) use ($oThat, $sColumn, $sValue, $nId) {
      $oSelect->where(array($sColumn => $sValue));
      $oSelect->where(array('id <> ?' => $nId));
    });
    return $oResultSet->count();
  }

  public function rollBack()
  {
    $this->getAdapter()->getDriver()->getConnection()->rollback();
  }

  public function setEntities(ResultSet $oResultSet)
  {
    $aEntities = array();
    if ($oResultSet->count()) {
      foreach ($oResultSet as $oRow) {
        foreach ($oRow as $sKey => $sValue) {
          $oRow->$sKey = $this->getTranslatedValue($sKey, $sValue);
        }
        $oEntity = $this->createNewEntity();
        $oEntity->setOptions($oRow);
        $aEntities[] = $oEntity;
      }
    }
    return $aEntities;
  }
}

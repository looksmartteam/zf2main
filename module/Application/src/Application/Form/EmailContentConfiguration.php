<?php
namespace Application\Form;

use Application\FormAbstract;

class EmailContentConfiguration extends FormAbstract
{
  public function __construct($oServiceLocator, $sFormName)
  {
    parent::__construct($oServiceLocator, $sFormName);
  }

  public function init()
  {
    if ($this->getServiceLocator()->has('EmailContentConfigurationTypeTableService')) {
      $oEmailContentConfigurationTypeTable = $this->getServiceLocator()->get('EmailContentConfigurationTypeTableService');
      $oEmailContentConfigurationTable = $this->getServiceLocator()->get('EmailContentConfigurationTableService');
      $oEmailContentConfigurationType = $oEmailContentConfigurationTypeTable->getAll();
      $oEmailContentConfigurationGreetingType = $oEmailContentConfigurationTable->getAll(array('email_content_configuration_type_id' => 1));
      $oEmailContentConfigurationConclusionType = $oEmailContentConfigurationTable->getAll(array('email_content_configuration_type_id' => 3));
    }
    $aEmailContentConfigurationType = array();
    $aEmailContentConfigurationGreetingType = array();
    $aEmailContentConfigurationConclusionType = array();
    if (isset($oEmailContentConfigurationType)) {
      foreach ($oEmailContentConfigurationType as $oValue) {
        $aEmailContentConfigurationType[$oValue->id] = $oValue->name;
      }
    }
    if (isset($oEmailContentConfigurationGreetingType)) {
      foreach ($oEmailContentConfigurationGreetingType as $oValue) {
        $aEmailContentConfigurationGreetingType[$oValue->id] = $oValue->trigger_action;
      }
    }
    if (isset($oEmailContentConfigurationConclusionType)) {
      foreach ($oEmailContentConfigurationConclusionType as $oValue) {
        $aEmailContentConfigurationConclusionType[$oValue->id] = $oValue->trigger_action;
      }
    }
    $this->add(array(
      'type' => 'Select',
      'name' => 'email_content_configuration_type_id',
      'options' => array(
        'label' => 'Typ sekcji',
        'value_options' => $aEmailContentConfigurationType,
        'empty_option' => '',
        'tab_id' => 'config',
      ),
    ));
    $this->add(array(
      'type' => 'Text',
      'name' => 'trigger_action',
      'options' => array(
        'label' => 'Nazwa akcji',
      ),
    ));
    $this->add(array(
      'type' => 'Textarea',
      'name' => 'subject',
      'options' => array(
        'label' => 'Temat wiadomości',
      ),
    ));
    $this->add(array(
      'type' => 'Select',
      'name' => 'email_content_configuration_greeting_type_id',
      'options' => array(
        'label' => 'Powitanie',
        'value_options' => $aEmailContentConfigurationGreetingType,
        'empty_option' => '',
        'tab_id' => 'config',
      ),
    ));
    $this->add(array(
      'type' => 'Textarea',
      'name' => 'message',
      'options' => array(
        'label' => 'Wiadomość',
        'class' => 'ckeditor',
      ),
    ));
    $this->add(array(
      'type' => 'Select',
      'name' => 'email_content_configuration_conclusion_type_id',
      'options' => array(
        'label' => 'Zakończenie',
        'value_options' => $aEmailContentConfigurationConclusionType,
        'empty_option' => '',
        'tab_id' => 'config',
      ),
    ));
    $this->add(array(
      'type' => 'Checkbox',
      'name' => 'active',
      'options' => array(
        'label' => 'Treść aktywna',
      ),
    ));
    $this->add(array(
      'type' => 'Button',
      'name' => 'submit_button',
      'options' => array(
        'label' => 'Zapisz',
      ),
    ));
  }

  /**
   * Set a single option for an element
   *
   * @param  string $key
   * @param  mixed $value
   * @return self
   */
  public function setOption($key, $value)
  {
    // TODO: Implement setOption() method.
  }
}
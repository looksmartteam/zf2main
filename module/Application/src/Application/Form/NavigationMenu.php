<?php
namespace Application\Form;

use Application\FormAbstract;

class NavigationMenu extends FormAbstract
{
  public function __construct($oServiceLocator, $sFormName)
  {
    parent::__construct($oServiceLocator, $sFormName);
  }

  public function init()
  {
    $this->_aTabsLabel = array(
      'label' => 'Etykieta',
      'config' => 'Konfiguracja',
      'seo' => 'SEO',
    );
    if ($this->getServiceLocator()->has('NavigationMenuConfigurationTableService')) {
      $oNavigationMenuConfigurationTable = $this->getServiceLocator()->get('NavigationMenuConfigurationTableService');
      $oMenuConfiguration = $oNavigationMenuConfigurationTable->getAll();
    }
    if ($this->getServiceLocator()->has('NavigationLevelTableService')) {
      $oNavigationLevelTable = $this->getServiceLocator()->get('NavigationLevelTableService');
      $oLevels = $oNavigationLevelTable->getAll();
    }
    if ($this->getServiceLocator()->has('NavigationModuleTableService')) {
      $oNavigationModuleTable = $this->getServiceLocator()->get('NavigationModuleTableService');
      $oModules = $oNavigationModuleTable->getAll();
    }
    if ($this->getServiceLocator()->has('NavigationControllerTableService')) {
      $oNavigationControllerTable = $this->getServiceLocator()->get('NavigationControllerTableService');
      $oControllers = $oNavigationControllerTable->getAll();
    }
    if ($this->getServiceLocator()->has('NavigationActionTableService')) {
      $oNavigationActionTable = $this->getServiceLocator()->get('NavigationActionTableService');
      $oActions = $oNavigationActionTable->getAll();
    }
    if ($this->getServiceLocator()->has('UserRoleTableService')) {
      $oUserRoleTable = $this->getServiceLocator()->get('UserRoleTableService');
      $oUserRoles = $oUserRoleTable->getAll();
    }
    if ($this->getServiceLocator()->has('NavigationMenuTableService')) {
      $oNavigationMenuTable = $this->getServiceLocator()->get('NavigationMenuTableService');
      $oMenu = $oNavigationMenuTable->getAll(array('navigation_menu_configuration_id' => 1, 'navigation_level_id' => 1));
      $oSubmenu = $oNavigationMenuTable->getAll(array('navigation_menu_configuration_id' => 1, 'navigation_level_id' => 2));
    }
    if ($this->getServiceLocator()->has('NavigationMenuTypeTableService')) {
      $oNavigationMenuTypeTable = $this->getServiceLocator()->get('NavigationMenuTypeTableService');
      $oMenuType = $oNavigationMenuTypeTable->getAll();
    }
    if ($this->getServiceLocator()->has('NavigationLayoutTableService')) {
      $oNavigationLayoutTable = $this->getServiceLocator()->get('NavigationLayoutTableService');
      $oLayout = $oNavigationLayoutTable->getAll();
    }
    if ($this->getServiceLocator()->has('SiteSeoRobotsTableService')) {
      $oSiteSeoRobotsTable = $this->getServiceLocator()->get('SiteSeoRobotsTableService');
      $oSiteSeoRobots = $oSiteSeoRobotsTable->getAll();
    }
    if ($this->getServiceLocator()->has('SiteSeoMetaKeywordsTableService')) {
      $oSiteSeoMetaKeywordsTable = $this->getServiceLocator()->get('SiteSeoMetaKeywordsTableService');
      $oSiteSeoMetaKeywords = $oSiteSeoMetaKeywordsTable->getAll();
    }
    $aMenuConfigurations = array();
    $aLevels = array();
    $aModules = array();
    $aControllers = array();
    $aActions = array();
    $aUserRoles = array();
    $aMenu = array();
    $aSubmenu = array();
    $aMenuType = array();
    $aLayout = array();
    $aSiteSeoRobots = array();
    $aSiteSeoMetaKeywords = array();
    if (isset($oMenuConfiguration)) {
      foreach ($oMenuConfiguration as $oValue) {
        $aMenuConfigurations[$oValue->id] = $oValue->configuration_name;
      }
    }
    if (isset($oLevels)) {
      foreach ($oLevels as $oValue) {
        $aLevels[$oValue->id] = $oValue->level . ' - ' . $oValue->name;
      }
    }
    if (isset($oModules)) {
      foreach ($oModules as $oValue) {
        $aModules[$oValue->id] = $oValue->value;
      }
    }
    if (isset($oControllers)) {
      foreach ($oControllers as $oValue) {
        $aControllers[$oValue->id] = $oValue->value;
      }
    }
    if (isset($oActions)) {
      foreach ($oActions as $oValue) {
        $aActions[$oValue->id] = $oValue->value;
      }
    }
    if (isset($oUserRoles)) {
      foreach ($oUserRoles as $oValue) {
        $aUserRoles[$oValue->id] = $oValue->role_name;
      }
    }
    if (isset($oMenu)) {
      foreach ($oMenu as $oValue) {
        $aMenu[$oValue->id] = $oValue->label_name;
      }
    }
    if (isset($oSubmenu)) {
      foreach ($oSubmenu as $oValue) {
        $aSubmenu[$oValue->id] = $oValue->label_name;
      }
    }
    if (isset($oMenuType)) {
      foreach ($oMenuType as $oValue) {
        $aMenuType[$oValue->id] = $oValue->value;
      }
    }
    if (isset($oLayout)) {
      foreach ($oLayout as $oValue) {
        $aLayout[$oValue->id] = $oValue->name;
      }
    }
    if (isset($oSiteSeoRobots)) {
      foreach ($oSiteSeoRobots as $oValue) {
        $aSiteSeoRobots[$oValue->id] = $oValue->description;
      }
    }
    if (isset($oSiteSeoMetaKeywords)) {
      foreach ($oSiteSeoMetaKeywords as $oValue) {
        $aSiteSeoMetaKeywords[$oValue->id] = $oValue->value;
      }
    }
    $this->add(array(
      'type' => 'Select',
      'name' => 'navigation_menu_configuration_id',
      'options' => array(
        'label' => 'Konfiguracja menu',
        'value_options' => $aMenuConfigurations,
        'empty_option' => '',
        'tab_id' => 'config',
      ),
    ));
    $this->add(array(
      'type' => 'Select',
      'name' => 'navigation_menu_type_id',
      'options' => array(
        'label' => 'Typ menu',
        'value_options' => $aMenuType,
        'empty_option' => '',
        'tab_id' => 'config',
      ),
    ));
    $this->add(array(
      'type' => 'Select',
      'name' => 'navigation_layout_id',
      'options' => array(
        'label' => 'Layout',
        'value_options' => $aLayout,
        'empty_option' => '',
        'tab_id' => 'config',
      ),
    ));
    $this->add(array(
      'type' => 'Select',
      'name' => 'active',
      'options' => array(
        'label' => 'Widoczne w menu',
        'value_options' => array(1 => 'Tak', 0 => 'Nie'),
        'empty_option' => '',
        'tab_id' => 'config',
      ),
    ));
    $this->add(array(
      'type' => 'Select',
      'name' => 'navigation_level_id',
      'options' => array(
        'label' => 'Poziom',
        'value_options' => $aLevels,
        'empty_option' => '',
        'tab_id' => 'config',
      ),
    ));
    $this->add(array(
      'type' => 'Select',
      'name' => 'navigation_parent_menu_id',
      'options' => array(
        'label' => 'Rodzic (menu nadrzędne)',
        'value_options' => $aMenu,
        'empty_option' => '',
        'tab_id' => 'config',
      ),
    ));
    $this->add(array(
      'type' => 'Select',
      'name' => 'navigation_parent_submenu_id',
      'options' => array(
        'label' => 'Rodzic (submenu nadrzędne)',
        'value_options' => $aSubmenu,
        'empty_option' => '',
        'tab_id' => 'config',
      ),
    ));
    $this->add(array(
      'type' => 'Text',
      'name' => 'label_name',
      'options' => array(
        'label' => 'Etykieta nawigacji',
        'tab_id' => 'label',
      ),
    ));
    $this->add(array(
      'type' => 'Textarea',
      'name' => 'description',
      'options' => array(
        'label' => 'Opis',
        'tab_id' => 'label',
      ),
    ));
    $this->add(array(
      'type' => 'Select',
      'name' => 'navigation_module_id',
      'options' => array(
        'label' => 'Moduł',
        'value_options' => $aModules,
        //'empty_option' => '',
        'tab_id' => 'config',
      )
    ));
    $this->add(array(
      'type' => 'Select',
      'name' => 'navigation_controller_id',
      'options' => array(
        'label' => 'Kontroler',
        'value_options' => $aControllers,
        //'empty_option' => '',
        'tab_id' => 'config',
      )
    ));
    $this->add(array(
      'type' => 'Select',
      'name' => 'navigation_action_id',
      'options' => array(
        'label' => 'Akcja',
        'value_options' => $aActions,
        //'empty_option' => '',
        'tab_id' => 'config',
      )
    ));
    $this->add(array(
      'type' => 'Select',
      'name' => 'user_role_id',
      'options' => array(
        'label' => 'Dostęp do zasobu dla grupy użytkowników',
        'value_options' => $aUserRoles,
        'tab_id' => 'config',
      ),
      'attributes' => array(
        'multiple' => 'multiple',
      ),
    ));
    $this->add(array(
      'type' => 'Select',
      'name' => 'site_seo_robots_id',
      'options' => array(
        'label' => 'Tryb indeksowania przez roboty',
        'value_options' => $aSiteSeoRobots,
        'tab_id' => 'seo',
      ),
    ));
    $this->add(array(
      'type' => 'Textarea',
      'name' => 'head_title',
      'options' => array(
        'label' => 'Tytuł strony',
        'tab_id' => 'seo',
      ),
    ));
    $this->add(array(
      'type' => 'Select',
      'name' => 'site_seo_meta_keywords_id',
      'options' => array(
        'label' => 'Słowa kluczowe',
        'value_options' => $aSiteSeoMetaKeywords,
        'tab_id' => 'seo',
      ),
      'attributes' => array(
        'multiple' => 'multiple',
      ),
    ));
    $this->add(array(
      'type' => 'Textarea',
      'name' => 'meta_description',
      'options' => array(
        'label' => 'Opis',
        'tab_id' => 'seo',
      ),
    ));
    $this->add(array(
      'type' => 'Button',
      'name' => 'submit_button',
      'options' => array(
        'label' => 'Zapisz',
      ),
    ));
  }

  /**
   * Set a single option for an element
   *
   * @param  string $key
   * @param  mixed $value
   * @return self
   */
  public function setOption($key, $value)
  {
    // TODO: Implement setOption() method.
  }
}
<?php
namespace Application\Form;

use Application\FormAbstract;
use Zend\Form\Element;

class ChangeMenuConfiguration extends FormAbstract
{
  public function __construct($oServiceLocator, $sFormName)
  {
    parent::__construct($oServiceLocator, $sFormName);
  }

  public function init()
  {
    if ($this->getServiceLocator()->has('NavigationModuleTableService')) {
      $oNavigationModuleTable = $this->getServiceLocator()->get('NavigationModuleTableService');
      $oModules = $oNavigationModuleTable->getAll();
    }
    $aModules = array();
    if (isset($oModules)) {
      foreach ($oModules as $oValue) {
        $aModules[$oValue->id] = $oValue->value;
      }
    }
    $oNavigationMenuConfigurationTable = $this->getServiceLocator()->get('NavigationMenuConfigurationTableService');
    $oMenuConfiguration = $oNavigationMenuConfigurationTable->getAll();
    $aMenuConfigurations = array();
    if (isset($oMenuConfiguration)) {
      foreach ($oMenuConfiguration as $oValue) {
        $aMenuConfigurations[$oValue->id] = $oValue->configuration_name;
      }
    }
    $this->add(array(
      'type' => 'Select',
      'name' => 'navigation_menu_configuration_id',
      'options' => array(
        'label' => 'Konfiguracja menu',
        'value_options' => $aMenuConfigurations,
        'empty_option' => '',
      ),
    ));
    $this->add(array(
      'type' => 'Select',
      'name' => 'navigation_module_id',
      'options' => array(
        'label' => 'Moduł',
        'value_options' => $aModules,
        'empty_option' => '',
      )
    ));
    $this->add(array(
      'type' => 'Button',
      'name' => 'submit_button',
      'options' => array(
        'label' => 'Zapisz',
      ),
    ));
  }

  /**
   * Set a single option for an element
   *
   * @param  string $key
   * @param  mixed $value
   * @return self
   */
  public function setOption($key, $value)
  {
    // TODO: Implement setOption() method.
  }
}
<?php
namespace Application\Form;

use Application\FormAbstract;

class SiteSeoMetaKeywords extends FormAbstract
{
  public function __construct($oServiceLocator, $sFormName)
  {
    parent::__construct($oServiceLocator, $sFormName);
  }

  public function init()
  {
    $this->add(array(
      'type' => 'Text',
      'name' => 'value',
      'options' => array(
        'label' => 'Słowo kluczowe',
      ),
    ));
    $this->add(array(
      'type' => 'Select',
      'name' => 'active',
      'options' => array(
        'label' => 'Widoczne',
        'value_options' => array(1 => 'Tak', 0 => 'Nie'),
      ),
    ));
    $this->add(array(
      'type' => 'Checkbox',
      'name' => 'primary',
      'options' => array(
        'label' => 'Słowo kluczowe główne',
      ),
    ));
    $this->add(array(
      'type' => 'Button',
      'name' => 'submit_button',
      'options' => array(
        'label' => 'Zapisz',
      ),
    ));
  }

  /**
   * Set a single option for an element
   *
   * @param  string $key
   * @param  mixed $value
   * @return self
   */
  public function setOption($key, $value)
  {
    // TODO: Implement setOption() method.
  }
}

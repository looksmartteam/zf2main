<?php
namespace Application\Form;

use Application\FormAbstract;

class EmailConfiguration extends FormAbstract
{
  public function __construct($oServiceLocator, $sFormName)
  {
    parent::__construct($oServiceLocator, $sFormName);
  }

  public function init()
  {
    $this->add(array(
      'type' => 'Text',
      'name' => 'host',
      'options' => array(
        'label' => 'Host',
      ),
    ));
    $this->add(array(
      'type' => 'Text',
      'name' => 'username',
      'options' => array(
        'label' => 'Nazwa użytkownika',
      ),
    ));
    $this->add(array(
      'type' => 'Text',
      'name' => 'password',
      'options' => array(
        'label' => 'Hasło',
      ),
    ));
    $this->add(array(
      'type' => 'Text',
      'name' => 'name',
      'options' => array(
        'label' => 'Nazwa',
        'modal_title_popover' => 'W kliencie pocztowym pole "Wysłany przez"',
      ),
    ));
    $this->add(array(
      'type' => 'Text',
      'name' => 'from',
      'options' => array(
        'label' => 'Adres e-mail',
        'modal_title_popover' => 'W kliencie pocztowym opcja "Odpowiedz"',
      ),
    ));
    $this->add(array(
      'type' => 'Checkbox',
      'name' => 'primary',
      'options' => array(
        'label' => 'Konfiguracja bieżąca',
      ),
    ));
    $this->add(array(
      'type' => 'Button',
      'name' => 'submit_button',
      'options' => array(
        'label' => 'Zapisz',
      ),
    ));
  }

  /**
   * Set a single option for an element
   *
   * @param  string $key
   * @param  mixed $value
   * @return self
   */
  public function setOption($key, $value)
  {
    // TODO: Implement setOption() method.
  }
}
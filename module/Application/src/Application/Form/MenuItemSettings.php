<?php
namespace Application\Form;

use Application\FormAbstract;

class MenuItemSettings extends FormAbstract
{
  public function __construct($oServiceLocator, $sFormName)
  {
    parent::__construct($oServiceLocator, $sFormName);
  }

  public function init()
  {
    if ($oUserRoleTable = $this->getServiceLocator()->has('UserRoleTableService')) {
      $oUserRoleTable = $this->getServiceLocator()->get('UserRoleTableService');
      $oUserRole = $oUserRoleTable->getAll();
    }
    if (isset($oUserRole)) {
      foreach ($oUserRole as $oValue) {
        $aUserRole[$oValue->id] = $oValue->role_name;
      }
    }
    $this->add(array(
      'type' => 'Select',
      'name' => 'active',
      'options' => array(
        'label' => 'Widoczne w menu',
        'value_options' => array(1 => 'Tak', 0 => 'Nie'),
      ),
    ));
    $this->add(array(
      'type' => 'Text',
      'name' => 'label_name',
      'options' => array(
        'label' => 'Etykieta nawigacji',
      ),
    ));
    $this->add(array(
      'type' => 'Textarea',
      'name' => 'description',
      'options' => array(
        'label' => 'Opis',
      ),
    ));
    $this->add(array(
      'type' => 'Select',
      'name' => 'user_role_id',
      'options' => array(
        'label' => 'Dostęp do zasobu dla grupy użytkowników',
        'value_options' => $aUserRole,
      ),
      'attributes' => array(
        'multiple' => 'multiple',
      ),
    ));
    $this->add(array(
      'type' => 'Button',
      'name' => 'submit_button',
      'options' => array(
        'label' => 'Zapisz',
      ),
    ));
  }

  /**
   * Set a single option for an element
   *
   * @param  string $key
   * @param  mixed $value
   * @return self
   */
  public function setOption($key, $value)
  {
    // TODO: Implement setOption() method.
  }
}

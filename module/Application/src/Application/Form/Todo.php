<?php
namespace Application\Form;

use Application\FormAbstract;

class Todo extends FormAbstract
{
  public function __construct($oServiceLocator, $sFormName)
  {
    parent::__construct($oServiceLocator, $sFormName);
  }

  public function init()
  {
    if ($this->getServiceLocator()->has('TodoPriorityTableService')) {
      $oTodoPriorityTable = $this->getServiceLocator()->get('TodoPriorityTableService');
      $oTodoPriority = $oTodoPriorityTable->getAll();
    }
    $aTodoPriority = array();
    if (isset($oTodoPriority)) {
      foreach ($oTodoPriority as $oValue) {
        $aTodoPriority[$oValue->id] = $oValue->name;
      }
    }
    $this->add(array(
      'type' => 'Select',
      'name' => 'todo_priority_id',
      'options' => array(
        'label' => 'Priorytet',
        'value_options' => $aTodoPriority,
        'empty_option' => '',
      ),
    ));
    $this->add(array(
      'type' => 'Textarea',
      'name' => 'description',
      'options' => array(
        'label' => 'Opis',
        'class' => 'ckeditor',
      ),
    ));
    $this->add(array(
      'type' => 'Checkbox',
      'name' => 'active',
      'options' => array(
        'label' => 'Aktywny',
      ),
    ));
    $this->add(array(
      'type' => 'Button',
      'name' => 'submit_button',
      'options' => array(
        'label' => 'Zapisz',
      ),
    ));
  }

  /**
   * Set a single option for an element
   *
   * @param  string $key
   * @param  mixed $value
   * @return self
   */
  public function setOption($key, $value)
  {
    // TODO: Implement setOption() method.
  }
}
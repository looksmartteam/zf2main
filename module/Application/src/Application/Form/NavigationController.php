<?php
namespace Application\Form;

use Application\FormAbstract;

class NavigationController extends FormAbstract
{
  public function __construct($oServiceLocator, $sFormName)
  {
    parent::__construct($oServiceLocator, $sFormName);
  }

  public function init()
  {
    $this->add(array(
      'type' => 'Text',
      'name' => 'value',
      'options' => array(
        'label' => 'Nazwa',
      ),
    ));
    $this->add(array(
      'type' => 'Button',
      'name' => 'submit_button',
      'options' => array(
        'label' => 'Zapisz',
      ),
    ));
  }

  /**
   * Set a single option for an element
   *
   * @param  string $key
   * @param  mixed $value
   * @return self
   */
  public function setOption($key, $value)
  {
    // TODO: Implement setOption() method.
  }
}

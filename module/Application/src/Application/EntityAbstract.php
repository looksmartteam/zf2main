<?php
namespace Application;
abstract class EntityAbstract
{
  public $id;

  public function __get($sName)
  {
    $aSnippedName = explode('_', $sName);
    $sKey = '';
    if (is_array($aSnippedName) && count($aSnippedName)) {
      foreach ($aSnippedName as $sSnippedName) {
        $sKey .= ucfirst($sSnippedName);
      }
    }
    $sMethodName = 'get' . ucfirst($sKey);
    if (!method_exists($this, $sMethodName)) {
      return null;
    }
    return $this->$sMethodName();
  }

  public function __set($sName, $sValue)
  {
    $aSnippedName = explode('_', $sName);
    $sKey = '';
    if (is_array($aSnippedName) && count($aSnippedName)) {
      foreach ($aSnippedName as $sSnippedName) {
        $sKey .= ucfirst($sSnippedName);
      }
    }
    $sMethodName = 'set' . ucfirst($sKey);
    if (!method_exists($this, $sMethodName)) {
      return null;
    }
    return $this->$sMethodName($sValue);
  }

  public function getCreatedDate($nCreatedDate)
  {
    $this->created_date = (int)$nCreatedDate;
    $this->user_created_date = $this->created_date;
    return $this;
  }

  public function getEditId($nEditId)
  {
    if (is_numeric($nEditId) && $nEditId > 0) {
      $this->edit_id = (int)$nEditId;
    } else {
      $this->edit_id = null;
    }
    return $this;
  }

  public function getId($nId)
  {
    if (is_numeric($nId) && $nId > 0) {
      $this->id = (int)$nId;
    } else {
      $this->id = null;
    }
    return $this;
  }

  public function getOptions(\ArrayObject $oData) //wypełnia obiekt entity wartościami pobranymi z db
  {
    $aMethods = get_class_methods($this);
    foreach ($oData as $sKey => $mValue) {
      $aSnippedName = explode('_', $sKey);
      $sMethodName = '';
      if (is_array($aSnippedName) && count($aSnippedName)) {
        foreach ($aSnippedName as $sSnippedName) {
          $sMethodName .= ucfirst($sSnippedName);
        }
      }
      $sFullMethodName = 'get' . ucfirst($sMethodName);
      if (!in_array($sFullMethodName, $aMethods)) {
        $sFullMethodName = 'set' . ucfirst($sMethodName);
      }
      if (in_array($sFullMethodName, $aMethods)) {
        $this->$sFullMethodName($mValue);
      } else if (property_exists($this, $sKey)) {
        $this->$sKey = $mValue;
      }
    }
    return $this;
  }

  public function getServiceLocator()
  {
    return $this->_oServiceLocator;
  }

  public function getUpdatedDate($nUpdatedDate)
  {
    if ($nUpdatedDate) {
      $this->updated_date = (int)$nUpdatedDate;
      $this->user_updated_date = $this->updated_date;
    }
    return $this;
  }

  public function setCreatedDate()
  {
    $this->created_date = time();
    return $this;
  }

  public function setEditId($nEditId)
  {
    if (is_numeric($nEditId) && $nEditId > 0) {
      $this->edit_id = (int)$nEditId;
    } else {
      $this->edit_id = null;
    }
    return $this;
  }

  public function setId($nId)
  {
    if (is_numeric($nId) && $nId > 0) {
      $this->id = (int)$nId;
    } else {
      $this->id = null;
    }
    return $this;
  }

  public function setOptions(\ArrayObject $oData, $aVars = null, $bReverseArray = false) //wypełnia obiekt entity wartosciami z tablic POST, GET itd; nowo przygotowany obiekt entity zostanie następnie wstawiony do db
  {
    $aData = array();
    $aMethods = get_class_methods($this);
    foreach ($oData as $sKey => $mValue) {
      $aSnippedName = explode('_', $sKey);
      $sMethodName = '';
      if (is_array($aSnippedName) && count($aSnippedName)) {
        foreach ($aSnippedName as $sSnippedName) {
          $sMethodName .= ucfirst($sSnippedName);
        }
      }
      $sFullMethodName = 'set' . ucfirst($sMethodName);
      if (in_array($sFullMethodName, $aMethods)) {
        $this->$sFullMethodName($mValue);
      } else if (property_exists($this, $sKey)) {
        $this->$sKey = $mValue;
      }
    }
    if (is_array($aVars)) { //edit db row
      $aProperties = get_object_vars($this);
      if (!count($aVars)) {
        $aVars = (array)$oData;
        $bReverseArray = true;
      }
      foreach ($aVars as $mKey => $mValue) {
        if ($bReverseArray) {
          $mValue = $mKey;
        }
        if (array_key_exists($mValue, $aProperties)) {
          $aData[$mValue] = $this->$mValue;
        }
      }
      if (array_key_exists('updated_date', $aProperties)) {
        $aData['updated_date'] = time();
      }
      return $aData;
    }
    return $this;
  }

  public function setServiceLocator($oServiceLocator)
  {
    $this->_oServiceLocator = $oServiceLocator;
    return $this;
  }

  public function setUpdatedDate()
  {
    $this->updated_date = time();
    return $this;
  }

  public function setUserCreatedDate($nCreatedDate)
  {
    $this->user_created_date = date('d.m.Y', $nCreatedDate);
    return $this;
  }

  public function setUserUpdatedDate($nUpdatedDate)
  {
    $this->user_updated_date = date('d.m.Y', $nUpdatedDate);
    return $this;
  }
}

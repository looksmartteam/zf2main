<?php
namespace Application\View\Helper;

use RecursiveIteratorIterator;
use Zend\Navigation\AbstractContainer;
use Zend\Navigation\Page\AbstractPage;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Helper\Navigation\Menu;

class ApplicationMenu extends Menu implements ServiceLocatorAwareInterface
{
  protected function renderNormalMenu(AbstractContainer $oContainer, $sUlClass, $sIndent, $nMinDepth, $nMaxDepth, $bOnlyActive, $bEscapeLabels, $bAddClassToItemList, $sLiActiveClass)
  {
    $sHtml = '';
    //find deepest active
    $aFound = $this->findActive($oContainer, $nMinDepth, $nMaxDepth);
    if ($aFound) {
      $oFoundPage = $aFound['page'];
      $oFoundDepth = $aFound['depth'];
    } else {
      $oFoundPage = null;
    }
    //create iterator
    $oIterator = new RecursiveIteratorIterator($oContainer, RecursiveIteratorIterator::SELF_FIRST);
    if (is_int($nMaxDepth)) {
      $oIterator->setMaxDepth($nMaxDepth);
    }
    //iterate container
    $nPrevDepth = -1;
    foreach ($oIterator as $oPage) {
      $nDepth = $oIterator->getDepth();
      $bIsActive = $oPage->isActive(true);
      if ($nDepth < $nMinDepth || !$this->accept($oPage)) {
        //page is below minDepth or not accepted by acl/visibility
        continue;
      } elseif ($bOnlyActive && !$bIsActive) {
        //page is not active itself, but might be in the active branch
        $bAccept = false;
        if ($oFoundPage) {
          if ($oFoundPage->hasPage($oPage)) {
            //accept if page is a direct child of the active page
            $bAccept = true;
          } elseif ($oFoundPage->getParent()->hasPage($oPage)) {
            //page is a sibling of the active page...
            if (!$oFoundPage->hasPages() || is_int($nMaxDepth) && $oFoundDepth + 1 > $nMaxDepth) {
              //accept if active page has no children, or the
              //children are too deep to be rendered
              $bAccept = true;
            }
          }
        }
        if (!$bAccept) {
          continue;
        }
      }
      //make sure indentation is correct
      $nDepth -= $nMinDepth;
      $sMyIndent = $sIndent . str_repeat('', $nDepth);
      if ($nDepth > $nPrevDepth) {
        //start new ul tag
        if ($sUlClass && $nDepth == 0) {
          $sUlClass = ' class="' . $sUlClass . '"';
        } else if ($oPage->get('pages_container_class')) {
          $sUlClass = ' class="' . $oPage->get('pages_container_class') . '"';
        } else {
          $sUlClass = '';
        }
        $sHtml .= $sMyIndent . '<ul' . $sUlClass . '>' . self::EOL;
      } elseif ($nPrevDepth > $nDepth) {
        //close li/ul tags until we're at current depth
        for ($nIdx = $nPrevDepth; $nIdx > $nDepth; $nIdx--) {
          $sInd = $sIndent . str_repeat('', $nIdx);
          $sHtml .= $sInd . '</li>' . self::EOL;
          $sHtml .= $sInd . '</ul>' . self::EOL;
        }
        //close previous li tag
        $sHtml .= $sMyIndent . '</li>' . self::EOL;
      } else {
        //close previous li tag
        $sHtml .= $sMyIndent . '</li>' . self::EOL;
      }
      //render li tag and page
      $aLiClasses = array();
      //Is page active?
      if ($bIsActive) {
        $aLiClasses[] = 'active';
      }
      if ($oPage->get('wrap_class')) {
        $aLiClasses[] = $oPage->get('wrap_class');
      }
      $sLiClass = empty($aLiClasses) ? '' : ' class="' . implode(' ', $aLiClasses) . '"';
      $sHtml .= $sMyIndent . '<li' . $sLiClass . '>' . self::EOL . $sMyIndent . '' . $this->htmlify($oPage, $bEscapeLabels, $bAddClassToItemList) . self::EOL;
      //store as previous depth for next iteration
      $nPrevDepth = $nDepth;
    }
    if ($sHtml) {
      //done iterating container; close open ul/li tags
      for ($nIdx = $nPrevDepth + 1; $nIdx > 0; $nIdx--) {
        $sMyIndent = $sIndent . str_repeat('', $nIdx - 1);
        $sHtml .= $sMyIndent . '</li>' . self::EOL . $sMyIndent . '</ul>' . self::EOL;
      }
      $sHtml = rtrim($sHtml, self::EOL);
    }
    return $sHtml;
  }

  public function __invoke($oContainer = null)
  {
    parent::__invoke($oContainer);
    if ($this->serviceLocator->getServiceLocator()->get('ControllerPluginManager')->has('AclPlugin')) {
      $oAclPlugin = $this->serviceLocator->getServiceLocator()->get('ControllerPluginManager')->get('AclPlugin');
      if (isset($oAclPlugin)) {
        //$this->setAcl($oAclPlugin->getAcl());
        //$this->setRole($oAclPlugin->getRole());
      }
    }
    return $this;
  }

  public function getServiceLocator()
  {
    return $this->serviceLocator;
  }

  public function htmlify(AbstractPage $oPage, $bEscapeLabel = true, $bAddClassToItemList = false)
  {
    //get label and title for translating
    $sLabel = $oPage->getLabel();
    $sTitle = $oPage->getTitle();
    //translate label and title?
    if (null !== ($oTranslator = $this->getTranslator())) {
      $sTextDomain = $this->getTranslatorTextDomain();
      if (is_string($sLabel) && !empty($sLabel)) {
        $sLabel = $oTranslator->translate($sLabel, $sTextDomain);
      }
      if (is_string($sTitle) && !empty($sTitle)) {
        $sTitle = $oTranslator->translate($sTitle, $sTextDomain);
      }
    }
    //get attribs for element
    $aAttribs = array(
      'id' => $oPage->getId(),
      'title' => $sTitle,
    );
    $aAttr = $oPage->get('attribs');
    if (is_array($aAttr)) {
      $aAttribs = $aAttribs + $aAttr;
    }
    //does page have a href?
    $sHref = $oPage->getHref();
    if ($sHref) {
      $sElement = 'a';
      $aAttribs['href'] = $sHref;
      $aAttribs['target'] = $oPage->getTarget();
    } else {
      $sElement = 'span';
    }
    $sHtml = '<' . $sElement . $this->htmlAttribs($aAttribs) . '>';
    if ($bEscapeLabel === true) {
      $escaper = $this->view->plugin('escapeHtml');
      $sHtml .= $escaper($sLabel);
    } else {
      $sHtml .= $sLabel;
    }
    $sHtml .= '</' . $sElement . '>';
    return $sHtml;
  }

  public function setServiceLocator(ServiceLocatorInterface $oServiceLocator)
  {
    $this->serviceLocator = $oServiceLocator;
    return $this;
  }
}

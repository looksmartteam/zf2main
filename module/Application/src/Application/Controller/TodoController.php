<?php
namespace Application\Controller;

use Application\Model\Entity\Todo as TodoEntity;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class TodoController extends AbstractActionController
{
  protected $_oTodoTable;

  protected function getTodoTable()
  {
    if (!$this->_oTodoTable && $this->getServiceLocator()->has('TodoTableService')) {
      $this->_oTodoTable = $this->getServiceLocator()->get('TodoTableService');
    }
    if (!isset($this->_oTodoTable)) {
      throw \Exception();
    }
    return $this->_oTodoTable;
  }

  public function addtodoajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $oTodoForm = $this->getServiceLocator()->get('TodoFormService');
      $oTodoForm->setData($this->getRequest()->getPost());
      if ($oTodoForm->isValid()) {
        $oTodoEntity = new TodoEntity();
        $oData = new \ArrayObject($oTodoForm->getData());
        if ((int)$oData->offsetGet('primary') === 1) {
          $oResetData = new \ArrayObject(array('primary' => 0));
          $this->getTodoTable()->editRow($oTodoEntity->setOptions($oResetData, array('primary')));
        }
        $oData->offsetSet('email_configuration_id', $this->getTodoTable()->addRow($oTodoEntity->setOptions($oData)));
        if ($oData->offsetGet('email_configuration_id')) {
          $bSuccess = true;
        }
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function autocompletetodoajaxAction()
  {
    $oResponse = $this->getResponse();
    $aFilter = null;
    $aTodo = array();
    $sColumnName = $this->params()->fromQuery('column_name') !== '' ? $this->params()->fromQuery('column_name') : null;
    if (is_array($this->params()->fromQuery('filter')) && count($this->params()->fromQuery('filter'))) {
      $aFilter = $this->params()->fromQuery('filter');
      foreach ($aFilter as $sKey => $sValue) {
        if (strlen(trim($sValue)))
          $aFilter[$sKey] = trim($sValue);
        else
          unset($aFilter[$sKey]);
      }
    }
    if (isset($aFilter)) {
      $aRowset = $this->getTodoTable()->getAll(array('filter' => $aFilter));
      $aLabels = array();
      foreach ($aRowset as $nKey => $oTodo) {
        if (!in_array($oTodo->$sColumnName, $aLabels)) {
          $aTodo[$nKey]['label'] = strip_tags($oTodo->$sColumnName);
          array_push($aLabels, $oTodo->$sColumnName);
        }
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode(array('rowset' => $aTodo)));
    return $oResponse;
  }

  public function deletetodoajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $nId = $this->params()->fromPost('id') !== '' ? (int)$this->params()->fromPost('id') : null;
      if ($nId) {
        $this->getTodoTable()->deleteRow($nId);
        $bSuccess = true;
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function edittodoajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $oTodoForm = $this->getServiceLocator()->get('TodoFormService');
      $oTodoForm->setData($this->getRequest()->getPost());
      if ($oTodoForm->isValid()) {
        $oTodoEntity = new TodoEntity();
        $oData = new \ArrayObject($oTodoForm->getData());
        if ((int)$oData->offsetGet('primary') === 1) {
          $oResetData = new \ArrayObject(array('primary' => 0));
          $this->getTodoTable()->editRow($oTodoEntity->setOptions($oResetData, array('primary')));
        }
        $this->getTodoTable()->editRow($oTodoEntity->setOptions($oData, array()));
        $bSuccess = true;
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function indexAction()
  {
    $oAddNewTodoForm = $this->getServiceLocator()->get('TodoFormService');
    $oEditTodoForm = $this->getServiceLocator()->get('EditTodoFormService');
    return new ViewModel(
      array(
        'add_new_to_do_form' => $oAddNewTodoForm,
        'edit_to_do_form' => $oEditTodoForm,
      )
    );
  }

  public function loadtodoajaxAction()
  {
    if ($this->getRequest()->isPost()) {
      $oResponse = $this->getResponse();
      $aSort = null;
      $aFilter = null;
      if (is_array($this->params()->fromPost('filter')) && count($this->params()->fromPost('filter'))) {
        $aFilter = $this->params()->fromPost('filter');
        foreach ($aFilter as $sKey => $sValue) {
          if (strlen(trim($sValue)))
            $aFilter[$sKey] = trim($sValue);
          else
            unset($aFilter[$sKey]);
        }
      }
      $sSortColumn = $this->params()->fromPost('sort_column') !== '' ? $this->params()->fromPost('sort_column') : 'id';
      $sSortMethod = $this->params()->fromPost('sort_method') !== '' ? $this->params()->fromPost('sort_method') : 'asc';
      $nLimit = $this->params()->fromPost('limit');
      $nOffset = $this->params()->fromPost('offset');
      if (isset($sSortColumn) && isset($sSortMethod))
        $aSort = array('sort_column' => $sSortColumn, 'sort_method' => $sSortMethod);
      if (isset($nLimit) && is_numeric($nLimit)) {
        $nLimit = (int)$nLimit;
      } else {
        $nLimit = 20;
      }
      if (isset($nOffset) && is_numeric($nOffset)) {
        $nOffset = (int)$nOffset;
      } else {
        $nOffset = 0;
      }
      $aModules = $this->getTodoTable()->getAll(array('filter' => $aFilter, 'sort' => $aSort, 'limit' => $nLimit, 'offset' => $nOffset));
      $nCount = count($this->getTodoTable()->getAll(array('filter' => $aFilter, 'sort' => $aSort)));
      $oResponse->setContent(\Zend\Json\Json::encode(array('rowset' => $aModules, 'num_rows' => $nCount)));
    }
    return $oResponse;
  }

  public function secendoptiontodoajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $sSecendOption = $this->params()->fromPost('second_opinion');
      $sColumnName = $this->params()->fromPost('column_name');
      $nId = $this->params()->fromPost('id') !== '' ? (int)$this->params()->fromPost('id') : null;
      $nResetAll = $this->params()->fromPost('reset_all') !== '' ? (int)$this->params()->fromPost('reset_all') : null;
      if ($nId) {
        $oTodoEntity = new TodoEntity();
        switch ($sSecendOption) {
          case 'enable_disable':
            $oTodo = $this->getTodoTable()->getRow(array('id' => $nId));
            if (isset($oTodo->$sColumnName)) {
              if ($nResetAll && $oTodo->$sColumnName === 0) {
                $oResetData = new \ArrayObject(array($sColumnName => 0));
                $this->getTodoTable()->editRow($oTodoEntity->setOptions($oResetData, array($sColumnName)));
              }
              if ($oTodo->$sColumnName === 1) {
                $oData = new \ArrayObject(array('id' => $nId, $sColumnName => 0));
                $this->getTodoTable()->editRow($oTodoEntity->setOptions($oData, array('id', $sColumnName)));
                $bSuccess = true;
              } else if ($oTodo->$sColumnName === 0) {
                $oData = new \ArrayObject(array('id' => $nId, $sColumnName => 1));
                $this->getTodoTable()->editRow($oTodoEntity->setOptions($oData, array('id', $sColumnName)));
                $bSuccess = true;
              }
            }
            break;
        }
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }
}
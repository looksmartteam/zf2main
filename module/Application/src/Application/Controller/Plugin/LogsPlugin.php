<?php
namespace Application\Controller\Plugin;

use Application\Model\Entity\Logs as LogsEntity;
use Zend\Authentication\Adapter\DbTable;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

define('IPINFO_API_KEY', '52f02b4e5df67e6631c01c1a275b4e9aa27fda7fc07e9149eb3b3bf3a1c119fe');

class LogsPlugin extends AbstractPlugin implements ServiceLocatorAwareInterface, EventManagerAwareInterface
{
  private $_oAuthPlugin;
  private $_oGeolocationTable;
  private $_oLogsTable;
  private $_oServiceLocator;

  public function getEventManager()
  {
    if (null === $this->events) {
      $this->setEventManager(new EventManager());
    }
    return $this->events;
  }

  public function getGeolocationTable()
  {
    if (!$this->_oGeolocationTable && $this->getServiceLocator()->has('GeolocationTableService')) {
      $this->_oGeolocationTable = $this->getServiceLocator()->get('GeolocationTableService');
    }
    return $this->_oGeolocationTable;
  }

  public function getIPAddress()
  {
    if (!empty($_SERVER["HTTP_CLIENT_IP"])) {
      $sIPAddress = $_SERVER["HTTP_CLIENT_IP"];
    } elseif (!empty($_SERVER["HTTP_X_FORWARDED_FOR"])) {
      $sIPAddress = $_SERVER["HTTP_X_FORWARDED_FOR"];
    } else {
      $sIPAddress = $_SERVER["REMOTE_ADDR"];
    }
    //$oIPInfo = new \Application\IPInfo(IPINFO_API_KEY, 'json');
    //var_dump(json_decode($oIPInfo->getCity($sIPAddress)));
    return $sIPAddress;
  }

  public function getLogsTable()
  {
    if (!$this->_oLogsTable && $this->getServiceLocator()->has('LogsTableService')) {
      $this->_oLogsTable = $this->getServiceLocator()->get('LogsTableService');
    }
    return $this->_oLogsTable;
  }

  public function getServiceLocator()
  {
    return $this->_oServiceLocator;
  }

  public function getUserId()
  {
    $nUserId = null;
    if ($this->_oAuthPlugin && $this->_oAuthPlugin->hasIdentity() && $this->_oAuthPlugin->getStorageData('id')) {
      $nUserId = $this->_oAuthPlugin->getStorageData('id');
    }
    return $nUserId;
  }

  public function init($e)
  {
    $this->setServiceLocator($e->getApplication()->getServiceManager());
    if ($this->getServiceLocator()->has('AuthService')) {
      $this->_oAuthPlugin = $this->getServiceLocator()->get('AuthService');
    }
    $oLogsEntity = new LogsEntity();
    $sModuleName = strtolower(substr(get_class($e->getTarget()), 0, strpos(get_class($e->getTarget()), '\\')));
    $sControllerName = strtolower($e->getRouteMatch()->getParam('__CONTROLLER__'));
    $sActionName = strtolower($e->getRouteMatch()->getParam('action'));
    $this->getLogsTable()->addRow($oLogsEntity->setOptions(new \ArrayObject(
        array(
          'module' => $sModuleName,
          'controller' => $sControllerName,
          'action' => $sActionName,
          'user_id' => $this->getUserId(),
          'ip_address' => $this->getIPAddress(),
          'geolocation_id' => $this->getGeolocationTable()->find(array('ip_address' => $this->getIPAddress()))
        )
      )
    )
    );
  }

  public function setEventManager(EventManagerInterface $events)
  {
    $events->setIdentifiers(array(
      __CLASS__,
      get_called_class(),
    ));
    $this->events = $events;
    return $this;
  }

  public function setServiceLocator(ServiceLocatorInterface $oServiceLocator)
  {
    $this->_oServiceLocator = $oServiceLocator;
    return $this;
  }
}
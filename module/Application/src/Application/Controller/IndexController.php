<?php
namespace Application\Controller;

use Doctrine\ORM\EntityManager;
use Frlnc\Slack\Core\Commander;
use Frlnc\Slack\Http\CurlInteractor;
use Frlnc\Slack\Http\SlackResponseFactory;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Firebase\FirebaseLib;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
const DEFAULT_URL = 'https://beelab.firebaseio.com/';
const DEFAULT_TOKEN = 'wiLL0ANl1zLthh7YMFGx74SKUvvfRttnUDm4Y03V';
const DEFAULT_PATH = '/test';
class IndexController extends AbstractActionController
{
  protected $_oEntityManager;

  protected function getEntityManager()
  {
    if (null === $this->_oEntityManager) {
      $this->setEntityManager($this->getServiceLocator()->get('\Doctrine\ORM\EntityManager'));
    }
    return $this->_oEntityManager;
  }

  protected function setEntityManager(EntityManager $oEntityManager)
  {
    $this->_oEntityManager = $oEntityManager;
    return $this;
  }

  public function indexAction()
  {
    return new ViewModel(array('test' => 1));
  }

  public function validateformajaxAction()
  {
    if ($this->getRequest()->isPost()) {
      $oResponse = $this->getResponse();
      $aPostData = $this->getRequest()->getPost();
      $aClassName = explode('_', $aPostData['form_name']);
      if (is_array($aClassName) && count($aClassName)) {
        $sClassName = '';
        foreach ($aClassName as $nKey => $sValue) {
          $sClassName .= ucfirst($sValue);
        }
      } else {
        $sClassName = ucfirst($aPostData['form_name']);
      }
      $aValidationGroup = $aPostData['validation_group'];
      $sFormServiceName = $sClassName . 'FormService';
      $sInputFilterServiceName = $sClassName . 'InputFilterService';
      $aValidElements = array();
      $aResponse = array();
      if ($this->getServiceLocator()->has($sFormServiceName) && $this->getServiceLocator()->has($sInputFilterServiceName) && is_array($aPostData['valid'])) {
        $oFormInstance = $this->getServiceLocator()->get($sFormServiceName);
        $oInputFilterInstance = $this->getServiceLocator()->get($sInputFilterServiceName);
        if (isset($aPostData['valid'])) {
          foreach ($aPostData['valid'] as $aValue) {
            if ($aValue['tag_name'] === 'select' && strpos($aValue['name'], '[]')) {
              $sNewElementName = str_replace('[]', '', $aValue['name']);
              if (!isset($aValidElements[$sNewElementName])) {
                $aValidElements[$sNewElementName] = array();
              }
              if ($aValue['value']) {
                foreach ($aValue['value'] as $nKey => $mValue) {
                  array_push($aValidElements[$sNewElementName], $mValue);
                }
              }
            } else {
              if (isset($aValue['name'])) {
                $aValidElements[$aValue['name']] = $aValue['value'];
              }
            }
          }
        }
        if (is_array($aValidationGroup) && count($aValidationGroup)) {
          $oFormInstance->setValidationGroup($aValidationGroup);
        }
        $oFormInstance->setInputFilter($oInputFilterInstance->getInputFilter())->setData($aValidElements);
        if (isset($aPostData['fake_required'])) {
          foreach ($aPostData['fake_required'] as $aValue) {
            if (isset($aValue['depend_element'])) {
              $oFormInstance->getInputFilter()->get($aValue['name'])->setRequired(true);
            }
          }
        }
        if (!$oFormInstance->isValid()) {
          $aResponse = $oFormInstance->getMessages();
        }
      }
      $oResponse->setContent(\Zend\Json\Json::encode(array('response' => $aResponse)));
      return $oResponse;
    }
  }
}
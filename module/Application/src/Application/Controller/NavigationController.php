<?php
namespace Application\Controller;

use Application\MakeHash;
use Application\Model\Entity\NavigationAction as NavigationActionEntity;
use Application\Model\Entity\NavigationController as NavigationControllerEntity;
use Application\Model\Entity\NavigationMenu as NavigationMenuEntity;
use Application\Model\Entity\NavigationMenuConfiguration as NavigationMenuConfigurationEntity;
use Application\Model\Entity\NavigationModule as NavigationModuleEntity;
use Application\Model\Entity\NavigationOption as NavigationOptionEntity;
use Application\Model\Entity\NavigationPrivilege as NavigationPrivilegeEntity;
use Application\Model\Entity\NavigationPrivilegeUserRole as NavigationPrivilegeUserRoleEntity;
use Application\Model\Entity\NavigationResource as NavigationResourceEntity;
use Application\Model\Entity\SiteSeo as SiteSeoEntity;
use Application\Model\Entity\SiteSeoMetaKeywords as SiteSeoMetaKeywordsEntity;
use Application\Model\Entity\SiteSeoSiteSeoMetaKeywords as SiteSeoSiteSeoMetaKeywordsEntity;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class NavigationController extends AbstractActionController
{
  protected $_oNavigationActionTable;
  protected $_oNavigationControllerTable;
  protected $_oNavigationLevelTable;
  protected $_oNavigationMenuConfigurationTable;
  protected $_oNavigationMenuTable;
  protected $_oNavigationModuleTable;
  protected $_oNavigationOptionTable;
  protected $_oNavigationOptionUserRoleTable;
  protected $_oNavigationPrivilegeTable;
  protected $_oNavigationPrivilegeUserRoleTable;
  protected $_oNavigationResourceTable;
  protected $_oSiteSeoMetaKeywordsTable;
  protected $_oSiteSeoSiteSeoMetaKeywordsTable;
  protected $_oSiteSeoTable;

  protected function endsWith($sNeedle, $sHaystack)
  {
    $nLength = strlen($sNeedle);
    if ($nLength == 0) {
      return true;
    }
    return (substr($sHaystack, -$nLength) === $sNeedle);
  }

  protected function getNavigationActionTable()
  {
    if (!$this->_oNavigationActionTable && $this->getServiceLocator()->has('NavigationActionTableService')) {
      $this->_oNavigationActionTable = $this->getServiceLocator()->get('NavigationActionTableService');
    }
    return $this->_oNavigationActionTable;
  }

  protected function getNavigationControllerTable()
  {
    if (!$this->_oNavigationControllerTable && $this->getServiceLocator()->has('NavigationControllerTableService')) {
      $this->_oNavigationControllerTable = $this->getServiceLocator()->get('NavigationControllerTableService');
    }
    return $this->_oNavigationControllerTable;
  }

  protected function getNavigationLevelTable()
  {
    if (!$this->_oNavigationLevelTable && $this->getServiceLocator()->has('NavigationLevelTableService')) {
      $this->_oNavigationLevelTable = $this->getServiceLocator()->get('NavigationLevelTableService');
    }
    return $this->_oNavigationLevelTable;
  }

  protected function getNavigationMenuConfigurationTable()
  {
    if (!$this->_oNavigationMenuConfigurationTable && $this->getServiceLocator()->has('NavigationMenuConfigurationTableService')) {
      $this->_oNavigationMenuConfigurationTable = $this->getServiceLocator()->get('NavigationMenuConfigurationTableService');
    }
    return $this->_oNavigationMenuConfigurationTable;
  }

  protected function getNavigationMenuTable()
  {
    if (!$this->_oNavigationMenuTable && $this->getServiceLocator()->has('NavigationMenuTableService')) {
      $this->_oNavigationMenuTable = $this->getServiceLocator()->get('NavigationMenuTableService');
    }
    return $this->_oNavigationMenuTable;
  }

  protected function getNavigationModuleTable()
  {
    if (!$this->_oNavigationModuleTable && $this->getServiceLocator()->has('NavigationModuleTableService')) {
      $this->_oNavigationModuleTable = $this->getServiceLocator()->get('NavigationModuleTableService');
    }
    return $this->_oNavigationModuleTable;
  }

  protected function getNavigationOptionTable()
  {
    if (!$this->_oNavigationOptionTable && $this->getServiceLocator()->has('NavigationOptionTableService')) {
      $this->_oNavigationOptionTable = $this->getServiceLocator()->get('NavigationOptionTableService');
    }
    return $this->_oNavigationOptionTable;
  }

  protected function getNavigationOptionUserRoleTable()
  {
    if (!$this->_oNavigationOptionUserRoleTable && $this->getServiceLocator()->has('NavigationOptionUserRoleTableService')) {
      $this->_oNavigationOptionUserRoleTable = $this->getServiceLocator()->get('NavigationOptionUserRoleTableService');
    }
    return $this->_oNavigationOptionUserRoleTable;
  }

  protected function getNavigationPrivilegeTable()
  {
    if (!$this->_oNavigationPrivilegeTable && $this->getServiceLocator()->has('NavigationPrivilegeTableService')) {
      $this->_oNavigationPrivilegeTable = $this->getServiceLocator()->get('NavigationPrivilegeTableService');
    }
    return $this->_oNavigationPrivilegeTable;
  }

  protected function getNavigationPrivilegeUserRoleTable()
  {
    if (!$this->_oNavigationPrivilegeUserRoleTable && $this->getServiceLocator()->has('NavigationPrivilegeUserRoleTableService')) {
      $this->_oNavigationPrivilegeUserRoleTable = $this->getServiceLocator()->get('NavigationPrivilegeUserRoleTableService');
    }
    return $this->_oNavigationPrivilegeUserRoleTable;
  }

  protected function getNavigationResourceTable()
  {
    if (!$this->_oNavigationResourceTable && $this->getServiceLocator()->has('NavigationResourceTableService')) {
      $this->_oNavigationResourceTable = $this->getServiceLocator()->get('NavigationResourceTableService');
    }
    return $this->_oNavigationResourceTable;
  }

  protected function getSiteHref($sLabel)
  {
    $sNewLabel = '';
    $sTempLabel = trim(strtolower($sLabel));
    $sTempLabel = explode(' ', $sTempLabel);
    foreach ($sTempLabel as $nKey => $sValue) {
      if ($nKey)
        $sNewLabel .= '-' . $sValue;
      else
        $sNewLabel .= $sValue;
    }
    return $this->removeAccents($sNewLabel);
  }

  protected function getSiteSeoMetaKeywordsTable()
  {
    if (!$this->_oSiteSeoMetaKeywordsTable && $this->getServiceLocator()->has('SiteSeoMetaKeywordsTableService')) {
      $this->_oSiteSeoMetaKeywordsTable = $this->getServiceLocator()->get('SiteSeoMetaKeywordsTableService');
    }
    return $this->_oSiteSeoMetaKeywordsTable;
  }

  protected function getSiteSeoSiteSeoMetaKeywordsTable()
  {
    if (!$this->_oSiteSeoSiteSeoMetaKeywordsTable && $this->getServiceLocator()->has('SiteSeoSiteSeoMetaKeywordsTableService')) {
      $this->_oSiteSeoSiteSeoMetaKeywordsTable = $this->getServiceLocator()->get('SiteSeoSiteSeoMetaKeywordsTableService');
    }
    return $this->_oSiteSeoSiteSeoMetaKeywordsTable;
  }

  protected function getSiteSeoTable()
  {
    if (!$this->_oSiteSeoTable && $this->getServiceLocator()->has('SiteSeoTableService')) {
      $this->_oSiteSeoTable = $this->getServiceLocator()->get('SiteSeoTableService');
    }
    return $this->_oSiteSeoTable;
  }

  protected function removeAccents($sStr)
  {
    $aPolishChars = array('ą', 'ć', 'ę', 'ł', 'ń', 'ó', 'ś', 'ż', 'ź');
    $aReplace = array('a', 'c', 'e', 'l', 'n', 'o', 's', 'z', 'z');
    return str_replace($aPolishChars, $aReplace, $sStr);
  }

  protected function repairNavigationMenu($bOnlyAction = true, $bOnlyAjaxAction = false, $bAddAjaxAction = false)
  {
    $oNavigationModuleEntity = new NavigationModuleEntity();
    $oNavigationControllerEntity = new NavigationControllerEntity();
    $oNavigationActionEntity = new NavigationActionEntity();
    $oModuleManager = $this->getServiceLocator()->get('ModuleManager');
    $sDir = PATH . 'module';
    $aModules = array();
    $aControllers = array();
    $aActions = array();
    $aNavigationMenuId = array();
    $aNavigationAjaxMenuId = array();
    if (file_exists($sDir)) {
      $oModules = new \DirectoryIterator($sDir);
      foreach ($oModules as $oModule) {
        if (!$oModule->isDot() && $oModule->isDir() && array_key_exists($oModule->__toString(), $oModuleManager->getLoadedModules())) {
          $sModuleNameUpper = $oModule->__toString();
          $sModuleNameLower = strtolower($oModule->__toString());
          array_push($aModules, $sModuleNameLower);
          $nNavigationModuleId = $this->getNavigationModuleTable()->addRow($oNavigationModuleEntity->setOptions(new \ArrayObject(array('value' => $sModuleNameLower))));
          if (file_exists($sDir . '/' . $sModuleNameUpper . '/src/' . $sModuleNameUpper . '/Controller')) {
            $oControllers = new \DirectoryIterator($sDir . '/' . $sModuleNameUpper . '/src/' . $sModuleNameUpper . '/Controller');
            foreach ($oControllers as $oController) {
              if (!$oController->isDot() && !$oController->isDir()) {
                $sControllerNameUpper = str_replace('Controller.php', '', $oController->__toString());
                $sControllerNameLower = strtolower(str_replace('Controller.php', '', $oController->__toString()));
                array_push($aControllers, $sControllerNameLower);
                $nNavigationControllerId = $this->getNavigationControllerTable()->addRow($oNavigationControllerEntity->setOptions(new \ArrayObject(array('value' => $sControllerNameLower))));
                $sClassName = '\\' . $sModuleNameUpper . '\\Controller\\' . $sControllerNameUpper . 'Controller';
                $aNavigationActionId = array();
                $aNavigationAjaxActionId = array();
                if (class_exists($sClassName)) {
                  foreach (get_class_methods(new $sClassName) as $sActionName) {
                    if ($this->endsWith('Action', $sActionName) && !$this->endsWith('ajaxAction', $sActionName) && !in_array($sActionName, array('notFoundAction', 'getMethodFromAction'))) {
                      $sActionNameLower = strtolower(str_replace('Action', '', $sActionName));
                      array_push($aActions, $sActionNameLower);
                      $nNavigationActionId = $this->getNavigationActionTable()->addRow($oNavigationActionEntity->setOptions(new \ArrayObject(array('value' => $sActionNameLower, 'ajax' => 0))));
                      if ($bOnlyAction) {
                        $aNavigationActionId[$nNavigationActionId] = $sActionNameLower;
                      }
                    } else if ($this->endsWith('ajaxAction', $sActionName)) {
                      $sActionNameLower = strtolower(str_replace('Action', '', $sActionName));
                      array_push($aActions, $sActionNameLower);
                      $nNavigationActionId = $this->getNavigationActionTable()->addRow($oNavigationActionEntity->setOptions(new \ArrayObject(array('value' => $sActionNameLower, 'ajax' => 1))));
                      if ($bOnlyAjaxAction) {
                        $aNavigationActionId[$nNavigationActionId] = $sActionNameLower;
                      }
                      if ($bAddAjaxAction) {
                        $nNavigationOptionId = $this->getNavigationOptionTable()->findRow(array(
                          'navigation_module_id' => $nNavigationModuleId,
                          'navigation_controller_id' => $nNavigationControllerId,
                          'navigation_action_id' => $nNavigationActionId,
                        ));
                        if (!$nNavigationOptionId) {
                          $aNavigationAjaxActionId[$nNavigationActionId] = $sActionNameLower;
                        }
                      }
                    }
                  }
                }
                $aNavigationMenuId[$nNavigationModuleId][$nNavigationControllerId] = $aNavigationActionId;
                if ($bAddAjaxAction) {
                  $aNavigationAjaxMenuId[$nNavigationModuleId][$nNavigationControllerId] = $aNavigationAjaxActionId;
                }
              }
            }
          }
        }
      }
    }
    $oModules = $this->getNavigationModuleTable()->getAll();
    $oControllers = $this->getNavigationControllerTable()->getAll();
    $oActions = $this->getNavigationActionTable()->getAll();
    if ($oModules) {
      foreach ($oModules as $oModule) {
        if (!in_array($oModule->value, $aModules)) {
          $this->getNavigationModuleTable()->deleteRow($oModule->id);
        }
      }
    }
    if ($oControllers) {
      foreach ($oControllers as $oController) {
        if (!in_array($oController->value, $aControllers)) {
          $this->getNavigationControllerTable()->deleteRow($oController->id);
        }
      }
    }
    if ($oActions) {
      foreach ($oActions as $oAction) {
        if (!in_array($oAction->value, $aActions)) {
          $this->getNavigationActionTable()->deleteRow($oAction->id);
        }
      }
    }
    if ($bAddAjaxAction && $aNavigationAjaxMenuId) {
      $oNavigationResourceEntity = new NavigationResourceEntity();
      $oNavigationPrivilegeEntity = new NavigationPrivilegeEntity();
      $oNavigationPrivilegeUserRoleEntity = new NavigationPrivilegeUserRoleEntity();
      $oNavigationOptionEntity = new NavigationOptionEntity();
      $oNavigationMenuEntity = new NavigationMenuEntity();
      $nIdx = 1;
      foreach ($aNavigationAjaxMenuId as $nNavigationModuleId => $aNavigationControllerId) {
        if (count($aNavigationControllerId)) {
          foreach ($aNavigationControllerId as $nNavigationControllerId => $aNavigationActionId) {
            if (count($aNavigationActionId)) {
              foreach ($aNavigationActionId as $nNavigationActionId => $sNavigationActionName) {
                $oMakeHash = new MakeHash();
                $aUserRoleId = array(2);
                if ($sNavigationActionName === 'validateformajax') {
                  $aUserRoleId = array(1, 2);
                }
                $oData = new \ArrayObject(array(
                  'navigation_menu_configuration_id' => 2,
                  'navigation_menu_type_id' => 1,
                  'navigation_level_id' => 1,
                  'navigation_parent_menu_id' => null,
                  'navigation_parent_submenu_id' => null,
                  'navigation_layout_id' => null,
                  'label_name' => $sNavigationActionName,
                  'description' => null,
                  'image_id' => null,
                  'order' => $nIdx++,
                  'active' => 0,
                  'navigation_module_id' => $nNavigationModuleId,
                  'navigation_controller_id' => $nNavigationControllerId,
                  'navigation_action_id' => $nNavigationActionId,
                  'user_role_id' => $aUserRoleId,
                ));
                try {
                  $this->getNavigationMenuTable()->beginTransaction();
                  if (!is_numeric($oData->offsetGet('navigation_module_id'))) {
                    $oData->offsetSet('navigation_module_id', $this->getNavigationModuleTable()->findRow(array('value' => $oData->offsetGet('navigation_module_id'))));
                    if (!is_numeric($oData->offsetGet('navigation_module_id'))) {
                      throw new \Exception();
                    }
                  }
                  if (!is_numeric($oData->offsetGet('navigation_controller_id'))) {
                    $oData->offsetSet('navigation_controller_id', $this->getNavigationControllerTable()->findRow(array('value' => $oData->offsetGet('navigation_controller_id'))));
                    if (!is_numeric($oData->offsetGet('navigation_controller_id'))) {
                      throw new \Exception();
                    }
                  }
                  if (!is_numeric($oData->offsetGet('navigation_action_id'))) {
                    $oData->offsetSet('navigation_action_id', $this->getNavigationActionTable()->findRow(array('value' => $oData->offsetGet('navigation_action_id'))));
                    if (!is_numeric($oData->offsetGet('navigation_action_id'))) {
                      throw new \Exception();
                    }
                  }
                  $oData->offsetSet('navigation_module_name', $this->getNavigationModuleTable()->getRow(array('id' => $oData->offsetGet('navigation_module_id')))->value);
                  $oData->offsetSet('navigation_controller_name', $this->getNavigationControllerTable()->getRow(array('id' => $oData->offsetGet('navigation_controller_id')))->value);
                  $oData->offsetSet('navigation_action_name', $this->getNavigationActionTable()->getRow(array('id' => $oData->offsetGet('navigation_action_id')))->value);
                  $oNavigationResourceValue = new \ArrayObject(array(
                    'value' => $oData->offsetGet('navigation_module_name')
                  ));
                  $oNavigationPrivilegeValue = new \ArrayObject(array(
                    'value' => $oData->offsetGet('navigation_controller_name') . ':' . $oData->offsetGet('navigation_action_name'),
                    //'unique_key' => $oMakeHash->generate(),
                    'unique_key' => md5(
                      $oData->offsetGet('navigation_module_name') .
                      $oData->offsetGet('navigation_controller_name') .
                      $oData->offsetGet('navigation_action_name') .
                      $this->getSiteHref($oData->offsetGet('label_name')) .
                      'html'
                    )
                  ));
                  $oData->offsetSet('navigation_resource_id', $this->getNavigationResourceTable()->addRow($oNavigationResourceEntity->setOptions($oNavigationResourceValue)));
                  $oData->offsetSet('navigation_privilege_id', $this->getNavigationPrivilegeTable()->addRow($oNavigationPrivilegeEntity->setOptions($oNavigationPrivilegeValue)));
                  if ($oData->offsetGet('navigation_privilege_id') && is_array($oData->offsetGet('user_role_id')) && count($oData->offsetGet('user_role_id'))) {
                    foreach ($oData->offsetGet('user_role_id') as $nValue) {
                      $oNavigationPrivilegeUserRoleValue = new \ArrayObject(array(
                        'navigation_privilege_id' => $oData->offsetGet('navigation_privilege_id'),
                        'user_role_id' => $nValue
                      ));
                      $this->getNavigationPrivilegeUserRoleTable()->addRow($oNavigationPrivilegeUserRoleEntity->setOptions($oNavigationPrivilegeUserRoleValue));
                    }
                  }
                  if ($oData->offsetGet('navigation_resource_id') && $oData->offsetGet('navigation_privilege_id')) {
                    $oNavigationOptionValue = new \ArrayObject(array(
                      'navigation_module_id' => $oData->offsetGet('navigation_module_id'),
                      'navigation_controller_id' => $oData->offsetGet('navigation_controller_id'),
                      'navigation_action_id' => $oData->offsetGet('navigation_action_id'),
                      'navigation_privilege_id' => $oData->offsetGet('navigation_privilege_id'),
                      'navigation_resource_id' => $oData->offsetGet('navigation_resource_id')
                    ));
                    $oData->offsetSet('navigation_option_id', $this->getNavigationOptionTable()->addRow($oNavigationOptionEntity->setOptions($oNavigationOptionValue)));
                    if ($oData->offsetGet('navigation_option_id')) {
                      $oNavigationMenuValue = new \ArrayObject(array(
                        'navigation_menu_configuration_id' => $oData->offsetGet('navigation_menu_configuration_id'),
                        'navigation_menu_type_id' => $oData->offsetGet('navigation_menu_type_id'),
                        'navigation_option_id' => $oData->offsetGet('navigation_option_id'),
                        'navigation_level_id' => $oData->offsetGet('navigation_level_id'),
                        'navigation_parent_menu_id' => $oData->offsetGet('navigation_parent_menu_id'),
                        'navigation_parent_submenu_id' => $oData->offsetGet('navigation_parent_submenu_id'),
                        'navigation_layout_id' => $oData->offsetGet('navigation_layout_id'),
                        'label_name' => $oData->offsetGet('label_name'),
                        'description' => $oData->offsetGet('description'),
                        'image_id' => $oData->offsetGet('image_id'),
                        'order' => $oData->offsetGet('order'),
                        'active' => $oData->offsetGet('active')
                      ));
                      $oData->offsetSet('navigation_menu_id', $this->getNavigationMenuTable()->addRow($oNavigationMenuEntity->setOptions($oNavigationMenuValue)));
                    }
                    if ($oData->offsetGet('navigation_menu_id')) {
                      $this->getNavigationMenuTable()->commit();
                    }
                  }
                } catch (\Exception $e) {
                  $this->getNavigationMenuTable()->rollBack();
                }
              }
            }
          }
        }
      }
    }
    return $aNavigationMenuId;
  }

  public function actionsAction()
  {
    $oAddNewNavigationActionForm = $this->getServiceLocator()->get('NavigationActionFormService');
    $oEditNavigationActionForm = $this->getServiceLocator()->get('EditNavigationActionFormService');
    return new ViewModel(array('add_new_navigation_action_form' => $oAddNewNavigationActionForm, 'edit_navigation_action_form' => $oEditNavigationActionForm));
  }

  public function addactionajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $oNavigationActionForm = $this->getServiceLocator()->get('NavigationActionFormService');
      $oNavigationActionForm->setData($this->getRequest()->getPost());
      if ($oNavigationActionForm->isValid()) {
        $oNavigationActionEntity = new NavigationActionEntity();
        $oData = new \ArrayObject($oNavigationActionForm->getData());
        $oData->offsetSet('navigation_action_id', $this->getNavigationActionTable()->addRow($oNavigationActionEntity->setOptions($oData)));
        if ($oData->offsetGet('navigation_action_id')) {
          $bSuccess = true;
        }
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function addcontrollerajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $oNavigationControllerForm = $this->getServiceLocator()->get('NavigationControllerFormService');
      $oNavigationControllerForm->setData($this->getRequest()->getPost());
      if ($oNavigationControllerForm->isValid()) {
        $oNavigationControllerEntity = new NavigationControllerEntity();
        $oData = new \ArrayObject($oNavigationControllerForm->getData());
        $oData->offsetSet('navigation_controller_id', $this->getNavigationControllerTable()->addRow($oNavigationControllerEntity->setOptions($oData)));
        if ($oData->offsetGet('navigation_controller_id')) {
          $bSuccess = true;
        }
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function addmenuconfigurationajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $oNavigationMenuConfigurationForm = $this->getServiceLocator()->get('NavigationMenuConfigurationFormService');
      $oNavigationMenuConfigurationForm->setData($this->getRequest()->getPost());
      if ($oNavigationMenuConfigurationForm->isValid()) {
        $oNavigationMenuConfigurationEntity = new NavigationMenuConfigurationEntity();
        $oData = new \ArrayObject($oNavigationMenuConfigurationForm->getData());
        if ((int)$oData->offsetGet('primary') === 1) {
          $oResetData = new \ArrayObject(array('primary' => 0));
          $this->getNavigationMenuConfigurationTable()->editRow($oNavigationMenuConfigurationEntity->setOptions($oResetData, array('primary')));
        }
        $oData->offsetSet('navigation_menu_configuration_id', $this->getNavigationMenuConfigurationTable()->addRow($oNavigationMenuConfigurationEntity->setOptions($oData)));
        if ($oData->offsetGet('navigation_menu_configuration_id')) {
          $bSuccess = true;
        }
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function addmetakeywordajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $oSiteSeoMetaKeywordsForm = $this->getServiceLocator()->get('SiteSeoMetaKeywordsFormService');
      $oSiteSeoMetaKeywordsForm->setData($this->getRequest()->getPost());
      if ($oSiteSeoMetaKeywordsForm->isValid()) {
        $oSiteSeoMetaKeywordsEntity = new SiteSeoMetaKeywordsEntity();
        $oData = new \ArrayObject($oSiteSeoMetaKeywordsForm->getData());
        $oData->offsetSet('navigation_action_id', $this->getSiteSeoMetaKeywordsTable()->addRow($oSiteSeoMetaKeywordsEntity->setOptions($oData)));
        if ($oData->offsetGet('navigation_action_id')) {
          $bSuccess = true;
        }
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function addmoduleajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $oNavigationModuleForm = $this->getServiceLocator()->get('NavigationModuleFormService');
      $oNavigationModuleForm->setData($this->getRequest()->getPost());
      if ($oNavigationModuleForm->isValid()) {
        $oNavigationModuleEntity = new NavigationModuleEntity();
        $oData = new \ArrayObject($oNavigationModuleForm->getData());
        $oData->offsetSet('navigation_module_id', $this->getNavigationModuleTable()->addRow($oNavigationModuleEntity->setOptions($oData)));
        if ($oData->offsetGet('navigation_module_id')) {
          $bSuccess = true;
        }
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function autocompleteactionajaxAction()
  {
    $oResponse = $this->getResponse();
    $aFilter = null;
    $aActions = array();
    $sColumnName = $this->params()->fromQuery('column_name') !== '' ? $this->params()->fromQuery('column_name') : null;
    if (is_array($this->params()->fromQuery('filter')) && count($this->params()->fromQuery('filter'))) {
      $aFilter = $this->params()->fromQuery('filter');
      foreach ($aFilter as $sKey => $sValue) {
        if (strlen(trim($sValue)))
          $aFilter[$sKey] = trim($sValue);
        else
          unset($aFilter[$sKey]);
      }
    }
    if (isset($aFilter)) {
      $aRowset = $this->getNavigationActionTable()->getAll(array('filter' => $aFilter));
      $aLabels = array();
      foreach ($aRowset as $nKey => $oActions) {
        if (!in_array($oActions->$sColumnName, $aLabels)) {
          $aActions[$nKey]['label'] = $oActions->$sColumnName;
          array_push($aLabels, $oActions->$sColumnName);
        }
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode(array('rowset' => $aActions)));
    return $oResponse;
  }

  public function autocompletecontrollerajaxAction()
  {
    $oResponse = $this->getResponse();
    $aFilter = null;
    $aControllers = array();
    $sColumnName = $this->params()->fromQuery('column_name') !== '' ? $this->params()->fromQuery('column_name') : null;
    if (is_array($this->params()->fromQuery('filter')) && count($this->params()->fromQuery('filter'))) {
      $aFilter = $this->params()->fromQuery('filter');
      foreach ($aFilter as $sKey => $sValue) {
        if (strlen(trim($sValue)))
          $aFilter[$sKey] = trim($sValue);
        else
          unset($aFilter[$sKey]);
      }
    }
    if (isset($aFilter)) {
      $aRowset = $this->getNavigationControllerTable()->getAll(array('filter' => $aFilter));
      $aLabels = array();
      foreach ($aRowset as $nKey => $oControllers) {
        if (!in_array($oControllers->$sColumnName, $aLabels)) {
          $aControllers[$nKey]['label'] = $oControllers->$sColumnName;
          array_push($aLabels, $oControllers->$sColumnName);
        }
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode(array('rowset' => $aControllers)));
    return $oResponse;
  }

  public function autocompletemenuconfigurationajaxAction()
  {
    $oResponse = $this->getResponse();
    $aFilter = null;
    $aActions = array();
    $sColumnName = $this->params()->fromQuery('column_name') !== '' ? $this->params()->fromQuery('column_name') : null;
    if (is_array($this->params()->fromQuery('filter')) && count($this->params()->fromQuery('filter'))) {
      $aFilter = $this->params()->fromQuery('filter');
      foreach ($aFilter as $sKey => $sValue) {
        if (strlen(trim($sValue)))
          $aFilter[$sKey] = trim($sValue);
        else
          unset($aFilter[$sKey]);
      }
    }
    if (isset($aFilter)) {
      $aRowset = $this->getNavigationMenuConfigurationTable()->getAll(array('filter' => $aFilter));
      $aLabels = array();
      foreach ($aRowset as $nKey => $oActions) {
        if (!in_array($oActions->$sColumnName, $aLabels)) {
          $aActions[$nKey]['label'] = $oActions->$sColumnName;
          array_push($aLabels, $oActions->$sColumnName);
        }
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode(array('rowset' => $aActions)));
    return $oResponse;
  }

  public function autocompletemetakeywordajaxAction()
  {
    $oResponse = $this->getResponse();
    $aFilter = null;
    $aMetaKeywords = array();
    $sColumnName = $this->params()->fromQuery('column_name') !== '' ? $this->params()->fromQuery('column_name') : null;
    if (is_array($this->params()->fromQuery('filter')) && count($this->params()->fromQuery('filter'))) {
      $aFilter = $this->params()->fromQuery('filter');
      foreach ($aFilter as $sKey => $sValue) {
        if (strlen(trim($sValue)))
          $aFilter[$sKey] = trim($sValue);
        else
          unset($aFilter[$sKey]);
      }
    }
    if (isset($aFilter)) {
      $aRowset = $this->getSiteSeoMetaKeywordsTable()->getAll(array('filter' => $aFilter));
      $aLabels = array();
      foreach ($aRowset as $nKey => $oMetaKeywords) {
        if (!in_array($oMetaKeywords->$sColumnName, $aLabels)) {
          $aMetaKeywords[$nKey]['label'] = $oMetaKeywords->$sColumnName;
          array_push($aLabels, $oMetaKeywords->$sColumnName);
        }
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode(array('rowset' => $aMetaKeywords)));
    return $oResponse;
  }

  public function autocompletemoduleajaxAction()
  {
    $oResponse = $this->getResponse();
    $aFilter = null;
    $aModules = array();
    $sColumnName = $this->params()->fromQuery('column_name') !== '' ? $this->params()->fromQuery('column_name') : null;
    if (is_array($this->params()->fromQuery('filter')) && count($this->params()->fromQuery('filter'))) {
      $aFilter = $this->params()->fromQuery('filter');
      foreach ($aFilter as $sKey => $sValue) {
        if (strlen(trim($sValue)))
          $aFilter[$sKey] = trim($sValue);
        else
          unset($aFilter[$sKey]);
      }
    }
    if (isset($aFilter)) {
      $aRowset = $this->getNavigationModuleTable()->getAll(array('filter' => $aFilter));
      $aLabels = array();
      foreach ($aRowset as $nKey => $oModules) {
        if (!in_array($oModules->$sColumnName, $aLabels)) {
          $aModules[$nKey]['label'] = $oModules->$sColumnName;
          array_push($aLabels, $oModules->$sColumnName);
        }
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode(array('rowset' => $aModules)));
    return $oResponse;
  }

  public function controllersAction()
  {
    $oAddNewNavigationControllerForm = $this->getServiceLocator()->get('NavigationControllerFormService');
    $oEditNavigationControllerForm = $this->getServiceLocator()->get('EditNavigationControllerFormService');
    return new ViewModel(array('add_new_navigation_controller_form' => $oAddNewNavigationControllerForm, 'edit_navigation_controller_form' => $oEditNavigationControllerForm));
  }

  public function deleteactionajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $nId = $this->params()->fromPost('id') !== '' ? (int)$this->params()->fromPost('id') : null;
      if ($nId) {
        $this->getNavigationActionTable()->deleteRow($nId);
        $bSuccess = true;
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function deletecontrollerajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $nId = $this->params()->fromPost('id') !== '' ? (int)$this->params()->fromPost('id') : null;
      if ($nId) {
        $this->getNavigationControllerTable()->deleteRow($nId);
        $bSuccess = true;
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function deletemenuconfigurationajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $nId = $this->params()->fromPost('id') !== '' ? (int)$this->params()->fromPost('id') : null;
      if ($nId) {
        $this->getNavigationMenuConfigurationTable()->deleteRow($nId);
        $bSuccess = true;
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function deletemetakeywordajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $nId = $this->params()->fromPost('id') !== '' ? (int)$this->params()->fromPost('id') : null;
      if ($nId) {
        $this->getSiteSeoMetaKeywordsTable()->deleteRow($nId);
        $bSuccess = true;
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function deletemoduleajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $nId = $this->params()->fromPost('id') !== '' ? (int)$this->params()->fromPost('id') : null;
      if ($nId) {
        $this->getNavigationModuleTable()->deleteRow($nId);
        $bSuccess = true;
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function deletenavigationmenuajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $nId = $this->params()->fromPost('id') !== '' ? (int)$this->params()->fromPost('id') : null;
      if ($nId) {
        $this->getNavigationMenuTable()->deleteRow($nId);
        $bSuccess = true;
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function editcontrollerajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $oNavigationControllerForm = $this->getServiceLocator()->get('NavigationControllerFormService');
      $oNavigationControllerForm->setData($this->getRequest()->getPost());
      if ($oNavigationControllerForm->isValid()) {
        $oNavigationControllerEntity = new NavigationControllerEntity();
        $oData = new \ArrayObject($oNavigationControllerForm->getData());
        $this->getNavigationControllerTable()->editRow($oNavigationControllerEntity->setOptions($oData, array()));
        $bSuccess = true;
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function editemailcontentconfigurationajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $oNavigationActionForm = $this->getServiceLocator()->get('NavigationActionFormService');
      $oNavigationActionForm->setData($this->getRequest()->getPost());
      if ($oNavigationActionForm->isValid()) {
        $oNavigationActionEntity = new NavigationActionEntity();
        $oData = new \ArrayObject($oNavigationActionForm->getData());
        $this->getNavigationActionTable()->editRow($oNavigationActionEntity->setOptions($oData, array()));
        $bSuccess = true;
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function editmenuconfigurationajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $oNavigationMenuConfigurationForm = $this->getServiceLocator()->get('NavigationMenuConfigurationFormService');
      $oNavigationMenuConfigurationForm->setData($this->getRequest()->getPost());
      if ($oNavigationMenuConfigurationForm->isValid()) {
        $oNavigationMenuConfigurationEntity = new NavigationMenuConfigurationEntity();
        $oData = new \ArrayObject($oNavigationMenuConfigurationForm->getData());
        if ((int)$oData->offsetGet('primary') === 1) {
          $oResetData = new \ArrayObject(array('primary' => 0));
          $this->getNavigationMenuConfigurationTable()->editRow($oNavigationMenuConfigurationEntity->setOptions($oResetData, array('primary')));
        }
        $this->getNavigationMenuConfigurationTable()->editRow($oNavigationMenuConfigurationEntity->setOptions($oData, array()));
        $bSuccess = true;
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function editmenuitemsettingsajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $oMenuItemSettingsForm = $this->getServiceLocator()->get('MenuItemSettingsFormService');
      $oMenuItemSettingsForm->setData($this->getRequest()->getPost());
      if ($oMenuItemSettingsForm->isValid()) {
        $oNavigationMenuEntity = new NavigationMenuEntity();
        $oNavigationPrivilegeUserRoleEntity = new NavigationPrivilegeUserRoleEntity();
        $oData = new \ArrayObject($oMenuItemSettingsForm->getData());
        try {
          $this->getNavigationMenuTable()->beginTransaction();
          $nNavigationMenuId = $oData->offsetGet('id');
          $nNavigationOptionId = $this->getNavigationMenuTable()->getRow(array('id' => $nNavigationMenuId))->navigation_option_id;
          /*
          $nNavigationPrivilegeId = $this->getNavigationOptionTable()->getRow(array('id' => $nNavigationOptionId))->navigation_privilege_id;
          if ($oData->offsetGet('user_role_id')) {
            $this->getNavigationPrivilegeUserRoleTable()->deleteRow(array('navigation_privilege_id' => $nNavigationPrivilegeId));
            if (is_array($oData->offsetGet('user_role_id')) && count($oData->offsetGet('user_role_id'))) {
              foreach ($oData->offsetGet('user_role_id') as $nValue) {
                $oNavigationPrivilegeUserRoleValue = new \ArrayObject(array(
                  'navigation_privilege_id' => $nNavigationPrivilegeId,
                  'user_role_id' => $nValue
                ));
                $this->getNavigationPrivilegeUserRoleTable()->addRow($oNavigationPrivilegeUserRoleEntity->setOptions($oNavigationPrivilegeUserRoleValue));
              }
            }
          }
          */
          $this->getNavigationMenuTable()->editRow($oNavigationMenuEntity->setOptions($oData, array('id', 'label_name', 'description', 'active')));
          $this->getNavigationMenuTable()->commit();
          $bSuccess = true;
        } catch (\Exception $e) {
          $bSuccess = $e->getMessage();
          $this->getNavigationMenuTable()->rollBack();
        }
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function editmetakeywordajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $oSiteSeoMetaKeywordsForm = $this->getServiceLocator()->get('SiteSeoMetaKeywordsFormService');
      $oSiteSeoMetaKeywordsForm->setData($this->getRequest()->getPost());
      if ($oSiteSeoMetaKeywordsForm->isValid()) {
        $oSiteSeoMetaKeywordsEntity = new SiteSeoMetaKeywordsEntity();
        $oData = new \ArrayObject($oSiteSeoMetaKeywordsForm->getData());
        $this->getSiteSeoMetaKeywordsTable()->editRow($oSiteSeoMetaKeywordsEntity->setOptions($oData, array()));
        $bSuccess = true;
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function editmoduleajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $oNavigationModuleForm = $this->getServiceLocator()->get('NavigationModuleFormService');
      $oNavigationModuleForm->setData($this->getRequest()->getPost());
      if ($oNavigationModuleForm->isValid()) {
        $oNavigationModuleEntity = new NavigationModuleEntity();
        $oData = new \ArrayObject($oNavigationModuleForm->getData());
        $this->getNavigationModuleTable()->editRow($oNavigationModuleEntity->setOptions($oData, array()));
        $bSuccess = true;
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function editnavigationmenuajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $oNavigationMenuForm = $this->getServiceLocator()->get('NavigationMenuFormService');
      $oNavigationMenuForm->setData($this->getRequest()->getPost());
      if ($oNavigationMenuForm->isValid()) {
        $oNavigationResourceEntity = new NavigationResourceEntity();
        $oNavigationPrivilegeEntity = new NavigationPrivilegeEntity();
        $oNavigationPrivilegeUserRoleEntity = new NavigationPrivilegeUserRoleEntity();
        $oNavigationOptionEntity = new NavigationOptionEntity();
        $oNavigationMenuEntity = new NavigationMenuEntity();
        $oSiteSeoSiteSeoMetaKeywordsEntity = new SiteSeoSiteSeoMetaKeywordsEntity();
        $oSiteSeoEntity = new SiteSeoEntity();
        $oData = new \ArrayObject($oNavigationMenuForm->getData());
        try {
          $this->getNavigationMenuTable()->beginTransaction();
          $nNavigationMenuId = $oData->offsetGet('id');
          $nNavigationOptionId = $this->getNavigationMenuTable()->getRow(array('id' => $nNavigationMenuId))->navigation_option_id;
          $nSiteSeoId = $this->getNavigationMenuTable()->getRow(array('id' => $nNavigationMenuId))->site_seo_id;
          $nNavigationResourceId = $this->getNavigationOptionTable()->getRow(array('id' => $nNavigationOptionId))->navigation_resource_id;
          //$nNavigationPrivilegeId = $this->getNavigationOptionTable()->getRow(array('id' => $nNavigationOptionId))->navigation_privilege_id;
          //if ($nNavigationMenuId && $nNavigationOptionId && $nNavigationResourceId && $nNavigationPrivilegeId) {
          if ($nNavigationMenuId && $nNavigationOptionId && $nNavigationResourceId) {
            if (!is_numeric($oData->offsetGet('navigation_module_id'))) {
              $oData->offsetSet('navigation_module_id', $this->getNavigationModuleTable()->findRow(array('value' => $oData->offsetGet('navigation_module_id'))));
              if (!is_numeric($oData->offsetGet('navigation_module_id'))) {
                throw new \Exception();
              }
            }
            if (!is_numeric($oData->offsetGet('navigation_controller_id'))) {
              $oData->offsetSet('navigation_controller_id', $this->getNavigationControllerTable()->findRow(array('value' => $oData->offsetGet('navigation_controller_id'))));
              if (!is_numeric($oData->offsetGet('navigation_controller_id'))) {
                throw new \Exception();
              }
            }
            if (!is_numeric($oData->offsetGet('navigation_action_id'))) {
              $oData->offsetSet('navigation_action_id', $this->getNavigationActionTable()->findRow(array('value' => $oData->offsetGet('navigation_action_id'))));
              if (!is_numeric($oData->offsetGet('navigation_action_id'))) {
                throw new \Exception();
              }
            }
            $oData->offsetSet('navigation_module_name', $this->getNavigationModuleTable()->getRow(array('id' => $oData->offsetGet('navigation_module_id')))->value);
            $oData->offsetSet('navigation_controller_name', $this->getNavigationControllerTable()->getRow(array('id' => $oData->offsetGet('navigation_controller_id')))->value);
            $oData->offsetSet('navigation_action_name', $this->getNavigationActionTable()->getRow(array('id' => $oData->offsetGet('navigation_action_id')))->value);
            $oNavigationResourceValue = new \ArrayObject(array(
              'id' => $nNavigationResourceId,
              'value' => $oData->offsetGet('navigation_module_name')
            ));
            /*
            $oNavigationPrivilegeValue = new \ArrayObject(array(
              'id' => $nNavigationPrivilegeId,
              'value' => $oData->offsetGet('navigation_controller_name') . ':' . $oData->offsetGet('navigation_action_name'),
              //'unique_key' => $oMakeHash->generate(),
              'unique_key' => md5(
                $oData->offsetGet('navigation_module_name') .
                $oData->offsetGet('navigation_controller_name') .
                $oData->offsetGet('navigation_action_name') .
                $this->getSiteHref($oData->offsetGet('label_name')) .
                'html'
              ),
            ));
            */
            $oNavigationOptionValue = new \ArrayObject(array(
              'id' => $nNavigationOptionId,
              'navigation_module_id' => $oData->offsetGet('navigation_module_id'),
              'navigation_controller_id' => $oData->offsetGet('navigation_controller_id'),
              'navigation_action_id' => $oData->offsetGet('navigation_action_id'),
              //'navigation_privilege_id' => $nNavigationPrivilegeId,
              'navigation_resource_id' => $nNavigationResourceId
            ));
            if (is_numeric($oData->offsetGet('navigation_parent_menu_id'))) {
              $nNavigationParentMenuId = $oData->offsetGet('navigation_parent_menu_id');
            } else if ($oData->offsetGet('navigation_parent_submenu_id')) {
              $nNavigationParentMenuId = $oData->offsetGet('navigation_parent_submenu_id');
            }
            $oNavigationMenuValue = new \ArrayObject(array(
              'id' => $nNavigationMenuId,
              'navigation_menu_configuration_id' => $oData->offsetGet('navigation_menu_configuration_id'),
              'navigation_menu_type_id' => $oData->offsetGet('navigation_menu_type_id'),
              'navigation_option_id' => $nNavigationOptionId,
              'navigation_level_id' => $oData->offsetGet('navigation_level_id'),
              'navigation_parent_menu_id' => $nNavigationParentMenuId,
              'navigation_layout_id' => $oData->offsetGet('navigation_layout_id'),
              'site_seo_id' => $nSiteSeoId,
              'label_name' => $oData->offsetGet('label_name'),
              'description' => $oData->offsetGet('description'),
              'image_id' => $oData->offsetGet('image_id'),
              //'order' => $oData->offsetGet('order'),
              'active' => $oData->offsetGet('active')
            ));
            if ($oData->offsetGet('user_role_id')) {
              $this->getNavigationPrivilegeUserRoleTable()->deleteRow(array('navigation_privilege_id' => $nNavigationPrivilegeId));
              if (is_array($oData->offsetGet('user_role_id')) && count($oData->offsetGet('user_role_id'))) {
                foreach ($oData->offsetGet('user_role_id') as $nValue) {
                  $oNavigationPrivilegeUserRoleValue = new \ArrayObject(array(
                    'navigation_privilege_id' => $nNavigationPrivilegeId,
                    'user_role_id' => $nValue
                  ));
                  $this->getNavigationPrivilegeUserRoleTable()->addRow($oNavigationPrivilegeUserRoleEntity->setOptions($oNavigationPrivilegeUserRoleValue));
                }
              }
            }
            if ($nSiteSeoId) {
              $oSiteSeoValue = new \ArrayObject(array(
                'id' => $nSiteSeoId,
                'site_seo_robots_id' => $oData->offsetGet('site_seo_robots_id'),
                'head_title' => $oData->offsetGet('head_title'),
                'meta_description' => $oData->offsetGet('meta_description'),
              ));
              $this->getSiteSeoTable()->editRow($oSiteSeoEntity->setOptions($oSiteSeoValue, array()));
              $this->getSiteSeoSiteSeoMetaKeywordsTable()->deleteRow(array('site_seo_id' => $nSiteSeoId));
              if (is_array($oData->offsetGet('site_seo_meta_keywords_id')) && count($oData->offsetGet('site_seo_meta_keywords_id'))) {
                foreach ($oData->offsetGet('site_seo_meta_keywords_id') as $nValue) {
                  $oSiteSeoSiteSeoMetaKeywordsValue = new \ArrayObject(array(
                    'site_seo_id' => $nSiteSeoId,
                    'site_seo_meta_keywords_id' => $nValue,
                  ));
                  $this->getSiteSeoSiteSeoMetaKeywordsTable()->addRow($oSiteSeoSiteSeoMetaKeywordsEntity->setOptions($oSiteSeoSiteSeoMetaKeywordsValue));
                }
              }
            } else {
              $oSiteSeoValue = new \ArrayObject(array(
                'site_seo_robots_id' => $oData->offsetGet('site_seo_robots_id'),
                'head_title' => $oData->offsetGet('head_title'),
                'meta_description' => $oData->offsetGet('meta_description'),
              ));
              $oData->offsetSet('site_seo_id', $this->getSiteSeoTable()->addRow($oSiteSeoEntity->setOptions($oSiteSeoValue)));
              if ($oData->offsetGet('site_seo_id') && is_array($oData->offsetGet('site_seo_meta_keywords_id')) && count($oData->offsetGet('site_seo_meta_keywords_id'))) {
                foreach ($oData->offsetGet('site_seo_meta_keywords_id') as $nValue) {
                  $oSiteSeoSiteSeoMetaKeywordsValue = new \ArrayObject(array(
                    'site_seo_id' => $oData->offsetGet('site_seo_id'),
                    'site_seo_meta_keywords_id' => $nValue,
                  ));
                  $this->getSiteSeoSiteSeoMetaKeywordsTable()->addRow($oSiteSeoSiteSeoMetaKeywordsEntity->setOptions($oSiteSeoSiteSeoMetaKeywordsValue));
                }
              }
              $oNavigationMenuValue->offsetSet('site_seo_id', $oData->offsetGet('site_seo_id'));
            }
            $this->getNavigationResourceTable()->editRow($oNavigationResourceEntity->setOptions($oNavigationResourceValue, array()));
            //$this->getNavigationPrivilegeTable()->editRow($oNavigationPrivilegeEntity->setOptions($oNavigationPrivilegeValue, array()));
            $this->getNavigationOptionTable()->editRow($oNavigationOptionEntity->setOptions($oNavigationOptionValue, array()));
            $this->getNavigationMenuTable()->editRow($oNavigationMenuEntity->setOptions($oNavigationMenuValue, array()));
            $this->getNavigationMenuTable()->commit();
            $bSuccess = true;
          }
        } catch (\Exception $e) {
          $this->getNavigationMenuTable()->rollBack();
        }
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function indexAction()
  {
    return new ViewModel();
  }

  public function loadactionsajaxAction()
  {
    if ($this->getRequest()->isPost()) {
      $oResponse = $this->getResponse();
      $aSort = null;
      $aFilter = null;
      if (is_array($this->params()->fromPost('filter')) && count($this->params()->fromPost('filter'))) {
        $aFilter = $this->params()->fromPost('filter');
        foreach ($aFilter as $sKey => $sValue) {
          if (strlen(trim($sValue)))
            $aFilter[$sKey] = trim($sValue);
          else
            unset($aFilter[$sKey]);
        }
      }
      $sSortColumn = $this->params()->fromPost('sort_column') !== '' ? $this->params()->fromPost('sort_column') : 'id';
      $sSortMethod = $this->params()->fromPost('sort_method') !== '' ? $this->params()->fromPost('sort_method') : 'asc';
      $nLimit = $this->params()->fromPost('limit');
      $nOffset = $this->params()->fromPost('offset');
      if (isset($sSortColumn) && isset($sSortMethod))
        $aSort = array('sort_column' => $sSortColumn, 'sort_method' => $sSortMethod);
      if (isset($nLimit) && is_numeric($nLimit)) {
        $nLimit = (int)$nLimit;
      } else {
        $nLimit = 20;
      }
      if (isset($nOffset) && is_numeric($nOffset)) {
        $nOffset = (int)$nOffset;
      } else {
        $nOffset = 0;
      }
      $aActions = $this->getNavigationActionTable()->getAll(array('filter' => $aFilter, 'sort' => $aSort, 'limit' => $nLimit, 'offset' => $nOffset));
      $nCount = count($this->getNavigationActionTable()->getAll(array('filter' => $aFilter, 'sort' => $aSort)));
      $oResponse->setContent(\Zend\Json\Json::encode(array('rowset' => $aActions, 'num_rows' => $nCount)));
      return $oResponse;
    }
  }

  public function loadcontrollersajaxAction()
  {
    if ($this->getRequest()->isPost()) {
      $oResponse = $this->getResponse();
      $aSort = null;
      $aFilter = null;
      if (is_array($this->params()->fromPost('filter')) && count($this->params()->fromPost('filter'))) {
        $aFilter = $this->params()->fromPost('filter');
        foreach ($aFilter as $sKey => $sValue) {
          if (strlen(trim($sValue)))
            $aFilter[$sKey] = trim($sValue);
          else
            unset($aFilter[$sKey]);
        }
      }
      $sSortColumn = $this->params()->fromPost('sort_column') !== '' ? $this->params()->fromPost('sort_column') : 'id';
      $sSortMethod = $this->params()->fromPost('sort_method') !== '' ? $this->params()->fromPost('sort_method') : 'asc';
      $nLimit = $this->params()->fromPost('limit');
      $nOffset = $this->params()->fromPost('offset');
      if (isset($sSortColumn) && isset($sSortMethod))
        $aSort = array('sort_column' => $sSortColumn, 'sort_method' => $sSortMethod);
      if (isset($nLimit) && is_numeric($nLimit)) {
        $nLimit = (int)$nLimit;
      } else {
        $nLimit = 20;
      }
      if (isset($nOffset) && is_numeric($nOffset)) {
        $nOffset = (int)$nOffset;
      } else {
        $nOffset = 0;
      }
      $aControllers = $this->getNavigationControllerTable()->getAll(array('filter' => $aFilter, 'sort' => $aSort, 'limit' => $nLimit, 'offset' => $nOffset));
      $nCount = count($this->getNavigationControllerTable()->getAll(array('filter' => $aFilter, 'sort' => $aSort)));
      $oResponse->setContent(\Zend\Json\Json::encode(array('rowset' => $aControllers, 'num_rows' => $nCount)));
      return $oResponse;
    }
  }

  public function loadmenuconfigurationsajaxAction()
  {
    if ($this->getRequest()->isPost()) {
      $oResponse = $this->getResponse();
      $aSort = null;
      $aFilter = null;
      if (is_array($this->params()->fromPost('filter')) && count($this->params()->fromPost('filter'))) {
        $aFilter = $this->params()->fromPost('filter');
        foreach ($aFilter as $sKey => $sValue) {
          if (strlen(trim($sValue)))
            $aFilter[$sKey] = trim($sValue);
          else
            unset($aFilter[$sKey]);
        }
      }
      $sSortColumn = $this->params()->fromPost('sort_column') !== '' ? $this->params()->fromPost('sort_column') : 'id';
      $sSortMethod = $this->params()->fromPost('sort_method') !== '' ? $this->params()->fromPost('sort_method') : 'asc';
      $nLimit = $this->params()->fromPost('limit');
      $nOffset = $this->params()->fromPost('offset');
      if (isset($sSortColumn) && isset($sSortMethod))
        $aSort = array('sort_column' => $sSortColumn, 'sort_method' => $sSortMethod);
      if (isset($nLimit) && is_numeric($nLimit)) {
        $nLimit = (int)$nLimit;
      } else {
        $nLimit = 20;
      }
      if (isset($nOffset) && is_numeric($nOffset)) {
        $nOffset = (int)$nOffset;
      } else {
        $nOffset = 0;
      }
      $aMenuConfigurations = $this->getNavigationMenuConfigurationTable()->getAll(array('filter' => $aFilter, 'sort' => $aSort, 'limit' => $nLimit, 'offset' => $nOffset));
      $nCount = count($this->getNavigationMenuConfigurationTable()->getAll(array('filter' => $aFilter, 'sort' => $aSort)));
      $oResponse->setContent(\Zend\Json\Json::encode(array('rowset' => $aMenuConfigurations, 'num_rows' => $nCount)));
      return $oResponse;
    }
  }

  public function loadmenuitemsettingsajaxAction()
  {
    if ($this->getRequest()->isPost()) {
      $oResponse = $this->getResponse();
      $aNavigationMenuId = $this->repairNavigationMenu(true, true);
      $aSort = null;
      $aFilter = null;
      if (is_array($this->params()->fromPost('filter')) && count($this->params()->fromPost('filter'))) {
        $aFilter = $this->params()->fromPost('filter');
        foreach ($aFilter as $sKey => $sValue) {
          if (strlen(trim($sValue)))
            $aFilter[$sKey] = trim($sValue);
          else
            unset($aFilter[$sKey]);
        }
      }
      $sSortColumn = $this->params()->fromPost('sort_column') !== '' ? $this->params()->fromPost('sort_column') : 'id';
      $sSortMethod = $this->params()->fromPost('sort_method') !== '' ? $this->params()->fromPost('sort_method') : 'asc';
      $nLimit = $this->params()->fromPost('limit');
      $nOffset = $this->params()->fromPost('offset');
      if (isset($sSortColumn) && isset($sSortMethod))
        $aSort = array('sort_column' => $sSortColumn, 'sort_method' => $sSortMethod);
      if (isset($nLimit) && is_numeric($nLimit)) {
        $nLimit = (int)$nLimit;
      } else {
        $nLimit = 20;
      }
      if (isset($nOffset) && is_numeric($nOffset)) {
        $nOffset = (int)$nOffset;
      } else {
        $nOffset = 0;
      }
      $aMenu = $this->getNavigationMenuTable()->getAll(array('filter' => $aFilter, 'sort' => $aSort));
      foreach ($aMenu as $nKey => $oValue) {
        //$oValue->user_role_id = $this->getNavigationPrivilegeUserRoleTable()->getAll(array('navigation_privilege_id' => $oValue->navigation_privilege_id));
        $oValue->site_seo_meta_keywords_id = $this->getSiteSeoSiteSeoMetaKeywordsTable()->getAll(array('site_seo_id' => $oValue->site_seo_id));
      }
      $nCount = count($this->getNavigationMenuTable()->getAll(array('filter' => $aFilter, 'sort' => $aSort)));
      $oResponse->setContent(\Zend\Json\Json::encode(array('rowset' => $aMenu, 'num_rows' => $nCount, 'navigation_menu_id' => $aNavigationMenuId)));
      return $oResponse;
    }
  }

  public function loadmetakeywordsajaxAction()
  {
    if ($this->getRequest()->isPost()) {
      $oResponse = $this->getResponse();
      $aSort = null;
      $aFilter = null;
      if (is_array($this->params()->fromPost('filter')) && count($this->params()->fromPost('filter'))) {
        $aFilter = $this->params()->fromPost('filter');
        foreach ($aFilter as $sKey => $sValue) {
          if (strlen(trim($sValue)))
            $aFilter[$sKey] = trim($sValue);
          else
            unset($aFilter[$sKey]);
        }
      }
      $sSortColumn = $this->params()->fromPost('sort_column') !== '' ? $this->params()->fromPost('sort_column') : 'id';
      $sSortMethod = $this->params()->fromPost('sort_method') !== '' ? $this->params()->fromPost('sort_method') : 'asc';
      $nLimit = $this->params()->fromPost('limit');
      $nOffset = $this->params()->fromPost('offset');
      if (isset($sSortColumn) && isset($sSortMethod))
        $aSort = array('sort_column' => $sSortColumn, 'sort_method' => $sSortMethod);
      if (isset($nLimit) && is_numeric($nLimit)) {
        $nLimit = (int)$nLimit;
      } else {
        $nLimit = 20;
      }
      if (isset($nOffset) && is_numeric($nOffset)) {
        $nOffset = (int)$nOffset;
      } else {
        $nOffset = 0;
      }
      $aMetaKeywords = $this->getSiteSeoMetaKeywordsTable()->getAll(array('filter' => $aFilter, 'sort' => $aSort, 'limit' => $nLimit, 'offset' => $nOffset));
      $nCount = count($this->getSiteSeoMetaKeywordsTable()->getAll(array('filter' => $aFilter, 'sort' => $aSort)));
      $oResponse->setContent(\Zend\Json\Json::encode(array('rowset' => $aMetaKeywords, 'num_rows' => $nCount)));
      return $oResponse;
    }
  }

  public function loadmodulesajaxAction()
  {
    if ($this->getRequest()->isPost()) {
      $oResponse = $this->getResponse();
      $aSort = null;
      $aFilter = null;
      if (is_array($this->params()->fromPost('filter')) && count($this->params()->fromPost('filter'))) {
        $aFilter = $this->params()->fromPost('filter');
        foreach ($aFilter as $sKey => $sValue) {
          if (strlen(trim($sValue)))
            $aFilter[$sKey] = trim($sValue);
          else
            unset($aFilter[$sKey]);
        }
      }
      $sSortColumn = $this->params()->fromPost('sort_column') !== '' ? $this->params()->fromPost('sort_column') : 'id';
      $sSortMethod = $this->params()->fromPost('sort_method') !== '' ? $this->params()->fromPost('sort_method') : 'asc';
      $nLimit = $this->params()->fromPost('limit');
      $nOffset = $this->params()->fromPost('offset');
      if (isset($sSortColumn) && isset($sSortMethod))
        $aSort = array('sort_column' => $sSortColumn, 'sort_method' => $sSortMethod);
      if (isset($nLimit) && is_numeric($nLimit)) {
        $nLimit = (int)$nLimit;
      } else {
        $nLimit = 20;
      }
      if (isset($nOffset) && is_numeric($nOffset)) {
        $nOffset = (int)$nOffset;
      } else {
        $nOffset = 0;
      }
      $aModules = $this->getNavigationModuleTable()->getAll(array('filter' => $aFilter, 'sort' => $aSort, 'limit' => $nLimit, 'offset' => $nOffset));
      $nCount = count($this->getNavigationModuleTable()->getAll(array('filter' => $aFilter, 'sort' => $aSort)));
      $oResponse->setContent(\Zend\Json\Json::encode(array('rowset' => $aModules, 'num_rows' => $nCount)));
    }
    return $oResponse;
  }

  public function menuAction()
  {
    $oApp = $this->getServiceLocator()->get('AppService');
    $oAclSessionContainer = new \User\Model\AclSessionContainer();
    $oAclSessionContainer->setIsInit(0);
    $this->repairNavigationMenu(true, true, true);
    $oMenuItemSettingsForm = $this->getServiceLocator()->get('MenuItemSettingsFormService');
    $oChangeMenuConfigurationForm = $this->getServiceLocator()->get('ChangeMenuConfigurationFormService');
    $oAddNewNavigationMenuForm = $this->getServiceLocator()->get('NavigationMenuFormService');
    $oEditNavigationMenuForm = $this->getServiceLocator()->get('EditNavigationMenuFormService');
    $oAddNewNavigationMenuConfigurationForm = $this->getServiceLocator()->get('NavigationMenuConfigurationFormService');
    $oEditNavigationMenuConfigurationForm = $this->getServiceLocator()->get('EditNavigationMenuConfigurationFormService');
    $oAddNewSiteSeoMetaKeywordsForm = $this->getServiceLocator()->get('SiteSeoMetaKeywordsFormService');
    $oEditSiteSeoMetaKeywordsForm = $this->getServiceLocator()->get('EditSiteSeoMetaKeywordsFormService');
    $aNavigationMenu = array();
    $nNavigationMenuConfigurationId = null;
    $nNavigationModuleId = null;
    $aSort = array('sort_column' => 'order', 'sort_method' => 'asc');
    if ($this->getRequest()->isPost() && $this->params()->fromPost('form_name') === 'change_menu_configuration') {
      $oChangeMenuConfigurationForm->setData($this->getRequest()->getPost());
      if ($oChangeMenuConfigurationForm->isValid()) {
        $nNavigationMenuConfigurationId = (int)$this->params()->fromPost('navigation_menu_configuration_id');
        $nNavigationModuleId = (int)$this->params()->fromPost('navigation_module_id');
      }
    } else {
      $nNavigationMenuConfigurationId = $this->getNavigationMenuConfigurationTable()->getRow()->id;
      $oChangeMenuConfigurationForm->setData(array('navigation_menu_configuration_id' => $nNavigationMenuConfigurationId));
    }
    if ($nNavigationMenuConfigurationId && $nNavigationModuleId) {
      $aMenu = $this->getNavigationMenuTable()->getAll(array(
          'navigation_menu_configuration_id' => $nNavigationMenuConfigurationId,
          'navigation_level_id' => 1,
          'navigation_module_id' => $nNavigationModuleId,
          'sort' => $aSort
        )
      );
    } else if ($nNavigationMenuConfigurationId && !$nNavigationModuleId) {
      $aMenu = $this->getNavigationMenuTable()->getAll(array(
          'navigation_menu_configuration_id' => $nNavigationMenuConfigurationId,
          'navigation_level_id' => 1,
          'sort' => $aSort
        )
      );
    } else if (!$nNavigationMenuConfigurationId && $nNavigationModuleId) {
      $aMenu = $this->getNavigationMenuTable()->getAll(array(
          'navigation_level_id' => 1,
          'navigation_module_id' => $nNavigationModuleId,
          'sort' => $aSort
        )
      );
    } else {
      $aMenu = $this->getNavigationMenuTable()->getAll(array(
          'navigation_level_id' => 1,
          'sort' => $aSort
        )
      );
    }
    if (count($aMenu)) {
      foreach ($aMenu as $oMenu) {
        $aNavigationMenu[$oMenu->id] = array('id' => $oMenu->id, 'navigation_level_id' => $oMenu->navigation_level_id, 'label_name' => $oMenu->label_name);
        $aSubmenu = $this->getNavigationMenuTable()->getAll(array('navigation_parent_menu_id' => $oMenu->id, 'sort' => $aSort));
        if (count($aSubmenu)) {
          foreach ($aSubmenu as $oSubmenu) {
            $aNavigationMenu[$oMenu->id]['submenu'][$oSubmenu->id] = array('id' => $oSubmenu->id, 'navigation_level_id' => $oSubmenu->navigation_level_id, 'label_name' => $oSubmenu->label_name);
            $aSubsubmenu = $this->getNavigationMenuTable()->getAll(array('navigation_parent_menu_id' => $oSubmenu->id, 'sort' => $aSort));
            if (count($aSubsubmenu)) {
              foreach ($aSubsubmenu as $oSubsubmenu) {
                $aNavigationMenu[$oMenu->id]['submenu'][$oSubmenu->id]['subsubmenu'][$oSubsubmenu->id] = array('id' => $oSubsubmenu->id, 'navigation_level_id' => $oSubsubmenu->navigation_level_id, 'label_name' => $oSubsubmenu->label_name);
              }
            }
          }
        }
      }
    }
    if ($this->getRequest()->isPost() && $this->params()->fromPost('form_name') === 'navigation_menu') {
      try {
        $oAddNewNavigationMenuForm->setData($this->getRequest()->getPost());
        if ($oAddNewNavigationMenuForm->isValid()) {
          $oNavigationResourceEntity = new NavigationResourceEntity();
          $oNavigationPrivilegeEntity = new NavigationPrivilegeEntity();
          $oNavigationPrivilegeUserRoleEntity = new NavigationPrivilegeUserRoleEntity();
          $oNavigationOptionEntity = new NavigationOptionEntity();
          $oNavigationMenuEntity = new NavigationMenuEntity();
          $oSiteSeoEntity = new SiteSeoEntity();
          $oSiteSeoMetaKeywordsEntity = new SiteSeoMetaKeywordsEntity();
          $oSiteSeoSiteSeoMetaKeywordsEntity = new SiteSeoSiteSeoMetaKeywordsEntity();
          $oData = new \ArrayObject($oAddNewNavigationMenuForm->getData());
          $oMakeHash = new MakeHash();
          try {
            $this->getNavigationMenuTable()->beginTransaction();
            if (!is_numeric($oData->offsetGet('navigation_module_id'))) {
              $oData->offsetSet('navigation_module_id', $this->getNavigationModuleTable()->findRow(array('value' => $oData->offsetGet('navigation_module_id'))));
              if (!is_numeric($oData->offsetGet('navigation_module_id'))) {
                throw new \Exception();
              }
            }
            if (!is_numeric($oData->offsetGet('navigation_controller_id'))) {
              $oData->offsetSet('navigation_controller_id', $this->getNavigationControllerTable()->findRow(array('value' => $oData->offsetGet('navigation_controller_id'))));
              if (!is_numeric($oData->offsetGet('navigation_controller_id'))) {
                throw new \Exception();
              }
            }
            if (!is_numeric($oData->offsetGet('navigation_action_id'))) {
              $oData->offsetSet('navigation_action_id', $this->getNavigationActionTable()->findRow(array('value' => $oData->offsetGet('navigation_action_id'))));
              if (!is_numeric($oData->offsetGet('navigation_action_id'))) {
                throw new \Exception();
              }
            }
            $oData->offsetSet('navigation_module_name', $this->getNavigationModuleTable()->getRow(array('id' => $oData->offsetGet('navigation_module_id')))->value);
            $oData->offsetSet('navigation_controller_name', $this->getNavigationControllerTable()->getRow(array('id' => $oData->offsetGet('navigation_controller_id')))->value);
            $oData->offsetSet('navigation_action_name', $this->getNavigationActionTable()->getRow(array('id' => $oData->offsetGet('navigation_action_id')))->value);
            $oNavigationResourceValue = new \ArrayObject(array(
              'value' => $oData->offsetGet('navigation_module_name')
            ));
            $oNavigationPrivilegeValue = new \ArrayObject(array(
              'value' => $oData->offsetGet('navigation_controller_name') . ':' . $oData->offsetGet('navigation_action_name'),
              //'unique_key' => $oMakeHash->generate(),
              'unique_key' => md5(
                $oData->offsetGet('navigation_module_name') .
                $oData->offsetGet('navigation_controller_name') .
                $oData->offsetGet('navigation_action_name') .
                $this->getSiteHref($oData->offsetGet('label_name')) . 'html'
              )
            ));
            $oData->offsetSet('navigation_resource_id', $this->getNavigationResourceTable()->addRow($oNavigationResourceEntity->setOptions($oNavigationResourceValue)));
            $oData->offsetSet('navigation_privilege_id', $this->getNavigationPrivilegeTable()->addRow($oNavigationPrivilegeEntity->setOptions($oNavigationPrivilegeValue)));
            if (false && $oData->offsetGet('navigation_privilege_id') && is_array($oData->offsetGet('user_role_id')) && count($oData->offsetGet('user_role_id'))) {
              foreach ($oData->offsetGet('user_role_id') as $nValue) {
                $oNavigationPrivilegeUserRoleValue = new \ArrayObject(array(
                  'navigation_privilege_id' => $oData->offsetGet('navigation_privilege_id'),
                  'user_role_id' => $nValue
                ));
                $this->getNavigationPrivilegeUserRoleTable()->addRow($oNavigationPrivilegeUserRoleEntity->setOptions($oNavigationPrivilegeUserRoleValue));
              }
            }
            if ($oData->offsetGet('navigation_resource_id') && $oData->offsetGet('navigation_privilege_id')) {
              $oNavigationOptionValue = new \ArrayObject(array(
                'navigation_module_id' => $oData->offsetGet('navigation_module_id'),
                'navigation_controller_id' => $oData->offsetGet('navigation_controller_id'),
                'navigation_action_id' => $oData->offsetGet('navigation_action_id'),
                'navigation_resource_id' => $oData->offsetGet('navigation_resource_id'),
                'navigation_privilege_id' => $oData->offsetGet('navigation_privilege_id'),
              ));
              $oData->offsetSet('navigation_option_id', $this->getNavigationOptionTable()->addRow($oNavigationOptionEntity->setOptions($oNavigationOptionValue)));
              if ($oData->offsetGet('navigation_option_id')) {
                $oSiteSeoValue = new \ArrayObject(array(
                  'site_seo_robots_id' => $oData->offsetGet('site_seo_robots_id'),
                  'head_title' => $oData->offsetGet('head_title'),
                  'meta_description' => $oData->offsetGet('meta_description'),
                ));
                $oData->offsetSet('site_seo_id', $this->getSiteSeoTable()->addRow($oSiteSeoEntity->setOptions($oSiteSeoValue)));
                if ($oData->offsetGet('site_seo_id') && is_array($oData->offsetGet('site_seo_meta_keywords_id')) && count($oData->offsetGet('site_seo_meta_keywords_id'))) {
                  foreach ($oData->offsetGet('site_seo_meta_keywords_id') as $nValue) {
                    $oSiteSeoSiteSeoMetaKeywordsValue = new \ArrayObject(array(
                      'site_seo_id' => $oData->offsetGet('site_seo_id'),
                      'site_seo_meta_keywords_id' => $nValue,
                    ));
                    $this->getSiteSeoSiteSeoMetaKeywordsTable()->addRow($oSiteSeoSiteSeoMetaKeywordsEntity->setOptions($oSiteSeoSiteSeoMetaKeywordsValue));
                  }
                }
                $oNavigationMenuValue = new \ArrayObject(array(
                  'navigation_menu_configuration_id' => $oData->offsetGet('navigation_menu_configuration_id'),
                  'navigation_menu_type_id' => $oData->offsetGet('navigation_menu_type_id'),
                  'navigation_option_id' => $oData->offsetGet('navigation_option_id'),
                  'navigation_level_id' => $oData->offsetGet('navigation_level_id'),
                  'navigation_parent_menu_id' => $oData->offsetGet('navigation_parent_menu_id'),
                  'navigation_parent_submenu_id' => $oData->offsetGet('navigation_parent_submenu_id'),
                  'navigation_layout_id' => $oData->offsetGet('navigation_layout_id'),
                  'site_seo_id' => $oData->offsetGet('site_seo_id'),
                  'label_name' => $oData->offsetGet('label_name'),
                  'description' => $oData->offsetGet('description'),
                  'image_id' => $oData->offsetExists('image_id') ? $oData->offsetGet('image_id') : null,
                  //'order' => $oData->offsetExists('order') ? $oData->offsetGet('order') : null,
                  'active' => $oData->offsetGet('active')
                ));
                $oData->offsetSet('navigation_menu_id', $this->getNavigationMenuTable()->addRow($oNavigationMenuEntity->setOptions($oNavigationMenuValue)));
              }
              if ($oData->offsetGet('navigation_menu_id')) {
                $this->getNavigationMenuTable()->commit();
                $this->redirect()->toRoute('application_navigation/application_navigation_process', array('action' => 'menu'), array());
              }
            }
          } catch (\Exception $e) {
            $this->getNavigationMenuTable()->rollBack();
            $oApp->dump(array('debug' => $e->getMessage()));
            exit;
          }
        }
      } catch (\Exception $e) {
        \Zend\Debug\Debug::dump($e->getMessage());
        $oApp->dump(array('debug' => $e->getMessage()));
        exit;
      }
    }
    return new ViewModel(
      array(
        'navigation_menu' => $aNavigationMenu,
        'add_new_navigation_menu_form' => $oAddNewNavigationMenuForm,
        'edit_navigation_menu_form' => $oEditNavigationMenuForm,
        'menu_item_settings' => $oMenuItemSettingsForm,
        'change_menu_configuration_form' => $oChangeMenuConfigurationForm,
        'add_new_navigation_menu_configuration_form' => $oAddNewNavigationMenuConfigurationForm,
        'edit_navigation_menu_configuration_form' => $oEditNavigationMenuConfigurationForm,
        'add_new_site_seo_meta_keyword_form' => $oAddNewSiteSeoMetaKeywordsForm,
        'edit_site_seo_meta_keyword_form' => $oEditSiteSeoMetaKeywordsForm,
      )
    );
  }

  public function menuconfigurationAction()
  {
    $oAddNewNavigationMenuConfigurationForm = $this->getServiceLocator()->get('NavigationMenuConfigurationFormService');
    $oEditNavigationMenuConfigurationForm = $this->getServiceLocator()->get('EditNavigationMenuConfigurationFormService');
    return new ViewModel(array('add_new_navigation_menu_configuration_form' => $oAddNewNavigationMenuConfigurationForm, 'edit_navigation_menu_configuration_form' => $oEditNavigationMenuConfigurationForm));
  }

  public function metakeywordsAction()
  {
    $oAddNewSiteSeoMetaKeywordsForm = $this->getServiceLocator()->get('SiteSeoMetaKeywordsFormService');
    $oEditSiteSeoMetaKeywordsForm = $this->getServiceLocator()->get('EditSiteSeoMetaKeywordsFormService');
    return new ViewModel(array('add_new_site_seo_meta_keyword_form' => $oAddNewSiteSeoMetaKeywordsForm, 'edit_site_seo_meta_keyword_form' => $oEditSiteSeoMetaKeywordsForm));
  }

  public function modulesAction()
  {
    $oAddNewNavigationModuleForm = $this->getServiceLocator()->get('NavigationModuleFormService');
    $oEditNavigationModuleForm = $this->getServiceLocator()->get('EditNavigationModuleFormService');
    return new ViewModel(array('add_new_navigation_module_form' => $oAddNewNavigationModuleForm, 'edit_navigation_module_form' => $oEditNavigationModuleForm));
  }

  public function savenavigationmenuschemeajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $aPostData = $this->getRequest()->getPost();
      $oNavigationMenuEntity = new NavigationMenuEntity();
      $nIdx = 0;
      if (is_array($aPostData['order']) && count($aPostData['order'])) {
        foreach ($aPostData['order'] as $aMenu) {
          $nIdx++;
          $oData = new \ArrayObject(array('id' => $aMenu['id'], 'navigation_level_id' => 1, 'navigation_parent_menu_id' => null, 'order' => $nIdx));
          $this->getNavigationMenuTable()->editRow($oNavigationMenuEntity->setOptions($oData, array('id', 'navigation_level_id', 'navigation_parent_menu_id', 'order')));
          if (isset($aMenu['children'])) {
            foreach ($aMenu['children'] as $aSubmenu) {
              $nIdx++;
              $oData = new \ArrayObject(array('id' => $aSubmenu['id'], 'navigation_level_id' => 2, 'navigation_parent_menu_id' => $aMenu['id'], 'order' => $nIdx));
              $this->getNavigationMenuTable()->editRow($oNavigationMenuEntity->setOptions($oData, array('id', 'navigation_level_id', 'navigation_parent_menu_id', 'order')));
              if (isset($aSubmenu['children'])) {
                foreach ($aSubmenu['children'] as $aSubsubmenu) {
                  $nIdx++;
                  $oData = new \ArrayObject(array('id' => $aSubsubmenu['id'], 'navigation_level_id' => 3, 'navigation_parent_menu_id' => $aSubmenu['id'], 'order' => $nIdx));
                  $this->getNavigationMenuTable()->editRow($oNavigationMenuEntity->setOptions($oData, array('id', 'navigation_level_id', 'navigation_parent_menu_id', 'order')));
                }
              }
            }
          }
        }
        $bSuccess = true;
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function secendoptionmenuconfigurationajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $sSecondOption = $this->params()->fromPost('second_opinion');
      $sColumnName = $this->params()->fromPost('column_name');
      $nId = $this->params()->fromPost('id') !== '' ? (int)$this->params()->fromPost('id') : null;
      $nResetAll = $this->params()->fromPost('reset_all') !== '' ? (int)$this->params()->fromPost('reset_all') : null;
      if ($nId) {
        $oNavigationMenuConfigurationEntity = new NavigationMenuConfigurationEntity();
        switch ($sSecondOption) {
          case 'enable_disable':
            $oMenuConfiguration = $this->getNavigationMenuConfigurationTable()->getRow(array('id' => $nId));
            if (isset($oMenuConfiguration->$sColumnName)) {
              if ($nResetAll && $oMenuConfiguration->$sColumnName === 0) {
                $oResetData = new \ArrayObject(array($sColumnName => 0));
                $this->getNavigationMenuConfigurationTable()->editRow($oNavigationMenuConfigurationEntity->setOptions($oResetData, array($sColumnName)));
              }
              if ($oMenuConfiguration->$sColumnName === 1) {
                $oData = new \ArrayObject(array('id' => $nId, $sColumnName => 0));
                $this->getNavigationMenuConfigurationTable()->editRow($oNavigationMenuConfigurationEntity->setOptions($oData, array('id', $sColumnName)));
                $bSuccess = true;
              } else if ($oMenuConfiguration->$sColumnName === 0) {
                $oData = new \ArrayObject(array('id' => $nId, $sColumnName => 1));
                $this->getNavigationMenuConfigurationTable()->editRow($oNavigationMenuConfigurationEntity->setOptions($oData, array('id', $sColumnName)));
                $bSuccess = true;
              }
            }
            break;
        }
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function secendoptionmetakeywordajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $sSecendOption = $this->params()->fromPost('second_opinion');
      $sColumnName = $this->params()->fromPost('column_name');
      $nId = $this->params()->fromPost('id') !== '' ? (int)$this->params()->fromPost('id') : null;
      $nResetAll = $this->params()->fromPost('reset_all') !== '' ? (int)$this->params()->fromPost('reset_all') : null;
      if ($nId) {
        $oNavigationMenuConfigurationEntity = new NavigationMenuConfigurationEntity();
        switch ($sSecendOption) {
          case 'enable_disable':
            $oMenuConfiguration = $this->getSiteSeoMetaKeywordsTable()->getRow(array('id' => $nId));
            if (isset($oMenuConfiguration->$sColumnName)) {
              if ($nResetAll && $oMenuConfiguration->$sColumnName === 0) {
                //$oResetData = new \ArrayObject(array($sColumnName => 0));
                //$this->getSiteSeoMetaKeywordsTable()->editRow($oNavigationMenuConfigurationEntity->setOptions($oResetData, array($sColumnName)));
              }
              if ($oMenuConfiguration->$sColumnName === 1) {
                $oData = new \ArrayObject(array('id' => $nId, $sColumnName => 0));
                $this->getSiteSeoMetaKeywordsTable()->editRow($oNavigationMenuConfigurationEntity->setOptions($oData, array('id', $sColumnName)));
                $bSuccess = true;
              } else if ($oMenuConfiguration->$sColumnName === 0) {
                $oData = new \ArrayObject(array('id' => $nId, $sColumnName => 1));
                $this->getSiteSeoMetaKeywordsTable()->editRow($oNavigationMenuConfigurationEntity->setOptions($oData, array('id', $sColumnName)));
                $bSuccess = true;
              }
            }
            break;
        }
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }
}
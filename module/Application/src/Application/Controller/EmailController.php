<?php
namespace Application\Controller;

use Application\Model\Entity\EmailConfiguration as EmailConfigurationEntity;
use Application\Model\Entity\EmailContentConfiguration as EmailContentConfigurationEntity;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class EmailController extends AbstractActionController
{
  protected $_oEmailContentConfigurationTable;

  protected function getEmailConfigurationTable()
  {
    if (!$this->_oEmailConfigurationTable && $this->getServiceLocator()->has('EmailConfigurationTableService')) {
      $this->_oEmailConfigurationTable = $this->getServiceLocator()->get('EmailConfigurationTableService');
    }
    if (!isset($this->_oEmailConfigurationTable)) {
      throw \Exception();
    }
    return $this->_oEmailConfigurationTable;
  }

  protected function getEmailContentConfigurationTable()
  {
    if (!$this->_oEmailContentConfigurationTable && $this->getServiceLocator()->has('EmailContentConfigurationTableService')) {
      $this->_oEmailContentConfigurationTable = $this->getServiceLocator()->get('EmailContentConfigurationTableService');
    }
    if (!isset($this->_oEmailContentConfigurationTable)) {
      throw \Exception();
    }
    return $this->_oEmailContentConfigurationTable;
  }

  protected function getTodoTable()
  {
    if (!$this->_oTodoTable && $this->getServiceLocator()->has('TodoTableService')) {
      $this->_oTodoTable = $this->getServiceLocator()->get('TodoTableService');
    }
    if (!isset($this->_oTodoTable)) {
      throw \Exception();
    }
    return $this->_oTodoTable;
  }

  public function addemailconfigurationajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $oEmailConfigurationForm = $this->getServiceLocator()->get('EmailConfigurationFormService');
      $oEmailConfigurationForm->setData($this->getRequest()->getPost());
      if ($oEmailConfigurationForm->isValid()) {
        $oEmailConfigurationEntity = new EmailConfigurationEntity();
        $oData = new \ArrayObject($oEmailConfigurationForm->getData());
        if ((int)$oData->offsetGet('primary') === 1) {
          $oResetData = new \ArrayObject(array('primary' => 0));
          $this->getEmailConfigurationTable()->editRow($oEmailConfigurationEntity->setOptions($oResetData, array('primary')));
        }
        $oData->offsetSet('email_configuration_id', $this->getEmailConfigurationTable()->addRow($oEmailConfigurationEntity->setOptions($oData)));
        if ($oData->offsetGet('email_configuration_id')) {
          $bSuccess = true;
        }
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function addemailcontentconfigurationajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $oEmailContentConfigurationForm = $this->getServiceLocator()->get('EmailContentConfigurationFormService');
      $oEmailContentConfigurationForm->setData($this->getRequest()->getPost());
      if ($oEmailContentConfigurationForm->isValid()) {
        $oEmailContentConfigurationEntity = new EmailContentConfigurationEntity();
        $oData = new \ArrayObject($oEmailContentConfigurationForm->getData());
        $oData->offsetSet('email_content_configuration_id', $this->getEmailContentConfigurationTable()->addRow($oEmailContentConfigurationEntity->setOptions($oData)));
        if ($oData->offsetGet('email_content_configuration_id')) {
          $bSuccess = true;
        }
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function autocompleteemailconfigurationajaxAction()
  {
    $oResponse = $this->getResponse();
    $aFilter = null;
    $aEmailConfiguration = array();
    $sColumnName = $this->params()->fromQuery('column_name') !== '' ? $this->params()->fromQuery('column_name') : null;
    if (is_array($this->params()->fromQuery('filter')) && count($this->params()->fromQuery('filter'))) {
      $aFilter = $this->params()->fromQuery('filter');
      foreach ($aFilter as $sKey => $sValue) {
        if (strlen(trim($sValue)))
          $aFilter[$sKey] = trim($sValue);
        else
          unset($aFilter[$sKey]);
      }
    }
    if (isset($aFilter)) {
      $aRowset = $this->getEmailConfigurationTable()->getAll(array('filter' => $aFilter));
      $aLabels = array();
      foreach ($aRowset as $nKey => $oEmailConfiguration) {
        if (!in_array($oEmailConfiguration->$sColumnName, $aLabels)) {
          $aEmailConfiguration[$nKey]['label'] = $oEmailConfiguration->$sColumnName;
          array_push($aLabels, $oEmailConfiguration->$sColumnName);
        }
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode(array('rowset' => $aEmailConfiguration)));
    return $oResponse;
  }

  public function autocompleteemailcontentconfigurationajaxAction()
  {
    $oResponse = $this->getResponse();
    $aFilter = null;
    $aEmailContentConfiguration = array();
    $sColumnName = $this->params()->fromQuery('column_name') !== '' ? $this->params()->fromQuery('column_name') : null;
    if (is_array($this->params()->fromQuery('filter')) && count($this->params()->fromQuery('filter'))) {
      $aFilter = $this->params()->fromQuery('filter');
      foreach ($aFilter as $sKey => $sValue) {
        if (strlen(trim($sValue)))
          $aFilter[$sKey] = trim($sValue);
        else
          unset($aFilter[$sKey]);
      }
    }
    if (isset($aFilter)) {
      $aRowset = $this->getEmailContentConfigurationTable()->getAll(array('filter' => $aFilter));
      $aLabels = array();
      foreach ($aRowset as $nKey => $oEmailContentConfiguration) {
        if (!in_array($oEmailContentConfiguration->$sColumnName, $aLabels)) {
          $aEmailContentConfiguration[$nKey]['label'] = $oEmailContentConfiguration->$sColumnName;
          array_push($aLabels, $oEmailContentConfiguration->$sColumnName);
        }
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode(array('rowset' => $aEmailContentConfiguration)));
    return $oResponse;
  }

  public function deleteemailconfigurationajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $nId = $this->params()->fromPost('id') !== '' ? (int)$this->params()->fromPost('id') : null;
      if ($nId) {
        $this->getEmailConfigurationTable()->deleteRow($nId);
        $bSuccess = true;
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function deleteemailcontentconfigurationajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $nId = $this->params()->fromPost('id') !== '' ? (int)$this->params()->fromPost('id') : null;
      if ($nId) {
        $this->getEmailContentConfigurationTable()->deleteRow($nId);
        $bSuccess = true;
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function editemailconfigurationajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $oEmailConfigurationForm = $this->getServiceLocator()->get('EmailConfigurationFormService');
      $oEmailConfigurationForm->setData($this->getRequest()->getPost());
      if ($oEmailConfigurationForm->isValid()) {
        $oEmailConfigurationEntity = new EmailConfigurationEntity();
        $oData = new \ArrayObject($oEmailConfigurationForm->getData());
        if ((int)$oData->offsetGet('primary') === 1) {
          $oResetData = new \ArrayObject(array('primary' => 0));
          $this->getEmailConfigurationTable()->editRow($oEmailConfigurationEntity->setOptions($oResetData, array('primary')));
        }
        $this->getEmailConfigurationTable()->editRow($oEmailConfigurationEntity->setOptions($oData, array()));
        $bSuccess = true;
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function editemailcontentconfigurationajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $oEmailContentConfigurationForm = $this->getServiceLocator()->get('EmailContentConfigurationFormService');
      $oEmailContentConfigurationForm->setData($this->getRequest()->getPost());
      if ($oEmailContentConfigurationForm->isValid()) {
        $oEmailContentConfigurationEntity = new EmailContentConfigurationEntity();
        $oData = new \ArrayObject($oEmailContentConfigurationForm->getData());
        $this->getEmailContentConfigurationTable()->editRow($oEmailContentConfigurationEntity->setOptions($oData, array()));
        $bSuccess = true;
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function indexAction()
  {
    $oAddNewEmailContentConfigurationForm = $this->getServiceLocator()->get('EmailContentConfigurationFormService');
    $oEditEmailContentConfigurationForm = $this->getServiceLocator()->get('EmailContentConfigurationFormService');
    $oAddNewEmailConfigurationForm = $this->getServiceLocator()->get('EmailConfigurationFormService');
    $oEditEmailConfigurationForm = $this->getServiceLocator()->get('EmailConfigurationFormService');
    return new ViewModel(
      array(
        'add_new_email_content_configuration_form' => $oAddNewEmailContentConfigurationForm,
        'edit_email_content_configuration_form' => $oEditEmailContentConfigurationForm,
        'add_new_email_configuration_form' => $oAddNewEmailConfigurationForm,
        'edit_email_configuration_form' => $oEditEmailConfigurationForm
      )
    );
  }

  public function loademailconfigurationajaxAction()
  {
    if ($this->getRequest()->isPost()) {
      $oResponse = $this->getResponse();
      $aSort = null;
      $aFilter = null;
      if (is_array($this->params()->fromPost('filter')) && count($this->params()->fromPost('filter'))) {
        $aFilter = $this->params()->fromPost('filter');
        foreach ($aFilter as $sKey => $sValue) {
          if (strlen(trim($sValue)))
            $aFilter[$sKey] = trim($sValue);
          else
            unset($aFilter[$sKey]);
        }
      }
      $sSortColumn = $this->params()->fromPost('sort_column') !== '' ? $this->params()->fromPost('sort_column') : 'id';
      $sSortMethod = $this->params()->fromPost('sort_method') !== '' ? $this->params()->fromPost('sort_method') : 'asc';
      $nLimit = $this->params()->fromPost('limit');
      $nOffset = $this->params()->fromPost('offset');
      if (isset($sSortColumn) && isset($sSortMethod))
        $aSort = array('sort_column' => $sSortColumn, 'sort_method' => $sSortMethod);
      if (isset($nLimit) && is_numeric($nLimit)) {
        $nLimit = (int)$nLimit;
      } else {
        $nLimit = 20;
      }
      if (isset($nOffset) && is_numeric($nOffset)) {
        $nOffset = (int)$nOffset;
      } else {
        $nOffset = 0;
      }
      $aModules = $this->getEmailConfigurationTable()->getAll(array('filter' => $aFilter, 'sort' => $aSort, 'limit' => $nLimit, 'offset' => $nOffset));
      $nCount = count($this->getEmailConfigurationTable()->getAll(array('filter' => $aFilter, 'sort' => $aSort)));
      $oResponse->setContent(\Zend\Json\Json::encode(array('rowset' => $aModules, 'num_rows' => $nCount)));
    }
    return $oResponse;
  }

  public function loademailcontentconfigurationajaxAction()
  {
    if ($this->getRequest()->isPost()) {
      $oResponse = $this->getResponse();
      $aSort = null;
      $aFilter = null;
      if (is_array($this->params()->fromPost('filter')) && count($this->params()->fromPost('filter'))) {
        $aFilter = $this->params()->fromPost('filter');
        foreach ($aFilter as $sKey => $sValue) {
          if (strlen(trim($sValue)))
            $aFilter[$sKey] = trim($sValue);
          else
            unset($aFilter[$sKey]);
        }
      }
      $sSortColumn = $this->params()->fromPost('sort_column') !== '' ? $this->params()->fromPost('sort_column') : 'id';
      $sSortMethod = $this->params()->fromPost('sort_method') !== '' ? $this->params()->fromPost('sort_method') : 'asc';
      $nLimit = $this->params()->fromPost('limit');
      $nOffset = $this->params()->fromPost('offset');
      if (isset($sSortColumn) && isset($sSortMethod))
        $aSort = array('sort_column' => $sSortColumn, 'sort_method' => $sSortMethod);
      if (isset($nLimit) && is_numeric($nLimit)) {
        $nLimit = (int)$nLimit;
      } else {
        $nLimit = 20;
      }
      if (isset($nOffset) && is_numeric($nOffset)) {
        $nOffset = (int)$nOffset;
      } else {
        $nOffset = 0;
      }
      $aModules = $this->getEmailContentConfigurationTable()->getAll(array('filter' => $aFilter, 'sort' => $aSort, 'limit' => $nLimit, 'offset' => $nOffset));
      $nCount = count($this->getEmailContentConfigurationTable()->getAll(array('filter' => $aFilter, 'sort' => $aSort)));
      $oResponse->setContent(\Zend\Json\Json::encode(array('rowset' => $aModules, 'num_rows' => $nCount)));
    }
    return $oResponse;
  }

  public function secendoptionemailconfigurationajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $sSecendOption = $this->params()->fromPost('second_opinion');
      $sColumnName = $this->params()->fromPost('column_name');
      $nId = $this->params()->fromPost('id') !== '' ? (int)$this->params()->fromPost('id') : null;
      $nResetAll = $this->params()->fromPost('reset_all') !== '' ? (int)$this->params()->fromPost('reset_all') : null;
      if ($nId) {
        $oEmailConfigurationEntity = new EmailConfigurationEntity();
        switch ($sSecendOption) {
          case 'enable_disable':
            $oEmailConfiguration = $this->getEmailConfigurationTable()->getRow(array('id' => $nId));
            if (isset($oEmailConfiguration->$sColumnName)) {
              if ($nResetAll && $oEmailConfiguration->$sColumnName === 0) {
                $oResetData = new \ArrayObject(array($sColumnName => 0));
                $this->getEmailConfigurationTable()->editRow($oEmailConfigurationEntity->setOptions($oResetData, array($sColumnName)));
              }
              if ($oEmailConfiguration->$sColumnName === 1) {
                $oData = new \ArrayObject(array('id' => $nId, $sColumnName => 0));
                $this->getEmailConfigurationTable()->editRow($oEmailConfigurationEntity->setOptions($oData, array('id', $sColumnName)));
                $bSuccess = true;
              } else if ($oEmailConfiguration->$sColumnName === 0) {
                $oData = new \ArrayObject(array('id' => $nId, $sColumnName => 1));
                $this->getEmailConfigurationTable()->editRow($oEmailConfigurationEntity->setOptions($oData, array('id', $sColumnName)));
                $bSuccess = true;
              }
            }
            break;
        }
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function secendoptionemailcontentconfigurationajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $sSecendOption = $this->params()->fromPost('second_opinion');
      $sColumnName = $this->params()->fromPost('column_name');
      $nId = $this->params()->fromPost('id') !== '' ? (int)$this->params()->fromPost('id') : null;
      $nResetAll = $this->params()->fromPost('reset_all') !== '' ? (int)$this->params()->fromPost('reset_all') : null;
      if ($nId) {
        $oEmailContentConfigurationEntity = new EmailContentConfigurationEntity();
        switch ($sSecendOption) {
          case 'enable_disable':
            $oEmailContentConfiguration = $this->getEmailContentConfigurationTable()->getRow(array('id' => $nId));
            if (isset($oEmailContentConfiguration->$sColumnName)) {
              if ($nResetAll && $oEmailContentConfiguration->$sColumnName === 0) {
                //$oResetData = new \ArrayObject(array($sColumnName => 0));
                //$this->getEmailContentConfigurationTable()->editRow($oEmailContentConfigurationEntity->setOptions($oResetData, array($sColumnName)));
              }
              if ($oEmailContentConfiguration->$sColumnName === 1) {
                $oData = new \ArrayObject(array('id' => $nId, $sColumnName => 0));
                $this->getEmailContentConfigurationTable()->editRow($oEmailContentConfigurationEntity->setOptions($oData, array('id', $sColumnName)));
                $bSuccess = true;
              } else if ($oEmailContentConfiguration->$sColumnName === 0) {
                $oData = new \ArrayObject(array('id' => $nId, $sColumnName => 1));
                $this->getEmailContentConfigurationTable()->editRow($oEmailContentConfigurationEntity->setOptions($oData, array('id', $sColumnName)));
                $bSuccess = true;
              }
            }
            break;
        }
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }
}
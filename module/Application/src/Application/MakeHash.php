<?php
namespace Application;
class MakeHash
{
  private $_aSource = array();
  private $_bUseDigits = true;
  private $_bUseLowercaseLetters = true;
  private $_bUseSpecialChars = true;
  private $_bUseUpperLetters = true;
  private $_nPasswordLength = 300;
  private $_nSourceCounter = null;
  private $_sPassword = null;

  private function initialize()
  {
    $this->_sPassword = '';
    if ($this->_bUseSpecialChars) {
      $this->_aSource[] = '!@#$%^*_-+=?:()|~';
    }
    if ($this->_bUseDigits) {
      $this->_aSource[] = '0123456789';
    }
    if ($this->_bUseLowercaseLetters) {
      $this->_aSource[] = 'abcdefghijklmnopqrstuvwxyząćęłńóśźż';
    }
    if ($this->_bUseUpperLetters) {
      $this->_aSource[] = 'ABCDEFGHIJKLMNOPQRSTUVWXYZĄĆĘŁŃÓŚŹŻ';
    }
    if (!isset($this->_aSource)) {
      die('');
    }
    $this->_nSourceCounter = sizeof($this->_aSource);
  }

  public function __get($name)
  {
    return $this->$name;
  }

  public function generate($nLenght = null)
  {
    $this->initialize();
    for ($i = 0; $i < $this->_nPasswordLength; $i++) {
      $temp = mt_rand(0, $this->_nSourceCounter - 1);
      $temp2 = mt_rand(0, strlen($this->_aSource[$temp]) - 1);
      $this->_sPassword .= $this->_aSource[$temp][$temp2] . uniqid() . md5(uniqid()) . md5(time());
    }
    $sHash = md5(sha1(md5($this->_sPassword . time())));
    if (!$nLenght) {
      return $sHash;
    } else if ($nLenght < strlen($sHash)) {
      return substr($sHash, 0, $nLenght);
    } else {
      return $sHash;
    }
  }
}

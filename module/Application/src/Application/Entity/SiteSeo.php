<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SiteSeo
 *
 * @ORM\Table(name="site_seo", indexes={@ORM\Index(name="fk_site_seo_robots_site_seo", columns={"site_seo_robots_id"})})
 * @ORM\Entity
 */
class SiteSeo extends \Application\DoctrineEntityAbstract
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  /**
   * @var string
   *
   * @ORM\Column(name="head_title", type="string", length=255, nullable=true)
   */
  private $headTitle;
  /**
   * @var string
   *
   * @ORM\Column(name="meta_description", type="text", length=65535, nullable=true)
   */
  private $metaDescription;
  /**
   * @var \Application\Entity\SiteSeoRobots
   *
   * @ORM\ManyToOne(targetEntity="Application\Entity\SiteSeoRobots")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="site_seo_robots_id", referencedColumnName="id")
   * })
   */
  private $siteSeoRobots;
  /**
   * @var \Doctrine\Common\Collections\Collection
   *
   * @ORM\ManyToMany(targetEntity="Application\Entity\SiteSeoMetaKeywords", inversedBy="siteSeo")
   * @ORM\JoinTable(name="site_seo_site_seo_meta_keywords",
   *   joinColumns={
   *     @ORM\JoinColumn(name="site_seo_id", referencedColumnName="id")
   *   },
   *   inverseJoinColumns={
   *     @ORM\JoinColumn(name="site_seo_meta_keywords_id", referencedColumnName="id")
   *   }
   * )
   */
  private $siteSeoMetaKeywords;

  /**
   * Constructor
   */
  public function __construct()
  {
    $this->siteSeoMetaKeywords = new \Doctrine\Common\Collections\ArrayCollection();
  }

  /**
   * Add siteSeoMetaKeyword
   *
   * @param \Application\Entity\SiteSeoMetaKeywords $siteSeoMetaKeyword
   *
   * @return SiteSeo
   */
  public function addSiteSeoMetaKeyword(\Application\Entity\SiteSeoMetaKeywords $siteSeoMetaKeyword)
  {
    $this->siteSeoMetaKeywords[] = $siteSeoMetaKeyword;
    return $this;
  }

  /**
   * Get headTitle
   *
   * @return string
   */
  public function getHeadTitle()
  {
    return $this->headTitle;
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get metaDescription
   *
   * @return string
   */
  public function getMetaDescription()
  {
    return $this->metaDescription;
  }

  /**
   * Get siteSeoMetaKeywords
   *
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getSiteSeoMetaKeywords()
  {
    return $this->siteSeoMetaKeywords;
  }

  /**
   * Get siteSeoRobots
   *
   * @return \Application\Entity\SiteSeoRobots
   */
  public function getSiteSeoRobots()
  {
    return $this->siteSeoRobots;
  }

  /**
   * Remove siteSeoMetaKeyword
   *
   * @param \Application\Entity\SiteSeoMetaKeywords $siteSeoMetaKeyword
   */
  public function removeSiteSeoMetaKeyword(\Application\Entity\SiteSeoMetaKeywords $siteSeoMetaKeyword)
  {
    $this->siteSeoMetaKeywords->removeElement($siteSeoMetaKeyword);
  }

  /**
   * Set headTitle
   *
   * @param string $headTitle
   *
   * @return SiteSeo
   */
  public function setHeadTitle($headTitle)
  {
    $this->headTitle = $headTitle;
    return $this;
  }

  /**
   * Set metaDescription
   *
   * @param string $metaDescription
   *
   * @return SiteSeo
   */
  public function setMetaDescription($metaDescription)
  {
    $this->metaDescription = $metaDescription;
    return $this;
  }

  /**
   * Set siteSeoRobots
   *
   * @param \Application\Entity\SiteSeoRobots $siteSeoRobots
   *
   * @return SiteSeo
   */
  public function setSiteSeoRobots(\Application\Entity\SiteSeoRobots $siteSeoRobots = null)
  {
    $this->siteSeoRobots = $siteSeoRobots;
    return $this;
  }
}

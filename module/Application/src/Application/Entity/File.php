<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * File
 *
 * @ORM\Table(name="file")
 * @ORM\Entity
 */
class File extends \Application\DoctrineEntityAbstract
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  /**
   * @var string
   *
   * @ORM\Column(name="name", type="string", length=255, nullable=true)
   */
  private $name;
  /**
   * @var string
   *
   * @ORM\Column(name="unique_name", type="string", length=32, nullable=true)
   */
  private $uniqueName;
  /**
   * @var string
   *
   * @ORM\Column(name="extension", type="string", length=10, nullable=true)
   */
  private $extension;
  /**
   * @var string
   *
   * @ORM\Column(name="description", type="string", length=255, nullable=true)
   */
  private $description;
  /**
   * @var string
   *
   * @ORM\Column(name="href", type="string", length=255, nullable=true)
   */
  private $href;
  /**
   * @var integer
   *
   * @ORM\Column(name="created_date", type="integer", nullable=false)
   */
  private $createdDate;

  /**
   * Get createdDate
   *
   * @return integer
   */
  public function getCreatedDate()
  {
    return $this->createdDate;
  }

  /**
   * Get description
   *
   * @return string
   */
  public function getDescription()
  {
    return $this->description;
  }

  /**
   * Get extension
   *
   * @return string
   */
  public function getExtension()
  {
    return $this->extension;
  }

  /**
   * Get href
   *
   * @return string
   */
  public function getHref()
  {
    return $this->href;
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get name
   *
   * @return string
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * Get uniqueName
   *
   * @return string
   */
  public function getUniqueName()
  {
    return $this->uniqueName;
  }

  /**
   * Set createdDate
   *
   * @param integer $createdDate
   *
   * @return File
   */
  public function setCreatedDate($createdDate)
  {
    $this->createdDate = $createdDate;
    return $this;
  }

  /**
   * Set description
   *
   * @param string $description
   *
   * @return File
   */
  public function setDescription($description)
  {
    $this->description = $description;
    return $this;
  }

  /**
   * Set extension
   *
   * @param string $extension
   *
   * @return File
   */
  public function setExtension($extension)
  {
    $this->extension = $extension;
    return $this;
  }

  /**
   * Set href
   *
   * @param string $href
   *
   * @return File
   */
  public function setHref($href)
  {
    $this->href = $href;
    return $this;
  }

  /**
   * Set name
   *
   * @param string $name
   *
   * @return File
   */
  public function setName($name)
  {
    $this->name = $name;
    return $this;
  }

  /**
   * Set uniqueName
   *
   * @param string $uniqueName
   *
   * @return File
   */
  public function setUniqueName($uniqueName)
  {
    $this->uniqueName = $uniqueName;
    return $this;
  }
}

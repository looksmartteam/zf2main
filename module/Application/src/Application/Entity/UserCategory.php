<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserCategory
 *
 * @ORM\Table(name="user_category")
 * @ORM\Entity
 */
class UserCategory extends \Application\DoctrineEntityAbstract
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  /**
   * @var string
   *
   * @ORM\Column(name="name", type="string", length=45, nullable=true)
   */
  private $name;
  /**
   * @var boolean
   *
   * @ORM\Column(name="active", type="boolean", nullable=true)
   */
  private $active;

  /**
   * Get active
   *
   * @return boolean
   */
  public function getActive()
  {
    return $this->active;
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get name
   *
   * @return string
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * Set active
   *
   * @param boolean $active
   *
   * @return UserCategory
   */
  public function setActive($active)
  {
    $this->active = $active;
    return $this;
  }

  /**
   * Set name
   *
   * @param string $name
   *
   * @return UserCategory
   */
  public function setName($name)
  {
    $this->name = $name;
    return $this;
  }
}

<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EmailConfiguration
 *
 * @ORM\Table(name="email_configuration")
 * @ORM\Entity
 */
class EmailConfiguration extends \Application\DoctrineEntityAbstract
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  /**
   * @var string
   *
   * @ORM\Column(name="host", type="string", length=50, nullable=false)
   */
  private $host;
  /**
   * @var string
   *
   * @ORM\Column(name="username", type="string", length=50, nullable=false)
   */
  private $username;
  /**
   * @var string
   *
   * @ORM\Column(name="password", type="string", length=50, nullable=false)
   */
  private $password;
  /**
   * @var string
   *
   * @ORM\Column(name="name", type="string", length=100, nullable=false)
   */
  private $name;
  /**
   * @var string
   *
   * @ORM\Column(name="from", type="string", length=100, nullable=false)
   */
  private $from;
  /**
   * @var boolean
   *
   * @ORM\Column(name="primary", type="boolean", nullable=false)
   */
  private $primary = '0';

  /**
   * Get from
   *
   * @return string
   */
  public function getFrom()
  {
    return $this->from;
  }

  /**
   * Get host
   *
   * @return string
   */
  public function getHost()
  {
    return $this->host;
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get name
   *
   * @return string
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * Get password
   *
   * @return string
   */
  public function getPassword()
  {
    return $this->password;
  }

  /**
   * Get primary
   *
   * @return boolean
   */
  public function getPrimary()
  {
    return $this->primary;
  }

  /**
   * Get username
   *
   * @return string
   */
  public function getUsername()
  {
    return $this->username;
  }

  /**
   * Set from
   *
   * @param string $from
   *
   * @return EmailConfiguration
   */
  public function setFrom($from)
  {
    $this->from = $from;
    return $this;
  }

  /**
   * Set host
   *
   * @param string $host
   *
   * @return EmailConfiguration
   */
  public function setHost($host)
  {
    $this->host = $host;
    return $this;
  }

  /**
   * Set name
   *
   * @param string $name
   *
   * @return EmailConfiguration
   */
  public function setName($name)
  {
    $this->name = $name;
    return $this;
  }

  /**
   * Set password
   *
   * @param string $password
   *
   * @return EmailConfiguration
   */
  public function setPassword($password)
  {
    $this->password = $password;
    return $this;
  }

  /**
   * Set primary
   *
   * @param boolean $primary
   *
   * @return EmailConfiguration
   */
  public function setPrimary($primary)
  {
    $this->primary = $primary;
    return $this;
  }

  /**
   * Set username
   *
   * @param string $username
   *
   * @return EmailConfiguration
   */
  public function setUsername($username)
  {
    $this->username = $username;
    return $this;
  }
}

<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NavigationLayout
 *
 * @ORM\Table(name="navigation_layout")
 * @ORM\Entity
 */
class NavigationLayout extends \Application\DoctrineEntityAbstract
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  /**
   * @var string
   *
   * @ORM\Column(name="name", type="string", length=50, nullable=false)
   */
  private $name;

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get name
   *
   * @return string
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * Set name
   *
   * @param string $name
   *
   * @return NavigationLayout
   */
  public function setName($name)
  {
    $this->name = $name;
    return $this;
  }
}

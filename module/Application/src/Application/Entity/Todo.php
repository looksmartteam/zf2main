<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Todo
 *
 * @ORM\Table(name="todo")
 * @ORM\Entity
 */
class Todo extends \Application\DoctrineEntityAbstract
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  /**
   * @var string
   *
   * @ORM\Column(name="description", type="text", length=65535, nullable=false)
   */
  private $description;
  /**
   * @var integer
   *
   * @ORM\Column(name="todo_priority_id", type="integer", nullable=false)
   */
  private $todoPriorityId;
  /**
   * @var integer
   *
   * @ORM\Column(name="created_date", type="integer", nullable=false)
   */
  private $createdDate;
  /**
   * @var integer
   *
   * @ORM\Column(name="updated_date", type="integer", nullable=false)
   */
  private $updatedDate;
  /**
   * @var boolean
   *
   * @ORM\Column(name="active", type="boolean", nullable=false)
   */
  private $active = '1';

  /**
   * Get active
   *
   * @return boolean
   */
  public function getActive()
  {
    return $this->active;
  }

  /**
   * Get createdDate
   *
   * @return integer
   */
  public function getCreatedDate()
  {
    return $this->createdDate;
  }

  /**
   * Get description
   *
   * @return string
   */
  public function getDescription()
  {
    return $this->description;
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get todoPriorityId
   *
   * @return integer
   */
  public function getTodoPriorityId()
  {
    return $this->todoPriorityId;
  }

  /**
   * Get updatedDate
   *
   * @return integer
   */
  public function getUpdatedDate()
  {
    return $this->updatedDate;
  }

  /**
   * Set active
   *
   * @param boolean $active
   *
   * @return Todo
   */
  public function setActive($active)
  {
    $this->active = $active;
    return $this;
  }

  /**
   * Set createdDate
   *
   * @param integer $createdDate
   *
   * @return Todo
   */
  public function setCreatedDate($createdDate)
  {
    $this->createdDate = $createdDate;
    return $this;
  }

  /**
   * Set description
   *
   * @param string $description
   *
   * @return Todo
   */
  public function setDescription($description)
  {
    $this->description = $description;
    return $this;
  }

  /**
   * Set todoPriorityId
   *
   * @param integer $todoPriorityId
   *
   * @return Todo
   */
  public function setTodoPriorityId($todoPriorityId)
  {
    $this->todoPriorityId = $todoPriorityId;
    return $this;
  }

  /**
   * Set updatedDate
   *
   * @param integer $updatedDate
   *
   * @return Todo
   */
  public function setUpdatedDate($updatedDate)
  {
    $this->updatedDate = $updatedDate;
    return $this;
  }
}

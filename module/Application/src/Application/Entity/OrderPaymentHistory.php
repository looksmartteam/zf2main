<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderPaymentHistory
 *
 * @ORM\Table(name="order_payment_history", indexes={@ORM\Index(name="fk_item_order_payment_history", columns={"item_id"}), @ORM\Index(name="fk_order_payment_order_payment_history", columns={"order_payment_id"})})
 * @ORM\Entity
 */
class OrderPaymentHistory extends \Application\DoctrineEntityAbstract
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  /**
   * @var integer
   *
   * @ORM\Column(name="date", type="integer", nullable=false)
   */
  private $date;
  /**
   * @var \Application\Entity\Item
   *
   * @ORM\ManyToOne(targetEntity="Application\Entity\Item")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="item_id", referencedColumnName="id")
   * })
   */
  private $item;
  /**
   * @var \Application\Entity\OrderPayment
   *
   * @ORM\ManyToOne(targetEntity="Application\Entity\OrderPayment")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="order_payment_id", referencedColumnName="id")
   * })
   */
  private $orderPayment;

  /**
   * Get date
   *
   * @return integer
   */
  public function getDate()
  {
    return $this->date;
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get item
   *
   * @return \Application\Entity\Item
   */
  public function getItem()
  {
    return $this->item;
  }

  /**
   * Get orderPayment
   *
   * @return \Application\Entity\OrderPayment
   */
  public function getOrderPayment()
  {
    return $this->orderPayment;
  }

  /**
   * Set date
   *
   * @param integer $date
   *
   * @return OrderPaymentHistory
   */
  public function setDate($date)
  {
    $this->date = $date;
    return $this;
  }

  /**
   * Set item
   *
   * @param \Application\Entity\Item $item
   *
   * @return OrderPaymentHistory
   */
  public function setItem(\Application\Entity\Item $item = null)
  {
    $this->item = $item;
    return $this;
  }

  /**
   * Set orderPayment
   *
   * @param \Application\Entity\OrderPayment $orderPayment
   *
   * @return OrderPaymentHistory
   */
  public function setOrderPayment(\Application\Entity\OrderPayment $orderPayment = null)
  {
    $this->orderPayment = $orderPayment;
    return $this;
  }
}

<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EmailContentConfigurationType
 *
 * @ORM\Table(name="email_content_configuration_type")
 * @ORM\Entity
 */
class EmailContentConfigurationType extends \Application\DoctrineEntityAbstract
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  /**
   * @var string
   *
   * @ORM\Column(name="name", type="string", length=50, nullable=false)
   */
  private $name;

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get name
   *
   * @return string
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * Set name
   *
   * @param string $name
   *
   * @return EmailContentConfigurationType
   */
  public function setName($name)
  {
    $this->name = $name;
    return $this;
  }
}

<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NavigationAction
 *
 * @ORM\Table(name="navigation_action", uniqueConstraints={@ORM\UniqueConstraint(name="value_UNIQUE", columns={"value"})})
 * @ORM\Entity
 */
class NavigationAction extends \Application\DoctrineEntityAbstract
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  /**
   * @var string
   *
   * @ORM\Column(name="value", type="string", length=45, nullable=false)
   */
  private $value;
  /**
   * @var boolean
   *
   * @ORM\Column(name="ajax", type="boolean", nullable=true)
   */
  private $ajax = '0';
  /**
   * @var integer
   *
   * @ORM\Column(name="created_date", type="integer", nullable=false)
   */
  private $createdDate;
  /**
   * @var integer
   *
   * @ORM\Column(name="updated_date", type="integer", nullable=false)
   */
  private $updatedDate;

  /**
   * Get ajax
   *
   * @return boolean
   */
  public function getAjax()
  {
    return $this->ajax;
  }

  /**
   * Get createdDate
   *
   * @return integer
   */
  public function getCreatedDate()
  {
    return $this->createdDate;
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get updatedDate
   *
   * @return integer
   */
  public function getUpdatedDate()
  {
    return $this->updatedDate;
  }

  /**
   * Get value
   *
   * @return string
   */
  public function getValue()
  {
    return $this->value;
  }

  /**
   * Set ajax
   *
   * @param boolean $ajax
   *
   * @return NavigationAction
   */
  public function setAjax($ajax)
  {
    $this->ajax = $ajax;
    return $this;
  }

  /**
   * Set createdDate
   *
   * @param integer $createdDate
   *
   * @return NavigationAction
   */
  public function setCreatedDate($createdDate)
  {
    $this->createdDate = $createdDate;
    return $this;
  }

  /**
   * Set updatedDate
   *
   * @param integer $updatedDate
   *
   * @return NavigationAction
   */
  public function setUpdatedDate($updatedDate)
  {
    $this->updatedDate = $updatedDate;
    return $this;
  }

  /**
   * Set value
   *
   * @param string $value
   *
   * @return NavigationAction
   */
  public function setValue($value)
  {
    $this->value = $value;
    return $this;
  }
}

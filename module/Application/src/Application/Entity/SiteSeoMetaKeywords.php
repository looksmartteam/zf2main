<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SiteSeoMetaKeywords
 *
 * @ORM\Table(name="site_seo_meta_keywords")
 * @ORM\Entity
 */
class SiteSeoMetaKeywords extends \Application\DoctrineEntityAbstract
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  /**
   * @var string
   *
   * @ORM\Column(name="value", type="string", length=255, nullable=false)
   */
  private $value;
  /**
   * @var boolean
   *
   * @ORM\Column(name="primary", type="boolean", nullable=false)
   */
  private $primary;
  /**
   * @var boolean
   *
   * @ORM\Column(name="visible", type="boolean", nullable=false)
   */
  private $visible;
  /**
   * @var \Doctrine\Common\Collections\Collection
   *
   * @ORM\ManyToMany(targetEntity="Application\Entity\SiteSeo", mappedBy="siteSeoMetaKeywords")
   */
  private $siteSeo;

  /**
   * Constructor
   */
  public function __construct()
  {
    $this->siteSeo = new \Doctrine\Common\Collections\ArrayCollection();
  }

  /**
   * Add siteSeo
   *
   * @param \Application\Entity\SiteSeo $siteSeo
   *
   * @return SiteSeoMetaKeywords
   */
  public function addSiteSeo(\Application\Entity\SiteSeo $siteSeo)
  {
    $this->siteSeo[] = $siteSeo;
    return $this;
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get primary
   *
   * @return boolean
   */
  public function getPrimary()
  {
    return $this->primary;
  }

  /**
   * Get siteSeo
   *
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getSiteSeo()
  {
    return $this->siteSeo;
  }

  /**
   * Get value
   *
   * @return string
   */
  public function getValue()
  {
    return $this->value;
  }

  /**
   * Get visible
   *
   * @return boolean
   */
  public function getVisible()
  {
    return $this->visible;
  }

  /**
   * Remove siteSeo
   *
   * @param \Application\Entity\SiteSeo $siteSeo
   */
  public function removeSiteSeo(\Application\Entity\SiteSeo $siteSeo)
  {
    $this->siteSeo->removeElement($siteSeo);
  }

  /**
   * Set primary
   *
   * @param boolean $primary
   *
   * @return SiteSeoMetaKeywords
   */
  public function setPrimary($primary)
  {
    $this->primary = $primary;
    return $this;
  }

  /**
   * Set value
   *
   * @param string $value
   *
   * @return SiteSeoMetaKeywords
   */
  public function setValue($value)
  {
    $this->value = $value;
    return $this;
  }

  /**
   * Set visible
   *
   * @param boolean $visible
   *
   * @return SiteSeoMetaKeywords
   */
  public function setVisible($visible)
  {
    $this->visible = $visible;
    return $this;
  }
}

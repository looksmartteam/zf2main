<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserRole
 *
 * @ORM\Table(name="user_role")
 * @ORM\Entity
 */
class UserRole extends \Application\DoctrineEntityAbstract
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  /**
   * @var string
   *
   * @ORM\Column(name="role_name", type="string", length=45, nullable=true)
   */
  private $roleName;
  /**
   * @var \Doctrine\Common\Collections\Collection
   *
   * @ORM\ManyToMany(targetEntity="Application\Entity\NavigationOption", mappedBy="userRole")
   */
  private $navigationOption;

  /**
   * Constructor
   */
  public function __construct()
  {
    $this->navigationOption = new \Doctrine\Common\Collections\ArrayCollection();
  }

  /**
   * Add navigationOption
   *
   * @param \Application\Entity\NavigationOption $navigationOption
   *
   * @return UserRole
   */
  public function addNavigationOption(\Application\Entity\NavigationOption $navigationOption)
  {
    $this->navigationOption[] = $navigationOption;
    return $this;
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get navigationOption
   *
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getNavigationOption()
  {
    return $this->navigationOption;
  }

  /**
   * Get roleName
   *
   * @return string
   */
  public function getRoleName()
  {
    return $this->roleName;
  }

  /**
   * Remove navigationOption
   *
   * @param \Application\Entity\NavigationOption $navigationOption
   */
  public function removeNavigationOption(\Application\Entity\NavigationOption $navigationOption)
  {
    $this->navigationOption->removeElement($navigationOption);
  }

  /**
   * Set roleName
   *
   * @param string $roleName
   *
   * @return UserRole
   */
  public function setRoleName($roleName)
  {
    $this->roleName = $roleName;
    return $this;
  }
}

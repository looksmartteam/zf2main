<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EmailContentConfiguration
 *
 * @ORM\Table(name="email_content_configuration", indexes={@ORM\Index(name="fk_email_content_configuration_type_email_content_configuration", columns={"email_content_configuration_type_id"})})
 * @ORM\Entity
 */
class EmailContentConfiguration extends \Application\DoctrineEntityAbstract
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  /**
   * @var string
   *
   * @ORM\Column(name="trigger_action", type="string", length=100, nullable=false)
   */
  private $triggerAction;
  /**
   * @var string
   *
   * @ORM\Column(name="subject", type="string", length=255, nullable=false)
   */
  private $subject;
  /**
   * @var integer
   *
   * @ORM\Column(name="email_content_configuration_greeting_type_id", type="integer", nullable=true)
   */
  private $emailContentConfigurationGreetingTypeId;
  /**
   * @var string
   *
   * @ORM\Column(name="message", type="text", length=65535, nullable=false)
   */
  private $message;
  /**
   * @var integer
   *
   * @ORM\Column(name="email_content_configuration_conclusion_type_id", type="integer", nullable=true)
   */
  private $emailContentConfigurationConclusionTypeId;
  /**
   * @var boolean
   *
   * @ORM\Column(name="active", type="boolean", nullable=false)
   */
  private $active;
  /**
   * @var \Application\Entity\EmailContentConfigurationType
   *
   * @ORM\ManyToOne(targetEntity="Application\Entity\EmailContentConfigurationType")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="email_content_configuration_type_id", referencedColumnName="id")
   * })
   */
  private $emailContentConfigurationType;

  /**
   * Get active
   *
   * @return boolean
   */
  public function getActive()
  {
    return $this->active;
  }

  /**
   * Get emailContentConfigurationConclusionTypeId
   *
   * @return integer
   */
  public function getEmailContentConfigurationConclusionTypeId()
  {
    return $this->emailContentConfigurationConclusionTypeId;
  }

  /**
   * Get emailContentConfigurationGreetingTypeId
   *
   * @return integer
   */
  public function getEmailContentConfigurationGreetingTypeId()
  {
    return $this->emailContentConfigurationGreetingTypeId;
  }

  /**
   * Get emailContentConfigurationType
   *
   * @return \Application\Entity\EmailContentConfigurationType
   */
  public function getEmailContentConfigurationType()
  {
    return $this->emailContentConfigurationType;
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get message
   *
   * @return string
   */
  public function getMessage()
  {
    return $this->message;
  }

  /**
   * Get subject
   *
   * @return string
   */
  public function getSubject()
  {
    return $this->subject;
  }

  /**
   * Get triggerAction
   *
   * @return string
   */
  public function getTriggerAction()
  {
    return $this->triggerAction;
  }

  /**
   * Set active
   *
   * @param boolean $active
   *
   * @return EmailContentConfiguration
   */
  public function setActive($active)
  {
    $this->active = $active;
    return $this;
  }

  /**
   * Set emailContentConfigurationConclusionTypeId
   *
   * @param integer $emailContentConfigurationConclusionTypeId
   *
   * @return EmailContentConfiguration
   */
  public function setEmailContentConfigurationConclusionTypeId($emailContentConfigurationConclusionTypeId)
  {
    $this->emailContentConfigurationConclusionTypeId = $emailContentConfigurationConclusionTypeId;
    return $this;
  }

  /**
   * Set emailContentConfigurationGreetingTypeId
   *
   * @param integer $emailContentConfigurationGreetingTypeId
   *
   * @return EmailContentConfiguration
   */
  public function setEmailContentConfigurationGreetingTypeId($emailContentConfigurationGreetingTypeId)
  {
    $this->emailContentConfigurationGreetingTypeId = $emailContentConfigurationGreetingTypeId;
    return $this;
  }

  /**
   * Set emailContentConfigurationType
   *
   * @param \Application\Entity\EmailContentConfigurationType $emailContentConfigurationType
   *
   * @return EmailContentConfiguration
   */
  public function setEmailContentConfigurationType(\Application\Entity\EmailContentConfigurationType $emailContentConfigurationType = null)
  {
    $this->emailContentConfigurationType = $emailContentConfigurationType;
    return $this;
  }

  /**
   * Set message
   *
   * @param string $message
   *
   * @return EmailContentConfiguration
   */
  public function setMessage($message)
  {
    $this->message = $message;
    return $this;
  }

  /**
   * Set subject
   *
   * @param string $subject
   *
   * @return EmailContentConfiguration
   */
  public function setSubject($subject)
  {
    $this->subject = $subject;
    return $this;
  }

  /**
   * Set triggerAction
   *
   * @param string $triggerAction
   *
   * @return EmailContentConfiguration
   */
  public function setTriggerAction($triggerAction)
  {
    $this->triggerAction = $triggerAction;
    return $this;
  }
}

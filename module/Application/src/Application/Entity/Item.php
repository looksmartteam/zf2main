<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Item
 *
 * @ORM\Table(name="item")
 * @ORM\Entity
 */
class Item extends \Application\DoctrineEntityAbstract
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  /**
   * @var string
   *
   * @ORM\Column(name="name", type="string", length=255, nullable=false)
   */
  private $name;
  /**
   * @var float
   *
   * @ORM\Column(name="price_net", type="float", precision=10, scale=0, nullable=true)
   */
  private $priceNet;
  /**
   * @var float
   *
   * @ORM\Column(name="price_gross", type="float", precision=10, scale=0, nullable=true)
   */
  private $priceGross;
  /**
   * @var boolean
   *
   * @ORM\Column(name="active", type="boolean", nullable=true)
   */
  private $active;

  /**
   * Get active
   *
   * @return boolean
   */
  public function getActive()
  {
    return $this->active;
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get name
   *
   * @return string
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * Get priceGross
   *
   * @return float
   */
  public function getPriceGross()
  {
    return $this->priceGross;
  }

  /**
   * Get priceNet
   *
   * @return float
   */
  public function getPriceNet()
  {
    return $this->priceNet;
  }

  /**
   * Set active
   *
   * @param boolean $active
   *
   * @return Item
   */
  public function setActive($active)
  {
    $this->active = $active;
    return $this;
  }

  /**
   * Set name
   *
   * @param string $name
   *
   * @return Item
   */
  public function setName($name)
  {
    $this->name = $name;
    return $this;
  }

  /**
   * Set priceGross
   *
   * @param float $priceGross
   *
   * @return Item
   */
  public function setPriceGross($priceGross)
  {
    $this->priceGross = $priceGross;
    return $this;
  }

  /**
   * Set priceNet
   *
   * @param float $priceNet
   *
   * @return Item
   */
  public function setPriceNet($priceNet)
  {
    $this->priceNet = $priceNet;
    return $this;
  }
}

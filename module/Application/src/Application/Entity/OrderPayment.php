<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderPayment
 *
 * @ORM\Table(name="order_payment")
 * @ORM\Entity
 */
class OrderPayment extends \Application\DoctrineEntityAbstract
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  /**
   * @var string
   *
   * @ORM\Column(name="session_id", type="string", length=32, nullable=false)
   */
  private $sessionId;
  /**
   * @var string
   *
   * @ORM\Column(name="order_id", type="string", length=32, nullable=true)
   */
  private $orderId;
  /**
   * @var integer
   *
   * @ORM\Column(name="amount", type="integer", nullable=false)
   */
  private $amount;
  /**
   * @var string
   *
   * @ORM\Column(name="description", type="string", length=50, nullable=false)
   */
  private $description;
  /**
   * @var string
   *
   * @ORM\Column(name="first_name", type="string", length=100, nullable=false)
   */
  private $firstName;
  /**
   * @var string
   *
   * @ORM\Column(name="last_name", type="string", length=100, nullable=false)
   */
  private $lastName;
  /**
   * @var string
   *
   * @ORM\Column(name="email_address", type="string", length=100, nullable=false)
   */
  private $emailAddress = '0';
  /**
   * @var boolean
   *
   * @ORM\Column(name="start", type="boolean", nullable=false)
   */
  private $start = '0';
  /**
   * @var integer
   *
   * @ORM\Column(name="date_start", type="integer", nullable=false)
   */
  private $dateStart = '0';
  /**
   * @var boolean
   *
   * @ORM\Column(name="end", type="boolean", nullable=false)
   */
  private $end = '0';
  /**
   * @var integer
   *
   * @ORM\Column(name="date_end", type="integer", nullable=false)
   */
  private $dateEnd = '0';

  /**
   * Get amount
   *
   * @return integer
   */
  public function getAmount()
  {
    return $this->amount;
  }

  /**
   * Get dateEnd
   *
   * @return integer
   */
  public function getDateEnd()
  {
    return $this->dateEnd;
  }

  /**
   * Get dateStart
   *
   * @return integer
   */
  public function getDateStart()
  {
    return $this->dateStart;
  }

  /**
   * Get description
   *
   * @return string
   */
  public function getDescription()
  {
    return $this->description;
  }

  /**
   * Get emailAddress
   *
   * @return string
   */
  public function getEmailAddress()
  {
    return $this->emailAddress;
  }

  /**
   * Get end
   *
   * @return boolean
   */
  public function getEnd()
  {
    return $this->end;
  }

  /**
   * Get firstName
   *
   * @return string
   */
  public function getFirstName()
  {
    return $this->firstName;
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get lastName
   *
   * @return string
   */
  public function getLastName()
  {
    return $this->lastName;
  }

  /**
   * Get orderId
   *
   * @return string
   */
  public function getOrderId()
  {
    return $this->orderId;
  }

  /**
   * Get sessionId
   *
   * @return string
   */
  public function getSessionId()
  {
    return $this->sessionId;
  }

  /**
   * Get start
   *
   * @return boolean
   */
  public function getStart()
  {
    return $this->start;
  }

  /**
   * Set amount
   *
   * @param integer $amount
   *
   * @return OrderPayment
   */
  public function setAmount($amount)
  {
    $this->amount = $amount;
    return $this;
  }

  /**
   * Set dateEnd
   *
   * @param integer $dateEnd
   *
   * @return OrderPayment
   */
  public function setDateEnd($dateEnd)
  {
    $this->dateEnd = $dateEnd;
    return $this;
  }

  /**
   * Set dateStart
   *
   * @param integer $dateStart
   *
   * @return OrderPayment
   */
  public function setDateStart($dateStart)
  {
    $this->dateStart = $dateStart;
    return $this;
  }

  /**
   * Set description
   *
   * @param string $description
   *
   * @return OrderPayment
   */
  public function setDescription($description)
  {
    $this->description = $description;
    return $this;
  }

  /**
   * Set emailAddress
   *
   * @param string $emailAddress
   *
   * @return OrderPayment
   */
  public function setEmailAddress($emailAddress)
  {
    $this->emailAddress = $emailAddress;
    return $this;
  }

  /**
   * Set end
   *
   * @param boolean $end
   *
   * @return OrderPayment
   */
  public function setEnd($end)
  {
    $this->end = $end;
    return $this;
  }

  /**
   * Set firstName
   *
   * @param string $firstName
   *
   * @return OrderPayment
   */
  public function setFirstName($firstName)
  {
    $this->firstName = $firstName;
    return $this;
  }

  /**
   * Set lastName
   *
   * @param string $lastName
   *
   * @return OrderPayment
   */
  public function setLastName($lastName)
  {
    $this->lastName = $lastName;
    return $this;
  }

  /**
   * Set orderId
   *
   * @param string $orderId
   *
   * @return OrderPayment
   */
  public function setOrderId($orderId)
  {
    $this->orderId = $orderId;
    return $this;
  }

  /**
   * Set sessionId
   *
   * @param string $sessionId
   *
   * @return OrderPayment
   */
  public function setSessionId($sessionId)
  {
    $this->sessionId = $sessionId;
    return $this;
  }

  /**
   * Set start
   *
   * @param boolean $start
   *
   * @return OrderPayment
   */
  public function setStart($start)
  {
    $this->start = $start;
    return $this;
  }
}

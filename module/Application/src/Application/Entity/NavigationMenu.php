<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NavigationMenu
 *
 * @ORM\Table(name="navigation_menu", indexes={@ORM\Index(name="fk_navigation_option_navigation_menu", columns={"navigation_option_id"}), @ORM\Index(name="fk_navigation_level_navigation_menu", columns={"navigation_level_id"}), @ORM\Index(name="fk_navigation_menu_configuration_navigation_menu", columns={"navigation_menu_configuration_id"}), @ORM\Index(name="fk_navigation_menu_navigation_menu", columns={"navigation_parent_menu_id"}), @ORM\Index(name="fk_navigation_menu_type_navigation_menu", columns={"navigation_menu_type_id"})})
 * @ORM\Entity
 */
class NavigationMenu extends \Application\DoctrineEntityAbstract
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  /**
   * @var integer
   *
   * @ORM\Column(name="navigation_layout_id", type="integer", nullable=true)
   */
  private $navigationLayoutId;
  /**
   * @var string
   *
   * @ORM\Column(name="label_name", type="string", length=100, nullable=true)
   */
  private $labelName;
  /**
   * @var string
   *
   * @ORM\Column(name="description", type="string", length=100, nullable=true)
   */
  private $description;
  /**
   * @var integer
   *
   * @ORM\Column(name="image_id", type="integer", nullable=true)
   */
  private $imageId;
  /**
   * @var boolean
   *
   * @ORM\Column(name="order", type="boolean", nullable=true)
   */
  private $order;
  /**
   * @var \Application\Entity\NavigationLevel
   *
   * @ORM\ManyToOne(targetEntity="Application\Entity\NavigationLevel")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="navigation_level_id", referencedColumnName="id")
   * })
   */
  private $navigationLevel;
  /**
   * @var \Application\Entity\NavigationMenuConfiguration
   *
   * @ORM\ManyToOne(targetEntity="Application\Entity\NavigationMenuConfiguration")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="navigation_menu_configuration_id", referencedColumnName="id")
   * })
   */
  private $navigationMenuConfiguration;
  /**
   * @var \Application\Entity\NavigationMenu
   *
   * @ORM\ManyToOne(targetEntity="Application\Entity\NavigationMenu")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="navigation_parent_menu_id", referencedColumnName="id")
   * })
   */
  private $navigationParentMenu;
  /**
   * @var \Application\Entity\NavigationMenuType
   *
   * @ORM\ManyToOne(targetEntity="Application\Entity\NavigationMenuType")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="navigation_menu_type_id", referencedColumnName="id")
   * })
   */
  private $navigationMenuType;
  /**
   * @var \Application\Entity\NavigationOption
   *
   * @ORM\ManyToOne(targetEntity="Application\Entity\NavigationOption")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="navigation_option_id", referencedColumnName="id")
   * })
   */
  private $navigationOption;

  /**
   * Get description
   *
   * @return string
   */
  public function getDescription()
  {
    return $this->description;
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get imageId
   *
   * @return integer
   */
  public function getImageId()
  {
    return $this->imageId;
  }

  /**
   * Get labelName
   *
   * @return string
   */
  public function getLabelName()
  {
    return $this->labelName;
  }

  /**
   * Get navigationLayoutId
   *
   * @return integer
   */
  public function getNavigationLayoutId()
  {
    return $this->navigationLayoutId;
  }

  /**
   * Get navigationLevel
   *
   * @return \Application\Entity\NavigationLevel
   */
  public function getNavigationLevel()
  {
    return $this->navigationLevel;
  }

  /**
   * Get navigationMenuConfiguration
   *
   * @return \Application\Entity\NavigationMenuConfiguration
   */
  public function getNavigationMenuConfiguration()
  {
    return $this->navigationMenuConfiguration;
  }

  /**
   * Get navigationMenuType
   *
   * @return \Application\Entity\NavigationMenuType
   */
  public function getNavigationMenuType()
  {
    return $this->navigationMenuType;
  }

  /**
   * Get navigationOption
   *
   * @return \Application\Entity\NavigationOption
   */
  public function getNavigationOption()
  {
    return $this->navigationOption;
  }

  /**
   * Get navigationParentMenu
   *
   * @return \Application\Entity\NavigationMenu
   */
  public function getNavigationParentMenu()
  {
    return $this->navigationParentMenu;
  }

  /**
   * Get order
   *
   * @return boolean
   */
  public function getOrder()
  {
    return $this->order;
  }

  /**
   * Set description
   *
   * @param string $description
   *
   * @return NavigationMenu
   */
  public function setDescription($description)
  {
    $this->description = $description;
    return $this;
  }

  /**
   * Set imageId
   *
   * @param integer $imageId
   *
   * @return NavigationMenu
   */
  public function setImageId($imageId)
  {
    $this->imageId = $imageId;
    return $this;
  }

  /**
   * Set labelName
   *
   * @param string $labelName
   *
   * @return NavigationMenu
   */
  public function setLabelName($labelName)
  {
    $this->labelName = $labelName;
    return $this;
  }

  /**
   * Set navigationLayoutId
   *
   * @param integer $navigationLayoutId
   *
   * @return NavigationMenu
   */
  public function setNavigationLayoutId($navigationLayoutId)
  {
    $this->navigationLayoutId = $navigationLayoutId;
    return $this;
  }

  /**
   * Set navigationLevel
   *
   * @param \Application\Entity\NavigationLevel $navigationLevel
   *
   * @return NavigationMenu
   */
  public function setNavigationLevel(\Application\Entity\NavigationLevel $navigationLevel = null)
  {
    $this->navigationLevel = $navigationLevel;
    return $this;
  }

  /**
   * Set navigationMenuConfiguration
   *
   * @param \Application\Entity\NavigationMenuConfiguration $navigationMenuConfiguration
   *
   * @return NavigationMenu
   */
  public function setNavigationMenuConfiguration(\Application\Entity\NavigationMenuConfiguration $navigationMenuConfiguration = null)
  {
    $this->navigationMenuConfiguration = $navigationMenuConfiguration;
    return $this;
  }

  /**
   * Set navigationMenuType
   *
   * @param \Application\Entity\NavigationMenuType $navigationMenuType
   *
   * @return NavigationMenu
   */
  public function setNavigationMenuType(\Application\Entity\NavigationMenuType $navigationMenuType = null)
  {
    $this->navigationMenuType = $navigationMenuType;
    return $this;
  }

  /**
   * Set navigationOption
   *
   * @param \Application\Entity\NavigationOption $navigationOption
   *
   * @return NavigationMenu
   */
  public function setNavigationOption(\Application\Entity\NavigationOption $navigationOption = null)
  {
    $this->navigationOption = $navigationOption;
    return $this;
  }

  /**
   * Set navigationParentMenu
   *
   * @param \Application\Entity\NavigationMenu $navigationParentMenu
   *
   * @return NavigationMenu
   */
  public function setNavigationParentMenu(\Application\Entity\NavigationMenu $navigationParentMenu = null)
  {
    $this->navigationParentMenu = $navigationParentMenu;
    return $this;
  }

  /**
   * Set order
   *
   * @param boolean $order
   *
   * @return NavigationMenu
   */
  public function setOrder($order)
  {
    $this->order = $order;
    return $this;
  }
}

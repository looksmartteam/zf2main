<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tableb
 *
 * @ORM\Table(name="tableb")
 * @ORM\Entity
 */
class Tableb extends \Application\DoctrineEntityAbstract
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  /**
   * @var string
   *
   * @ORM\Column(name="value", type="string", length=45, nullable=true)
   */
  private $value;
  /**
   * @var \Doctrine\Common\Collections\Collection
   *
   * @ORM\ManyToMany(targetEntity="Application\Entity\Tablea", mappedBy="tableb")
   */
  private $tablea;

  /**
   * Constructor
   */
  public function __construct()
  {
    $this->tablea = new \Doctrine\Common\Collections\ArrayCollection();
  }

  /**
   * Add tablea
   *
   * @param \Application\Entity\Tablea $tablea
   *
   * @return Tableb
   */
  public function addTablea(\Application\Entity\Tablea $tablea)
  {
    $this->tablea[] = $tablea;
    return $this;
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get tablea
   *
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getTablea()
  {
    return $this->tablea;
  }

  /**
   * Get value
   *
   * @return string
   */
  public function getValue()
  {
    return $this->value;
  }

  /**
   * Remove tablea
   *
   * @param \Application\Entity\Tablea $tablea
   */
  public function removeTablea(\Application\Entity\Tablea $tablea)
  {
    $this->tablea->removeElement($tablea);
  }

  /**
   * Set value
   *
   * @param string $value
   *
   * @return Tableb
   */
  public function setValue($value)
  {
    $this->value = $value;
    return $this;
  }
}

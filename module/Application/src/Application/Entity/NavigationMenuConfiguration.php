<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NavigationMenuConfiguration
 *
 * @ORM\Table(name="navigation_menu_configuration")
 * @ORM\Entity
 */
class NavigationMenuConfiguration extends \Application\DoctrineEntityAbstract
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  /**
   * @var string
   *
   * @ORM\Column(name="configuration_name", type="string", length=50, nullable=false)
   */
  private $configurationName;
  /**
   * @var boolean
   *
   * @ORM\Column(name="primary", type="boolean", nullable=false)
   */
  private $primary = '0';

  /**
   * Get configurationName
   *
   * @return string
   */
  public function getConfigurationName()
  {
    return $this->configurationName;
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get primary
   *
   * @return boolean
   */
  public function getPrimary()
  {
    return $this->primary;
  }

  /**
   * Set configurationName
   *
   * @param string $configurationName
   *
   * @return NavigationMenuConfiguration
   */
  public function setConfigurationName($configurationName)
  {
    $this->configurationName = $configurationName;
    return $this;
  }

  /**
   * Set primary
   *
   * @param boolean $primary
   *
   * @return NavigationMenuConfiguration
   */
  public function setPrimary($primary)
  {
    $this->primary = $primary;
    return $this;
  }
}

<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NavigationMenuType
 *
 * @ORM\Table(name="navigation_menu_type")
 * @ORM\Entity
 */
class NavigationMenuType extends \Application\DoctrineEntityAbstract
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  /**
   * @var string
   *
   * @ORM\Column(name="value", type="string", length=50, nullable=false)
   */
  private $value;

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get value
   *
   * @return string
   */
  public function getValue()
  {
    return $this->value;
  }

  /**
   * Set value
   *
   * @param string $value
   *
   * @return NavigationMenuType
   */
  public function setValue($value)
  {
    $this->value = $value;
    return $this;
  }
}

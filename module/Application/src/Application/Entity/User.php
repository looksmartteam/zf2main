<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table(name="user", uniqueConstraints={@ORM\UniqueConstraint(name="user_name", columns={"nick"})}, indexes={@ORM\Index(name="fk_user_user_role1_idx", columns={"user_role_id"})})
 * @ORM\Entity
 */
class User extends \Application\DoctrineEntityAbstract
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  /**
   * @var string
   *
   * @ORM\Column(name="email_address", type="string", length=80, nullable=true)
   */
  private $emailAddress;
  /**
   * @var string
   *
   * @ORM\Column(name="nick", type="string", length=255, nullable=true)
   */
  private $nick;
  /**
   * @var string
   *
   * @ORM\Column(name="password", type="string", length=32, nullable=true)
   */
  private $password;
  /**
   * @var string
   *
   * @ORM\Column(name="salt", type="string", length=40, nullable=true)
   */
  private $salt;
  /**
   * @var integer
   *
   * @ORM\Column(name="created_date", type="integer", nullable=false)
   */
  private $createdDate;
  /**
   * @var integer
   *
   * @ORM\Column(name="updated_date", type="integer", nullable=false)
   */
  private $updatedDate;
  /**
   * @var boolean
   *
   * @ORM\Column(name="active", type="boolean", nullable=false)
   */
  private $active;
  /**
   * @var \Application\Entity\UserRole
   *
   * @ORM\ManyToOne(targetEntity="Application\Entity\UserRole")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="user_role_id", referencedColumnName="id")
   * })
   */
  private $userRole;

  /**
   * Get active
   *
   * @return boolean
   */
  public function getActive()
  {
    return $this->active;
  }

  /**
   * Get createdDate
   *
   * @return integer
   */
  public function getCreatedDate()
  {
    return $this->createdDate;
  }

  /**
   * Get emailAddress
   *
   * @return string
   */
  public function getEmailAddress()
  {
    return $this->emailAddress;
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get nick
   *
   * @return string
   */
  public function getNick()
  {
    return $this->nick;
  }

  /**
   * Get password
   *
   * @return string
   */
  public function getPassword()
  {
    return $this->password;
  }

  /**
   * Get salt
   *
   * @return string
   */
  public function getSalt()
  {
    return $this->salt;
  }

  /**
   * Get updatedDate
   *
   * @return integer
   */
  public function getUpdatedDate()
  {
    return $this->updatedDate;
  }

  /**
   * Get userRole
   *
   * @return \Application\Entity\UserRole
   */
  public function getUserRole()
  {
    return $this->userRole;
  }

  /**
   * Set active
   *
   * @param boolean $active
   *
   * @return User
   */
  public function setActive($active)
  {
    $this->active = $active;
    return $this;
  }

  /**
   * Set createdDate
   *
   * @param integer $createdDate
   *
   * @return User
   */
  public function setCreatedDate($createdDate)
  {
    $this->createdDate = $createdDate;
    return $this;
  }

  /**
   * Set emailAddress
   *
   * @param string $emailAddress
   *
   * @return User
   */
  public function setEmailAddress($emailAddress)
  {
    $this->emailAddress = $emailAddress;
    return $this;
  }

  /**
   * Set nick
   *
   * @param string $nick
   *
   * @return User
   */
  public function setNick($nick)
  {
    $this->nick = $nick;
    return $this;
  }

  /**
   * Set password
   *
   * @param string $password
   *
   * @return User
   */
  public function setPassword($password)
  {
    $this->password = $password;
    return $this;
  }

  /**
   * Set salt
   *
   * @param string $salt
   *
   * @return User
   */
  public function setSalt($salt)
  {
    $this->salt = $salt;
    return $this;
  }

  /**
   * Set updatedDate
   *
   * @param integer $updatedDate
   *
   * @return User
   */
  public function setUpdatedDate($updatedDate)
  {
    $this->updatedDate = $updatedDate;
    return $this;
  }

  /**
   * Set userRole
   *
   * @param \Application\Entity\UserRole $userRole
   *
   * @return User
   */
  public function setUserRole(\Application\Entity\UserRole $userRole = null)
  {
    $this->userRole = $userRole;
    return $this;
  }
}

<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NavigationResource
 *
 * @ORM\Table(name="navigation_resource")
 * @ORM\Entity
 */
class NavigationResource extends \Application\DoctrineEntityAbstract
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  /**
   * @var string
   *
   * @ORM\Column(name="value", type="string", length=45, nullable=false)
   */
  private $value;

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get value
   *
   * @return string
   */
  public function getValue()
  {
    return $this->value;
  }

  /**
   * Set value
   *
   * @param string $value
   *
   * @return NavigationResource
   */
  public function setValue($value)
  {
    $this->value = $value;
    return $this;
  }
}

<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NavigationPrivilege
 *
 * @ORM\Table(name="navigation_privilege")
 * @ORM\Entity
 */
class NavigationPrivilege extends \Application\DoctrineEntityAbstract
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  /**
   * @var string
   *
   * @ORM\Column(name="value", type="string", length=45, nullable=false)
   */
  private $value;
  /**
   * @var string
   *
   * @ORM\Column(name="unique_key", type="string", length=32, nullable=true)
   */
  private $uniqueKey;

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get uniqueKey
   *
   * @return string
   */
  public function getUniqueKey()
  {
    return $this->uniqueKey;
  }

  /**
   * Get value
   *
   * @return string
   */
  public function getValue()
  {
    return $this->value;
  }

  /**
   * Set uniqueKey
   *
   * @param string $uniqueKey
   *
   * @return NavigationPrivilege
   */
  public function setUniqueKey($uniqueKey)
  {
    $this->uniqueKey = $uniqueKey;
    return $this;
  }

  /**
   * Set value
   *
   * @param string $value
   *
   * @return NavigationPrivilege
   */
  public function setValue($value)
  {
    $this->value = $value;
    return $this;
  }
}

<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CartItem
 *
 * @ORM\Table(name="cart_item", indexes={@ORM\Index(name="fk_item_cart_item", columns={"item_id"}), @ORM\Index(name="fk_cart_cart_item", columns={"cart_id"})})
 * @ORM\Entity
 */
class CartItem extends \Application\DoctrineEntityAbstract
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="NONE")
   */
  private $id;
  /**
   * @var integer
   *
   * @ORM\Column(name="count", type="integer", nullable=false)
   */
  private $count;
  /**
   * @var integer
   *
   * @ORM\Column(name="created_date", type="integer", nullable=false)
   */
  private $createdDate;
  /**
   * @var integer
   *
   * @ORM\Column(name="updated_date", type="integer", nullable=false)
   */
  private $updatedDate;
  /**
   * @var \Application\Entity\Cart
   *
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="NONE")
   * @ORM\OneToOne(targetEntity="Application\Entity\Cart")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="cart_id", referencedColumnName="id")
   * })
   */
  private $cart;
  /**
   * @var \Application\Entity\Item
   *
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="NONE")
   * @ORM\OneToOne(targetEntity="Application\Entity\Item")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="item_id", referencedColumnName="id")
   * })
   */
  private $item;

  /**
   * Get cart
   *
   * @return \Application\Entity\Cart
   */
  public function getCart()
  {
    return $this->cart;
  }

  /**
   * Get count
   *
   * @return integer
   */
  public function getCount()
  {
    return $this->count;
  }

  /**
   * Get createdDate
   *
   * @return integer
   */
  public function getCreatedDate()
  {
    return $this->createdDate;
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get item
   *
   * @return \Application\Entity\Item
   */
  public function getItem()
  {
    return $this->item;
  }

  /**
   * Get updatedDate
   *
   * @return integer
   */
  public function getUpdatedDate()
  {
    return $this->updatedDate;
  }

  /**
   * Set cart
   *
   * @param \Application\Entity\Cart $cart
   *
   * @return CartItem
   */
  public function setCart(\Application\Entity\Cart $cart)
  {
    $this->cart = $cart;
    return $this;
  }

  /**
   * Set count
   *
   * @param integer $count
   *
   * @return CartItem
   */
  public function setCount($count)
  {
    $this->count = $count;
    return $this;
  }

  /**
   * Set createdDate
   *
   * @param integer $createdDate
   *
   * @return CartItem
   */
  public function setCreatedDate($createdDate)
  {
    $this->createdDate = $createdDate;
    return $this;
  }

  /**
   * Set id
   *
   * @param integer $id
   *
   * @return CartItem
   */
  public function setId($id)
  {
    $this->id = $id;
    return $this;
  }

  /**
   * Set item
   *
   * @param \Application\Entity\Item $item
   *
   * @return CartItem
   */
  public function setItem(\Application\Entity\Item $item)
  {
    $this->item = $item;
    return $this;
  }

  /**
   * Set updatedDate
   *
   * @param integer $updatedDate
   *
   * @return CartItem
   */
  public function setUpdatedDate($updatedDate)
  {
    $this->updatedDate = $updatedDate;
    return $this;
  }
}

<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Geolocation
 *
 * @ORM\Table(name="geolocation")
 * @ORM\Entity
 */
class Geolocation extends \Application\DoctrineEntityAbstract
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  /**
   * @var integer
   *
   * @ORM\Column(name="ip_start", type="bigint", nullable=true)
   */
  private $ipStart;
  /**
   * @var integer
   *
   * @ORM\Column(name="ip_end", type="bigint", nullable=true)
   */
  private $ipEnd;
  /**
   * @var string
   *
   * @ORM\Column(name="city", type="string", length=100, nullable=true)
   */
  private $city;
  /**
   * @var string
   *
   * @ORM\Column(name="county", type="string", length=100, nullable=true)
   */
  private $county;
  /**
   * @var string
   *
   * @ORM\Column(name="voivodeship", type="string", length=100, nullable=true)
   */
  private $voivodeship;

  /**
   * Get city
   *
   * @return string
   */
  public function getCity()
  {
    return $this->city;
  }

  /**
   * Get county
   *
   * @return string
   */
  public function getCounty()
  {
    return $this->county;
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get ipEnd
   *
   * @return integer
   */
  public function getIpEnd()
  {
    return $this->ipEnd;
  }

  /**
   * Get ipStart
   *
   * @return integer
   */
  public function getIpStart()
  {
    return $this->ipStart;
  }

  /**
   * Get voivodeship
   *
   * @return string
   */
  public function getVoivodeship()
  {
    return $this->voivodeship;
  }

  /**
   * Set city
   *
   * @param string $city
   *
   * @return Geolocation
   */
  public function setCity($city)
  {
    $this->city = $city;
    return $this;
  }

  /**
   * Set county
   *
   * @param string $county
   *
   * @return Geolocation
   */
  public function setCounty($county)
  {
    $this->county = $county;
    return $this;
  }

  /**
   * Set ipEnd
   *
   * @param integer $ipEnd
   *
   * @return Geolocation
   */
  public function setIpEnd($ipEnd)
  {
    $this->ipEnd = $ipEnd;
    return $this;
  }

  /**
   * Set ipStart
   *
   * @param integer $ipStart
   *
   * @return Geolocation
   */
  public function setIpStart($ipStart)
  {
    $this->ipStart = $ipStart;
    return $this;
  }

  /**
   * Set voivodeship
   *
   * @param string $voivodeship
   *
   * @return Geolocation
   */
  public function setVoivodeship($voivodeship)
  {
    $this->voivodeship = $voivodeship;
    return $this;
  }
}

<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cart
 *
 * @ORM\Table(name="cart", indexes={@ORM\Index(name="fk_user_cart", columns={"user_id"})})
 * @ORM\Entity
 */
class Cart extends \Application\DoctrineEntityAbstract
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  /**
   * @var integer
   *
   * @ORM\Column(name="created_date", type="integer", nullable=false)
   */
  private $createdDate;
  /**
   * @var integer
   *
   * @ORM\Column(name="updated_date", type="integer", nullable=false)
   */
  private $updatedDate;
  /**
   * @var \Application\Entity\User
   *
   * @ORM\ManyToOne(targetEntity="Application\Entity\User")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
   * })
   */
  private $user;

  /**
   * Get createdDate
   *
   * @return integer
   */
  public function getCreatedDate()
  {
    return $this->createdDate;
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get updatedDate
   *
   * @return integer
   */
  public function getUpdatedDate()
  {
    return $this->updatedDate;
  }

  /**
   * Get user
   *
   * @return \Application\Entity\User
   */
  public function getUser()
  {
    return $this->user;
  }

  /**
   * Set createdDate
   *
   * @param integer $createdDate
   *
   * @return Cart
   */
  public function setCreatedDate($createdDate)
  {
    $this->createdDate = $createdDate;
    return $this;
  }

  /**
   * Set updatedDate
   *
   * @param integer $updatedDate
   *
   * @return Cart
   */
  public function setUpdatedDate($updatedDate)
  {
    $this->updatedDate = $updatedDate;
    return $this;
  }

  /**
   * Set user
   *
   * @param \Application\Entity\User $user
   *
   * @return Cart
   */
  public function setUser(\Application\Entity\User $user = null)
  {
    $this->user = $user;
    return $this;
  }
}

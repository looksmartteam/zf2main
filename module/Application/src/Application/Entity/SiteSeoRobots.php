<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SiteSeoRobots
 *
 * @ORM\Table(name="site_seo_robots")
 * @ORM\Entity
 */
class SiteSeoRobots extends \Application\DoctrineEntityAbstract
{
  /**
   * @var boolean
   *
   * @ORM\Column(name="id", type="boolean", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  /**
   * @var string
   *
   * @ORM\Column(name="name", type="string", length=50, nullable=true)
   */
  private $name;
  /**
   * @var string
   *
   * @ORM\Column(name="description", type="string", length=255, nullable=true)
   */
  private $description;

  /**
   * Get description
   *
   * @return string
   */
  public function getDescription()
  {
    return $this->description;
  }

  /**
   * Get id
   *
   * @return boolean
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get name
   *
   * @return string
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * Set description
   *
   * @param string $description
   *
   * @return SiteSeoRobots
   */
  public function setDescription($description)
  {
    $this->description = $description;
    return $this;
  }

  /**
   * Set name
   *
   * @param string $name
   *
   * @return SiteSeoRobots
   */
  public function setName($name)
  {
    $this->name = $name;
    return $this;
  }
}

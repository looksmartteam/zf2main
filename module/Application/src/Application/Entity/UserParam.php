<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserParam
 *
 * @ORM\Table(name="user_param", indexes={@ORM\Index(name="fk_user_user_param", columns={"user_id"}), @ORM\Index(name="fk_user_param_user_category1_idx", columns={"user_category_id"})})
 * @ORM\Entity
 */
class UserParam extends \Application\DoctrineEntityAbstract
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="NONE")
   */
  private $id;
  /**
   * @var string
   *
   * @ORM\Column(name="pesel", type="string", length=11, nullable=false)
   */
  private $pesel;
  /**
   * @var integer
   *
   * @ORM\Column(name="borrower_hash", type="integer", nullable=true)
   */
  private $borrowerHash;
  /**
   * @var string
   *
   * @ORM\Column(name="bbarcode", type="string", length=50, nullable=true)
   */
  private $bbarcode;
  /**
   * @var string
   *
   * @ORM\Column(name="btype", type="string", length=20, nullable=true)
   */
  private $btype;
  /**
   * @var string
   *
   * @ORM\Column(name="borrower_note", type="string", length=255, nullable=true)
   */
  private $borrowerNote;
  /**
   * @var string
   *
   * @ORM\Column(name="student_id", type="string", length=50, nullable=true)
   */
  private $studentId;
  /**
   * @var string
   *
   * @ORM\Column(name="employee_id", type="string", length=50, nullable=true)
   */
  private $employeeId;
  /**
   * @var string
   *
   * @ORM\Column(name="first_name", type="string", length=100, nullable=true)
   */
  private $firstName;
  /**
   * @var string
   *
   * @ORM\Column(name="last_name", type="string", length=100, nullable=true)
   */
  private $lastName;
  /**
   * @var string
   *
   * @ORM\Column(name="street", type="string", length=100, nullable=true)
   */
  private $street;
  /**
   * @var string
   *
   * @ORM\Column(name="post_code", type="string", length=20, nullable=true)
   */
  private $postCode;
  /**
   * @var string
   *
   * @ORM\Column(name="city", type="string", length=100, nullable=true)
   */
  private $city;
  /**
   * @var string
   *
   * @ORM\Column(name="phone_number", type="string", length=20, nullable=true)
   */
  private $phoneNumber;
  /**
   * @var integer
   *
   * @ORM\Column(name="last_activity", type="integer", nullable=false)
   */
  private $lastActivity;
  /**
   * @var \Application\Entity\UserCategory
   *
   * @ORM\ManyToOne(targetEntity="Application\Entity\UserCategory")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="user_category_id", referencedColumnName="id")
   * })
   */
  private $userCategory;
  /**
   * @var \Application\Entity\User
   *
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="NONE")
   * @ORM\OneToOne(targetEntity="Application\Entity\User")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
   * })
   */
  private $user;

  /**
   * Get bbarcode
   *
   * @return string
   */
  public function getBbarcode()
  {
    return $this->bbarcode;
  }

  /**
   * Get borrowerHash
   *
   * @return integer
   */
  public function getBorrowerHash()
  {
    return $this->borrowerHash;
  }

  /**
   * Get borrowerNote
   *
   * @return string
   */
  public function getBorrowerNote()
  {
    return $this->borrowerNote;
  }

  /**
   * Get btype
   *
   * @return string
   */
  public function getBtype()
  {
    return $this->btype;
  }

  /**
   * Get city
   *
   * @return string
   */
  public function getCity()
  {
    return $this->city;
  }

  /**
   * Get employeeId
   *
   * @return string
   */
  public function getEmployeeId()
  {
    return $this->employeeId;
  }

  /**
   * Get firstName
   *
   * @return string
   */
  public function getFirstName()
  {
    return $this->firstName;
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get lastActivity
   *
   * @return integer
   */
  public function getLastActivity()
  {
    return $this->lastActivity;
  }

  /**
   * Get lastName
   *
   * @return string
   */
  public function getLastName()
  {
    return $this->lastName;
  }

  /**
   * Get pesel
   *
   * @return string
   */
  public function getPesel()
  {
    return $this->pesel;
  }

  /**
   * Get phoneNumber
   *
   * @return string
   */
  public function getPhoneNumber()
  {
    return $this->phoneNumber;
  }

  /**
   * Get postCode
   *
   * @return string
   */
  public function getPostCode()
  {
    return $this->postCode;
  }

  /**
   * Get street
   *
   * @return string
   */
  public function getStreet()
  {
    return $this->street;
  }

  /**
   * Get studentId
   *
   * @return string
   */
  public function getStudentId()
  {
    return $this->studentId;
  }

  /**
   * Get user
   *
   * @return \Application\Entity\User
   */
  public function getUser()
  {
    return $this->user;
  }

  /**
   * Get userCategory
   *
   * @return \Application\Entity\UserCategory
   */
  public function getUserCategory()
  {
    return $this->userCategory;
  }

  /**
   * Set bbarcode
   *
   * @param string $bbarcode
   *
   * @return UserParam
   */
  public function setBbarcode($bbarcode)
  {
    $this->bbarcode = $bbarcode;
    return $this;
  }

  /**
   * Set borrowerHash
   *
   * @param integer $borrowerHash
   *
   * @return UserParam
   */
  public function setBorrowerHash($borrowerHash)
  {
    $this->borrowerHash = $borrowerHash;
    return $this;
  }

  /**
   * Set borrowerNote
   *
   * @param string $borrowerNote
   *
   * @return UserParam
   */
  public function setBorrowerNote($borrowerNote)
  {
    $this->borrowerNote = $borrowerNote;
    return $this;
  }

  /**
   * Set btype
   *
   * @param string $btype
   *
   * @return UserParam
   */
  public function setBtype($btype)
  {
    $this->btype = $btype;
    return $this;
  }

  /**
   * Set city
   *
   * @param string $city
   *
   * @return UserParam
   */
  public function setCity($city)
  {
    $this->city = $city;
    return $this;
  }

  /**
   * Set employeeId
   *
   * @param string $employeeId
   *
   * @return UserParam
   */
  public function setEmployeeId($employeeId)
  {
    $this->employeeId = $employeeId;
    return $this;
  }

  /**
   * Set firstName
   *
   * @param string $firstName
   *
   * @return UserParam
   */
  public function setFirstName($firstName)
  {
    $this->firstName = $firstName;
    return $this;
  }

  /**
   * Set id
   *
   * @param integer $id
   *
   * @return UserParam
   */
  public function setId($id)
  {
    $this->id = $id;
    return $this;
  }

  /**
   * Set lastActivity
   *
   * @param integer $lastActivity
   *
   * @return UserParam
   */
  public function setLastActivity($lastActivity)
  {
    $this->lastActivity = $lastActivity;
    return $this;
  }

  /**
   * Set lastName
   *
   * @param string $lastName
   *
   * @return UserParam
   */
  public function setLastName($lastName)
  {
    $this->lastName = $lastName;
    return $this;
  }

  /**
   * Set pesel
   *
   * @param string $pesel
   *
   * @return UserParam
   */
  public function setPesel($pesel)
  {
    $this->pesel = $pesel;
    return $this;
  }

  /**
   * Set phoneNumber
   *
   * @param string $phoneNumber
   *
   * @return UserParam
   */
  public function setPhoneNumber($phoneNumber)
  {
    $this->phoneNumber = $phoneNumber;
    return $this;
  }

  /**
   * Set postCode
   *
   * @param string $postCode
   *
   * @return UserParam
   */
  public function setPostCode($postCode)
  {
    $this->postCode = $postCode;
    return $this;
  }

  /**
   * Set street
   *
   * @param string $street
   *
   * @return UserParam
   */
  public function setStreet($street)
  {
    $this->street = $street;
    return $this;
  }

  /**
   * Set studentId
   *
   * @param string $studentId
   *
   * @return UserParam
   */
  public function setStudentId($studentId)
  {
    $this->studentId = $studentId;
    return $this;
  }

  /**
   * Set user
   *
   * @param \Application\Entity\User $user
   *
   * @return UserParam
   */
  public function setUser(\Application\Entity\User $user)
  {
    $this->user = $user;
    return $this;
  }

  /**
   * Set userCategory
   *
   * @param \Application\Entity\UserCategory $userCategory
   *
   * @return UserParam
   */
  public function setUserCategory(\Application\Entity\UserCategory $userCategory = null)
  {
    $this->userCategory = $userCategory;
    return $this;
  }
}

<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NavigationOption
 *
 * @ORM\Table(name="navigation_option", indexes={@ORM\Index(name="fk_navigation_option_navigation_module_idx", columns={"navigation_module_id"}), @ORM\Index(name="fk_navigation_option_navigation_controller_idx", columns={"navigation_controller_id"}), @ORM\Index(name="fk_navigation_option_navigation_action_idx", columns={"navigation_action_id"}), @ORM\Index(name="fk_navigation_option_navigation_resource_idx", columns={"navigation_resource_id"}), @ORM\Index(name="fk_navigation_option_navigation_privilege_idx", columns={"navigation_privilege_id"})})
 * @ORM\Entity
 */
class NavigationOption extends \Application\DoctrineEntityAbstract
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="NONE")
   */
  private $id;
  /**
   * @var \Application\Entity\NavigationAction
   *
   * @ORM\ManyToOne(targetEntity="Application\Entity\NavigationAction")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="navigation_action_id", referencedColumnName="id")
   * })
   */
  private $navigationAction;
  /**
   * @var \Application\Entity\NavigationController
   *
   * @ORM\ManyToOne(targetEntity="Application\Entity\NavigationController")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="navigation_controller_id", referencedColumnName="id")
   * })
   */
  private $navigationController;
  /**
   * @var \Application\Entity\NavigationModule
   *
   * @ORM\ManyToOne(targetEntity="Application\Entity\NavigationModule")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="navigation_module_id", referencedColumnName="id")
   * })
   */
  private $navigationModule;
  /**
   * @var \Application\Entity\NavigationPrivilege
   *
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="NONE")
   * @ORM\OneToOne(targetEntity="Application\Entity\NavigationPrivilege")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="navigation_privilege_id", referencedColumnName="id")
   * })
   */
  private $navigationPrivilege;
  /**
   * @var \Application\Entity\NavigationResource
   *
   * @ORM\ManyToOne(targetEntity="Application\Entity\NavigationResource")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="navigation_resource_id", referencedColumnName="id")
   * })
   */
  private $navigationResource;
  /**
   * @var \Doctrine\Common\Collections\Collection
   *
   * @ORM\ManyToMany(targetEntity="Application\Entity\UserRole", inversedBy="navigationOption")
   * @ORM\JoinTable(name="navigation_option_user_role",
   *   joinColumns={
   *     @ORM\JoinColumn(name="navigation_option_id", referencedColumnName="id")
   *   },
   *   inverseJoinColumns={
   *     @ORM\JoinColumn(name="user_role_id", referencedColumnName="id")
   *   }
   * )
   */
  private $userRole;

  /**
   * Constructor
   */
  public function __construct()
  {
    $this->userRole = new \Doctrine\Common\Collections\ArrayCollection();
  }

  /**
   * Add userRole
   *
   * @param \Application\Entity\UserRole $userRole
   *
   * @return NavigationOption
   */
  public function addUserRole(\Application\Entity\UserRole $userRole)
  {
    $this->userRole[] = $userRole;
    return $this;
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get navigationAction
   *
   * @return \Application\Entity\NavigationAction
   */
  public function getNavigationAction()
  {
    return $this->navigationAction;
  }

  /**
   * Get navigationController
   *
   * @return \Application\Entity\NavigationController
   */
  public function getNavigationController()
  {
    return $this->navigationController;
  }

  /**
   * Get navigationModule
   *
   * @return \Application\Entity\NavigationModule
   */
  public function getNavigationModule()
  {
    return $this->navigationModule;
  }

  /**
   * Get navigationPrivilege
   *
   * @return \Application\Entity\NavigationPrivilege
   */
  public function getNavigationPrivilege()
  {
    return $this->navigationPrivilege;
  }

  /**
   * Get navigationResource
   *
   * @return \Application\Entity\NavigationResource
   */
  public function getNavigationResource()
  {
    return $this->navigationResource;
  }

  /**
   * Get userRole
   *
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getUserRole()
  {
    return $this->userRole;
  }

  /**
   * Remove userRole
   *
   * @param \Application\Entity\UserRole $userRole
   */
  public function removeUserRole(\Application\Entity\UserRole $userRole)
  {
    $this->userRole->removeElement($userRole);
  }

  /**
   * Set id
   *
   * @param integer $id
   *
   * @return NavigationOption
   */
  public function setId($id)
  {
    $this->id = $id;
    return $this;
  }

  /**
   * Set navigationAction
   *
   * @param \Application\Entity\NavigationAction $navigationAction
   *
   * @return NavigationOption
   */
  public function setNavigationAction(\Application\Entity\NavigationAction $navigationAction = null)
  {
    $this->navigationAction = $navigationAction;
    return $this;
  }

  /**
   * Set navigationController
   *
   * @param \Application\Entity\NavigationController $navigationController
   *
   * @return NavigationOption
   */
  public function setNavigationController(\Application\Entity\NavigationController $navigationController = null)
  {
    $this->navigationController = $navigationController;
    return $this;
  }

  /**
   * Set navigationModule
   *
   * @param \Application\Entity\NavigationModule $navigationModule
   *
   * @return NavigationOption
   */
  public function setNavigationModule(\Application\Entity\NavigationModule $navigationModule = null)
  {
    $this->navigationModule = $navigationModule;
    return $this;
  }

  /**
   * Set navigationPrivilege
   *
   * @param \Application\Entity\NavigationPrivilege $navigationPrivilege
   *
   * @return NavigationOption
   */
  public function setNavigationPrivilege(\Application\Entity\NavigationPrivilege $navigationPrivilege)
  {
    $this->navigationPrivilege = $navigationPrivilege;
    return $this;
  }

  /**
   * Set navigationResource
   *
   * @param \Application\Entity\NavigationResource $navigationResource
   *
   * @return NavigationOption
   */
  public function setNavigationResource(\Application\Entity\NavigationResource $navigationResource = null)
  {
    $this->navigationResource = $navigationResource;
    return $this;
  }
}

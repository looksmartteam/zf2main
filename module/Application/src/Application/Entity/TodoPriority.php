<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TodoPriority
 *
 * @ORM\Table(name="todo_priority")
 * @ORM\Entity
 */
class TodoPriority extends \Application\DoctrineEntityAbstract
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  /**
   * @var string
   *
   * @ORM\Column(name="name", type="string", length=50, nullable=false)
   */
  private $name;
  /**
   * @var string
   *
   * @ORM\Column(name="color", type="string", length=50, nullable=true)
   */
  private $color;

  /**
   * Get color
   *
   * @return string
   */
  public function getColor()
  {
    return $this->color;
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get name
   *
   * @return string
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * Set color
   *
   * @param string $color
   *
   * @return TodoPriority
   */
  public function setColor($color)
  {
    $this->color = $color;
    return $this;
  }

  /**
   * Set name
   *
   * @param string $name
   *
   * @return TodoPriority
   */
  public function setName($name)
  {
    $this->name = $name;
    return $this;
  }
}

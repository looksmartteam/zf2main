<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tablea
 *
 * @ORM\Table(name="tablea")
 * @ORM\Entity
 */
class Tablea extends \Application\DoctrineEntityAbstract
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  /**
   * @var string
   *
   * @ORM\Column(name="value", type="string", length=45, nullable=true)
   */
  private $value;
  /**
   * @var \Doctrine\Common\Collections\Collection
   *
   * @ORM\ManyToMany(targetEntity="Application\Entity\Tableb", inversedBy="tablea")
   * @ORM\JoinTable(name="tablea_has_tableb",
   *   joinColumns={
   *     @ORM\JoinColumn(name="tableA_id", referencedColumnName="id")
   *   },
   *   inverseJoinColumns={
   *     @ORM\JoinColumn(name="tableB_id", referencedColumnName="id")
   *   }
   * )
   */
  private $tableb;

  /**
   * Constructor
   */
  public function __construct()
  {
    $this->tableb = new \Doctrine\Common\Collections\ArrayCollection();
  }

  /**
   * Add tableb
   *
   * @param \Application\Entity\Tableb $tableb
   *
   * @return Tablea
   */
  public function addTableb(\Application\Entity\Tableb $tableb)
  {
    $this->tableb[] = $tableb;
    return $this;
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get tableb
   *
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getTableb()
  {
    return $this->tableb;
  }

  /**
   * Get value
   *
   * @return string
   */
  public function getValue()
  {
    return $this->value;
  }

  /**
   * Remove tableb
   *
   * @param \Application\Entity\Tableb $tableb
   */
  public function removeTableb(\Application\Entity\Tableb $tableb)
  {
    $this->tableb->removeElement($tableb);
  }

  /**
   * Set value
   *
   * @param string $value
   *
   * @return Tablea
   */
  public function setValue($value)
  {
    $this->value = $value;
    return $this;
  }
}

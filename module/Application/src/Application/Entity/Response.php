<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Response
 *
 * @ORM\Table(name="response", indexes={@ORM\Index(name="fk_order_payment_response", columns={"order_payment_id"})})
 * @ORM\Entity
 */
class Response extends \Application\DoctrineEntityAbstract
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  /**
   * @var string
   *
   * @ORM\Column(name="payment_id", type="string", length=255, nullable=false)
   */
  private $paymentId;
  /**
   * @var string
   *
   * @ORM\Column(name="pos_id", type="string", length=255, nullable=false)
   */
  private $posId;
  /**
   * @var string
   *
   * @ORM\Column(name="session_id", type="string", length=32, nullable=false)
   */
  private $sessionId;
  /**
   * @var string
   *
   * @ORM\Column(name="order_id", type="string", length=32, nullable=false)
   */
  private $orderId;
  /**
   * @var string
   *
   * @ORM\Column(name="amount", type="string", length=255, nullable=false)
   */
  private $amount;
  /**
   * @var string
   *
   * @ORM\Column(name="status", type="string", length=255, nullable=false)
   */
  private $status;
  /**
   * @var string
   *
   * @ORM\Column(name="pay_type", type="string", length=255, nullable=false)
   */
  private $payType;
  /**
   * @var string
   *
   * @ORM\Column(name="pay_gw_name", type="string", length=255, nullable=false)
   */
  private $payGwName;
  /**
   * @var string
   *
   * @ORM\Column(name="desc", type="string", length=255, nullable=false)
   */
  private $desc;
  /**
   * @var string
   *
   * @ORM\Column(name="desc2", type="string", length=255, nullable=false)
   */
  private $desc2;
  /**
   * @var string
   *
   * @ORM\Column(name="create", type="string", length=255, nullable=false)
   */
  private $create;
  /**
   * @var string
   *
   * @ORM\Column(name="init", type="string", length=255, nullable=false)
   */
  private $init;
  /**
   * @var string
   *
   * @ORM\Column(name="sent", type="string", length=255, nullable=false)
   */
  private $sent;
  /**
   * @var string
   *
   * @ORM\Column(name="recv", type="string", length=255, nullable=false)
   */
  private $recv;
  /**
   * @var string
   *
   * @ORM\Column(name="cancel", type="string", length=255, nullable=false)
   */
  private $cancel;
  /**
   * @var string
   *
   * @ORM\Column(name="auth_fraud", type="string", length=255, nullable=false)
   */
  private $authFraud;
  /**
   * @var string
   *
   * @ORM\Column(name="ts", type="string", length=255, nullable=false)
   */
  private $ts;
  /**
   * @var string
   *
   * @ORM\Column(name="sig", type="string", length=255, nullable=false)
   */
  private $sig;
  /**
   * @var integer
   *
   * @ORM\Column(name="date", type="integer", nullable=false)
   */
  private $date;
  /**
   * @var \Application\Entity\OrderPayment
   *
   * @ORM\ManyToOne(targetEntity="Application\Entity\OrderPayment")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="order_payment_id", referencedColumnName="id")
   * })
   */
  private $orderPayment;

  /**
   * Get amount
   *
   * @return string
   */
  public function getAmount()
  {
    return $this->amount;
  }

  /**
   * Get authFraud
   *
   * @return string
   */
  public function getAuthFraud()
  {
    return $this->authFraud;
  }

  /**
   * Get cancel
   *
   * @return string
   */
  public function getCancel()
  {
    return $this->cancel;
  }

  /**
   * Get create
   *
   * @return string
   */
  public function getCreate()
  {
    return $this->create;
  }

  /**
   * Get date
   *
   * @return integer
   */
  public function getDate()
  {
    return $this->date;
  }

  /**
   * Get desc
   *
   * @return string
   */
  public function getDesc()
  {
    return $this->desc;
  }

  /**
   * Get desc2
   *
   * @return string
   */
  public function getDesc2()
  {
    return $this->desc2;
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get init
   *
   * @return string
   */
  public function getInit()
  {
    return $this->init;
  }

  /**
   * Get orderId
   *
   * @return string
   */
  public function getOrderId()
  {
    return $this->orderId;
  }

  /**
   * Get orderPayment
   *
   * @return \Application\Entity\OrderPayment
   */
  public function getOrderPayment()
  {
    return $this->orderPayment;
  }

  /**
   * Get payGwName
   *
   * @return string
   */
  public function getPayGwName()
  {
    return $this->payGwName;
  }

  /**
   * Get payType
   *
   * @return string
   */
  public function getPayType()
  {
    return $this->payType;
  }

  /**
   * Get paymentId
   *
   * @return string
   */
  public function getPaymentId()
  {
    return $this->paymentId;
  }

  /**
   * Get posId
   *
   * @return string
   */
  public function getPosId()
  {
    return $this->posId;
  }

  /**
   * Get recv
   *
   * @return string
   */
  public function getRecv()
  {
    return $this->recv;
  }

  /**
   * Get sent
   *
   * @return string
   */
  public function getSent()
  {
    return $this->sent;
  }

  /**
   * Get sessionId
   *
   * @return string
   */
  public function getSessionId()
  {
    return $this->sessionId;
  }

  /**
   * Get sig
   *
   * @return string
   */
  public function getSig()
  {
    return $this->sig;
  }

  /**
   * Get status
   *
   * @return string
   */
  public function getStatus()
  {
    return $this->status;
  }

  /**
   * Get ts
   *
   * @return string
   */
  public function getTs()
  {
    return $this->ts;
  }

  /**
   * Set amount
   *
   * @param string $amount
   *
   * @return Response
   */
  public function setAmount($amount)
  {
    $this->amount = $amount;
    return $this;
  }

  /**
   * Set authFraud
   *
   * @param string $authFraud
   *
   * @return Response
   */
  public function setAuthFraud($authFraud)
  {
    $this->authFraud = $authFraud;
    return $this;
  }

  /**
   * Set cancel
   *
   * @param string $cancel
   *
   * @return Response
   */
  public function setCancel($cancel)
  {
    $this->cancel = $cancel;
    return $this;
  }

  /**
   * Set create
   *
   * @param string $create
   *
   * @return Response
   */
  public function setCreate($create)
  {
    $this->create = $create;
    return $this;
  }

  /**
   * Set date
   *
   * @param integer $date
   *
   * @return Response
   */
  public function setDate($date)
  {
    $this->date = $date;
    return $this;
  }

  /**
   * Set desc
   *
   * @param string $desc
   *
   * @return Response
   */
  public function setDesc($desc)
  {
    $this->desc = $desc;
    return $this;
  }

  /**
   * Set desc2
   *
   * @param string $desc2
   *
   * @return Response
   */
  public function setDesc2($desc2)
  {
    $this->desc2 = $desc2;
    return $this;
  }

  /**
   * Set init
   *
   * @param string $init
   *
   * @return Response
   */
  public function setInit($init)
  {
    $this->init = $init;
    return $this;
  }

  /**
   * Set orderId
   *
   * @param string $orderId
   *
   * @return Response
   */
  public function setOrderId($orderId)
  {
    $this->orderId = $orderId;
    return $this;
  }

  /**
   * Set orderPayment
   *
   * @param \Application\Entity\OrderPayment $orderPayment
   *
   * @return Response
   */
  public function setOrderPayment(\Application\Entity\OrderPayment $orderPayment = null)
  {
    $this->orderPayment = $orderPayment;
    return $this;
  }

  /**
   * Set payGwName
   *
   * @param string $payGwName
   *
   * @return Response
   */
  public function setPayGwName($payGwName)
  {
    $this->payGwName = $payGwName;
    return $this;
  }

  /**
   * Set payType
   *
   * @param string $payType
   *
   * @return Response
   */
  public function setPayType($payType)
  {
    $this->payType = $payType;
    return $this;
  }

  /**
   * Set paymentId
   *
   * @param string $paymentId
   *
   * @return Response
   */
  public function setPaymentId($paymentId)
  {
    $this->paymentId = $paymentId;
    return $this;
  }

  /**
   * Set posId
   *
   * @param string $posId
   *
   * @return Response
   */
  public function setPosId($posId)
  {
    $this->posId = $posId;
    return $this;
  }

  /**
   * Set recv
   *
   * @param string $recv
   *
   * @return Response
   */
  public function setRecv($recv)
  {
    $this->recv = $recv;
    return $this;
  }

  /**
   * Set sent
   *
   * @param string $sent
   *
   * @return Response
   */
  public function setSent($sent)
  {
    $this->sent = $sent;
    return $this;
  }

  /**
   * Set sessionId
   *
   * @param string $sessionId
   *
   * @return Response
   */
  public function setSessionId($sessionId)
  {
    $this->sessionId = $sessionId;
    return $this;
  }

  /**
   * Set sig
   *
   * @param string $sig
   *
   * @return Response
   */
  public function setSig($sig)
  {
    $this->sig = $sig;
    return $this;
  }

  /**
   * Set status
   *
   * @param string $status
   *
   * @return Response
   */
  public function setStatus($status)
  {
    $this->status = $status;
    return $this;
  }

  /**
   * Set ts
   *
   * @param string $ts
   *
   * @return Response
   */
  public function setTs($ts)
  {
    $this->ts = $ts;
    return $this;
  }
}

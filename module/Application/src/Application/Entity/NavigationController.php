<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NavigationController
 *
 * @ORM\Table(name="navigation_controller", uniqueConstraints={@ORM\UniqueConstraint(name="value_UNIQUE", columns={"value"})})
 * @ORM\Entity
 */
class NavigationController extends \Application\DoctrineEntityAbstract
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  /**
   * @var string
   *
   * @ORM\Column(name="value", type="string", length=45, nullable=false)
   */
  private $value;
  /**
   * @var integer
   *
   * @ORM\Column(name="created_date", type="integer", nullable=false)
   */
  private $createdDate;
  /**
   * @var integer
   *
   * @ORM\Column(name="updated_date", type="integer", nullable=false)
   */
  private $updatedDate;

  /**
   * Get createdDate
   *
   * @return integer
   */
  public function getCreatedDate()
  {
    return $this->createdDate;
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get updatedDate
   *
   * @return integer
   */
  public function getUpdatedDate()
  {
    return $this->updatedDate;
  }

  /**
   * Get value
   *
   * @return string
   */
  public function getValue()
  {
    return $this->value;
  }

  /**
   * Set createdDate
   *
   * @param integer $createdDate
   *
   * @return NavigationController
   */
  public function setCreatedDate($createdDate)
  {
    $this->createdDate = $createdDate;
    return $this;
  }

  /**
   * Set updatedDate
   *
   * @param integer $updatedDate
   *
   * @return NavigationController
   */
  public function setUpdatedDate($updatedDate)
  {
    $this->updatedDate = $updatedDate;
    return $this;
  }

  /**
   * Set value
   *
   * @param string $value
   *
   * @return NavigationController
   */
  public function setValue($value)
  {
    $this->value = $value;
    return $this;
  }
}

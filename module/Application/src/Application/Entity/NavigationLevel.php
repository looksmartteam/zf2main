<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NavigationLevel
 *
 * @ORM\Table(name="navigation_level")
 * @ORM\Entity
 */
class NavigationLevel extends \Application\DoctrineEntityAbstract
{
  /**
   * @var boolean
   *
   * @ORM\Column(name="id", type="boolean", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  /**
   * @var boolean
   *
   * @ORM\Column(name="level", type="boolean", nullable=false)
   */
  private $level;
  /**
   * @var string
   *
   * @ORM\Column(name="name", type="string", length=20, nullable=false)
   */
  private $name;

  /**
   * Get id
   *
   * @return boolean
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get level
   *
   * @return boolean
   */
  public function getLevel()
  {
    return $this->level;
  }

  /**
   * Get name
   *
   * @return string
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * Set level
   *
   * @param boolean $level
   *
   * @return NavigationLevel
   */
  public function setLevel($level)
  {
    $this->level = $level;
    return $this;
  }

  /**
   * Set name
   *
   * @param string $name
   *
   * @return NavigationLevel
   */
  public function setName($name)
  {
    $this->name = $name;
    return $this;
  }
}

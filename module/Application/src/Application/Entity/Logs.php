<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Logs
 *
 * @ORM\Table(name="logs", indexes={@ORM\Index(name="fk_logs_geolocation", columns={"geolocation_id"})})
 * @ORM\Entity
 */
class Logs extends \Application\DoctrineEntityAbstract
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  /**
   * @var string
   *
   * @ORM\Column(name="module", type="string", length=100, nullable=false)
   */
  private $module;
  /**
   * @var string
   *
   * @ORM\Column(name="controller", type="string", length=100, nullable=false)
   */
  private $controller;
  /**
   * @var string
   *
   * @ORM\Column(name="action", type="string", length=100, nullable=false)
   */
  private $action;
  /**
   * @var integer
   *
   * @ORM\Column(name="user_id", type="integer", nullable=true)
   */
  private $userId;
  /**
   * @var string
   *
   * @ORM\Column(name="ip_address", type="string", length=15, nullable=true)
   */
  private $ipAddress;
  /**
   * @var string
   *
   * @ORM\Column(name="description", type="text", length=65535, nullable=false)
   */
  private $description;
  /**
   * @var integer
   *
   * @ORM\Column(name="date", type="integer", nullable=false)
   */
  private $date;
  /**
   * @var \Application\Entity\Geolocation
   *
   * @ORM\ManyToOne(targetEntity="Application\Entity\Geolocation")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="geolocation_id", referencedColumnName="id")
   * })
   */
  private $geolocation;

  /**
   * Get action
   *
   * @return string
   */
  public function getAction()
  {
    return $this->action;
  }

  /**
   * Get controller
   *
   * @return string
   */
  public function getController()
  {
    return $this->controller;
  }

  /**
   * Get date
   *
   * @return integer
   */
  public function getDate()
  {
    return $this->date;
  }

  /**
   * Get description
   *
   * @return string
   */
  public function getDescription()
  {
    return $this->description;
  }

  /**
   * Get geolocation
   *
   * @return \Application\Entity\Geolocation
   */
  public function getGeolocation()
  {
    return $this->geolocation;
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get ipAddress
   *
   * @return string
   */
  public function getIpAddress()
  {
    return $this->ipAddress;
  }

  /**
   * Get module
   *
   * @return string
   */
  public function getModule()
  {
    return $this->module;
  }

  /**
   * Get userId
   *
   * @return integer
   */
  public function getUserId()
  {
    return $this->userId;
  }

  /**
   * Set action
   *
   * @param string $action
   *
   * @return Logs
   */
  public function setAction($action)
  {
    $this->action = $action;
    return $this;
  }

  /**
   * Set controller
   *
   * @param string $controller
   *
   * @return Logs
   */
  public function setController($controller)
  {
    $this->controller = $controller;
    return $this;
  }

  /**
   * Set date
   *
   * @param integer $date
   *
   * @return Logs
   */
  public function setDate($date)
  {
    $this->date = $date;
    return $this;
  }

  /**
   * Set description
   *
   * @param string $description
   *
   * @return Logs
   */
  public function setDescription($description)
  {
    $this->description = $description;
    return $this;
  }

  /**
   * Set geolocation
   *
   * @param \Application\Entity\Geolocation $geolocation
   *
   * @return Logs
   */
  public function setGeolocation(\Application\Entity\Geolocation $geolocation = null)
  {
    $this->geolocation = $geolocation;
    return $this;
  }

  /**
   * Set ipAddress
   *
   * @param string $ipAddress
   *
   * @return Logs
   */
  public function setIpAddress($ipAddress)
  {
    $this->ipAddress = $ipAddress;
    return $this;
  }

  /**
   * Set module
   *
   * @param string $module
   *
   * @return Logs
   */
  public function setModule($module)
  {
    $this->module = $module;
    return $this;
  }

  /**
   * Set userId
   *
   * @param integer $userId
   *
   * @return Logs
   */
  public function setUserId($userId)
  {
    $this->userId = $userId;
    return $this;
  }
}

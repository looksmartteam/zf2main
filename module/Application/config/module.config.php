<?php
namespace Application;
return array(
  'router' => array(
    'routes' => array(
      'home' => array(
        'type' => 'Zend\Mvc\Router\Http\Literal',
        'options' => array(
          'route' => '/',
          'defaults' => array(
            '__NAMESPACE__' => 'Application\Controller',
            'controller' => 'Index',
            'action' => 'index',
          ),
        ),
        'may_terminate' => true,
        'child_routes' => array(
          'default' => array(
            'type' => 'Segment',
            'options' => array(
              'route' => '/[:controller[/:action]]',
              'constraints' => array(
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
              ),
              'defaults' => array(),
            ),
          ),
        ),
      ),
      'application_index' => array(
        'type' => 'Literal',
        'options' => array(
          'route' => '/application',
          'defaults' => array(
            '__NAMESPACE__' => 'Application\Controller',
            'controller' => 'Index',
            'action' => 'index',
          ),
        ),
        'may_terminate' => true,
        'child_routes' => array(
          'application_index_process' => array(
            'type' => 'Segment',
            'options' => array(
              'route' => '/[:controller[/:action]]',
              'constraints' => array(
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
              ),
              'defaults' => array(),
            ),
          ),
        ),
      ),
      'application_settings' => array(
        'type' => 'Zend\Mvc\Router\Http\Literal',
        'options' => array(
          'route' => '/application/settings',
          'defaults' => array(
            '__NAMESPACE__' => 'Application\Controller',
            'controller' => 'Settings',
            'action' => 'index',
          ),
        ),
        'may_terminate' => true,
        'child_routes' => array(
          'application_settings_process' => array(
            'type' => 'Segment',
            'options' => array(
              'route' => '/[:action]',
              'constraints' => array(
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
              ),
              'defaults' => array(),
            ),
          ),
        ),
      ),
      'application_navigation' => array(
        'type' => 'Zend\Mvc\Router\Http\Literal',
        'options' => array(
          'route' => '/application/navigation',
          'defaults' => array(
            '__NAMESPACE__' => 'Application\Controller',
            'controller' => 'Navigation',
            'action' => 'index',
          ),
        ),
        'may_terminate' => true,
        'child_routes' => array(
          'application_navigation_process' => array(
            'type' => 'Segment',
            'options' => array(
              'route' => '/[:action]',
              'constraints' => array(
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
              ),
              'defaults' => array(),
            ),
          ),
        ),
      ),
      'application_email' => array(
        'type' => 'Zend\Mvc\Router\Http\Literal',
        'options' => array(
          'route' => '/application/email',
          'defaults' => array(
            '__NAMESPACE__' => 'Application\Controller',
            'controller' => 'Email',
            'action' => 'index',
          ),
        ),
        'may_terminate' => true,
        'child_routes' => array(
          'application_email_process' => array(
            'type' => 'Segment',
            'options' => array(
              'route' => '/[:action]',
              'constraints' => array(
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
              ),
              'defaults' => array(),
            ),
          ),
        ),
      ),
      'application_todo' => array(
        'type' => 'Zend\Mvc\Router\Http\Literal',
        'options' => array(
          'route' => '/application/todo',
          'defaults' => array(
            '__NAMESPACE__' => 'Application\Controller',
            'controller' => 'Todo',
            'action' => 'index',
          ),
        ),
        'may_terminate' => true,
        'child_routes' => array(
          'application_todo_process' => array(
            'type' => 'Segment',
            'options' => array(
              'route' => '/[:action]',
              'constraints' => array(
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
              ),
              'defaults' => array(),
            ),
          ),
        ),
      ),
      'application_api' => array(
        'type' => 'Segment',
        'options' => array(
          'route' => '/api[/:id]',
          'constraints' => array(
            'id' => '[0-9]+',
          ),
          'defaults' => array(
            'controller' => 'Application\Controller\API',
          ),
        ),
      ),
    ),
  ),
  'service_manager' => array(
    'abstract_factories' => array(
      'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
      'Zend\Log\LoggerAbstractServiceFactory',
    ),
    'aliases' => array(
      'translator' => 'MvcTranslator',
    ),
    'factories' => array(
      'Navigation' => 'Application\Navigation\ApplicationNavigationFactory',
      'DefaultDbAdapter' => 'Application\Service\DefaultDbAdapter',
    ),
  ),
  'translator' => array(
    'locale' => 'pl_PL',
    'translation_file_patterns' => array(
      array(
        'type' => 'gettext',
        'base_dir' => __DIR__ . '/../language',
        'pattern' => '/%s/Layout.mo',
      ),
      array(
        'type' => 'phparray',
        'base_dir' => __DIR__ . '/../language',
        'pattern' => '/%s/Layout.php',
      ),
      array(
        'type' => 'phparray',
        'base_dir' => __DIR__ . '/../language',
        'pattern' => '/%s/Zend_Validate.php',
      ),
      array(
        'type' => 'phparray',
        'base_dir' => __DIR__ . '/../language',
        'pattern' => '/%s/Zend_Captcha.php',
      ),
    ),
  ),
  'controllers' => array(
    'invokables' => array(
      'Application\Controller\Index' => 'Application\Controller\IndexController',
      'Application\Controller\Email' => 'Application\Controller\EmailController',
      'Application\Controller\Navigation' => 'Application\Controller\NavigationController',
      'Application\Controller\Settings' => 'Application\Controller\SettingsController',
      'Application\Controller\Todo' => 'Application\Controller\TodoController',
    ),
  ),
  'controller_plugins' => array(
    'invokables' => array(
      'LogsPlugin' => 'Application\Controller\Plugin\LogsPlugin',
      'Application\Controller\Index' => 'Application\Controller\IndexController',
    )
  ),
  'view_manager' => array(
    'display_not_found_reason' => true,
    'display_exceptions' => true,
    'doctype' => 'HTML5',
    'not_found_template' => 'error/404',
    'exception_template' => 'error/index',
    'template_map' => array(
      'layout/layout_administrator' => __DIR__ . '/../view/layout/layout_administrator.phtml',
      'layout/layout_home' => __DIR__ . '/../view/layout/layout_home.phtml',
      'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
      'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
      'error/404' => __DIR__ . '/../view/error/404.phtml',
      'error/index' => __DIR__ . '/../view/error/index.phtml',
      'partials/breadcrumbs' => __DIR__ . '/../view/partials/breadcrumbs.phtml',
      'partials/confirm_modal' => __DIR__ . '/../view/partials/confirm_modal.phtml',
      'partials/form' => __DIR__ . '/../view/partials/default_form.phtml',
      'partials/default_form_modal' => __DIR__ . '/../view/partials/default_form_modal.phtml',
      'partials/empty_result_modal' => __DIR__ . '/../view/partials/empty_result_modal.phtml',
      'partials/table_default_control_panel' => __DIR__ . '/../view/partials/table_default_control_panel.phtml',
      'partials/table_default_row_buttons' => __DIR__ . '/../view/partials/table_default_row_buttons.phtml',
    ),
    'template_path_stack' => array(
      'application' => __DIR__ . '/../view',
    ),
    'strategies' => array(//'ViewJsonStrategy',
    ),
  ),
  'view_helpers' => array(
    'invokables' => array(
      'ApplicationMenu' => 'Application\View\Helper\ApplicationMenu',
    )
  ),
  'module_layouts' => array(
    'Application' => 'layout/layout_home',
  ),
  'doctrine' => array(
    'driver' => array(
      __NAMESPACE__ . '_driver' => array(
        'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
        'cache' => 'array',
        'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
      ),
      'orm_default' => array(
        'drivers' => array(
          __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
        )
      )
    )
  ),
);

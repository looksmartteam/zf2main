<?php
namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module
{
  public function getAutoloaderConfig()
  {
    return array(
      'Zend\Loader\StandardAutoloader' => array(
        'namespaces' => array(
          __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
        ),
      ),
    );
  }

  public function getConfig()
  {
    return include __DIR__ . '/config/module.config.php';
  }

  public function getServiceConfig()
  {
    $aFactories = array();
    $aSearchableDirectory = array('Form', 'Entity');
    foreach ($aSearchableDirectory as $sDirectory) {
      $sDir = __DIR__ . '/src/' . __NAMESPACE__ . '/' . $sDirectory;
      if (file_exists($sDir)) {
        $oFiles = new \DirectoryIterator($sDir);
        foreach ($oFiles as $oFileInfo) {
          $bIsFile = strpos($oFileInfo->__toString(), '~');
          if (!$oFileInfo->isDot() && !$oFileInfo->isDir() && !$bIsFile) {
            $sClassName = str_replace('.php', '', $oFileInfo->__toString());
            if ($sDirectory == 'Entity') {
              $aFactories[$sClassName . 'DoctrineEntityService'] = function ($oServiceLocator) use ($sClassName, $sDirectory) {
                $sClassName = __NAMESPACE__ . '\\' . $sDirectory . '\\' . $sClassName;
                $oNewClassInstance = new $sClassName();
                return $oNewClassInstance;
              };
            } else if ($sDirectory == 'Form') {
              $aSnippedName = preg_split('/(?=[A-Z])/', $sClassName);
              if (is_array($aSnippedName) && count($aSnippedName)) {
                $sKey = '';
                $nIdx = count($aSnippedName);
                foreach ($aSnippedName as $sSnippedName) {
                  $nIdx--;
                  if (strlen($sSnippedName)) {
                    $sKey .= lcfirst($sSnippedName);
                    if ($nIdx) {
                      $sKey .= '_';
                    }
                  }
                }
                $sFormName = $sKey;
                $sEdidFormName = 'edit_' . $sKey;
              }
              $aFactories[$sClassName . 'FormService'] = function ($oServiceLocator) use ($sClassName, $sDirectory, $sFormName) {
                $sClassName = __NAMESPACE__ . '\\' . $sDirectory . '\\' . $sClassName;
                $oNewClassInstance = new $sClassName($oServiceLocator, $sFormName);
                return $oNewClassInstance;
              };
              $aFactories['Edit' . $sClassName . 'FormService'] = function ($oServiceLocator) use ($sClassName, $sDirectory, $sEdidFormName) {
                $sClassName = __NAMESPACE__ . '\\' . $sDirectory . '\\' . $sClassName;
                $oNewClassInstance = new $sClassName($oServiceLocator, $sEdidFormName);
                return $oNewClassInstance;
              };
            }
          }
        }
      }
    }
    $aSearchableDirectory = array('Entity', 'InputFilter', 'Repository');
    foreach ($aSearchableDirectory as $sDirectory) {
      $sDir = __DIR__ . '/src/' . __NAMESPACE__ . '/Model/' . $sDirectory;
      if (file_exists($sDir)) {
        $oFiles = new \DirectoryIterator($sDir);
        foreach ($oFiles as $oFileInfo) {
          $bIsFile = strpos($oFileInfo->__toString(), '~');
          if (!$oFileInfo->isDot() && !$oFileInfo->isDir() && !$bIsFile) {
            $sClassName = str_replace('.php', '', $oFileInfo->__toString());
            if ($sDirectory == 'Entity') {
              $aFactories[$sClassName . 'NativEntityService'] = function ($oServiceLocator) use ($sClassName, $sDirectory) {
                $sClassName = __NAMESPACE__ . '\Model\\' . $sDirectory . '\\' . $sClassName;
                $oNewClassInstance = new $sClassName($oServiceLocator);
                return $oNewClassInstance;
              };
            } else if ($sDirectory == 'Repository') {
              $aFactories[$sClassName . 'TableService'] = function ($oServiceLocator) use ($sClassName, $sDirectory) {
                $sClassName = __NAMESPACE__ . '\Model\\' . $sDirectory . '\\' . $sClassName;
                $oDbAdapter = $oServiceLocator->get('DefaultDbAdapter');
                $oNewClassInstance = new $sClassName($oDbAdapter);
                return $oNewClassInstance;
              };
            } else {
              $aFactories[$sClassName . $sDirectory . 'Service'] = function ($oServiceLocator) use ($sClassName, $sDirectory) {
                $sClassName = __NAMESPACE__ . '\Model\\' . $sDirectory . '\\' . $sClassName;
                $oNewClassInstance = new $sClassName($oServiceLocator);
                return $oNewClassInstance;
              };
            }
          }
        }
      }
    }
    $aFactories['AppService'] = function ($oServiceLocator) {
      $sClassName = __NAMESPACE__ . '\Service\App';
      $oNewClassInstance = new $sClassName($oServiceLocator);
      return $oNewClassInstance;
    };
    return array(
      'factories' => $aFactories,
    );
  }

  public function onBootstrap(MvcEvent $e)
  {
    $oApplication = $e->getApplication();
    $oServiceLocator = $oApplication->getServiceManager();
    $oSharedManager = $oApplication->getEventManager()->getSharedManager();
    $oEventManager = $oApplication->getEventManager();
    $oModuleRouteListener = new ModuleRouteListener();
    $oModuleRouteListener->attach($oEventManager);
    $oRouter = $oServiceLocator->get('router');
    $oRequest = $oServiceLocator->get('request');
    $oMatchedRoute = $oRouter->match($oRequest);
    if (null !== $oMatchedRoute) {
      $oSharedManager->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', function ($e) use ($oServiceLocator) {
        if ($oServiceLocator->get('ControllerPluginManager')->has('LogsPlugin')) {
          //$oServiceLocator->get('ControllerPluginManager')->get('LogsPlugin')->init($e);
        }
      }, 2);
    }
    $oSharedManager->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', function ($e) use ($oServiceLocator) {
      $oController = $e->getTarget();
      $sControllerClassName = get_class($oController);
      $sModuleName = substr($sControllerClassName, 0, strpos($sControllerClassName, '\\'));
      $aConfig = $oServiceLocator->get('config');
      if (isset($aConfig['module_layouts'][$sModuleName])) {
        //$oController->layout($aConfig['module_layouts'][$sModuleName]);
      }
    }, 100);
    /*
    $oSharedManager->attach('User\Controller\Plugin\AuthPlugin', 'auth', function ($e) use ($oServiceLocator) {
      $oParam = $e->getParams();
      $oMail = new \Application\Mail($oServiceLocator);
      $oMail->fillMessage('sendLoginMessage', $oParam);
    }, -10);
    */
    $oTranslator = $oServiceLocator->get('translator');
    \Zend\Validator\AbstractValidator::setDefaultTranslator($oTranslator);
    //$oEventManager->attach('dispatch.error', new \Application\Listener\DispatchErrorHandlerListener(), 100);
    $oEventManager->attach(MvcEvent::EVENT_DISPATCH_ERROR, function (MvcEvent $oEvent) {
      $oViewModel = $oEvent->getViewModel();
      $oViewModel->setTemplate('layout/layout_404');
    }, -200);
  }
}
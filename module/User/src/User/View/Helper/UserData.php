<?php
namespace User\View\Helper;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Helper\AbstractHelper;

class UserData extends AbstractHelper implements ServiceLocatorAwareInterface
{
  private $_oAuthPlugin;
  private $_oServiceLocator;
  private $_oUser;

  public function __invoke()
  {
    $this->_oServiceLocator = $this->getServiceLocator();
    if ($this->_oServiceLocator->getServiceLocator()->has('AuthService')) {
      $this->_oAuthPlugin = $this->_oServiceLocator->getServiceLocator()->get('AuthService');
      if ($this->_oAuthPlugin->hasIdentity()) {
        $this->_oUser = $this->_oAuthPlugin->getStorageData();
        return $this->_oUser;
      }
    }
    return null;
  }

  public function getServiceLocator()
  {
    return $this->_oServiceLocator;
  }

  public function setServiceLocator(ServiceLocatorInterface $oServiceLocator)
  {
    $this->_oServiceLocator = $oServiceLocator;
    return $this;
  }
}

<?php
namespace User\Controller\Plugin;

use Zend\Authentication\Adapter\DbTable;
use Zend\Authentication\AuthenticationService;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class AuthPlugin extends AbstractPlugin implements ServiceLocatorAwareInterface, EventManagerAwareInterface
{
  private $_oAuth;
  private $_oServiceLocator;
  private $_oStorage;
  private $_oUserParamTable;
  private $_oUserRoleTable;
  private $_oUserTable;
  private $_sAuthColumn;
  protected $events;

  public function auth($sEmailAddress, $sPassword, $nRememberMe = 0)
  {
    $oAuthResult = $this->getAuth()->getAdapter()->setIdentity($sEmailAddress)->setCredential($sPassword)->authenticate();
    if ($oAuthResult->isValid()) {
      if ($nRememberMe === 1) {
        $this->_oStorage->setRememberMe(1);
        $this->getAuth()->setStorage($this->_oStorage);
      }
      if ($this->setStorageData()) {
        $aParams = $this->getStorageData();
        if ($aParams) {
          $this->getEventManager()->trigger(__FUNCTION__, $this, $aParams);
        }
        return true;
      }
    }
    return null;
  }

  public function cleanStorageData()
  {
    if ($this->getAuth()->hasIdentity()) {
      $this->_oStorage->forgetMe();
      $this->getAuth()->clearIdentity();
      return true;
    }
    return null;
  }

  public function getAuth()
  {
    return $this->_oAuth;
  }

  public function getEventManager()
  {
    if (null === $this->events) {
      $this->setEventManager(new EventManager());
    }
    return $this->events;
  }

  public function getServiceLocator()
  {
    return $this->_oServiceLocator;
  }

  public function getStorageData($mFiled = null)
  {
    if (is_string($mFiled)) {
      $mStorage = $this->_oStorage->read()->$mFiled;
    } else if (is_array($mFiled)) {
      $aStorage = array();
      $mStorage = $this->_oStorage->read();
      foreach ($mFiled as $sKey => $sValue) {
        $aStorage[$sValue] = $mStorage->$sValue;
      }
      $mStorage = $aStorage;
    } else {
      $mStorage = $this->_oStorage->read();
    }
    return $mStorage;
  }

  public function hasIdentity()
  {
    return $this->getAuth()->hasIdentity();
  }

  public function init()
  {
    $oDbAdapter = $this->getServiceLocator()->get('DefaultDbAdapter');
    $aConfig = $this->getServiceLocator()->get('config');
    $this->_sAuthColumn = $aConfig['authorization']['auth_column'];
    $this->_oAuth = new AuthenticationService();
    $this->_oStorage = new \User\Model\AuthStorage('auth_storage');
    $this->getAuth()->setAdapter(new DbTable($oDbAdapter, 'user', $this->_sAuthColumn, 'password', 'MD5(CONCAT(MD5(?),salt)) AND active = 1'));
    $this->getAuth()->setStorage($this->_oStorage);
    $this->_oUserTable = $this->getServiceLocator()->get('UserTableService');
    $this->_oUserRoleTable = $this->getServiceLocator()->get('UserRoleTableService');
    $this->_oUserParamTable = $this->getServiceLocator()->get('UserParamTableService');
  }

  public function setEventManager(EventManagerInterface $events)
  {
    $events->setIdentifiers(array(
      __CLASS__,
      get_called_class(),
    ));
    $this->events = $events;
    return $this;
  }

  public function setServiceLocator(ServiceLocatorInterface $oServiceLocator)
  {
    $this->_oServiceLocator = $oServiceLocator;
    $this->init();
    return $this;
  }

  public function setStorageData()
  {
    $oUserStd = new \stdClass();
    $nUserId = (int)$this->getAuth()->getAdapter()->getResultRowObject(null, array('password', 'salt'))->id;
    if ($nUserId) {
      $oUser = $this->_oUserTable->getRow(array('id' => $nUserId));
      $oUserRole = $this->_oUserRoleTable->getRow(array('id' => $oUser->user_role_id));
      $oUserParam = $this->_oUserParamTable->getRow(array('user_id' => $nUserId));
      foreach ($oUser as $sKey => $mValue) {
        $oUserStd->$sKey = $mValue;
      }
      foreach ($oUserRole as $sKey => $mValue) {
        if (!in_array($sKey, array('id'))) {
          $oUserStd->$sKey = $mValue;
        }
      }
      foreach ($oUserParam as $sKey => $mValue) {
        if (!in_array($sKey, array('user_id'))) {
          if (in_array($sKey, array('id'))) {
            $sKey = 'user_param_id';
          }
          $oUserStd->$sKey = $mValue;
        }
      }
      $this->_oStorage->write($oUserStd);
      return true;
    }
    return null;
  }
}
<?php
namespace User\Controller\Plugin;

use Zend\Authentication\Adapter\DbTable;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Permissions\Acl\Acl;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class AclPlugin extends AbstractPlugin implements ServiceLocatorAwareInterface, EventManagerAwareInterface
{
  protected $_oAcl;
  protected $_oAuthPlugin;
  protected $_oNavigationMenuTable;
  protected $_oNavigationPrivilegeUserRoleTable;
  protected $_oNavigationResourceTable;
  protected $_oServiceLocator;
  protected $_oUserRoleTable;

  protected function getNavigationMenuTable()
  {
    if (!$this->_oNavigationMenuTable && $this->_oServiceLocator->has('NavigationMenuTableService')) {
      $this->_oNavigationMenuTable = $this->_oServiceLocator->get('NavigationMenuTableService');
    }
    return $this->_oNavigationMenuTable;
  }

  protected function getNavigationPrivilegeUserRoleTable()
  {
    if (!$this->_oNavigationPrivilegeUserRoleTable && $this->_oServiceLocator->has('NavigationPrivilegeUserRoleTableService')) {
      $this->_oNavigationPrivilegeUserRoleTable = $this->_oServiceLocator->get('NavigationPrivilegeUserRoleTableService');
    }
    return $this->_oNavigationPrivilegeUserRoleTable;
  }

  protected function getNavigationResourceTable()
  {
    if (!$this->_oNavigationResourceTable && $this->_oServiceLocator->has('NavigationResourceTableService')) {
      $this->_oNavigationResourceTable = $this->_oServiceLocator->get('NavigationResourceTableService');
    }
    return $this->_oNavigationResourceTable;
  }

  protected function getUserRoleTable()
  {
    if (!$this->_oUserRoleTable && $this->_oServiceLocator->has('UserRoleTableService')) {
      $this->_oUserRoleTable = $this->_oServiceLocator->get('UserRoleTableService');
    }
    return $this->_oUserRoleTable;
  }

  protected function removeAccents($sStr)
  {
    $aPolishChars = array('ą', 'ć', 'ę', 'ł', 'ń', 'ó', 'ś', 'ż', 'ź');
    $aReplace = array('a', 'c', 'e', 'l', 'n', 'o', 's', 'z', 'z');
    return str_replace($aPolishChars, $aReplace, $sStr);
  }

  public function getAcl()
  {
    return $this->_oAcl;
  }

  public function getEventManager()
  {
    if (null === $this->events) {
      $this->setEventManager(new EventManager());
    }
    return $this->events;
  }

  public function getRole()
  {
    $sRole = 'guest';
    if ($this->_oAuthPlugin && $this->_oAuthPlugin->hasIdentity() && $this->_oAuthPlugin->getStorageData('role_name')) {
      $sRole = $this->_oAuthPlugin->getStorageData('role_name');
    }
    return $sRole;
  }

  public function getServiceLocator()
  {
    return $this->_oServiceLocator;
  }

  public function getSiteHref($sLabel)
  {
    $sNewLabel = '';
    $sTempLabel = trim(strtolower($sLabel));
    $sTempLabel = explode(' ', $sTempLabel);
    foreach ($sTempLabel as $nKey => $sValue) {
      if ($nKey)
        $sNewLabel .= '-' . $sValue;
      else
        $sNewLabel .= $sValue;
    }
    return $this->removeAccents($sNewLabel);
  }

  public function init($oMvcEvent)
  {
    $this->setServiceLocator($oMvcEvent->getApplication()->getServiceManager());
    if ($this->getServiceLocator()->has('AuthService')) {
      $this->_oAuthPlugin = $this->getServiceLocator()->get('AuthService');
    }
    $this->_oAcl = new Acl();
    $oAclSessionContainer = new \User\Model\AclSessionContainer();
    if ($oAclSessionContainer->getIsInit()) {
      $aResource = $oAclSessionContainer->getResource();
      $aUserRole = $oAclSessionContainer->getUserRole();
      $aMenu = $oAclSessionContainer->getMenu();
    } else {
      $aResource = $this->getNavigationResourceTable()->getAll();
      $aUserRole = $this->getUserRoleTable()->getAll();
      $aMenu = $this->getNavigationMenuTable()->getAll();
      if (is_array($aMenu) && count($aMenu)) {
        foreach ($aMenu as $oMenu) {
          $oMenu->user_role_id = $this->getNavigationPrivilegeUserRoleTable()->getAll(array('navigation_privilege_id' => $oMenu->navigation_privilege_id));
        }
      }
      $oAclSessionContainer->setResource($aResource);
      $oAclSessionContainer->setUserRole($aUserRole);
      $oAclSessionContainer->setMenu($aMenu);
      $oAclSessionContainer->setIsInit(1);
    }
    if (is_array($aResource) && count($aResource)) {
      foreach ($aResource as $oResource) {
        if (!$this->_oAcl->hasResource($oResource->value)) {
          $this->_oAcl->addResource(new Resource($oResource->value));
        }
      }
    }
    if (is_array($aUserRole) && count($aUserRole)) {
      foreach ($aUserRole as $oUserRole) {
        if (!$this->_oAcl->hasRole($oUserRole->role_name)) {
          $this->_oAcl->addRole(new Role($oUserRole->role_name));
        }
      }
    }
    if (is_array($aMenu) && count($aMenu)) {
      foreach ($aMenu as $oMenu) {
        if (is_array($oMenu->user_role_id) && count($oMenu->user_role_id)) {
          $aUserRole = array();
          foreach ($oMenu->user_role_id as $oUserRole) {
            array_push($aUserRole, $oUserRole->user_role_name);
          }
          $this->_oAcl->allow($aUserRole, $oMenu->navigation_resource_name, $oMenu->navigation_privilege_name);
          $sUniqueKey = md5($oMenu->navigation_module_name . $oMenu->navigation_controller_name . $oMenu->navigation_action_name . $this->getSiteHref($oMenu->label_name) . 'html');
          if ($sUniqueKey) {
            $this->_oAcl->allow($aUserRole, $oMenu->navigation_resource_name, $oMenu->navigation_privilege_name . $sUniqueKey);
          }
        }
      }
    }
    if (!$this->_oAcl->hasResource('application')) {
      $this->_oAcl->addResource(new Resource('application'));
    }
    if (!$this->_oAcl->hasResource('user')) {
      $this->_oAcl->addResource(new Resource('user'));
    }
    $this->_oAcl->deny();
    //$this->_oAcl->allow(array('guest', 'admin'), 'application', 'index:index');
    //$this->_oAcl->allow(array('guest', 'admin'), 'application', 'navigation:menu');
    $this->_oAcl->allow(array('guest'), 'user', 'index:login');
    $this->_oAcl->allow(array('admin'), 'user', 'index:logout');
    $sControllerClassName = get_class($oMvcEvent->getTarget());
    $sModule = strtolower(substr($sControllerClassName, 0, strpos($sControllerClassName, '\\')));
    $sController = strtolower($oMvcEvent->getRouteMatch()->getParam('__CONTROLLER__'));
    $sAction = strtolower($oMvcEvent->getRouteMatch()->getParam('action'));
    $sMenu = strtolower($oMvcEvent->getRouteMatch()->getParam('menu'));
    $sSubmenu = strtolower($oMvcEvent->getRouteMatch()->getParam('submenu'));
    $sSubsubmenu = strtolower($oMvcEvent->getRouteMatch()->getParam('subsubmenu'));
    $sFormat = strtolower($oMvcEvent->getRouteMatch()->getParam('format'));
    if ($sMenu && $sModule && $sController && $sAction) {
      $sLabel = $this->getSiteHref($sMenu);
      if ($sSubmenu) {
        $sLabel = $this->getSiteHref($sSubmenu);
        if ($sSubsubmenu) {
          $sLabel = $this->getSiteHref($sSubsubmenu);
        }
      }
      $sUniqueKey = md5($sModule . $sController . $sAction . $sLabel . $sFormat);
      if (!$this->_oAcl->isAllowed($this->getRole(), $sModule, $sController . ':' . $sAction . $sUniqueKey)) {
        $sUrl = $oMvcEvent->getRouter()->assemble(array('action' => 'login'), array('name' => 'user_index/user_index_process'));
        if ($this->getRole() === 'admin') {
          $sUrl = $oMvcEvent->getRouter()->assemble(array('action' => 'index'), array('name' => 'home'));
        }
        $oResponse = $oMvcEvent->getResponse();
        $oResponse->getHeaders()->addHeaderLine('Location', $sUrl);
        $oResponse->setStatusCode(302);
        $oResponse->sendHeaders();
        $oMvcEvent->stopPropagation();
      }
      if ($this->getServiceLocator()->has('VNavigationMenuTableService')) {
        $oMenu = $this->getServiceLocator()->get('VNavigationMenuTableService')->getRow(array(
          'navigation_module_name' => $sModule,
          'navigation_controller_name' => $sController,
          'navigation_action_name' => $sAction,
          'unique_key' => $sUniqueKey,
        ));
      }
    } else if ($sModule && $sController && $sAction) {
      if (!$this->_oAcl->isAllowed($this->getRole(), $sModule, $sController . ':' . $sAction)) {
        $sUrl = $oMvcEvent->getRouter()->assemble(array('action' => 'login'), array('name' => 'user_index/user_index_process'));
        if ($this->getRole() === 'admin') {
          $sUrl = $oMvcEvent->getRouter()->assemble(array('action' => 'index'), array('name' => 'home'));
        }
        $oResponse = $oMvcEvent->getResponse();
        $oResponse->getHeaders()->addHeaderLine('Location', $sUrl);
        $oResponse->setStatusCode(302);
        $oResponse->sendHeaders();
        $oMvcEvent->stopPropagation();
      }
      if ($this->getServiceLocator()->has('VNavigationMenuTableService')) {
        $oMenu = $this->getServiceLocator()->get('VNavigationMenuTableService')->getRow(array(
          'navigation_module_name' => $sModule,
          'navigation_controller_name' => $sController,
          'navigation_action_name' => $sAction,
        ));
      }
    }
    if ($oMenu instanceof \Application\Model\Entity\VNavigationMenu) {
      if ($this->getServiceLocator()->has('SiteSeoSiteSeoMetaKeywordsTableService') && $this->getServiceLocator()->has('SiteSeoMetaKeywordsTableService') && $this->getServiceLocator()->has('SiteSeoTableService')) {
        $sHeadTitle = '';
        $sMetaDescription = '';
        $sMetaKeywords = '';
        $aMetaKeywords = array();
        $sMetaRobots = 'index, follow';
        if ($oMenu->site_seo_id) {
          $oSiteSeo = $this->getServiceLocator()->get('SiteSeoTableService')->getRow(array('id' => $oMenu->site_seo_id));
          if ($oSiteSeo->head_title) {
            $sHeadTitle = $oSiteSeo->head_title;
          }
          if ($oSiteSeo->meta_description) {
            $sMetaDescription = $oSiteSeo->meta_description;
          }
          if ($oSiteSeo->site_seo_robots_name) {
            $sMetaRobots = $oSiteSeo->site_seo_robots_name;
          }
          $oMenu->site_seo_meta_keywords_id = $this->getServiceLocator()->get('SiteSeoSiteSeoMetaKeywordsTableService')->getAll(array('site_seo_id' => $oMenu->site_seo_id, 'active' => 1));
          if (count($oMenu->site_seo_meta_keywords_id)) {
            foreach ($oMenu->site_seo_meta_keywords_id as $oValue) {
              array_push($aMetaKeywords, $oValue->site_seo_meta_keywords_name);
            }
          }
        }
        $oDefaultMetaKeywords = $this->getServiceLocator()->get('SiteSeoMetaKeywordsTableService')->getAll(array('primary' => 1, 'active' => 1));
        if (count($oDefaultMetaKeywords)) {
          foreach ($oDefaultMetaKeywords as $oValue) {
            array_push($aMetaKeywords, $oValue->value);
          }
        }
        $aMetaKeywords = array_unique($aMetaKeywords);
        $nMetaKeywordsCount = count($aMetaKeywords);
        $nIdx = 1;
        if ($nMetaKeywordsCount) {
          foreach ($aMetaKeywords as $sValue) {
            if ($nMetaKeywordsCount == $nIdx) {
              $sMetaKeywords .= $sValue;
            } else {
              $sMetaKeywords .= $sValue . ',';
            }
            $nIdx++;
          }
        }
        $oRenderer = $this->getServiceLocator()->get('Zend\View\Renderer\PhpRenderer');
        $oRenderer->headTitle($sHeadTitle);
        $oRenderer->headMeta()->appendName('description', $sMetaDescription);
        $oRenderer->headMeta()->appendName('keywords', $sMetaKeywords);
        $oRenderer->headMeta()->appendName('robots', $sMetaRobots);
      }
      if ($oMenu->navigation_layout_name) {
        $oController = $oMvcEvent->getTarget();
        $oController->layout($oMenu->navigation_layout_name);
      }
    }
  }

  public function setEventManager(EventManagerInterface $events)
  {
    $events->setIdentifiers(array(
      __CLASS__,
      get_called_class(),
    ));
    $this->events = $events;
    return $this;
  }

  public function setServiceLocator(ServiceLocatorInterface $oServiceLocator)
  {
    $this->_oServiceLocator = $oServiceLocator;
    return $this;
  }
}

<?php
namespace User\Controller;

use User\Model\Entity\User as UserEntity;
use User\Model\Entity\UserParam as UserParamEntity;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
  protected $_oUserParamTable;
  protected $_oUserTable;

  protected function getUserParamTable()
  {
    if (!$this->_oUserParamTable && $this->getServiceLocator()->has('UserParamTableService')) {
      $this->_oUserParamTable = $this->getServiceLocator()->get('UserParamTableService');
    }
    return $this->_oUserParamTable;
  }

  protected function getUserTable()
  {
    if (!$this->_oUserTable && $this->getServiceLocator()->has('UserTableService')) {
      $this->_oUserTable = $this->getServiceLocator()->get('UserTableService');
    }
    return $this->_oUserTable;
  }

  public function adduserajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $oRegisterForm = $this->getServiceLocator()->get('RegisterFormService');
      $oRegisterForm->setData($this->getRequest()->getPost());
      if ($oRegisterForm->isValid()) {
        $this->getUserTable()->beginTransaction();
        $oUserEntity = new UserEntity();
        $oUserParamEntity = new UserParamEntity();
        $oData = new \ArrayObject($oRegisterForm->getData());
        $oData->offsetSet('user_id', $this->getUserTable()->addRow($oUserEntity->setOptions($oData)));
        if ($oData->offsetGet('user_id')) {
          $oData->offsetSet('user_param_id', $this->getUserParamTable()->addRow($oUserParamEntity->setOptions($oData)));
        }
        if ($oData->offsetGet('user_id') && $oData->offsetGet('user_param_id')) {
          $this->getUserTable()->commit();
          $bSuccess = true;
        } else {
          $this->getUserTable()->rollBack();
        }
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function autocompleteuserajaxAction()
  {
    $oResponse = $this->getResponse();
    $aFilter = null;
    $aUsers = array();
    $sColumnName = $this->params()->fromQuery('column_name') !== '' ? $this->params()->fromQuery('column_name') : null;
    if (is_array($this->params()->fromQuery('filter')) && count($this->params()->fromQuery('filter'))) {
      $aFilter = $this->params()->fromQuery('filter');
      foreach ($aFilter as $sKey => $sValue) {
        if (strlen(trim($sValue))) {
          $aFilter[$sKey] = trim($sValue);
        } else {
          unset($aFilter[$sKey]);
        }
      }
    }
    if (isset($aFilter)) {
      $aRowset = $this->getUserTable()->getAll(array('filter' => $aFilter));
      $aLabels = array();
      foreach ($aRowset as $nKey => $oUser) {
        if (!in_array($oUser->$sColumnName, $aLabels)) {
          $aUsers[$nKey]['label'] = $oUser->$sColumnName;
          array_push($aLabels, $oUser->$sColumnName);
        }
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode(array('rowset' => $aUsers)));
    return $oResponse;
  }

  public function deleteuserajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $nId = $this->params()->fromPost('id') !== '' ? (int)$this->params()->fromPost('id') : null;
      if ($nId) {
        $this->getUserTable()->deleteRow($nId);
        $bSuccess = true;
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function edituserajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $oEditUserForm = $this->getServiceLocator()->get('EditUserFormService');
      $oEditUserForm->setData($this->getRequest()->getPost());
      if ($oEditUserForm->isValid()) {
        $oUserEntity = new UserEntity();
        $oUserParamEntity = new UserParamEntity();
        $oData = new \ArrayObject($oEditUserForm->getData());
        $this->getUserTable()->editRow($oUserEntity->setOptions($oData));
        $this->getUserParamTable()->editRow($oUserParamEntity->setOptions($oData));
        $bSuccess = true;
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function indexAction()
  {
    return new ViewModel();
  }

  public function loadsettingsajaxAction()
  {
    if ($this->getRequest()->isPost()) {
      $oResponse = $this->getResponse();
      $aSort = null;
      $aFilter = null;
      if (is_array($this->params()->fromPost('filter')) && count($this->params()->fromPost('filter'))) {
        $aFilter = $this->params()->fromPost('filter');
        foreach ($aFilter as $sKey => $sValue) {
          if (strlen(trim($sValue))) {
            $aFilter[$sKey] = trim($sValue);
          } else {
            unset($aFilter[$sKey]);
          }
        }
      }
      $sSortColumn = $this->params()->fromPost('sort_column') !== '' ? $this->params()->fromPost('sort_column') : 'id';
      $sSortMethod = $this->params()->fromPost('sort_method') !== '' ? $this->params()->fromPost('sort_method') : 'asc';
      $nLimit = $this->params()->fromPost('limit');
      $nOffset = $this->params()->fromPost('offset');
      if (isset($sSortColumn) && isset($sSortMethod))
        $aSort = array('sort_column' => $sSortColumn, 'sort_method' => $sSortMethod);
      if (isset($nLimit) && is_numeric($nLimit)) {
        $nLimit = (int)$nLimit;
      } else {
        $nLimit = 20;
      }
      if (isset($nOffset) && is_numeric($nOffset)) {
        $nOffset = (int)$nOffset;
      } else {
        $nOffset = 0;
      }
      $aUsers = $this->getUserTable()->getAll(array('filter' => $aFilter, 'sort' => $aSort, 'limit' => $nLimit, 'offset' => $nOffset));
      $nCount = count($this->getUserTable()->getAll(array('filter' => $aFilter, 'sort' => $aSort)));
      $oResponse->setContent(\Zend\Json\Json::encode(array('rowset' => $aUsers, 'num_rows' => $nCount)));
    }
    return $oResponse;
  }

  public function loginAction()
  {
    $oLoginForm = $this->getServiceLocator()->get('LoginFormService');
    if ($this->getRequest()->isPost()) {
      $oLoginForm->setData($this->getRequest()->getPost());
      if ($oLoginForm->isValid()) {
        $oAuthPlugin = $this->getServiceLocator()->get('AuthService');
        $aConfig = $this->getServiceLocator()->get('config');
        $sAuthColumn = $aConfig['authorization']['auth_column'];
        $bSuccess = $oAuthPlugin->auth($this->getRequest()->getPost($sAuthColumn), $this->getRequest()->getPost('password'), $this->getRequest()->getPost('remember_me'));
        if ($bSuccess) {
          $this->redirect()->toRoute('home', array('action' => 'index'), array());
        }
      }
    }
    return new ViewModel(array('form' => $oLoginForm));
  }

  public function logoutAction()
  {
    $oAuthPlugin = $this->getServiceLocator()->get('AuthService');
    if ($oAuthPlugin->hasIdentity()) {
      $bSuccess = $oAuthPlugin->cleanStorageData();
      if ($bSuccess) {
        $this->redirect()->toRoute('home', array('action' => 'index'), array());
      }
    }
  }

  public function registerAction()
  {
    $oRegisterForm = $this->getServiceLocator()->get('RegisterFormService');
    if ($this->getRequest()->isPost()) {
      $oRegisterForm->setData($this->getRequest()->getPost());
      if ($oRegisterForm->isValid()) {
        $this->getUserTable()->beginTransaction();
        $oUserEntity = new UserEntity();
        $oUserParamEntity = new UserParamEntity();
        $oData = new \ArrayObject($oRegisterForm->getData());
        $oData->offsetSet('user_id', $this->getUserTable()->addRow($oUserEntity->setOptions($oData)));
        if ($oData->offsetGet('user_id')) {
          $oData->offsetSet('user_param_id', $this->getUserParamTable()->addRow($oUserParamEntity->setOptions($oData)));
        }
        if ($oData->offsetGet('user_id') && $oData->offsetGet('user_param_id')) {
          $this->getUserTable()->commit();
        } else {
          $this->getUserTable()->rollBack();
        }
      }
    }
    return new ViewModel(array('form' => $oRegisterForm));
  }

  public function secendoptionuserajaxAction()
  {
    $oResponse = $this->getResponse();
    $bSuccess = false;
    if ($this->getRequest()->isPost()) {
      $sSecendOption = $this->params()->fromPost('second_opinion');
      $sColumnName = $this->params()->fromPost('column_name');
      $nId = $this->params()->fromPost('id') !== '' ? (int)$this->params()->fromPost('id') : null;
      if ($nId) {
        $oUserEntity = new UserEntity();
        switch ($sSecendOption) {
          case 'enable_disable':
            $oUser = $this->getUserTable()->getRow(array('id' => $nId));
            if (isset($oUser->$sColumnName)) {
              if ($oUser->$sColumnName === 1) {
                $oData = new \ArrayObject(array('id' => $nId, $sColumnName => 0));
                $this->getUserTable()->changeActive($oUserEntity->setOptions($oData));
                $bSuccess = true;
              } else if ($oUser->$sColumnName === 0) {
                $oData = new \ArrayObject(array('id' => $nId, $sColumnName => 1));
                $this->getUserTable()->changeActive($oUserEntity->setOptions($oData));
                $bSuccess = true;
              }
            }
            break;
        }
      }
    }
    $oResponse->setContent(\Zend\Json\Json::encode($bSuccess));
    return $oResponse;
  }

  public function settingsAction()
  {
    $oEditUserForm = $this->getServiceLocator()->get('EditUserFormService');
    $oRegisterForm = $this->getServiceLocator()->get('RegisterFormService');
    return new ViewModel(array('edit_user_form' => $oEditUserForm, 'add_new_user_form' => $oRegisterForm));
  }
}

<?php
namespace User\Service\Form;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class EditUserFormService implements FactoryInterface
{
  public function createService(ServiceLocatorInterface $oServiceLocator)
  {
    $oForm = new \User\Form\EditUser();
    return $oForm;
  }
}

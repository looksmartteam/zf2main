<?php
namespace User\Service\InputFilter;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class EditUserInputFilterService implements FactoryInterface
{
  public function createService(ServiceLocatorInterface $oServiceLocator)
  {
    $oInputFilter = new \User\Model\InputFilter\EditUser();
    return $oInputFilter;
  }
}

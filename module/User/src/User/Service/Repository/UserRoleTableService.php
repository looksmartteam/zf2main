<?php
namespace User\Service\Repository;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class UserRoleTableService implements FactoryInterface
{
  public function createService(ServiceLocatorInterface $oServiceLocator)
  {
    $oDbAdapter = $oServiceLocator->get('DefaultDbAdapter');
    $oTable = new \User\Model\Repository\UserRole($oDbAdapter);
    return $oTable;
  }
}

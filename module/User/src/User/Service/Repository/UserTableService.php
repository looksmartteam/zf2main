<?php
namespace User\Service\Repository;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class UserTableService implements FactoryInterface
{
  public function createService(ServiceLocatorInterface $oServiceLocator)
  {
    $oDbAdapter = $oServiceLocator->get('DefaultDbAdapter');
    $oTable = new \User\Model\Repository\User($oDbAdapter);
    return $oTable;
  }
}

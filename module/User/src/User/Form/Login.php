<?php
namespace User\Form;

use Application\FormAbstract;

class Login extends FormAbstract
{
  public function __construct($oServiceLocator, $sFormName)
  {
    parent::__construct($oServiceLocator, $sFormName);
  }

  public function init()
  {
    $this->add(array(
      'type' => 'Text',
      'name' => $this->_sAuthColumn,
      'options' => array(
        'label' => $this->_sLoginLabel,
      ),
    ));
    $this->add(array(
      'type' => 'Password',
      'name' => 'password',
      'options' => array(
        'label' => 'Hasło',
      ),
    ));
    $this->add(array(
      'type' => 'Button',
      'name' => 'submit_button',
      'options' => array(
        'label' => 'Zaloguj',
      ),
    ));
  }

  /**
   * Set a single option for an element
   *
   * @param  string $key
   * @param  mixed $value
   * @return self
   */
  public function setOption($key, $value)
  {
    // TODO: Implement setOption() method.
  }
}

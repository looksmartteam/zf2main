<?php
namespace User\Form;

use Application\FormAbstract;
use Zend\Form\Element;

class Register extends FormAbstract
{
  public function __construct($oServiceLocator, $sFormName)
  {
    parent::__construct($oServiceLocator, $sFormName);
  }

  public function init()
  {
    $this->_aTabsLabel = array(
      'user' => 'Konto użytkownika',
      'user_param' => 'Dane użytkownika',
      'address' => 'Adres',
    );
    $oUserCategoryTable = $this->getServiceLocator()->get('UserCategoryTableService');
    $oAllUserCategory = $oUserCategoryTable->getAll();
    $aAllUserCategory = array();
    if (isset($oAllUserCategory)) {
      foreach ($oAllUserCategory as $oValue) {
        $aAllUserCategory[$oValue->id] = $oValue->name;
      }
    }
    $this->add(array(
      'type' => 'Text',
      'name' => 'email_address',
      'options' => array(
        'label' => 'Adres e-mail',
        'tab_id' => 'user',
      ),
    ));
    if ($this->_sAuthColumn === 'nick') {
      $this->add(array(
        'type' => 'Text',
        'name' => 'nick',
        'options' => array(
          'label' => $this->_sLoginLabel,
          'tab_id' => 'user',
        ),
      ));
    }
    $this->add(array(
      'type' => 'Password',
      'name' => 'password',
      'options' => array(
        'label' => 'Hasło',
        'tab_id' => 'user',
      ),
    ));
    $this->add(array(
      'type' => 'Select',
      'name' => 'user_category_id',
      'options' => array(
        'label' => 'Kategoria użytkownika',
        'value_options' => $aAllUserCategory,
        'tab_id' => 'user',
      ),
    ));
    $this->add(array(
      'type' => 'Checkbox',
      'name' => 'active',
      'options' => array(
        'label' => 'Konto aktywne',
        'checked_value' => '1',
        'unchecked_value' => '0',
        'use_hidden_element' => true,
        'tab_id' => 'user',
      ),
    ));
    $this->add(array(
      'type' => 'Text',
      'name' => 'first_name',
      'options' => array(
        'label' => 'Imię',
        'tab_id' => 'user_param',
      ),
    ));
    $this->add(array(
      'type' => 'Text',
      'name' => 'last_name',
      'options' => array(
        'label' => 'Nazwisko',
        'tab_id' => 'user_param',
      ),
    ));
    $this->add(array(
      'type' => 'Text',
      'name' => 'pesel',
      'options' => array(
        'label' => 'Pesel',
        'tab_id' => 'user_param',
      ),
    ));
    $this->add(array(
      'type' => 'Text',
      'name' => 'borrower_hash',
      'options' => array(
        'label' => 'borrower#',
        'tab_id' => 'user_param',
      ),
    ));
    $this->add(array(
      'type' => 'Text',
      'name' => 'student_id',
      'options' => array(
        'label' => 'Numer indeksu',
        'tab_id' => 'user_param',
      ),
    ));
    $this->add(array(
      'type' => 'Text',
      'name' => 'employee_id',
      'options' => array(
        'label' => 'Numer płacowy pracownika UAM',
        'tab_id' => 'user_param',
      ),
    ));
    $this->add(array(
      'type' => 'Text',
      'name' => 'phone_number',
      'options' => array(
        'label' => 'Numer telefonu',
        'tab_id' => 'user_param',
      ),
    ));
    $this->add(array(
      'type' => 'Text',
      'name' => 'street',
      'options' => array(
        'label' => 'Ulica',
        'tab_id' => 'address',
      ),
    ));
    $this->add(array(
      'type' => 'Text',
      'name' => 'city',
      'options' => array(
        'label' => 'Miejscowość',
        'tab_id' => 'address',
      ),
    ));
    $this->add(array(
      'type' => 'Text',
      'name' => 'post_code',
      'options' => array(
        'label' => 'Kod pocztowy',
        'tab_id' => 'address',
      ),
    ));
    $this->add(array(
      'type' => 'Button',
      'name' => 'submit_button',
      'options' => array(
        'label' => 'Zapisz'
      ),
    ));
  }

  /**
   * Set a single option for an element
   *
   * @param  string $key
   * @param  mixed $value
   * @return self
   */
  public function setOption($key, $value)
  {
    // TODO: Implement setOption() method.
  }
}

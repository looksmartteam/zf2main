<?php
namespace User\Model;

use Zend\Authentication\Storage\Session;

class AuthStorage extends Session
{
  public function forgetMe()
  {
    $this->session->getManager()->forgetMe();
  }

  public function setRememberMe($nRememberMe = 0, $nTime = 1209600)
  {
    if ($nRememberMe == 1) {
      $this->session->getManager()->rememberMe($nTime);
    }
  }
}
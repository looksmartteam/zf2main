<?php
namespace User\Model;

use Zend\Session\Container;

class AclSessionContainer extends Container
{
  public function __construct()
  {
    parent::__construct('acl_session');
  }

  public function getIsInit()
  {
    return $this->is_init;
  }

  public function getMenu()
  {
    return $this->menu;
  }

  public function getResource()
  {
    return $this->resource;
  }

  public function getUserRole()
  {
    return $this->user_role;
  }

  public function setIsInit($nValue)
  {
    $this->is_init = (int)$nValue;
    $this->setExpirationSeconds(6000, 'is_init');
  }

  public function setMenu($aValue)
  {
    $this->menu = $aValue;
  }

  public function setResource($aValue)
  {
    $this->resource = $aValue;
  }

  public function setUserRole($aValue)
  {
    $this->user_role = $aValue;
  }
}
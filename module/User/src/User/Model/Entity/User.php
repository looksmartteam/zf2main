<?php
namespace User\Model\Entity;

use Application\EntityAbstract;

class User extends EntityAbstract
{
  public $active;
  public $bbarcode;
  public $borrower_hash;
  public $borrower_note;
  public $btype;
  public $city;
  public $created_date;
  public $email_address;
  public $employee_id;
  public $first_name;
  public $last_activity;
  public $last_name;
  public $nick;
  public $password;
  public $pesel;
  public $phone_number;
  public $post_code;
  public $salt;
  public $street;
  public $student_id;
  public $updated_date;
  public $user_category_id;
  public $user_param_id;
  public $user_role_id;

  public function getPassword()
  {
    return null;
  }

  public function getSalt()
  {
    return null;
  }

  public function setActive($nActive)
  {
    $this->active = (int)$nActive;
  }

  public function setPassword($sPassword)
  {
    $this->setSalt();
    $this->password = md5(md5($sPassword) . $this->salt);
    return $this;
  }

  public function setSalt()
  {
    $sDynamicSalt = '';
    for ($nIdx = 0; $nIdx < 250; $nIdx++) {
      $sDynamicSalt .= md5(sha1(chr(rand(33, 126))));
    }
    $this->salt = sha1($sDynamicSalt);
    return $this;
  }

  public function setUserCategoryId($nUserCategoryId)
  {
    $this->user_category_id = (int)$nUserCategoryId == 0 ? null : (int)$nUserCategoryId;
    return $this;
  }

  public function setUserCategoryName($sUserCategoryName)
  {
    $this->user_category_name = $sUserCategoryName;
    return $this;
  }
}
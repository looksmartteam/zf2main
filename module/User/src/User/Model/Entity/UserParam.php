<?php
namespace User\Model\Entity;

use Application\EntityAbstract;

class UserParam extends EntityAbstract
{
  public $bbarcode;
  public $borrower_hash;
  public $borrower_note;
  public $btype;
  public $city;
  public $email_address;
  public $employee_id;
  public $first_name;
  public $last_activity;
  public $last_name;
  public $pesel;
  public $phone_number;
  public $post_code;
  public $street;
  public $student_id;
  public $user_category_id;
  public $user_category_name;
  public $user_id;
  public $user_param_id;

  public function getLastActivity($nLastActivity)
  {
    $this->last_activity = (int)$nLastActivity;
    $this->user_last_activity = $this->last_activity;
    return $this;
  }

  public function setLastActivity()
  {
    $this->last_activity = time();
    return $this;
  }

  public function setUserCategoryId($nUserCategoryId)
  {
    $this->user_category_id = (int)$nUserCategoryId == 0 ? null : (int)$nUserCategoryId;
    return $this;
  }

  public function setUserLastActivity($nLastActivity)
  {
    $this->user_last_activity = date('d.m.Y', $nLastActivity);
    return $this;
  }

  public function setUserParamId($nUserParamId)
  {
    $this->user_param_id = (int)$nUserParamId;
    return $this;
  }
}
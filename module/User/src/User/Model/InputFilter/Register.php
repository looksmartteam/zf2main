<?php
namespace User\Model\InputFilter;

use Application\InputFilterAbstract;
use Zend\InputFilter\InputFilter;

class Register extends InputFilterAbstract
{
  public function getInputFilter()
  {
    if (!$this->_oInputFilter) {
      $oInputFilter = new InputFilter();
      $oInputFilter->add(array(
        'name' => 'email_address',
        'required' => true,
        'filters' => array(
          array('name' => 'StripTags'),
          array('name' => 'StringTrim'),
        ),
        'validators' => array(
          array(
            'name' => 'StringLength',
            'options' => array(
              'encoding' => 'UTF-8',
              'min' => 1,
              'max' => 100,
            ),
          ),
          array(
            'name' => 'EmailAddress'
          ),
          array(
            'name' => 'Db\NoRecordExists',
            'options' => array(
              'adapter' => $this->_oDbAdapter,
              'table' => 'user',
              'field' => 'email_address',
            ),
          ),
        ),
      ));
      if ($this->_sAuthColumn === 'nick') {
        $oInputFilter->add(array(
          'name' => 'nick',
          'required' => true,
          'filters' => array(
            array('name' => 'StripTags'),
            array('name' => 'StringTrim'),
          ),
          'validators' => array(
            array(
              'name' => 'StringLength',
              'options' => array(
                'encoding' => 'UTF-8',
                'min' => 1,
                'max' => 100,
              ),
            ),
            array(
              'name' => 'Db\NoRecordExists',
              'options' => array(
                'adapter' => $this->_oDbAdapter,
                'table' => 'user',
                'field' => 'nick',
              ),
            ),
          ),
        ));
      }
      $oInputFilter->add(array(
        'name' => 'password',
        'required' => true,
        'filters' => array(
          array('name' => 'StripTags'),
          array('name' => 'StringTrim'),
        ),
        'validators' => array(
          array(
            'name' => 'StringLength',
            'options' => array(
              'encoding' => 'UTF-8',
              'min' => 1,
              'max' => 100,
            ),
          ),
        ),
      ));
      $oInputFilter->add(array(
        'name' => 'user_category_id',
        'required' => false,
      ));
      $oInputFilter->add(array(
        'name' => 'active',
        'required' => false,
      ));
      $oInputFilter->add(array(
        'name' => 'first_name',
        'required' => true,
        'filters' => array(
          array('name' => 'StripTags'),
          array('name' => 'StringTrim'),
        ),
        'validators' => array(
          array(
            'name' => 'StringLength',
            'options' => array(
              'encoding' => 'UTF-8',
              'min' => 1,
              'max' => 100,
            ),
          ),
        ),
      ));
      $oInputFilter->add(array(
        'name' => 'last_name',
        'required' => true,
        'filters' => array(
          array('name' => 'StripTags'),
          array('name' => 'StringTrim'),
        ),
        'validators' => array(
          array(
            'name' => 'StringLength',
            'options' => array(
              'encoding' => 'UTF-8',
              'min' => 1,
              'max' => 100,
            ),
          ),
        ),
      ));
      $oInputFilter->add(array(
        'name' => 'pesel',
        'required' => true,
        'filters' => array(
          array('name' => 'StripTags'),
          array('name' => 'StringTrim'),
        ),
        'validators' => array(
          array(
            'name' => 'StringLength',
            'options' => array(
              'encoding' => 'UTF-8',
              'min' => 11,
              'max' => 11,
            ),
          ),
//          array(
//            'name' => 'BUUAM\Model\InputFilter\Validator\Pesel',
//            'options' => array(
//              'sm' => $this->_oServiceLocator,
//            ),
//          ),
        ),
      ));
      $oInputFilter->add(array(
        'name' => 'borrower_hash',
        'required' => false,
        'filters' => array(
          array('name' => 'StripTags'),
          array('name' => 'StringTrim'),
        ),
      ));
      $oInputFilter->add(array(
        'name' => 'student_id',
        'required' => false,
        'filters' => array(
          array('name' => 'StripTags'),
          array('name' => 'StringTrim'),
        ),
      ));
      $oInputFilter->add(array(
        'name' => 'employee_id',
        'required' => false,
        'filters' => array(
          array('name' => 'StripTags'),
          array('name' => 'StringTrim'),
        ),
      ));
      $oInputFilter->add(array(
        'name' => 'phone_number',
        'required' => false,
        'filters' => array(
          array('name' => 'StripTags'),
          array('name' => 'StringTrim'),
        ),
        'validators' => array(
          array(
            'name' => 'PhoneNumber',
            'options' => array(
              'country' => 'PL',
              'allowed_types' => array(
                'general',
                'mobile',
                'personal',
              ),
            )
          ),
        ),
      ));
      $oInputFilter->add(array(
        'name' => 'street',
        'required' => false,
        'filters' => array(
          array('name' => 'StripTags'),
          array('name' => 'StringTrim'),
        ),
        'validators' => array(
          array(
            'name' => 'StringLength',
            'options' => array(
              'encoding' => 'UTF-8',
              'min' => 1,
              'max' => 100,
            ),
          ),
        ),
      ));
      $oInputFilter->add(array(
        'name' => 'city',
        'required' => false,
        'filters' => array(
          array('name' => 'StripTags'),
          array('name' => 'StringTrim'),
        ),
        'validators' => array(
          array(
            'name' => 'StringLength',
            'options' => array(
              'encoding' => 'UTF-8',
              'min' => 1,
              'max' => 100,
            ),
          ),
        ),
      ));
      $oInputFilter->add(array(
        'name' => 'post_code',
        'required' => false,
        'filters' => array(
          array('name' => 'StripTags'),
          array('name' => 'StringTrim'),
        ),
        'validators' => array(
          array(
            'name' => 'StringLength',
            'options' => array(
              'encoding' => 'UTF-8',
              'min' => 5,
              'max' => 6,
            ),
          ),
        ),
      ));
      $this->_oInputFilter = $oInputFilter;
    }
    return $this->_oInputFilter;
  }
}
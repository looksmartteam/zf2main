<?php
namespace User\Model\InputFilter;

use Application\InputFilterAbstract;
use Zend\InputFilter\InputFilter;

class Login extends InputFilterAbstract
{
  public function getInputFilter()
  {
    if (!$this->_oInputFilter) {
      $oInputFilter = new InputFilter();
      if ($this->_sAuthColumn === 'email_address') {
        $oInputFilter->add(array(
          'name' => 'email_address',
          'required' => true,
          'filters' => array(
            array('name' => 'StripTags'),
            array('name' => 'StringTrim'),
          ),
          'validators' => array(
            array(
              'name' => 'StringLength',
              'options' => array(
                'encoding' => 'UTF-8',
                'min' => 1,
                'max' => 100,
              ),
            ),
            array(
              'name' => 'EmailAddress'
            ),
            array(
              'name' => 'Db\RecordExists',
              'options' => array(
                'adapter' => $this->_oDbAdapter,
                'table' => 'user',
                'field' => 'email_address',
              ),
            ),
          ),
        ));
      } else if ($this->_sAuthColumn === 'nick') {
        $oInputFilter->add(array(
          'name' => 'nick',
          'required' => true,
          'filters' => array(
            array('name' => 'StripTags'),
            array('name' => 'StringTrim'),
          ),
          'validators' => array(
            array(
              'name' => 'StringLength',
              'options' => array(
                'encoding' => 'UTF-8',
                'min' => 1,
                'max' => 100,
              ),
            ),
            array(
              'name' => 'Db\RecordExists',
              'options' => array(
                'adapter' => $this->_oDbAdapter,
                'table' => 'user',
                'field' => 'nick',
              ),
            ),
          ),
        ));
      }
      $oInputFilter->add(array(
        'name' => 'password',
        'required' => true,
        'filters' => array(
          array('name' => 'StripTags'),
          array('name' => 'StringTrim'),
        ),
        'validators' => array(
          array(
            'name' => 'StringLength',
            'options' => array(
              'encoding' => 'UTF-8',
              'min' => 1,
              'max' => 100,
            ),
          ),
        ),
      ));
      $oInputFilter->add(array(
        'name' => 'remember_me',
        'required' => false,
      ));
      $this->_oInputFilter = $oInputFilter;
    }
    return $this->_oInputFilter;
  }
}
<?php
namespace User\Model\Repository;

use Application\RepositoryAbstract;
use User\Model\Entity\UserParam as Entity;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;

class UserParam extends RepositoryAbstract
{
  protected $table = 'user_param';

  public function __construct(Adapter $oAdapter)
  {
    parent::__construct($oAdapter);
  }

  protected function createNewEntity()
  {
    return new Entity();
  }

  public function addRow($oEntity)
  {
    $aData = array();
    $aData['user_id'] = $oEntity->user_id;
    $aData['user_category_id'] = $oEntity->user_category_id;
    $aData['pesel'] = $oEntity->pesel;
    $aData['borrower_hash'] = $oEntity->borrower_hash;
    $aData['bbarcode'] = $oEntity->bbarcode;
    $aData['btype'] = $oEntity->btype;
    $aData['borrower_note'] = $oEntity->borrower_note;
    $aData['student_id'] = $oEntity->student_id;
    $aData['employee_id'] = $oEntity->employee_id;
    $aData['first_name'] = $oEntity->first_name;
    $aData['last_name'] = $oEntity->last_name;
    $aData['street'] = $oEntity->street;
    $aData['post_code'] = $oEntity->post_code;
    $aData['city'] = $oEntity->city;
    $aData['phone_number'] = $oEntity->phone_number;
    $aData['last_activity'] = time();
    return parent::addRow($aData);
  }

  public function changePhoneNumber($oEntity)
  {
    $aData = array();
    $aData['id'] = $oEntity->user_param_id;
    $aData['phone_number'] = $oEntity->phone_number;
    return parent::editRow($aData);
  }

  public function getAll(array $aData = null)
  {
    $oThat = $this;
    $oResultSet = $this->select(function (Select $oSelect) use ($aData, $oThat) {
      $oSelect->join('user', 'user.id = ' . $oThat->table . '.user_id', array('email_address'));
      $oSelect->join('user_category', 'user_category.id = ' . $oThat->table . '.user_category_id', array('user_category_name' => 'name'), $oSelect::JOIN_LEFT);
      if ($aData) {
        foreach ($aData as $sKey => $mValue) {
          if ($sKey === 'filter' && isset($mValue)) {
            foreach ($mValue as $sColumnName => $sValue) {
              $sDataType = $oThat->getColumnDataType($sColumnName);
              if ($sColumnName === 'id') {
                $sColumnName = $oThat->table . '.' . $sColumnName;
              }
              if (in_array($sDataType, array('int', 'tinyint'))) {
                $oSelect->where(array($sColumnName . '=?' => (int)$sValue));
              } else {
                $oSelect->where->like($sColumnName, '%' . $sValue . '%');
              }
            }
          } else if ($sKey === 'sort' && is_string($mValue['sort_column']) && is_string($mValue['sort_method'])) {
            if ($mValue['sort_column'] === 'id') {
              $mValue['sort_column'] = $oThat->table . '.' . $mValue['sort_column'];
            }
            $oSelect->order(array($mValue['sort_column'] => $mValue['sort_method']));
          } else if ($sKey === 'limit' && isset($mValue) && is_numeric($mValue)) {
            $oSelect->limit($mValue);
          } else if ($sKey === 'offset' && isset($mValue) && is_numeric($mValue)) {
            $oSelect->offset($mValue);
          } else if (!in_array($sKey, array('filter', 'sort', 'limit', 'offset'))) {
            if ($sKey === 'id') {
              $sKey = $oThat->table . '.id';
            }
            $oSelect->where(array($sKey => $mValue));
          }
        }
      }
    });
    $aResult = $this->getEntities($oResultSet);
    return $aResult;
  }

  public function getRow(array $aData = null)
  {
    $oThat = $this;
    $oResultSet = $this->select(function (Select $oSelect) use ($aData, $oThat) {
      $oSelect->join('user', 'user.id = ' . $oThat->table . '.user_id', array('email_address'));
      $oSelect->join('user_category', 'user_category.id = ' . $oThat->table . '.user_category_id', array('user_category_name' => 'name'), $oSelect::JOIN_LEFT);
      if ($aData) {
        foreach ($aData as $sKey => $mValue) {
          $oSelect->where(array($sKey => $mValue));
        }
      }
      $oSelect->limit(1);
    });
    $aResult = $this->getEntities($oResultSet);
    if (count($aResult) === 1)
      return $aResult[0];
    return null;
  }
}
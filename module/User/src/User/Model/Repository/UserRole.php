<?php
namespace User\Model\Repository;

use Application\RepositoryAbstract;
use User\Model\Entity\UserRole as Entity;
use Zend\Db\Adapter\Adapter;

class UserRole extends RepositoryAbstract
{
  protected $table = 'user_role';

  public function __construct(Adapter $oAdapter)
  {
    parent::__construct($oAdapter);
  }

  protected function createNewEntity()
  {
    return new Entity();
  }
}
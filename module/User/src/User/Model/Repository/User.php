<?php
namespace User\Model\Repository;

use Application\RepositoryAbstract;
use User\Model\Entity\User as Entity;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;

class User extends RepositoryAbstract
{
  protected $table = 'user';

  public function __construct(Adapter $oAdapter)
  {
    parent::__construct($oAdapter);
  }

  protected function createNewEntity()
  {
    return new Entity();
  }

  public function addRow($oEntity)
  {
    $aData = array();
    $aData['user_role_id'] = 2;
    $aData['email_address'] = $oEntity->email_address;
    $aData['nick'] = $oEntity->nick;
    $aData['password'] = $oEntity->password;
    $aData['salt'] = $oEntity->salt;
    $aData['created_date'] = time();
    $aData['active'] = $oEntity->active;
    return parent::addRow($aData);
  }

  public function changeActive($oEntity)
  {
    $aData = array();
    $aData['id'] = $oEntity->id;
    $aData['active'] = $oEntity->active;
    return parent::editRow($aData);
  }

  public function changeEmailAddress($oEntity)
  {
    $aData = array();
    $aData['id'] = $oEntity->id;
    $aData['email_address'] = $oEntity->email_address;
    return parent::editRow($aData);
  }

  public function editRow($oEntity)
  {
    $aData = array();
    $aData['id'] = $oEntity->id;
    $aData['email_address'] = $oEntity->email_address;
    if ($this->_sAuthColumn === 'nick') {
      if ($oEntity->nick === '') {
        $aData['nick'] = null;
      } else {
        $aData['nick'] = $oEntity->nick;
      }
    }
    $aData['active'] = $oEntity->active;
    $aData['updated_date'] = time();
    return parent::editRow($aData);
  }

  public function getAll(array $aData = null)
  {
    $oThat = $this;
    $oResultSet = $this->select(function (Select $oSelect) use ($aData, $oThat) {
      $oSelect->join('user_param', 'user_param.user_id = user.id', array('user_param_id' => 'id', 'user_id', 'user_category_id', 'pesel', 'borrower_hash', 'bbarcode', 'btype', 'borrower_note', 'student_id', 'employee_id', 'first_name', 'last_name', 'street', 'post_code', 'city', 'phone_number', 'last_activity'));
      $oSelect->join('user_category', 'user_category.id = user_param.user_category_id', array('user_category_name' => 'name'), $oSelect::JOIN_LEFT);
      if ($aData) {
        foreach ($aData as $sKey => $mValue) {
          if ($sKey === 'filter' && isset($mValue)) {
            foreach ($mValue as $sColumnName => $sValue) {
              $sDataType = $oThat->getColumnDataType($sColumnName);
              if ($sColumnName === 'id') {
                $sColumnName = $oThat->table . '.' . $sColumnName;
              }
              if (in_array($sDataType, array('int', 'tinyint'))) {
                if ($sColumnName === 'created_date' || $sColumnName === 'updated_date') {
                  $oSelect->where(array($sColumnName . '>=?' => strtotime($sValue)));
                  $oSelect->where(array($sColumnName . '=<?' => strtotime($sValue) + 86400));
                } else {
                  $oSelect->where(array($sColumnName . '=?' => (int)$sValue));
                }
              } else {
                $oSelect->where->like($sColumnName, '%' . $sValue . '%');
              }
            }
          } else if ($sKey === 'sort' && is_string($mValue['sort_column']) && is_string($mValue['sort_method'])) {
            if ($mValue['sort_column'] === 'id') {
              $mValue['sort_column'] = $oThat->table . '.' . $mValue['sort_column'];
            }
            $oSelect->order(array($mValue['sort_column'] => $mValue['sort_method']));
          } else if ($sKey === 'limit' && isset($mValue) && is_numeric($mValue)) {
            $oSelect->limit($mValue);
          } else if ($sKey === 'offset' && isset($mValue) && is_numeric($mValue)) {
            $oSelect->offset($mValue);
          } else if (!in_array($sKey, array('filter', 'sort', 'limit', 'offset'))) {
            if ($sKey === 'id') {
              $sKey = $oThat->table . '.id';
            }
            $oSelect->where(array($sKey => $mValue));
          }
        }
      }
    });
    $aResult = $this->getEntities($oResultSet);
    return $aResult;
  }
}
<?php
namespace User\Model\Repository;

use Application\RepositoryAbstract;
use User\Model\Entity\UserCategory as Entity;
use Zend\Db\Adapter\Adapter;

class UserCategory extends RepositoryAbstract
{
  protected $table = 'user_category';

  public function __construct(Adapter $oAdapter)
  {
    parent::__construct($oAdapter);
  }

  protected function createNewEntity()
  {
    return new Entity();
  }
}
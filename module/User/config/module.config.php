<?php
namespace User;
return array(
  'router' => array(
    'routes' => array(
      'user_index' => array(
        'type' => 'Zend\Mvc\Router\Http\Literal',
        'options' => array(
          'route' => '/user',
          'defaults' => array(
            '__NAMESPACE__' => 'User\Controller',
            'controller' => 'Index',
            'action' => 'index',
          ),
        ),
        'may_terminate' => true,
        'child_routes' => array(
          'user_index_process' => array(
            'type' => 'Segment',
            'options' => array(
              'route' => '/[:action]',
              'constraints' => array(
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
              ),
              'defaults' => array(),
            ),
          ),
        ),
      ),
    ),
  ),
  'service_manager' => array(
    'factories' => array(
      'AclService' => 'User\Service\AclService',
      'AuthService' => 'User\Service\AuthService',
      'MailService' => 'User\Service\MailService',
    ),
  ),
  'controllers' => array(
    'invokables' => array(
      'User\Controller\Index' => 'User\Controller\IndexController',
      'User\Controller\Test' => 'User\Controller\TestController',
    ),
  ),
  'controller_plugins' => array(
    'invokables' => array(
      'AclPlugin' => 'User\Controller\Plugin\AclPlugin',
      'AuthPlugin' => 'User\Controller\Plugin\AuthPlugin',
    )
  ),
  'view_manager' => array(
    'template_map' => array(
      'layout/user' => __DIR__ . '/../view/layout/layout.phtml',
      'partials/user_data' => __DIR__ . '/../view/partials/user_data.phtml',
    ),
    'template_path_stack' => array(
      'user' => __DIR__ . '/../view',
    ),
  ),
  'view_helpers' => array(
    'invokables' => array(
      'UserData' => 'User\View\Helper\UserData',
    )
  ),
  'authorization' => array(
    //'auth_column' => 'email_address'
    'auth_column' => 'nick'
  ),
);

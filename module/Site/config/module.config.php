<?php
namespace Site;
return array(
  'router' => array(
    'routes' => array(
      'site_index' => array(
        'type' => 'Zend\Mvc\Router\Http\Literal',
        'options' => array(
          'route' => '/site',
          'defaults' => array(
            '__NAMESPACE__' => 'Site\Controller',
            'controller' => 'Index',
            'action' => 'index',
          ),
        ),
        'may_terminate' => false,
        'child_routes' => array(
          'site_index_process' => array(
            'type' => 'Segment',
            'options' => array(
              'route' => '/[:controller[/:action]]',
              'constraints' => array(
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
              ),
              'defaults' => array(),
            ),
          ),
        ),
      ),
      'site_index_menu' => array(
        'type' => 'Zend\Mvc\Router\Http\Literal',
        'options' => array(
          'route' => '/site',
          'defaults' => array(
            '__NAMESPACE__' => 'Site\Controller',
            'controller' => 'Index',
            'action' => 'index',
          ),
        ),
        'may_terminate' => false,
        'child_routes' => array(
          'site_index_menu_process' => array(
            'type' => 'Zend\Mvc\Router\Http\Regex',
            'options' => array(
              'regex' => '/(?<menu>[a-zA-Z0-9_-]+)(\.(?<format>(json|html|xml|rss)))?',
              'defaults' => array(
                '__NAMESPACE__' => 'Site\Controller',
                'controller' => 'Index',
                'action' => 'index',
                'menu' => false,
                'format' => false,
              ),
              'spec' => '/%menu%.%format%',
            ),
          ),
        ),
      ),
      'site_index_submenu' => array(
        'type' => 'Zend\Mvc\Router\Http\Literal',
        'options' => array(
          'route' => '/site',
          'defaults' => array(
            '__NAMESPACE__' => 'Site\Controller',
            'controller' => 'Index',
            'action' => 'index',
          ),
        ),
        'may_terminate' => false,
        'child_routes' => array(
          'site_index_submenu_process' => array(
            'type' => 'Zend\Mvc\Router\Http\Regex',
            'options' => array(
              'regex' => '/(?<menu>[a-zA-Z0-9_-]+)/(?<submenu>[a-zA-Z0-9_-]+)(\.(?<format>(json|html|xml|rss)))?',
              'defaults' => array(
                '__NAMESPACE__' => 'Site\Controller',
                'controller' => 'Index',
                'action' => 'index',
                'menu' => false,
                'submenu' => false,
                'format' => false,
              ),
              'spec' => '/%menu%/%submenu%.%format%',
            ),
          ),
        ),
      ),
      'site_index_subsubmenu' => array(
        'type' => 'Zend\Mvc\Router\Http\Literal',
        'options' => array(
          'route' => '/site',
          'defaults' => array(
            '__NAMESPACE__' => 'Site\Controller',
            'controller' => 'Index',
            'action' => 'index',
          ),
        ),
        'may_terminate' => false,
        'child_routes' => array(
          'site_index_subsubmenu_process' => array(
            'type' => 'Zend\Mvc\Router\Http\Regex',
            'options' => array(
              'regex' => '/(?<menu>[a-zA-Z0-9_-]+)/(?<submenu>[a-zA-Z0-9_-]+)/(?<subsubmenu>[a-zA-Z0-9_-]+)(\.(?<format>(json|html|xml|rss)))?',
              'defaults' => array(
                '__NAMESPACE__' => 'Site\Controller',
                'controller' => 'Index',
                'action' => 'index',
                'menu' => false,
                'submenu' => false,
                'subsubmenu' => false,
                'format' => false,
              ),
              'spec' => '/%menu%/%submenu%/%subsubmenu%.%format%',
            ),
          ),
        ),
      ),
    ),
  ),
  'service_manager' => array(
    'factories' => array(),
  ),
  'controllers' => array(
    'invokables' => array(
      'Site\Controller\Index' => 'Site\Controller\IndexController',
    ),
  ),
  'controller_plugins' => array(
    'invokables' => array()
  ),
  'view_manager' => array(
    'template_map' => array(),
    'template_path_stack' => array(
      'site' => __DIR__ . '/../view',
    ),
  ),
  'view_helpers' => array(
    'invokables' => array(),
  ),
  'module_layouts' => array(),
);

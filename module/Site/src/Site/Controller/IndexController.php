<?php
namespace Site\Controller;

use Zend\Mvc\Controller\AbstractActionController;

class IndexController extends AbstractActionController
{
  protected function getSiteHref($sLabel)
  {
    $sNewLabel = '';
    $sTempLabel = trim(strtolower($sLabel));
    $sTempLabel = explode(' ', $sTempLabel);
    foreach ($sTempLabel as $nKey => $sValue) {
      if ($nKey)
        $sNewLabel .= '-' . $sValue;
      else
        $sNewLabel .= $sValue;
    }
    return $this->removeAccents($sNewLabel);
  }

  protected function removeAccents($sStr)
  {
    $aPolishChars = array('ą', 'ć', 'ę', 'ł', 'ń', 'ó', 'ś', 'ż', 'ź');
    $aReplace = array('a', 'c', 'e', 'l', 'n', 'o', 's', 'z', 'z');
    return str_replace($aPolishChars, $aReplace, $sStr);
  }

  public function indexAction()
  {
  }
}
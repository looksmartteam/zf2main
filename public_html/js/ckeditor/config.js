/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function (config) {
  config.resize_enabled = false;
  config.toolbarCanCollapse = true;
  config.toolbarStartupExpanded = false;
};

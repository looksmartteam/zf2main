$(document).ready(function () {
  var current_requests = 0;
  $.ajaxPrefilter(function (options, originalOptions, jqXHR) {
    var base_url = '';
    if (options.url.indexOf(base_url) == 0) {
      base_url = '';
    }
    options.url = base_url + options.url;
    if (options.global === true) {
      current_requests++;
    }
  });
  $(document).ajaxSend(function (event, xhr, options) {
    $('#ajax_overlay_error').hide();
    $('#ajax_overlay_success').show();
    if (event.target.activeElement) {
      Ladda.stopAll();
      var element = event.target.activeElement;
      var $element = $(element);
      var local_name = $element[0].localName;
      if (local_name === 'button' && $(element).hasClass('ladda-button')) {
        var ladda = Ladda.create(element);
        ladda.start();
      }
    }
  });
  $(document).ajaxComplete(function (event, xhr, options) {
    if (current_requests && options.global === true) {
      current_requests--;
      setTimeout(function () {
        if (!current_requests) {
          Ladda.stopAll();
          $('.ladda-spinner').remove();
          $('#ajax_overlay_success').hide();
        }
      }, 100);
    }
  });
  $(document).ajaxError(function (event, xhr, options) {
    current_requests = 0;
    setTimeout(function () {
      if (!current_requests) {
        Ladda.stopAll();
        $('.ladda-spinner').remove();
        $('#ajax_overlay_success').hide();
        $('#ajax_overlay_error').show();
      }
    }, 100);
  });
  $('.dropdown-submenu > a').on('click', function (event) {
    var $t = $(this);
    $t.parents('.dropdown-menu').find('.dropdown-menu').hide();
    if ($t.next('.dropdown-menu').eq(0).is(':hidden')) {
      $t.next('.dropdown-menu').eq(0).show();
    } else {
      $t.next('.dropdown-menu').eq(0).hide();
    }
    event.preventDefault();
    event.stopPropagation();
  });
  $('li.dropdown-toggle').on('click', function (event) {
    var $t = $(this);
    if ($t.hasClass('open')) {
      $t.find('.dropdown-submenu > .dropdown-menu').hide();
    }
  });
  $('select').chosen({allow_single_deselect: true, display_disabled_options: false, width: "100%"}).addClass('chosen');
  $('input[type=checkbox]').on('change', function () {
    var checkbox_name = $(this).attr('name');
    var $checkbox_input_hidden = $(this).parent('dd').find('input[type=hidden][name=' + checkbox_name + ']');
    if ($checkbox_input_hidden.length) {
      var is_checked = $(this).prop('checked');
      if (is_checked) {
        $checkbox_input_hidden.val('1');
      } else {
        $checkbox_input_hidden.val('0');
      }
    }
  });
  $('input[type=text][class*=_date]').datepicker({
    language: 'pl',
    format: 'yyyy/mm/dd'
  }).addClass('datepicker');
  $('form').keypress(function (event) {
    var that = $(this);
    if (event.which === 13) {
      if (that.find('.fake_submit').length) {
        that.find('button[class*=submit]').trigger('click');
        event.preventDefault();
        event.stopPropagation();
      }
    }
  });
  $('a[data-toggle="tab"]').off('shown.bs.tab').on('shown.bs.tab', function (event) {
    var tab_id = $(event.target).attr('href').split('#')[1];
    if ($('#' + tab_id).length) {
      var $tab = $('#' + tab_id);
      $.each($tab.find('select'), function (index, select) {
        var width = $(select).parent('dd').width();
        if (!isNaN(width)) {
          $(select).parent('dd').find('.chosen-container').width(width);
        }
      });
    }
  });
  $('button[class*=submit]').on('click', function (event) {
    validate_ajax.validate($(this).parents('form'));
    event.preventDefault();
    event.stopPropagation();
  });
  $('.title_tooltip, .modal_title_tooltip').tooltip({'html': true, 'trigger': 'focus'});
  $('.title_popover, .modal_title_popover').popover({'html': true, 'trigger': 'focus', delay: {show: 500, hide: 0}});
  $('.modal_title_popover').on('shown.bs.popover', function () {
    if ($('body').hasClass('modal-open')) {
      $('.popover').css({
        'z-index': 2000
      });
    }
  });
  $('.modal_title_tooltip').on('shown.bs.tooltip', function () {
    if ($('body').hasClass('modal-open')) {
      $('.tooltip').css({
        'z-index': 2000
      });
    }
  });
  $('.email_content_configuration_type_id').chosen().change(function () {
    var that = $(this);
    var $form = that.parents('form');
    if (that.val() == '2') {
      $form.find('.email_content_configuration_conclusion_type_id').val('').trigger('chosen:updated').trigger('change').parents('dl').show();
      $form.find('.email_content_configuration_greeting_type_id').val('').trigger('chosen:updated').trigger('change').parents('dl').show();
      $form.find('.subject').parents('dl').show();
    } else {
      $form.find('.email_content_configuration_conclusion_type_id').val('').trigger('chosen:updated').trigger('change').parents('dl').hide();
      $form.find('.email_content_configuration_greeting_type_id').val('').trigger('chosen:updated').trigger('change').parents('dl').hide();
      $form.find('.subject').parents('dl').hide();
    }
    repair_select($form);
  });
  $('.price_net, .price_gross').on('change', function () {
    if ($(this).val().indexOf(',')) {
      $(this).val($(this).val().replace(/\,/g, '.'));
    }
  });
  $('.slider_new_borrower, .slider_borrower').hover(
    function () {
      $(this).find('.slider_borrower_box').animate({'background-color': 'rgba(255, 255, 255, 1)'}, 500);
    }, function () {
      $(this).find('.slider_borrower_box').animate({'background-color': 'rgba(255, 255, 255, 0.7)'}, 500);
    }
  );
  function clear_form($form, clear_error) {
    var clear_error = typeof clear_error !== 'undefined' ? clear_error : true;
    if ($form.length) {
      $form.find('.id').val('');
      $form.find('input[type!=hidden], textarea, select').val('');
      $form.find('input[type=checkbox]').prop('checked', false);
      $form.find('select').trigger('chosen:updated');
      if (clear_error) {
        $form.find('.has-error').removeClass('has-error').find('.has-feedback').removeClass('has-feedback');
        $form.find('dd span.form-control-feedback').remove();
        $form.find('.help-block').remove();
        $form.parents('.table_container').find('.has_error').removeClass('has_error');
        $form.parents('.tabs_form_container').find('.has_error').removeClass('has_error');
      }
    }
  }

  function repair_select($form) {
    $.each($form.find('select'), function (index, select) {
      var width = $(select).parent('dd').width();
      if (!isNaN(width)) {
        $(select).parent('dd').find('.chosen-container').width(width);
      }
    });
  }

  function populate_form($form, row, clear) {
    var clear = typeof clear !== 'undefined' ? clear : true;
    if (clear)
      clear_form($form);
    $.each(row, function (input_class, value) {
      var $element = $form.find('.' + input_class);
      if ($element.length) {
        if ($element.is('input:text') || $element.is('input:hidden') || $element.is('textarea')) {
          $element.val(value);
          if ($element.hasClass('ckeditor')) {
            var instance = $element.attr('id');
            CKEDITOR.instances[instance].setData(value, function () {
              this.checkDirty(false);
            });
          }
        } else if ($element.is('select')) {
          if ($element.attr('multiple')) {
            $.each(value, function (index, value) {
              $form.find('.' + input_class + ' option[value="' + value[input_class] + '"]').prop('selected', 'selected');
            });
          } else {
            $form.find('.' + input_class + ' option[value="' + value + '"]').prop('selected', 'selected');
          }
          $element.trigger('chosen:updated');
        } else if ($element.is('input:checkbox')) {
          if (value === 1) {
            $element.prop('checked', true);
          }
        }
      }
    });
    $('input[type=checkbox]').trigger('change');
  }

  function ckeditor_update_element() {
    for (instance in CKEDITOR.instances) {
      CKEDITOR.instances[instance].updateElement();
    }
  }

  function ckeditor_clear_data() {
    for (instance in CKEDITOR.instances) {
      CKEDITOR.instances[instance].setData('');
    }
  }

  function NewBorrower() {
    this.check_new_borrower_ajax_url = '/buuam/checknewborrowerajax';
    this.load_ajax_url = '/buuam/loadnewborrowerajax';
    this.$new_borrower_form = $('#new_borrower');
    this.$key = this.$new_borrower_form.find('.key');
    this.who = null;
    this.organization_btype = null;
    this.$is_student = this.$new_borrower_form.find('.is_student');
    this.$is_employee = this.$new_borrower_form.find('.is_employee');
    this.$is_borrower = this.$new_borrower_form.find('.is_borrower');
    this.$pesel = this.$new_borrower_form.find('.pesel');
    this.$student_id = this.$new_borrower_form.find('.student_id');
    this.$employee_id = this.$new_borrower_form.find('.employee_id');
    this.$email_address = this.$new_borrower_form.find('.email_address');
    this.$load_new_borrower_confirm_modal = $('#load_new_borrower_confirm_modal');
    this.$load_new_borrower_button = this.$load_new_borrower_confirm_modal.find('.load_new_borrower_button');
    this.$cancel_new_borrower_button = this.$load_new_borrower_confirm_modal.find('.cancel_button');
    this.$student_organization_key = $('.student_organization_key');
    this.$employee_organization_key = $('.employee_organization_key');
    this.$btype = $('.btype');
    this.init();
  }

  NewBorrower.prototype = {
    init: function () {
      var that = this;
      that.clear_form();
      that.hide_field();
      that.$pesel.on('change', function () {
        var $that = $(this);
        validate_ajax.validate(that.$new_borrower_form, ['pesel'], that.$pesel);
      });
      that.$pesel.on('post_validate', function () {
        if (validate_ajax.is_success) {
          var data = {};
          data.pesel = that.$pesel.val();
          $.ajax({
            async: true,
            url: that.check_new_borrower_ajax_url,
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function (response) {
              that.who = null;
              if (response.success === true) {
                that.pre_complete_form(response);
              } else {
                that.clear_form();
                that.hide_field();
              }
            }
          });
        } else {
          var temp_pesel = that.$pesel.val();
          var temp_employee_id = that.$employee_id.val();
          that.clear_form(false);
          that.$pesel.val(temp_pesel);
          that.hide_field();
        }
      });
      this.$student_id.on('change', function () {
        var $that = $(this);
        validate_ajax.validate(that.$new_borrower_form, ['student_id'], that.$student_id);
      });
      this.$student_id.on('post_validate', function () {
        if (that.who === 'student' && validate_ajax.is_success) {
          var data = {};
          data.who = that.who;
          data.pesel = that.$pesel.val();
          data.new_borrower_id = that.$student_id.val();
          $.ajax({
            async: true,
            url: that.load_ajax_url,
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function (response) {
              if (response.success === true) {
                that.fill_form(response);
              } else {
                var temp_pesel = that.$pesel.val();
                var temp_student_id = that.$student_id.val();
                that.clear_form();
                that.$pesel.val(temp_pesel);
                that.$student_id.val(temp_student_id);
                //that.hide_field();
              }
            }
          });
        }
      });
      this.$employee_id.on('change', function () {
        var $that = $(this);
        validate_ajax.validate(that.$new_borrower_form, ['employee_id'], that.$employee_id);
      });
      this.$employee_id.on('post_validate', function () {
        if (that.who === 'employee' && validate_ajax.is_success) {
          var data = {};
          data.who = that.who;
          data.pesel = that.$pesel.val();
          data.new_borrower_id = that.$employee_id.val();
          $.ajax({
            async: true,
            url: that.load_ajax_url,
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function (response) {
              if (response.success === true) {
                that.fill_form(response);
                that.$email_address.val(that.$employee_id.val() + '@amu.edu.pl');
              } else {
                var temp_pesel = that.$pesel.val();
                var temp_employee_id = that.$employee_id.val();
                that.clear_form();
                that.$pesel.val(temp_pesel);
                that.$employee_id.val(temp_employee_id);
                //that.hide_field();
              }
            }
          });
        }
      });
      that.$load_new_borrower_button.on('click', function () {
        that.complete_form();
        if (that.who === 'student') {
          that.$student_id.focus();
        } else if (that.who === 'employee') {
          that.$employee_id.focus();
        }
      });
      that.$load_new_borrower_confirm_modal.on('keypress', function (event) {
        if (event.keyCode === 13) {
          event.preventDefault();
          event.stopPropagation();
          that.$load_new_borrower_button.trigger('click');
        }
      });
      that.$pesel.on('keypress', function (event) {
        if (event.keyCode === 13) {
          $(this).trigger('change');
          event.preventDefault();
          event.stopPropagation();
        }
      });
      that.$student_id.on('keypress', function (event) {
        if (event.keyCode === 13) {
          $(this).trigger('change');
          event.preventDefault();
          event.stopPropagation();
        }
      });
      that.$employee_id.on('keypress', function (event) {
        if (event.keyCode === 13) {
          $(this).trigger('change');
          event.preventDefault();
          event.stopPropagation();
        }
      });
      this.$cancel_new_borrower_button.on('click', function () {
        that.who = 'borrower';
        that.$is_student.val(0).trigger('change');
        that.$is_employee.val(0).trigger('change');
        that.$is_borrower.val(1).trigger('change');
      });
    },
    pre_complete_form: function (response) {
      var that = this;
      that.who = response.who;
      that.$key.val(0);
      if (that.who === 'student') {
        that.$load_new_borrower_confirm_modal.modal('show');
      } else if (that.who === 'employee') {
        that.$load_new_borrower_confirm_modal.modal('show');
      } else if (that.who === 'borrower') {
        that.$is_student.val(0).trigger('change');
        that.$is_employee.val(0).trigger('change');
        that.$is_borrower.val(1).trigger('change');
      }
    },
    complete_form: function () {
      var that = this;
      if (that.who === 'student') {
        that.$is_student.val(1).trigger('change');
        that.$is_employee.val(0).trigger('change');
        that.$is_borrower.val(0).trigger('change');
      } else if (that.who === 'employee') {
        that.$is_student.val(0).trigger('change');
        that.$is_employee.val(1).trigger('change');
        that.$is_borrower.val(0).trigger('change');
      }
      that.$load_new_borrower_confirm_modal.modal('hide');
    },
    fill_form: function (response) {
      var that = this;
      populate_form(that.$new_borrower_form, response.new_borrower, false);
      var $post_code = that.$new_borrower_form.find('.post_code');
      if ($post_code.val().length == 5) {
        $post_code.val($post_code.val()[0] + $post_code.val()[1] + '-' + $post_code.val()[2] + $post_code.val()[3] + $post_code.val()[4]);
      }
      if (that.who === 'student') {
        that.init_hr_s_organization_hr_btype(response.new_borrower.organization_btype);
      } else if (that.who === 'employee') {
        that.init_hr_e_organization_hr_btype(response.new_borrower.organization_btype);
      }
    },
    clear_form: function (clear_error) {
      var that = this;
      var clear_error = typeof clear_error !== 'undefined' ? clear_error : true;
      clear_form(that.$new_borrower_form, clear_error);
      that.$key.val(0);
      that.$is_student.val(0).trigger('change');
      that.$is_employee.val(0).trigger('change');
      that.$is_borrower.val(0).trigger('change');
    },
    hide_field: function () {
      var that = this;
      that.$student_id.parents('dl').hide();
      that.$employee_id.parents('dl').hide();
      that.organization_btype = null;
      that.$student_organization_key.parents('dl').hide();
      that.$employee_organization_key.parents('dl').hide();
      that.$btype.parents('dl').hide();
      that.$is_student.on('change', function () {
        var value = parseInt(that.$is_student.val());
        if (value === 1) {
          that.$student_id.parents('dl').show();
        } else {
          that.$student_id.parents('dl').hide();
        }
      });
      that.$is_employee.on('change', function () {
        var value = parseInt(that.$is_employee.val());
        if (value === 1) {
          that.$employee_id.parents('dl').show();
        } else {
          that.$employee_id.parents('dl').hide();
        }
      });
      that.$is_borrower.on('change', function () {
        var value = parseInt(that.$is_borrower.val());
        if (value === 1) {
          that.$student_id.parents('dl').hide();
          that.$employee_id.parents('dl').hide();
        }
      });
    },
    reset_who: function () {
      var that = this;
      that.who = null;
    },
    init_hr_e_organization_hr_btype: function (organization_btype) {
      var that = this;
      that.organization_btype = organization_btype;
      that.$employee_organization_key.find('option').attr('disabled', true).end().val('').trigger('chosen:updated');
      that.$btype.find('option').attr('disabled', true).end().val('').trigger('chosen:updated');
      if (that.organization_btype) {
        var organization_key_default_value = '';
        $.each(that.organization_btype, function (index, value) {
          if (index === 0) {
            organization_key_default_value = value.hr_e_organization_id;
          }
          that.$employee_organization_key.find('option[value="' + value.hr_e_organization_id + '"]').attr('disabled', false);
        });
        if (!isNaN(organization_key_default_value)) {
          //that.$employee_organization_key.val(organization_key_default_value).trigger('chosen:updated');
          that.$employee_organization_key.trigger('chosen:updated');
          that.$employee_organization_key.parents('dl').show();
          var btype_default_value = '';
          $.each(that.organization_btype, function (index, value) {
            if (index === 0) {
              btype_default_value = value.hr_btype_id;
            }
            if (organization_key_default_value === value.hr_e_organization_id) {
              that.$btype.find('option[value="' + value.hr_btype_id + '"]').attr('disabled', false);
            }
          });
          if (!isNaN(btype_default_value)) {
            //that.$btype.val(btype_default_value).trigger('chosen:updated');
            that.$btype.trigger('chosen:updated');
            that.$btype.parents('dl').show();
          }
        }
      }
      that.$employee_organization_key.chosen().change(function () {
        var $t = $(this);
        var employee_organization_key = parseInt($t.val());
        that.$btype.find('option').attr('disabled', true);
        if (!isNaN(employee_organization_key)) {
          var btype_default_value = '';
          var idx = false;
          $.each(that.organization_btype, function (index, value) {
            if (employee_organization_key === value.hr_e_organization_id) {
              if (idx === false) {
                btype_default_value = value.hr_btype_id;
                idx = true;
              }
              that.$btype.find('option[value="' + value.hr_btype_id + '"]').attr('disabled', false);
            }
          });
        } else {
          that.$employee_organization_key.val('').trigger('chosen:updated');
          that.$btype.val('').trigger('chosen:updated');
        }
        if (!isNaN(btype_default_value)) {
          that.$btype.val(btype_default_value).trigger('chosen:updated');
          that.$btype.parents('dl').show();
        }
      });
    },
    init_hr_s_organization_hr_btype: function (organization_btype) {
      var that = this;
      that.organization_btype = organization_btype;
      that.$student_organization_key.find('option').attr('disabled', true).end().val('').trigger('chosen:updated');
      that.$btype.find('option').attr('disabled', true).end().val('').trigger('chosen:updated');
      if (that.organization_btype) {
        var organization_key_default_value = '';
        $.each(that.organization_btype, function (index, value) {
          if (index === 0) {
            organization_key_default_value = value.hr_s_organization_id;
          }
          that.$student_organization_key.find('option[value="' + value.hr_s_organization_id + '"]').attr('disabled', false);
        });
        if (!isNaN(organization_key_default_value)) {
          //that.$student_organization_key.val(organization_key_default_value).trigger('chosen:updated');
          that.$student_organization_key.trigger('chosen:updated');
          that.$student_organization_key.parents('dl').show();
          var btype_default_value = '';
          var idx = false;
          $.each(that.organization_btype, function (index, value) {
            if (idx === false) {
              btype_default_value = value.hr_btype_id;
              idx = true;
            }
            if (organization_key_default_value === value.hr_s_organization_id) {
              that.$btype.find('option[value="' + value.hr_btype_id + '"]').attr('disabled', false);
            }
          });
          if (!isNaN(btype_default_value)) {
            //that.$btype.val(btype_default_value).trigger('chosen:updated');
            that.$btype.trigger('chosen:updated');
            that.$btype.parents('dl').show();
          }
        }
      }
      that.$student_organization_key.chosen().change(function () {
        var $t = $(this);
        var student_organization_key = parseInt($t.val());
        that.$btype.find('option').attr('disabled', true);
        that.$btype.val('').trigger('chosen:updated');
        if (!isNaN(student_organization_key)) {
          var btype_default_value = ''
          var idx = false;
          $.each(that.organization_btype, function (index, value) {
            if (student_organization_key === value.hr_s_organization_id) {
              if (idx === false) {
                btype_default_value = value.hr_btype_id;
                idx = true;
              }
              that.$btype.find('option[value="' + value.hr_btype_id + '"]').attr('disabled', false);
            }
          });
        }
        if (!isNaN(btype_default_value)) {
          that.$btype.val(btype_default_value).trigger('chosen:updated');
          that.$btype.parents('dl').show();
        }
      });
    }
  }

  function Table($element, options) {
    this.$table_container = $element.parent('.table_container');
    this.$table = $element;
    this.$table_head_labels = $element.find('thead tr').eq(0);
    this.$pagination = $element.parent('.table_container').find('.pagination');
    this.params = options;
    this.rowset = null;
    this.number_of_rows = null;
    this.number_of_rows_per_page = 20;
    this.pagination_part_length = 3;
    this.current_page = 1;
    this.current_pagination_part = 1;
    this.sort_column = 'id';
    this.sort_method = 'asc';
    this.add_new_form_modal_id = 'add_new_form_modal';
    this.edit_form_modal_id = 'edit_form_modal';
    this.confirm_delete_modal_id = 'confirm_delete_modal';
    this.confirm_delete_modal_button = 'delete_button';
    if (options.sort_column !== undefined) {
      this.sort_column = options.sort_column;
    }
    if (options.sort_method !== undefined) {
      this.sort_method = options.sort_method;
    }
    if (options.edit_form_modal_id !== undefined && $('#' + options.edit_form_modal_id).length) {
      this.edit_form_modal_id = options.edit_form_modal_id;
    }
    if (options.add_new_form_modal_id !== undefined && $('#' + options.add_new_form_modal_id).length) {
      this.add_new_form_modal_id = options.add_new_form_modal_id;
    }
    if (options.confirm_delete_modal_id !== undefined && $('#' + options.confirm_delete_modal_id).length) {
      this.confirm_delete_modal_id = options.confirm_delete_modal_id;
    }
    if (options.confirm_delete_modal_button !== undefined && $('.' + options.confirm_delete_modal_button).length) {
      this.confirm_delete_modal_button = options.confirm_delete_modal_button;
    }
    this.init();
  }

  Table.prototype = {
    init: function () {
      var that = this;
      $.each(that.$table_head_labels.find('th'), function (index, th) {
        if ($(th).html() !== '' && $(th).find('input').length === 0) {
          var column_label = $(th).html();
          $(th).empty().append('<span class="column_label">' + column_label + '</span>');
        }
      });
      that.params = params;
      that.load_data();
      that.add_sortable();
      that.add_filterable();
      that.add_check_all();
      that.add_number_of_rows_per_page();
      that.add_control_panel();
    },
    set_response: function (response) {
      var that = this;
      that.$table_container.find('.table_control_panel').hide();
      that.$table.hide();
      that.$table.find('tbody').empty();
      that.number_of_rows = response.num_rows;
      if (parseInt(response.num_rows) > 0) {
        that.$table_container.find('.add_first_new_form_modal').hide();
        that.rowset = response.rowset;
        that.fill_table();
      } else if (parseInt(that.number_of_rows) === 0 && !that.$table.find('tbody').children().length) {
        that.$table_container.find('.add_first_new_form_modal').show();
      }
    },
    fill_table: function () {
      var that = this;
      var table_body = '';
      $.each(that.rowset, function (index, row) {
        var style = '';
        if (row['color'] != null) {
          style = 'style="color:' + row['color'] + '"';
        }
        if (row['active'] === 0 || row['visible'] === 0 || row['primary'] === 0) {
          table_body += '<tr id="row_id-' + row['id'] + '" class="disbable_row" ' + style + '>';
        } else {
          table_body += '<tr id="row_id-' + row['id'] + '" ' + style + '>';
        }
        $.each(that.$table_head_labels.find('th'), function (ii, th) {
          var column_name = $(th).data('column_name');
          if (column_name !== undefined && column_name !== '' && column_name !== 'table_default_row_buttons' && row[column_name] !== undefined && row[column_name] !== null) {
            if (column_name === 'id') {
              table_body += '<td><input class="checkbox" type="checkbox" name="checkbox" data-id="' + row[column_name] + '"></td>';
            } else if (column_name === 'active' || column_name === 'visible') {
              if (row[column_name] === 1) {
                table_body += '<td><i class="fa fa-eye action second_opinion" data-second_opinion="enable_disable" data-column_name="' + column_name + '" data-id="' + row['id'] + '"></i></td>';
              } else if (row[column_name] === 0) {
                table_body += '<td><i class="fa fa-eye-slash second_opinion" data-second_opinion="enable_disable" data-column_name="' + column_name + '" data-id="' + row['id'] + '"></i></td>';
              }
            } else if (column_name === 'primary') {
              if (row[column_name] === 1) {
                table_body += '<td><i class="fa fa-star action second_opinion" data-second_opinion="enable_disable" data-reset_all="1" data-column_name="' + column_name + '" data-id="' + row['id'] + '"></i></td>';
              } else if (row[column_name] === 0) {
                table_body += '<td><i class="fa fa-star-o second_opinion" data-second_opinion="enable_disable" data-reset_all="1" data-column_name="' + column_name + '" data-id="' + row['id'] + '"></i></td>';
              }
            }
            else {
              table_body += '<td>' + row[column_name] + '</td>';
            }
          } else if (column_name === 'table_default_row_buttons') {
            var row_buttons = $('.row_buttons').html();
            table_body += '<td>' + row_buttons + '</td>';
          } else {
            table_body += '<td></td>';
          }
        });
        table_body += '</tr>';
      });
      if (table_body.length) {
        that.$table.append(table_body);
        that.$table_container.find('.table_control_panel').show();
        that.$table.show();
        that.add_pagination();
        that.add_row_buttons();
        if (that.params.refresh_cart_ajax_url) {
          $.ajax({
            async: true,
            url: that.params.refresh_cart_ajax_url,
            type: 'POST',
            dataType: 'json',
            data: null,
            success: function (response) {
              $('.cart_items_count').html(response.cart_items_count);
              $('.cart_sum_price_gross').html(response.cart_sum_price_gross);
            }
          });
        }
        $('.second_opinion').on('click', function () {
          var $t = $(this);
          var data = {};
          data.second_opinion = $(this).data('second_opinion');
          data.column_name = $(this).data('column_name');
          data.id = $t.data('id');
          data.reset_all = $(this).data('reset_all');
          if (that.params.secend_option) {
            $.ajax({
              async: true,
              url: that.params.secend_option,
              type: 'POST',
              dataType: 'json',
              data: data,
              success: function (response) {
                that.load_data();
              }
            });
          }
        });
      }
    },
    load_data: function () {
      var that = this;
      var data = {};
      data.filter = {};
      $.each(that.$table.find('input.filter'), function (index, value) {
        if ($(value).val() !== '') {
          data.filter[$(value).data('column_name')] = $(value).val();
          that.current_page = 1;
        }
      });
      data.sort_column = that.sort_column;
      data.sort_method = that.sort_method;
      data.limit = that.number_of_rows_per_page;
      data.offset = (that.current_page - 1) * that.number_of_rows_per_page;
      if (that.params.load_ajax_url) {
        $.ajax({
          async: true,
          url: that.params.load_ajax_url,
          type: 'POST',
          dataType: 'json',
          data: data,
          success: function (response) {
            if (data.hasOwnProperty('filter') && response.num_rows === 0) {
              $('#empty_result_modal').modal('show');
            } else {
              that.set_response(response);
            }
          }
        });
      }
    },
    add_pagination: function () {
      var that = this;
      that.$pagination.empty();
      that.number_of_pages = Math.ceil(that.number_of_rows / that.number_of_rows_per_page);
      for (var page = 1; page <= that.number_of_pages; page++) {
        $('<li class="page_number-' + page + '"><a href="#">' + page + '</a></li>').on('click', {new_page: page},
          function (event) {
            that.current_page = event.data['new_page'];
            that.load_data();
          }).appendTo(that.$pagination);
      }
      $('<li class="prev_part"><a href="#">...</a></li>').prependTo(that.$pagination).on('click', function () {
        if (that.current_pagination_part > 1) {
          that.current_page = ((that.current_pagination_part - 1) * that.pagination_part_length);
          that.load_data();
        }
      });
      $('<li class="next_part"><a href="#">...</a></li>').appendTo(that.$pagination).on('click', function () {
        if (that.current_pagination_part < (that.number_of_pages / that.pagination_part_length)) {
          that.current_page = (that.current_pagination_part * that.pagination_part_length) + 1;
          that.load_data();
        }
      });
      $('<li class="prev"><a href="#">&laquo;</a></li>').prependTo(that.$pagination).on('click', function () {
        if (that.current_page > 1) {
          that.current_page--;
          that.load_data();
        }
      });
      $('<li class="next"><a href="#">&raquo;</a></li>').appendTo(that.$pagination).on('click', function () {
        if (that.current_page < that.number_of_pages) {
          that.current_page++;
          that.load_data();
        }
      });
      if (that.current_page < ((that.current_pagination_part - 1) * that.pagination_part_length) + 1) {
        that.current_pagination_part--;
      } else if (that.current_page > that.current_pagination_part * that.pagination_part_length) {
        that.current_pagination_part++;
      }
      if (that.current_pagination_part === 1) {
        $('.prev_part').hide();
      }
      if (that.current_pagination_part >= (that.number_of_pages / that.pagination_part_length)) {
        $('.next_part').hide();
      }
      that.$pagination.find('li[class^=page_number-]').hide().slice((that.current_pagination_part - 1) * that.pagination_part_length, that.current_pagination_part * that.pagination_part_length).show();
      that.$pagination.find('li').removeClass('active').removeClass('disable');
      that.$pagination.find('.page_number-' + that.current_page).addClass('active');
    },
    add_sortable: function () {
      var that = this;
      $.each(that.$table_head_labels.find('th'), function (i, th) {
        var $header = $(th);
        if ($header.hasClass('sortable')) {
          var column_name = $header.data('column_name');
          if ($header.data('sort_column_name') !== undefined) {
            column_name = $header.data('sort_column_name');
          }
          $header.on('click', function (event) {
            var $t = $(this);
            that.$table_head_labels.find('i.fa').remove();
            $t.append('<i class="fa fa-caret-down"></i>');
            if ($t.data('sort_method') === 'asc') {
              $t.data('sort_method', 'desc').children('i').addClass('fa-rotate-180');
            } else {
              $t.data('sort_method', 'asc');
            }
            that.sort_column = column_name;
            that.sort_method = $t.data('sort_method');
            that.load_data();
          }).data('sort_method', 'asc');
        }
      });
    },
    add_filterable: function () {
      var that = this;
      var $filterable = $('<tr></tr>');
      if (that.$table_head_labels.find('.filterable').length) {
        $.each(that.$table_head_labels.find('th'), function (index, th) {
            var $header = $(th);
            var $filter_element = $('<th></th>');
            if ($header.hasClass('filterable')) {
              var column_name = $header.data('column_name');
              var filter_column_name = '';
              if ($header.data('filter_column_name') !== undefined) {
                filter_column_name = $header.data('filter_column_name');
              } else {
                filter_column_name = column_name;
              }
              var placeholder = $header.find('span.column_label').html();
              $filter_element.append(
                $('<input type="text">')
                  .data('column_name', filter_column_name)
                  .addClass(column_name)
                  .attr('placeholder', placeholder)
                  .addClass('filter form-control')
                  .keypress(function (event) {
                    if (event.which === 13) {
                      that.load_data();
                      event.preventDefault();
                      event.stopPropagation();
                    }
                  }).autocomplete({
                    source: function (request, response) {
                      if (that.params.autocomplete_ajax_url) {
                        var data = {};
                        data.filter = {};
                        $.each(that.$table.find('input.filter'), function (index, value) {
                          if ($(value).val() !== '') {
                            data.filter[$(value).data('column_name')] = $(value).val();
                          }
                        });
                        data.column_name = column_name;
                        $.ajax({
                          async: true,
                          url: that.params.autocomplete_ajax_url,
                          dataType: 'json',
                          data: data,
                          success: function (data) {
                            response(data.rowset);
                          }
                        });
                      }
                    },
                    select: function (event, ui) {
                      that.load_data();
                    },
                    minLength: 0
                  }
                )
              );
            }
            $filterable.append($filter_element);
          }
        );
        that.$table.find('thead').append($filterable);
        $('thead input[type=text][class*=_date]').datepicker({
          language: 'pl',
          format: 'dd.mm.yyyy'
        }).on('changeDate', function (ev) {
          $('.filter_button').trigger('click');
        }).addClass('datepicker');
      }
    },
    add_check_all: function () {
      var that = this;
      that.$table_container.find('.check_all_top').on('click', function () {
        that.$table.find('td input.checkbox:visible').prop('checked', $(this).is(':checked'));
      });
    },
    add_number_of_rows_per_page: function () {
      var that = this;
      that.$table_container.find('.number_of_rows_per_page').on('change', function () {
        that.current_page = 1;
        that.current_pagination_part = 1;
        that.number_of_rows_per_page = parseInt($(this).find('option:selected').val());
        that.load_data();
      }).trigger('chosen:updated');
      $('.control_panel .chosen-container').width(70);
    },
    add_control_panel: function () {
      var that = this;
      that.$table_container.find('.reset_button').on('click', function () {
        that.current_page = 1;
        that.$table_head_labels.find('i.fa').remove();
        that.sort_column = 'id';
        that.sort_method = 'asc';
        if (that.params.sort_column !== undefined) {
          that.sort_column = that.params.sort_column;
        }
        if (that.params.sort_method !== undefined) {
          that.sort_method = that.params.sort_method;
        }
        $.each(that.$table.find('input.filter'), function (index, value) {
          if ($(value).val() !== '') {
            $(value).val('');
          }
        });
        that.load_data();
      });
      that.$table_container.find('.filter_button').on('click', function () {
        that.load_data();
      });
      that.$table_container.find('.copy_form_modal').on('click', function () {
        if (that.$table.find('td input.checkbox:checked').length !== 1) {
          event.preventDefault();
          event.stopPropagation();
        }
      });
      that.$table_container.find('.add_new_form_modal').on('click', function () {
        if (that.$table.find('td input.checkbox:checked').length === 1) {
          event.preventDefault();
          event.stopPropagation();
        }
      });
      that.$table_container.find('.add_to_cart_button').on('click', function () {
        $.each(that.$table.find('td input.checkbox:checked'), function (index, checkbox) {
          var $t = $(this);
          var data = {};
          data.id = $t.data('id');
          if (!isNaN(data.id) && that.params.add_to_cart_ajax_url) {
            $.ajax({
              async: true,
              url: that.params.add_to_cart_ajax_url,
              type: 'POST',
              dataType: 'json',
              data: data,
              success: function (response) {
              }
            });
            that.load_data();
          }
        });
      });
      that.$table_container.find('.' + that.confirm_delete_modal_button).on('click', function () {
        $.each(that.$table.find('td input.checkbox:checked'), function (index, checkbox) {
          var $t = $(this);
          var data = {};
          data.id = $t.data('id');
          if (!isNaN(data.id) && that.params.delete_ajax_url) {
            $.ajax({
              async: true,
              url: that.params.delete_ajax_url,
              type: 'POST',
              dataType: 'json',
              data: data,
              success: function (response) {
                if (response === true) {
                  $t.parents('tr').remove();
                  that.load_data();
                }
              }
            });
          }
        });
        $('#' + that.confirm_delete_modal_id).modal('hide');
      });
      that.$table_container.find('#' + that.edit_form_modal_id).on('show.bs.modal', function (event) {
        if (that.$table.find('td input.checkbox:checked').length === 1) {
          var id = that.$table.find('td input.checkbox:checked').data('id');
          if (!isNaN(id)) {
            var data = {};
            data.filter = {};
            data.filter['id'] = id;
            if (that.params.load_ajax_url) {
              $.ajax({
                async: true,
                url: that.params.load_ajax_url,
                type: 'POST',
                dataType: 'json',
                data: data,
                success: function (response) {
                  if (response.rowset.length === 1 && response.num_rows === 1) {
                    var $form = $('#' + that.edit_form_modal_id).find('form');
                    populate_form($form, response.rowset[0]);
                  }
                }
              });
            }
          }
        } else {
          event.preventDefault();
          event.stopPropagation();
        }
      });
      that.$table_container.find('#' + that.edit_form_modal_id).on('shown.bs.modal', function (event) {
        var $form = $(this).find('form');
        repair_select($form);
      });
      that.$table_container.find('#' + that.edit_form_modal_id).on('hidden.bs.modal', function (event) {
        var $form = $(this).find('form');
        clear_form($form);
        ckeditor_clear_data();
      });
      that.$table_container.find('#' + that.add_new_form_modal_id).on('show.bs.modal', function (event) {
        if (that.$table.find('td input.checkbox:checked').length === 1) {
          var id = that.$table.find('td input.checkbox:checked').data('id');
          if (!isNaN(id)) {
            var data = {};
            data.filter = {};
            data.filter['id'] = id;
            if (that.params.load_ajax_url) {
              $.ajax({
                async: true,
                url: that.params.load_ajax_url,
                type: 'POST',
                dataType: 'json',
                data: data,
                success: function (response) {
                  if (response.rowset.length === 1 && response.num_rows === 1) {
                    var $form = $('#' + that.add_new_form_modal_id).find('form');
                    populate_form($form, response.rowset[0]);
                  }
                }
              });
            }
          }
        }
      });
      that.$table_container.find('#' + that.add_new_form_modal_id).on('shown.bs.modal', function (event) {
        var $form = $(this).find('form');
        repair_select($form);
      });
      that.$table_container.find('#' + that.add_new_form_modal_id).on('hidden.bs.modal', function (event) {
        var $form = $(this).find('form');
        clear_form($form);
        ckeditor_clear_data();
      });
      that.$table_container.find('#' + that.confirm_delete_modal_id).on('show.bs.modal', function (event) {
        if (that.$table.find('td input.checkbox:checked').length === 0) {
          event.preventDefault();
          event.stopPropagation();
        }
      });
    },
    add_row_buttons: function () {
      var that = this;
      that.$table_container.find('.row_edit_button').off('click').on('click', function () {
        that.$table.find('td input.checkbox:visible').prop('checked', false);
        var id = $(this).parents('tr').find('.checkbox').prop('checked', true).data('id');
        if (!isNaN(id)) {
          if (that.edit_form_modal_id !== undefined && $('#' + that.edit_form_modal_id).length) {
            $('#' + that.edit_form_modal_id).modal('show');
          } else {
            $('#edit_form_modal').modal('show');
          }
        }
      });
      that.$table_container.find('.row_copy_button').off('click').on('click', function () {
        that.$table.find('td input.checkbox:visible').prop('checked', false);
        var id = $(this).parents('tr').find('.checkbox').prop('checked', true).data('id');
        if (!isNaN(id)) {
          if (that.add_new_form_modal_id !== undefined && $('#' + that.add_new_form_modal_id).length) {
            $('#' + that.add_new_form_modal_id).modal('show');
          } else {
            $('#add_new_form_modal_id').modal('show');
          }
        }
      });
      that.$table_container.find('.row_delete_button').off('click').on('click', function () {
        that.$table.find('td input.checkbox:visible').prop('checked', false);
        $(this).parents('tr').find('.checkbox').prop('checked', true);
        if (that.confirm_delete_modal_id !== undefined && $('#' + that.confirm_delete_modal_id).length) {
          $('#' + that.confirm_delete_modal_id).modal('show');
        } else {
          $('#confirm_delete_modal').modal('show');
        }
      });
      that.$table_container.find('.row_add_to_cart_button').off('click').on('click', function () {
        that.$table.find('td input.checkbox:visible').prop('checked', false);
        var id = $(this).parents('tr').find('.checkbox').prop('checked', true).data('id');
        if (!isNaN(id)) {
          var data = {};
          data.id = id;
          if (that.params.add_to_cart_ajax_url) {
            $.ajax({
              async: true,
              url: that.params.add_to_cart_ajax_url,
              type: 'POST',
              dataType: 'json',
              data: data
            });
          }
          that.load_data();
        }
      });
    }
  };

  function NavigationMenu() {
    this.load_ajax_url = '/application/navigation/loadmenuitemsettingsajax';
    this.delete_ajax_url = '/application/navigation/deletenavigationmenuajax';
    this.save_navigation_menu_scheme_ajax_url = '/application/navigation/savenavigationmenuschemeajax';
    this.$navigation_menu = $('#navigation_menu');
    this.navigation_menu_id = null;
    this.display_menu_item_settings = 'display_menu_item_settings';
    this.move_up_menu_item_settings = 'move_up_menu_item_settings';
    this.move_down_menu_item_settings = 'move_down_menu_item_settings';
    this.show_menu_item_settings = 'show_menu_item_settings';
    this.hide_menu_item_settings = 'hide_menu_item_settings';
    this.edit_menu_item_settings = 'edit_menu_item_settings';
    this.delete_menu_item_settings = 'delete_menu_item_settings';
    this.menu_item_settings = 'menu_item_settings';
    this.list_node_name = 'ol';
    this.item_node_name = 'li';
    this.root_class = 'dd';
    this.list_class = 'dd_list';
    this.item_class = 'dd_item';
    this.drag_class = 'dd_dragel';
    this.handle_class = 'dd_handle';
    this.collapsed_class = 'dd_collapsed';
    this.place_class = 'dd_placeholder';
    this.no_drag_class = 'dd_nodrag';
    this.empty_class = 'dd_empty';
    this.expand_btn_html = '';
    this.collapse_btn_html = '';
    this.group = 0;
    this.max_depth = 3;
    this.init();
  }

  NavigationMenu.prototype = {
    init: function () {
      var that = this;
      if (!that.$navigation_menu.length) {
        return false;
      }
      that.$navigation_menu.nestable({
        menu_item_settings: that.menu_item_settings,
        display_menu_item_settings: that.display_menu_item_settings,
        move_up_menu_item_settings: that.move_up_menu_item_settings,
        move_down_menu_item_settings: that.move_down_menu_item_settings,
        listNodeName: that.list_node_name,
        itemNodeName: that.item_node_name,
        rootClass: that.root_class,
        listClass: that.list_class,
        itemClass: that.item_class,
        dragClass: that.drag_class,
        handleClass: that.handle_class,
        collapsedClass: that.collapsed_class,
        placeClass: that.place_class,
        noDragClass: that.no_drag_class,
        emptyClass: that.empty_class,
        expandBtnHTML: that.expand_btn_html,
        collapseBtnHTML: that.collapse_btn_html,
        group: that.group,
        maxDepth: that.max_depth
      });
      that.init_navigation_option();
      that.add_menu_item_settings();
      that.add_reset_button();
      that.add_save_button();
      that.refresh_move_arrows();
    },
    add_menu_item_settings: function () {
      var that = this;
      //$('.navigation_layout_id').parents('dl').hide();
      $('.navigation_parent_menu_id').parents('dl').hide();
      $('.navigation_parent_submenu_id').parents('dl').hide();
      $('.navigation_level_id').chosen().change(function () {
        var $form = $(this).parents('form');
        var value = parseInt($(this).val());
        if (value === 1) {
          $form.find('.navigation_parent_menu_id').val('').trigger('chosen:updated').trigger('change').parents('dl').hide();
          $form.find('.navigation_parent_submenu_id').val('').trigger('chosen:updated').trigger('change').parents('dl').hide();
        } else if (value === 2) {
          $form.find('.navigation_parent_menu_id').val('').trigger('chosen:updated').trigger('change').parents('dl').show();
          $form.find('.navigation_parent_submenu_id').val('').trigger('chosen:updated').trigger('change').parents('dl').hide();
        } else if (value === 3) {
          $form.find('.navigation_parent_menu_id').val('').trigger('chosen:updated').trigger('change').parents('dl').hide();
          $form.find('.navigation_parent_submenu_id').val('').trigger('chosen:updated').trigger('change').parents('dl').show();
        }
        repair_select($form);
      });
      $('.navigation_menu_type_id').chosen().change(function () {
        var $t = $(this);
        var $form = $t.parents('form');
        var navigation_menu_type_id = parseInt($t.val());
        if (!isNaN(navigation_menu_type_id)) {
          if (navigation_menu_type_id === 1) {
            //$form.find('.navigation_layout_id').val('').trigger('chosen:updated').trigger('change').parents('dl').hide();
            $form.find('.navigation_module_id').val('').trigger('chosen:updated').trigger('change').parents('dl').show();
            $form.find('.navigation_controller_id').val('').trigger('chosen:updated').trigger('change').parents('dl').show();
            $form.find('.navigation_action_id').val('').trigger('chosen:updated').trigger('change').parents('dl').show();
            //$form.find('.user_role_id').find('option').attr('disabled', false).trigger('chosen:updated').trigger('change');
          } else if (navigation_menu_type_id === 2) {
            //$form.find('.navigation_layout_id').val('').trigger('chosen:updated').trigger('change').parents('dl').show();
            $form.find('.navigation_module_id').val(1).trigger('chosen:updated').trigger('change').parents('dl').hide();
            $form.find('.navigation_controller_id').val(1).trigger('chosen:updated').trigger('change').parents('dl').hide();
            $form.find('.navigation_action_id').val(1).trigger('chosen:updated').trigger('change').parents('dl').hide();
            //$form.find('.user_role_id').find('option').attr('disabled', true).trigger('chosen:updated').trigger('change');
          }
          repair_select($form);
        }
      });
      $('.navigation_module_id').chosen().change(function () {
        var $t = $(this);
        var $form = $t.parents('form');
        var navigation_module_id = parseInt($t.val());
        if (!isNaN(navigation_module_id)) {
          $form.find('.navigation_controller_id option').attr('disabled', true);
          $form.find('.navigation_action_id option').attr('disabled', true);
          if (that.navigation_menu_id) {
            $.each(that.navigation_menu_id[navigation_module_id], function (index, value) {
              $form.find('.navigation_controller_id').find('option[value="' + index + '"]').attr('disabled', false);
            });
          }
          $form.find('.navigation_controller_id').val('').trigger('chosen:updated').trigger('change');
          $form.find('.navigation_action_id').val('').trigger('chosen:updated').trigger('change');
        }
      });
      $('.navigation_controller_id').chosen().change(function () {
        var $t = $(this);
        var $form = $t.parents('form');
        var navigation_module_id = parseInt($form.find('.navigation_module_id').val());
        var navigation_controller_id = parseInt($t.val());
        if (!isNaN(navigation_module_id) && !isNaN(navigation_controller_id)) {
          $form.find('.navigation_action_id option').attr('disabled', true);
          if (that.navigation_menu_id) {
            $.each(that.navigation_menu_id[navigation_module_id][navigation_controller_id], function (index, value) {
              $form.find('.navigation_action_id').find('option[value="' + index + '"]').attr('disabled', false);
            });
          }
          $form.find('.navigation_action_id').val('').trigger('chosen:updated').trigger('change');
        }
      });
      $('.' + that.display_menu_item_settings).on('click', function (event) {
        var $t = $(this);
        if ($t.hasClass('active')) {
          $t.removeClass('active').parents('.' + that.handle_class).next('.' + that.menu_item_settings).hide();
        } else {
          $t.addClass('active');
          var $menu_item_settings = $(this).parents('.' + that.handle_class).next('.' + that.menu_item_settings);
          var $form = $menu_item_settings.find('form');
          var id = $(this).parents('.' + that.item_class).data('id');
          if (!isNaN(id)) {
            $menu_item_settings.show();
            repair_select($form);
            var data = {};
            data.filter = {};
            data.filter['id'] = id;
            if (that.load_ajax_url) {
              $.ajax({
                async: true,
                url: that.load_ajax_url,
                type: 'POST',
                dataType: 'json',
                data: data,
                success: function (response) {
                  if (response.rowset.length === 1 && response.num_rows === 1) {
                    populate_form($form, response.rowset[0]);
                  }
                }
              });
            }
          }
        }
        event.preventDefault();
        event.stopPropagation();
      }).show();
      $('.' + that.move_up_menu_item_settings).on('click', function (event) {
        var $t = $(this);
        var $item = $t.parents('.' + that.item_class).eq(0);
        if ($item.length == 1) {
          that.move_up($item);
        }
        event.preventDefault();
        event.stopPropagation();
      });
      $('.' + that.move_down_menu_item_settings).on('click', function (event) {
        var $t = $(this);
        var $item = $t.parents('.' + that.item_class).eq(0);
        if ($item.length == 1) {
          that.move_down($item);
        }
        event.preventDefault();
        event.stopPropagation();
      });
      $('.' + that.show_menu_item_settings).on('click', function (event) {
        var $t = $(this);
        var $menu_item_settings = $t.parents('.menu_item_settings');
        var $form = $menu_item_settings.find('form');
        $t.hide();
        $menu_item_settings.find('.' + that.hide_menu_item_settings).show();
        $form.find('select.active').val('1').trigger('chosen:updated').trigger('change');
        $form.find('button[class*=submit]').trigger('click');
        event.preventDefault();
        event.stopPropagation();
      });
      $('.' + that.hide_menu_item_settings).on('click', function (event) {
        var $t = $(this);
        var $menu_item_settings = $t.parents('.menu_item_settings');
        var $form = $menu_item_settings.find('form');
        $t.hide();
        $menu_item_settings.find('.' + that.show_menu_item_settings).show();
        $form.find('select.active').val('0').trigger('chosen:updated').trigger('change');
        $form.find('button[class*=submit]').trigger('click');
        event.preventDefault();
        event.stopPropagation();
      });
      $('.' + that.delete_menu_item_settings).on('click', function (event) {
        var $t = $(this);
        $('#delete_navigation_menu_confirm_modal').find('.delete_navigation_menu_button').data('id', $t.data('id')).end().modal('show');
        event.preventDefault();
        event.stopPropagation();
      });
      $('.edit_navigation_menu').on('click', function (event) {
        var $t = $(this);
        var $form = $('#edit_navigation_menu_form_modal form');
        var id = $t.data('id');
        if (!isNaN(id)) {
          $form.find('.id').val(id);
          var data = {};
          data.filter = {};
          data.filter['id'] = id;
          if (that.load_ajax_url) {
            $.ajax({
              async: true,
              url: that.load_ajax_url,
              type: 'POST',
              dataType: 'json',
              data: data,
              success: function (response) {
                if (response.rowset.length === 1 && response.num_rows === 1) {
                  populate_form($form, response.rowset[0]);
                  var navigation_menu_type_id = response.rowset[0].navigation_menu_type_id;
                  var navigation_level_id = response.rowset[0].navigation_level_id;
                  var navigation_parent_menu_id = response.rowset[0].navigation_parent_menu_id;
                  var navigation_module_id = response.rowset[0].navigation_module_id;
                  var navigation_controller_id = response.rowset[0].navigation_controller_id;
                  var navigation_action_id = response.rowset[0].navigation_action_id;
                  if (!isNaN(navigation_menu_type_id)) {
                    $('.navigation_menu_type_id').trigger('change');
                  }
                  if (!isNaN(navigation_level_id)) {
                    $('.navigation_level_id').trigger('change');
                    if (navigation_level_id === 2) {
                      $form.find('.navigation_parent_menu_id').val(navigation_parent_menu_id).trigger('chosen:updated').trigger('change');
                    } else if (navigation_level_id === 3) {
                      $form.find('.navigation_parent_submenu_id').val(navigation_parent_menu_id).trigger('chosen:updated').trigger('change');
                    }
                  }
                  if (!isNaN(navigation_module_id) && !isNaN(navigation_controller_id) && !isNaN(navigation_action_id)) {
                    $form.find('.navigation_module_id').val(navigation_module_id).trigger('chosen:updated').trigger('change');
                    $form.find('.navigation_controller_id').val(navigation_controller_id).trigger('chosen:updated').trigger('change');
                    $form.find('.navigation_action_id').val(navigation_action_id).trigger('chosen:updated').trigger('change');
                  }
                }
              }
            });
          }
        }
      });
      $('.copy_navigation_menu').on('click', function (event) {
        var $t = $(this);
        var $form = $('#add_new_navigation_menu_form_modal form');
        var id = $t.data('id');
        if (!isNaN(id)) {
          var data = {};
          data.filter = {};
          data.filter['id'] = id;
          if (that.load_ajax_url) {
            $.ajax({
              async: true,
              url: that.load_ajax_url,
              type: 'POST',
              dataType: 'json',
              data: data,
              success: function (response) {
                if (response.rowset.length === 1 && response.num_rows === 1) {
                  populate_form($form, response.rowset[0]);
                  var navigation_menu_type_id = response.rowset[0].navigation_menu_type_id;
                  var navigation_level_id = response.rowset[0].navigation_level_id;
                  var navigation_parent_menu_id = response.rowset[0].navigation_parent_menu_id;
                  var navigation_module_id = response.rowset[0].navigation_module_id;
                  var navigation_controller_id = response.rowset[0].navigation_controller_id;
                  var navigation_action_id = response.rowset[0].navigation_action_id;
                  if (!isNaN(navigation_menu_type_id)) {
                    $('.navigation_menu_type_id').trigger('change');
                  }
                  if (!isNaN(navigation_level_id)) {
                    $('.navigation_level_id').trigger('change');
                    if (navigation_level_id === 2) {
                      $form.find('.navigation_parent_menu_id').val(navigation_parent_menu_id).trigger('chosen:updated').trigger('change');
                    } else if (navigation_level_id === 3) {
                      $form.find('.navigation_parent_submenu_id').val(navigation_parent_menu_id).trigger('chosen:updated').trigger('change');
                    }
                  }
                  if (!isNaN(navigation_module_id) && !isNaN(navigation_controller_id) && !isNaN(navigation_action_id)) {
                    $form.find('.navigation_module_id').val(navigation_module_id).trigger('chosen:updated').trigger('change');
                    $form.find('.navigation_controller_id').val(navigation_controller_id).trigger('chosen:updated').trigger('change');
                    $form.find('.navigation_action_id').val(navigation_action_id).trigger('chosen:updated').trigger('change');
                  }
                }
              }
            });
          }
        }
      });
      $('.delete_navigation_menu_button').on('click', function (event) {
        var $t = $(this);
        var data = {};
        data.id = $t.data('id');
        if (!isNaN(data.id) && that.delete_ajax_url) {
          $.ajax({
            async: true,
            url: that.delete_ajax_url,
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function (response) {
              if (response === true) {
                $('.' + that.item_class + '[data-id="' + data.id + '"]').remove();
                $('.reset_button').trigger('click');
              }
            }
          });
        }
        $('#delete_navigation_menu_confirm_modal').modal('hide');
        event.preventDefault();
        event.stopPropagation();
      });
      $('#add_new_navigation_menu_form_modal, #edit_navigation_menu_form_modal').on('shown.bs.modal', function (event) {
        var $form = $(this).find('form');
        clear_form($form);
        repair_select($form);
      });
      $('#add_new_navigation_menu_form_modal, #edit_navigation_menu_form_modal').on('hidden.bs.modal', function (event) {
        var $form = $(this).find('form');
        clear_form($form);
        $('.navigation_parent_menu_id').parents('dl').hide();
        $('.navigation_parent_submenu_id').parents('dl').hide();
      });
      $('.label_name').on('change', function () {
        var $t = $(this);
        var $head_title = $t.parents('form').find('.head_title');
        if (!$head_title.val().length || $head_title.val() != $t.val()) {
          $head_title.val($t.val());
        }
      });
    },
    add_reset_button: function () {
      var that = this;
      if ($('.dd').length) {
        that.$navigation_menu.parent('.table_container').find('.reset_button').on('click', function (event) {
          var data = {};
          data.filter = {};
          if (that.load_ajax_url) {
            $.ajax({
              async: true,
              url: that.load_ajax_url,
              type: 'POST',
              dataType: 'json',
              data: data,
              success: function (response) {
                if (parseInt(response.num_rows) > 0) {
                  $('.reset_button, .save_button').show();
                  $.each(response.rowset, function (index, row) {
                    var $item_class = $('.' + that.item_class + '[data-id="' + row.id + '"]');
                    var $form = $item_class.find('form').eq(0);
                    populate_form($form, row);
                    $item_class.find('.' + that.handle_class).eq(0).find('.pull-left').html(row.label_name);
                    if (row.active === 1) {
                      $item_class.find('.' + that.show_menu_item_settings + '[data-id="' + row.id + '"]').hide();
                      $item_class.find('.' + that.hide_menu_item_settings + '[data-id="' + row.id + '"]').show();
                    } else if (row.active === 0) {
                      $item_class.find('.' + that.show_menu_item_settings + '[data-id="' + row.id + '"]').show();
                      $item_class.find('.' + that.hide_menu_item_settings + '[data-id="' + row.id + '"]').hide();
                    }
                  });
                } else {
                  $('.reset_button, .save_button').hide();
                }
              }
            });
          }
          event.preventDefault();
          event.stopPropagation();
        }).trigger('click');
      } else {
        that.init_navigation_option();
      }
    },
    add_save_button: function () {
      var that = this;
      $('.save_button').on('click', function (event) {
        var data = {};
        data.order = $('.dd').nestable('serialize');
        if (that.save_navigation_menu_scheme_ajax_url) {
          $.ajax({
            async: true,
            url: that.save_navigation_menu_scheme_ajax_url,
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function (response) {
              that.refresh_move_arrows();
            }
          });
        }
        event.preventDefault();
        event.stopPropagation();
      });
    },
    init_navigation_option: function () {
      var that = this;
      var data = {};
      data.filter = {};
      if (that.load_ajax_url) {
        $.ajax({
          async: true,
          url: that.load_ajax_url,
          type: 'POST',
          dataType: 'json',
          data: data,
          success: function (response) {
            if (response.navigation_menu_id) {
              that.navigation_menu_id = response.navigation_menu_id;
            }
          }
        });
      }
    },
    move_up: function ($item) {
      var that = this;
      var $prev = $item.prev();
      if ($prev.length == 0)
        return;
      $prev.css('z-index', 999).css('position', 'relative').animate({top: $item.height()}, 250);
      $item.css('z-index', 1000).css('position', 'relative').animate({top: '-' + $prev.height()}, 300, function () {
        $prev.css('z-index', '').css('top', '').css('position', '');
        $item.css('z-index', '').css('top', '').css('position', '');
        $item.insertBefore($prev);
        that.refresh_move_arrows();
      });
    },
    move_down: function ($item) {
      var that = this;
      var $next = $item.next();
      if ($next.length == 0)
        return;
      $next.css('z-index', 999).css('position', 'relative').animate({top: '-' + $item.height()}, 250);
      $item.css('z-index', 1000).css('position', 'relative').animate({top: $next.height()}, 300, function () {
        $next.css('z-index', '').css('top', '').css('position', '');
        $item.css('z-index', '').css('top', '').css('position', '');
        $item.insertAfter($next);
        that.refresh_move_arrows();
      });
    },
    refresh_move_arrows: function () {
      var that = this;
      $.each($('.' + that.move_up_menu_item_settings), function (index, value) {
        if ($(value).parents('.' + that.item_class).eq(0).prev().hasClass(that.item_class)) {
          $(value).show();
        } else {
          $(value).hide();
        }
      });
      $.each($('.' + that.move_down_menu_item_settings), function (index, value) {
        if ($(value).parents('.' + that.item_class).eq(0).next().hasClass(that.item_class)) {
          $(value).show();
        } else {
          $(value).hide();
        }
      });
    }
  }
  var validate_ajax = {
    $form: null,
    url: '/application/index/validateformajax',
    data: {},
    is_success: null,
    $trigger_element: null,
    validate: function ($form, validation_group, trigger_element) {
      ckeditor_update_element();
      var that = this;
      that.$form = $form;
      that.data['form_name'] = that.$form.attr('id');
      var valid = that.$form.find('input, textarea, select');
      if (valid.length) {
        that.data['valid'] = {};
        $.each(valid, function (index, value) {
          var $element = $(value);
          var label = '';
          that.data['valid'][index] = {};
          if ($element.attr('placeholder')) {
            label = $element.attr('placeholder');
          } else {
            label = '';
          }
          if ($element.attr('type') === 'checkbox') {
            that.data['valid'][index]['tag_name'] = 'checkbox';
            that.data['valid'][index]['name'] = $element.attr('name');
            that.data['valid'][index]['label'] = label;
            that.data['valid'][index]['value'] = $element.prev('input[name="' + $element.attr('name') + '"]').val();
          } else {
            that.data['valid'][index]['tag_name'] = ($element.prop('tagName')).toLowerCase();
            that.data['valid'][index]['name'] = $element.attr('name');
            that.data['valid'][index]['label'] = label;
            that.data['valid'][index]['value'] = $element.val();
          }
        });
      }
      var fake_required = that.$form.find('.fake_required');
      if (fake_required.length) {
        var idx = 0;
        that.data['fake_required'] = {};
        $.each(fake_required, function (index, value) {
          var depend_element = $(value).data('fake_required_depend_element').split(' ');
          $.each(depend_element, function (i, v) {
            var $value = that.$form.find($('.' + v));
            if (!isNaN($value.val()) && parseInt($value.val()) === 1) {
              that.data['fake_required'][idx] = {};
              that.data['fake_required'][idx]['name'] = $(value).attr('name');
              that.data['fake_required'][idx]['depend_element'] = v;
              idx++;
            }
          });
        });
      }
      if (typeof validation_group !== 'undefined') {
        that.data['validation_group'] = validation_group;
      } else {
        that.data['validation_group'] = null;
      }
      if (typeof trigger_element !== 'undefined') {
        that.$trigger_element = trigger_element;
      }
      that.data['hash'] = that.$form.find('.csrf_token').val();
      that.is_success = null;
      if (validate_ajax.url) {
        $.ajax({
          async: true,
          url: validate_ajax.url,
          type: 'POST',
          dataType: 'json',
          data: that.data,
          success: function (resp) {
            that.$form.find('.has-error').removeClass('has-error');
            that.$form.find('.has-feedback').removeClass('has-feedback');
            that.$form.find('dd span.form-control-feedback').remove();
            that.$form.find('.help-block').remove();
            that.$form.parents('.table_container').find('.has_error').removeClass('has_error');
            that.$form.parents('.tabs_form_container').find('.has_error').removeClass('has_error');
            if (resp !== null) {
              that.get_error_html(resp['response']);
              if (that.$form.find('.has-error').length === 0 && that.$form.find('.help-block').length === 0) {
                var $fake_submit = that.$form.find('.fake_submit');
                if (that.data['validation_group'] === null) {
                  if ($fake_submit.data('action_method') === 'ajax') {
                    ckeditor_update_element();
                    var data = {};
                    data = that.$form.find('input[type!=checkbox], textarea, select').serializeArray();
                    $.ajax({
                      async: true,
                      url: that.$form.attr('action'),
                      type: 'POST',
                      dataType: 'json',
                      data: data,
                      success: function (resp) {
                        if (resp === true) {
                          that.$form.parents('.table_container').find('.control_panel .reset_button').trigger('click');
                          $('[id*=_modal]:visible').modal('hide');
                          that.$form.parents('.chat_footer').find('.message').val('');
                        }
                      }
                    });
                  } else {
                    $('#ajax_overlay_reload').show();
                    $fake_submit.trigger('click');
                  }
                } else {
                  that.is_success = true;
                  that.$trigger_element.trigger('post_validate');
                }
              }
            }
          }
        });
        return false;
      }
    },
    get_error_html: function (resp) {
      var that = this;
      $.each(resp, function (index, value) {
        that.mark_error_field(index, value);
      });
    },
    mark_error_field: function (index, value) {
      var that = this;
      that.$form.find($('.' + index)).parents('dl').addClass('has-error');
      if (that.$form.find($('.' + index)).attr('type') !== 'hidden' && that.$form.find($('.' + index)).attr('type') !== 'checkbox' && !that.$form.parents('.chat_wrapper_fixed').length) {
        //that.$form.find($('.' + index)).parents('dd').addClass('has-feedback');
      }
      that.$form.find($('.' + index)).parents('dd').addClass('has-feedback');
      $.each(that.$form.find('.tab-pane dl.has-error'), function (index, dl) {
        var error_tab_pane_id = $(dl).parent('.tab-pane').attr('id');
        $.each($('.nav-tabs li a'), function (index, a) {
          if ($(a).attr('href') === '#' + error_tab_pane_id) {
            $(a).addClass('has_error');
          }
        });
      });
      var idx = 0;
      for (error_key in value) {
        if (!(value[error_key] instanceof Object) && value[error_key] !== true && !idx) {
          that.$form.find($('.' + index)).parents('dl').append('<p class="help-block">' + value[error_key] + '</p>');
          if (that.$form.find($('.' + index)).attr('type') !== 'hidden' && that.$form.find($('.' + index)).attr('type') !== 'checkbox' && !that.$form.parents('.chat_wrapper_fixed').length) {
            that.$form.find($('.' + index)).parents('dd').append('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
          }
        }
      }
    }
  };
  if ($('#navigation_menu').length) {
    $('#navigation_menu').parent('.table_container').show();
    new NavigationMenu();
  }
  if ($('.table_users').length) {
    $('.table_users').hide().parent('.table_container').find('.table_control_panel').hide();
    var params = {};
    params.load_ajax_url = '/user/loadsettingsajax';
    params.delete_ajax_url = '/user/deleteuserajax';
    params.autocomplete_ajax_url = '/user/autocompleteuserajax';
    params.secend_option = '/user/secendoptionuserajax';
    new Table($('.table_users'), params);
  }
  if ($('.table_items').length) {
    $('.table_items').hide().parent('.table_container').find('.table_control_panel').hide();
    var params = {};
    params.load_ajax_url = '/shop/loaditemsajax';
    params.delete_ajax_url = null;
    params.autocomplete_ajax_url = '/shop/autocompleteitemajax';
    params.add_to_cart_ajax_url = '/shop/cart/addtocartajax';
    params.refresh_cart_ajax_url = '/shop/cart/refreshcartajax';
    new Table($('.table_items'), params);
  }
  if ($('.table_cart_items').length) {
    $('.table_cart_items').hide().parent('.table_container').find('.table_control_panel').hide();
    var params = {};
    params.load_ajax_url = '/shop/cart/loadcartitemsajax';
    params.delete_ajax_url = '/shop/cart/deletecartitemajax';
    params.autocomplete_ajax_url = '/shop/cart/autocompletecartitemajax';
    params.add_to_cart_ajax_url = null;
    params.refresh_cart_ajax_url = '/shop/cart/refreshcartajax';
    new Table($('.table_cart_items'), params);
  }
  if ($('.table_navigation_modules').length) {
    $('.table_navigation_modules').hide().parent('.table_container').find('.table_control_panel').hide();
    var params = {};
    params.load_ajax_url = '/application/navigation/loadmodulesajax';
    params.delete_ajax_url = '/application/navigation/deletemoduleajax';
    params.autocomplete_ajax_url = '/application/navigation/autocompletemoduleajax';
    new Table($('.table_navigation_modules'), params);
  }
  if ($('.table_navigation_controllers').length) {
    $('.table_navigation_controllers').hide().parent('.table_container').find('.table_control_panel').hide();
    var params = {};
    params.load_ajax_url = '/application/navigation/loadcontrollersajax';
    params.delete_ajax_url = '/application/navigation/deletecontrollerajax';
    params.autocomplete_ajax_url = '/application/navigation/autocompletecontrollerajax';
    new Table($('.table_navigation_controllers'), params);
  }
  if ($('.table_navigation_actions').length) {
    $('.table_navigation_actions').hide().parent('.table_container').find('.table_control_panel').hide();
    var params = {};
    params.load_ajax_url = '/application/navigation/loadactionsajax';
    params.delete_ajax_url = '/application/navigation/deleteactionajax';
    params.autocomplete_ajax_url = '/application/navigation/autocompleteactionajax';
    new Table($('.table_navigation_actions'), params);
  }
  if ($('.table_navigation_menu_configurations').length) {
    $('.table_navigation_menu_configurations').hide().parent('.table_container').find('.table_control_panel').hide();
    var params = {};
    params.load_ajax_url = '/application/navigation/loadmenuconfigurationsajax';
    params.delete_ajax_url = '/application/navigation/deletemenuconfigurationajax';
    params.autocomplete_ajax_url = '/application/navigation/autocompletemenuconfigurationajax';
    params.secend_option = '/application/navigation/secendoptionmenuconfigurationajax';
    var unique_name = 'navigation_menu_configuration_';
    params.edit_form_modal_id = 'edit_' + unique_name + 'form_modal';
    params.add_new_form_modal_id = 'add_new_' + unique_name + 'form_modal';
    params.confirm_delete_modal_id = 'delete_' + unique_name + 'confirm_modal';
    params.confirm_delete_modal_button = 'delete_' + unique_name + 'button';
    new Table($('.table_navigation_menu_configurations'), params);
  }
  if ($('.table_meta_keywords').length) {
    $('.table_meta_keywords').hide().parent('.table_container').find('.table_control_panel').hide();
    var params = {};
    params.load_ajax_url = '/application/navigation/loadmetakeywordsajax';
    params.delete_ajax_url = '/application/navigation/deletemetakeywordajax';
    params.autocomplete_ajax_url = '/application/navigation/autocompletemetakeywordajax';
    params.secend_option = '/application/navigation/secendoptionmetakeywordajax';
    var unique_name = 'site_seo_meta_keywords_';
    params.edit_form_modal_id = 'edit_' + unique_name + 'form_modal';
    params.add_new_form_modal_id = 'add_new_' + unique_name + 'form_modal';
    params.confirm_delete_modal_id = 'delete_' + unique_name + 'confirm_modal';
    params.confirm_delete_modal_button = 'delete_' + unique_name + 'button';
    new Table($('.table_meta_keywords'), params);
  }
  if ($('.table_email_content_configuration').length) {
    $('.table_email_content_configuration').hide().parent('.table_container').find('.table_control_panel').hide();
    var params = {};
    params.load_ajax_url = '/application/email/loademailcontentconfigurationajax';
    params.delete_ajax_url = '/application/email/deleteemailcontentconfigurationajax';
    params.autocomplete_ajax_url = '/application/email/autocompleteemailcontentconfigurationajax';
    params.secend_option = '/application/email/secendoptionemailcontentconfigurationajax';
    params.sort_column = 'email_content_configuration_type_id';
    params.sort_method = 'asc';
    var unique_name = 'email_content_configuration_';
    params.edit_form_modal_id = 'edit_' + unique_name + 'form_modal';
    params.add_new_form_modal_id = 'add_new_' + unique_name + 'form_modal';
    params.confirm_delete_modal_id = 'delete_' + unique_name + 'confirm_modal';
    params.confirm_delete_modal_button = 'delete_' + unique_name + 'button';
    new Table($('.table_email_content_configuration'), params);
  }
  if ($('.table_email_configuration').length) {
    $('.table_email_configuration').hide().parent('.table_container').find('.table_control_panel').hide();
    var params = {};
    params.load_ajax_url = '/application/email/loademailconfigurationajax';
    params.delete_ajax_url = '/application/email/deleteemailconfigurationajax';
    params.autocomplete_ajax_url = '/application/email/autocompleteemailconfigurationajax';
    params.secend_option = '/application/email/secendoptionemailconfigurationajax';
    params.sort_column = 'id';
    params.sort_method = 'asc';
    var unique_name = 'email_configuration_';
    params.edit_form_modal_id = 'edit_' + unique_name + 'form_modal';
    params.add_new_form_modal_id = 'add_new_' + unique_name + 'form_modal';
    params.confirm_delete_modal_id = 'delete_' + unique_name + 'confirm_modal';
    params.confirm_delete_modal_button = 'delete_' + unique_name + 'button';
    new Table($('.table_email_configuration'), params);
  }
  if ($('.table_to_do').length) {
    $('.table_to_do').hide().parent('.table_container').find('.table_control_panel').hide();
    var params = {};
    params.load_ajax_url = '/application/todo/loadtodoajax';
    params.delete_ajax_url = '/application/todo/deletetodoajax';
    params.autocomplete_ajax_url = '/application/todo/autocompletetodoajax';
    params.secend_option = '/application/todo/secendoptiontodoajax';
    var unique_name = 'to_do_';
    params.edit_form_modal_id = 'edit_' + unique_name + 'form_modal';
    params.add_new_form_modal_id = 'add_new_' + unique_name + 'form_modal';
    params.confirm_delete_modal_id = 'delete_' + unique_name + 'confirm_modal';
    params.confirm_delete_modal_button = 'delete_' + unique_name + 'button';
    new Table($('.table_to_do'), params);
  }
  if ($('#new_borrower').length) {
    new NewBorrower();
  }
});
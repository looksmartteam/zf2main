@ECHO ON
SET BIN_TARGET=%~dp0\vendor\doctrine\doctrine-module\bin\doctrine-module.php
SET BIN_TARGET=%~dp0\vendor\doctrine\doctrine-module\bin\doctrine-module.php
php "%BIN_TARGET%" %* orm:convert-mapping --namespace="Application\Entity\\" --force --from-database annotation ./module/Application/src/ --extend="\Application\DoctrineEntityAbstract"
php "%BIN_TARGET%" %* orm:generate-entities ./module/Application/src/ --generate-annotations="true" --generate-methods="true"
@ECHO "%BIN_TARGET%"
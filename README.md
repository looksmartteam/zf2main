# zf2.main
php composer.phar install --prefer-source

php composer config --global github-oauth.github.com <TOKEN>

php doctrine-module.php orm:convert-mapping --namespace="Application\Entity\\" --force --from-database annotation ./module/Application/src/ --extend="\Application\EntityAbstract"
php doctrine-module.php orm:generate-entities ./module/Application/src/ --generate-annotations="true" --generate-methods="true"

/usr/local/php5.6/bin/php composer.phar update

http://www.doctrine-project.org/api/orm/2.5
http://www.doctrine-project.org/api/common/2.5

$member->getComments()->filter( function($entry) use ($idsToFilter) {
  return in_array($entry->getId(), $idsToFilter);
});

https://www.boxuk.com/insight/blog-posts/filtering-associations-with-doctrine-2

http://doctrine-orm.readthedocs.org/projects/doctrine-orm/en/latest/reference/dql-doctrine-query-language.html
http://doctrine-orm.readthedocs.org/projects/doctrine-orm/en/latest/reference/query-builder.html

$oEM = $this->getEntityManager();
$oObject = $oEM->getRepository('Application\Entity\NavigationOption')->find(1);
$oSerializer = \JMS\Serializer\SerializerBuilder::create()->build();
$sStr = $oSerializer->serialize($oObject, 'json');
$sStr = $oSerializer->serialize($oObject, 'xml');
$sStr = $oSerializer->serialize($oObject, 'xml');
$sStr = $oSerializer->serialize($oObject, 'yml');
$sStr = $oSerializer->toArray($oObject->getNavigationAction(), \JMS\Serializer\SerializationContext::create()->enableMaxDepthChecks());
Debug::dump($oObject->getNavigationAction());

$oApp = new \Application\App($this->getServiceLocator());
$oNavigationMenuEntity = $oApp->getEntityManager('NavigationMenu');
$oNavigationMenuEntity = $oApp->getRepository('NavigationMenu')->findAll();